# People - Adonis

[![Codeship Status for LegalShield/people.adonis](https://app.codeship.com/projects/c582a4d0-c0ba-0137-0b32-6eacb2ad235c/status?branch=develop)](https://app.codeship.com/projects/365930)

## Development Dependencies
The People application currently only requires the following development dependencies to run:
* Docker Compose
* Docker
* Jet

All of these dependencies can be retrieved via [brew](https://brew.sh/) if you do not already have them installed like so:
* `brew install docker-compose`
* `brew cask install docker`
* `brew cask install jet`

## Application Dependencies
The People application currently only requires the following application dependencies to run:
* Redis
* [adonis](https://github.com/LegalShield/adonis)

## Application Environment Variables
| Name               | Description                                                             | Default Value                        | Available Values                            |
| ------------------ | ----------------------------------------------------------------------- | ------------------------------------ | ------------------------------------------- |
| HEADLESS           | Determines whether Chrome should be ran in headless mode or not.        | true                                 | true, false                                 |
| REDIS_URL          | The URL for Redis.                                                      | redis://127.0.0.1:6379/              |                                             |
| NODE_ENV           | Environment in which the application will ran in.                       | development                          | development, test, sandbox, uat, production |
| PORT               | Port in which the application will be ran on.                           | 9990                                 |                                             |
| HTTPS              | Whether or not the application will force https or not.                 | true                                 | true, false                                 |
| SESSION_SECRET     | The secret key used for the session.                                    | super-secret                         |                                             |
| API_DOMAIN         | Domain of the Adonis API.                                               | http://localhost:3000                |                                             |
| APP_DOMAIN         | Domain of the Adonis People Application.                                | http://localhost:9990                |                                             |
| AUTH_CLIENT_ID     | Client ID used to authentication against the authentication server.     | 110ad811-9825-4de9-8203-ed3aaa31fc4c |                                             |
| AUTH_CLIENT_SECRET | Client secret used to authentication against the authentication server. | c07e7974-f428-4e3d-82d2-ea93973da7e2 |                                             |
| RAYGUN_API_KEY     | The API key used for Raygun.                                            | super-secret                         |                                             |
| REGION             | The region that the application will be deployed in.                    | NA                                   | NA, GB                                      |

## Running the Application
This application is configured to run using Docker/Docker Compose in the [pplsi repository](https://github.com/LegalShield/pplsi)

Follow the instructions in that repository to get everything setup first before continuing.

***THESE INSTRUCTIONS WILL NOT WORK UNLESS USING THIS APPLICATION FROM WITHIN THE PPLSI REPOSITORY.***

To start the application locally run `docker-compose up people.adonis`. (This requires Docker to be running in the background.)
Note, if you run `docker-compose up` it will bring the entire Adonis stack up. This will contain all the front end and back end applications. Specifying the specific service you want will only bring up that service with its dependencies.

This will only ever build the People application image if it is not already built. To force a rebuild use `docker-compose up --build people.adonis`.

You can run this application locally you will just have to make sure that the application dependencies are available and running locally.

Since docker uses docker service names for the Adonis API domain you will need add an entry to your `/etc/hosts` file so that okta login will work in your browser. The line that you should change should change from `127.0.0.1       localhost` to 
`127.0.0.1       localhost adonis`.

## Stopping the Application
Under normal scenarios the Docker containers should stop when you use `Ctrl + c`, but if they do not you can stop the containers manually by running `docker-compose stop`.

## Running Tests
### Karma
To run the Karma tests locally run `docker-compose run people.adonis npm run test`.

Sometimes it is useful to run the Karma tests locally to see the browser output in order to debug errors. This can still be done but must be done outside of docker. To do this you can run `ng test`. (You will have to have the angular CLI installed globally for this to work.)

### Protractor
To run the Protractor tests locally run `docker-compose run people.adonis npm run e2e`.

Sometimes it is useful to run the Protractor tests locally to see the browser output in order to debug errors. This can still be done but must be done outside of docker. To do this you can run `ng e2e`. (This requires all of the application dependencies to be available locally as well. They can be installed via brew and started with brew services.)

### Lint
To run the lint tests locally run `docker-compose run people.adonis npm run lint`.

### Jet
Sometimes it may be useful to run the CodeShip jet steps locally to try and debug an error that you are receiving on CodeShip. You can do this by running `jet steps`. In order to do this you have to have the codeship.aes file in the root of the project. You can download this from the project settings in CodeShip.
