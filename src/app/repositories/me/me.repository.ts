import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { map, tap } from 'rxjs/operators';

import { Me } from '../../models/me';

@Injectable()
export class MeRepository {
  me: Me = null;

  constructor(private httpClient: HttpClient) { }

  getMe(): Observable<Me | null> {
    if (this.me) {
      return of(this.me);
    } else {
      return this.httpClient.get('/me').pipe(
        tap((response: Response) => this.me = !!response ? this.me = new Me(response) : null),
        map(() => this.me)
      );
    }
  }

  clearCache(): void {
    this.me = null;
  }
}
