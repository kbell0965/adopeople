import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { getTestBed, TestBed } from '@angular/core/testing';

import { Me } from '../../models/me';
import { MeRepository } from './me.repository';
import { componentFactoryName } from '@angular/compiler';

describe('Me.Repository', () => {
  let http: HttpTestingController;
  let meRepository: MeRepository;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ MeRepository ],
      imports: [ HttpClientTestingModule ]
    });

    const injector = getTestBed();
    http = injector.get(HttpTestingController);
    meRepository = injector.get(MeRepository);
  });

  describe('getMe', () => {
    const response: object = {
      id: '1',
      name: 'John Doe',
      roles: [ 'csr' ]
    };

    describe('not cached', () => {
      it('makes a GET request to the me endpoint', (done) => {
        meRepository.getMe().subscribe(done);

        const request = http.expectOne('/me');
        expect(request.request.method).toBe('GET');
        request.flush(response);
      });

      describe('logged in', () => {
        it('caches me on the repository', (done) => {
          meRepository.getMe().subscribe((me: Me) => {
            expect(meRepository.me).toEqual(me);
            done();
          });

          const request = http.expectOne('/me');
          request.flush(response);
        });

        it('returns me', (done) => {
          meRepository.getMe().subscribe((me: Me) => {
            expect(me).toEqual(meRepository.me);
            done();
          });

          const request = http.expectOne('/me');
          request.flush(response);
        });
      });

      describe('not logged in', () => {
        it('does not cache me on the repository', (done) => {
          meRepository.getMe().subscribe((me: Me) => {
            expect(meRepository.me).toBeNull();
            done();
          });

          const request = http.expectOne('/me');
          request.flush(null);
        });

        it('returns null', (done) => {
          meRepository.getMe().subscribe((me: Me) => {
            expect(me).toBeFalsy();
            done();
          });

          const request = http.expectOne('/me');
          request.flush(null);
        });
      });
    });

    describe('cached', () => {
      it('returns the cached me', (done) => {
        const me: Me = new Me({ id: '1', name: 'John Doe', roles: [ 'csr' ] });
        meRepository.me = me;

        meRepository.getMe().subscribe((returnedMe: Me) => {
          expect(returnedMe).toBe(me);
          done();
        });

        http.expectNone('/me');
      });
    });
  });

  describe('clearCache', () => {
    it('sets me to null', () => {
      meRepository.me = new Me({ id: '1', name: 'John Doe', roles: [ 'csr' ] });

      meRepository.clearCache();

      expect(meRepository.me).toEqual(null);
    });
  });
});
