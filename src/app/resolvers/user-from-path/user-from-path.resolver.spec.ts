import { TestBed, getTestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { UserFromPathResolver } from './user-from-path.resolver';
import { PeopleUserRepository, PeopleModels } from '@pplsi-core/datalayer';
import { PeopleFactories } from '@pplsi-core/factories';
import { UserViewModel } from 'src/app/modules/users/models/user-view-model';

describe('UserFromPathResolver', () => {
  let injector: TestBed;
  let userFromPathResolver: UserFromPathResolver;
  let userRepository: PeopleUserRepository;
  const fakeUser: PeopleModels.User = PeopleFactories.UserFactory.build();
  const user: UserViewModel = new UserViewModel(fakeUser);
  const activatedRouteSnapshotStub = { params: { user_id: fakeUser['id'] } };

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UserFromPathResolver, PeopleUserRepository],
      imports: [HttpClientTestingModule]
    });

    injector = getTestBed();
    userFromPathResolver = injector.get(UserFromPathResolver);
    userRepository = injector.get(PeopleUserRepository);

    spyOn(userRepository, 'find').and.returnValue(of(fakeUser as any));
  });

  describe('#resolve', () => {
    it('gets user from the PeopleUserRepository', (done) => {
      userFromPathResolver.resolve(activatedRouteSnapshotStub as any).subscribe((returnedUser: PeopleModels.User) => {
        expect(returnedUser).toEqual(user);
        done();
      });
    });

    it('gets user from the PeopleUserRepository with the user id from the path', (done) => {
      userFromPathResolver.resolve(activatedRouteSnapshotStub as any).subscribe(() => {
        expect(userRepository.find).toHaveBeenCalledWith(fakeUser['id']);
        done();
      });
    });
  });
});
