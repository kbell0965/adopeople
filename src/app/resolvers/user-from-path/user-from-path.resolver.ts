import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Observable, fromEventPattern } from 'rxjs';
import { PeopleUserRepository, PeopleModels } from '@pplsi-core/datalayer';
import { UserViewModel } from 'src/app/modules/users/models/user-view-model';
import { map } from 'rxjs/operators';

@Injectable()
export class UserFromPathResolver implements Resolve<PeopleModels.User> {
  constructor(private userRepository: PeopleUserRepository) {}

  resolve(activatedRouteSnapshot: ActivatedRouteSnapshot): Observable<PeopleModels.User> {
    return this.userRepository.find(activatedRouteSnapshot.params['user_id'] as string)
      .pipe(map((user: PeopleModels.User) => new UserViewModel(user)));
  }
}
