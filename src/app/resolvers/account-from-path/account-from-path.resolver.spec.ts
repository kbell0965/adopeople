import { TestBed, getTestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { AccountViewModel } from '../../models/account-view-model';

import { AccountFromPathResolver } from './account-from-path.resolver';
import { PartnerAccountRepository } from '@pplsi-core/datalayer';
import { PartnerFactories } from '@pplsi-core/factories';

describe('AccountFromPathResolver', () => {
  let injector: TestBed;
  let accountFromPathResolver: AccountFromPathResolver;
  let accountRepository: PartnerAccountRepository;
  const fakeAccount: object = PartnerFactories.AccountFactory.build();
  const account: AccountViewModel = new AccountViewModel(fakeAccount);

  const activatedRouteSnapshotStub = {
    params: {
      account_id: fakeAccount['id']
    }
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        AccountFromPathResolver,
        PartnerAccountRepository
      ],
      imports: [
        HttpClientTestingModule
      ]
    });

    injector = getTestBed();
    accountFromPathResolver = injector.get(AccountFromPathResolver);
    accountRepository = injector.get(PartnerAccountRepository);

    spyOn(accountRepository, 'find').and.returnValue(of(fakeAccount as any));
  });

  describe('#resolve', () => {
    it('gets account from the PartnerAccountRepository', (done) => {
      accountFromPathResolver.resolve(activatedRouteSnapshotStub as any).subscribe((returnedAccount: AccountViewModel) => {
        expect(returnedAccount).toEqual(account);
        done();
      });
    });

    it('gets account from the PartnerAccountRepository with the account id from the path', (done) => {
      accountFromPathResolver.resolve(activatedRouteSnapshotStub as any).subscribe(() => {
        expect(accountRepository.find).toHaveBeenCalledWith(fakeAccount['id']);
        done();
      });
    });
  });
});
