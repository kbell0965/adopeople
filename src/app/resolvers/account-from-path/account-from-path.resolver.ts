import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from 'rxjs';

import { PartnerAccountRepository, PartnerModels } from '@pplsi-core/datalayer';
import { map } from 'rxjs/operators';
import { AccountViewModel } from '../../models/account-view-model';

@Injectable()
export class AccountFromPathResolver implements Resolve<AccountViewModel> {
  constructor(private accountRepository: PartnerAccountRepository) {}

  resolve(activatedRouteSnapshot: ActivatedRouteSnapshot): Observable<AccountViewModel> {
    const accountId = activatedRouteSnapshot.params['account_id'];

    return this.accountRepository.find(accountId as string).pipe(
      map((account: PartnerModels.Account) => new AccountViewModel(account))
    );
  }
}
