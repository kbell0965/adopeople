import { TestBed, getTestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { PeopleFulfillmentPartnerBranchRepository, PeopleModels } from '@pplsi-core/datalayer';
import { PeopleFactories } from '@pplsi-core/factories';
import { FulfillmentPartnerBranchesResolver } from './fulfillment-partner-branches.resolver';

describe('FulfillmentPartnerBranchesResolver', () => {
  let injector: TestBed;
  let fulfillmentPartnersBranchesResolver: FulfillmentPartnerBranchesResolver;
  let peopleFulfillmentPartnerBranchRepository: PeopleFulfillmentPartnerBranchRepository;
  const fakeFulfillmentPartnerBranches: PeopleModels.FulfillmentPartnerBranch[] = PeopleFactories
    .FulfillmentPartnerBranchFactory.buildList(1)
    .map((fulfillmentPartnerBranch: object) => new PeopleModels.FulfillmentPartnerBranch(fulfillmentPartnerBranch));

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FulfillmentPartnerBranchesResolver, PeopleFulfillmentPartnerBranchRepository],
      imports: [HttpClientTestingModule]
    });

    injector = getTestBed();
    fulfillmentPartnersBranchesResolver = injector.get(FulfillmentPartnerBranchesResolver);
    peopleFulfillmentPartnerBranchRepository = injector.get(PeopleFulfillmentPartnerBranchRepository);

    spyOn(peopleFulfillmentPartnerBranchRepository, 'findAll').and.returnValue(of(fakeFulfillmentPartnerBranches));
  });

  describe('#resolve', () => {
    it('gets fulfillment partner branches from the FulfillmentPartnerBranchesResolver', (done) => {
      fulfillmentPartnersBranchesResolver.resolve()
        .subscribe((returnedFulfillmentPartners: PeopleModels.FulfillmentPartner[]) => {
          expect(returnedFulfillmentPartners).toEqual(fakeFulfillmentPartnerBranches);
          done();
        });
    });
  });
});
