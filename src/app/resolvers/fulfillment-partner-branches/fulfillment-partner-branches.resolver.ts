import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { Observable } from 'rxjs';
import { PeopleFulfillmentPartnerBranchRepository, PeopleModels } from '@pplsi-core/datalayer';

@Injectable()
export class FulfillmentPartnerBranchesResolver implements Resolve<PeopleModels.FulfillmentPartnerBranch[]> {
  constructor(private fulfillmentPartnerRepository: PeopleFulfillmentPartnerBranchRepository) { }

  resolve(): Observable<PeopleModels.FulfillmentPartnerBranch[]> {
    return this.fulfillmentPartnerRepository.findAll();
  }
}
