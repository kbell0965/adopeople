import { PortalModule } from '@angular/cdk/portal';
import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
  MatIconModule,
  MatSidenavModule,
  MatToolbarModule,
  MatMenuModule,
  MatDialogModule,
  MatProgressBarModule
} from '@angular/material';
import { AppRoutingModule } from './app-routing.module';
import { RaygunErrorHandler } from './app.raygun';
import { LayoutComponent } from './components/layout/layout.component';
import { TopBarComponent } from './components/top-bar/top-bar.component';
import { LoginComponent } from './components/login/login.component';
import { DrawerService } from './services/drawer/drawer.service';
import { AuthenticationService } from './services/authentication/authentication.service';
import { MeService } from './services/me/me.service';
import { PermissionsService } from './services/permissions/permissions.service';
import { SideNavComponent } from './components/side-nav/side-nav.component';
import { PplsiCoreDatalayerModule } from '@pplsi-core/datalayer';
import { ConfidentialMessageComponent } from './components/confidential-message/confidential-message.component';
import { AuthGuard } from './guards/auth/auth.guard';
import { FeatherModule } from 'angular-feather';
import { Archive, Users, Share2 } from 'angular-feather/icons';
import { ResolutionEditModule } from './shared/components/resolution-edit/resolution-edit.module';
import { DataService } from './services/data/data.service';
import { UnauthorizedModule } from './shared/components/unauthorized/unauthorized.module';
import { SharedModule } from './shared/shared.module';
import { WindowService } from '@pplsi-core/shared-modules/dist/src/services/window.service';
import { UserRoleSlideoutModule } from './shared/components/user-role-slideout/user-role-slideout.module';
import { RoleEditModule } from './shared/components/role-edit/role-edit.module';

@NgModule({
  declarations: [
    LayoutComponent,
    TopBarComponent,
    LoginComponent,
    SideNavComponent,
    ConfidentialMessageComponent
  ],
  imports: [
    AppRoutingModule,
    BrowserAnimationsModule,
    BrowserModule,
    FeatherModule.pick({ Archive, Users, Share2 }),
    HttpClientModule,
    MatIconModule,
    MatSidenavModule,
    MatToolbarModule,
    MatMenuModule,
    MatDialogModule,
    MatProgressBarModule,
    PplsiCoreDatalayerModule.forRoot(),
    PortalModule,
    ResolutionEditModule,
    SharedModule,
    UnauthorizedModule,
    UserRoleSlideoutModule,
    RoleEditModule
  ],
  providers: [
    { provide: ErrorHandler, useClass: RaygunErrorHandler },
    AuthenticationService,
    AuthGuard,
    DrawerService,
    DataService,
    WindowService,
    MeService,
    PermissionsService
  ],
  bootstrap: [ LayoutComponent ]
})

export class AppModule { }
