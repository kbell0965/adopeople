import { Component } from '@angular/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MatSidenavModule } from '@angular/material';
import { Router } from '@angular/router';
import { By } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FeatherModule } from 'angular-feather';
import { Archive, Users } from 'angular-feather/icons';
import { SideNavComponent } from './side-nav.component';
import { PermittedDirective } from 'src/app/shared/directives/permitted/permitted.directive';
import { Me, MeRepository } from '@pplsi-core/datalayer';
import { WindowService } from '@pplsi-core/shared-modules/dist/src/services/window.service';
import { PermissionsService } from 'src/app/services/permissions/permissions.service';
import { MeService } from 'src/app/services/me/me.service';

@Component({
  selector: 'side-nav-projection-test',
  template: `
    <side-nav [loggedIn]='true'>
        <p>some content</p>
    </side-nav>
  `
})
// @ts-ignore
class TestComponent { }

describe('SideNavComponent.Template-Projection', () => {
  let component: TestComponent;
  let fixture: ComponentFixture<TestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [
        PermissionsService,
        MeService,
        WindowService,
        MeRepository,
        {
          provide: Router,
          useValue: { navigate: jasmine.createSpy('navigate') }
        }
      ],
      declarations: [
        TestComponent,
        SideNavComponent,
        PermittedDirective
      ],
      imports: [
        FeatherModule.pick({ Archive, Users }),
        MatSidenavModule,
        BrowserAnimationsModule,
        HttpClientTestingModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestComponent);

    component = fixture.componentInstance;

    fixture.detectChanges();
  });

  it('should project the contents into the side-nav component', () => {
    expect(
      fixture.debugElement.query(By.css('p')).nativeElement.innerHTML
    ).toContain('some content');
  });
});
