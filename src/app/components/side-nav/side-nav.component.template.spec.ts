import { async, ComponentFixture, TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { Router } from '@angular/router';
import { MatSidenavModule } from '@angular/material';
import { By } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SideNavComponent } from './side-nav.component';
import { FeatherModule } from 'angular-feather';
import { of } from 'rxjs';
import { Archive, Users, Share2 } from 'angular-feather/icons';
import { PermittedDirective } from 'src/app/shared/directives/permitted/permitted.directive';
import { Me, MeRepository } from '@pplsi-core/datalayer';
import { WindowService } from '@pplsi-core/shared-modules/dist/src/services/window.service';
import { PermissionsService } from 'src/app/services/permissions/permissions.service';
import { MeService } from 'src/app/services/me/me.service';
import { MeViewModel } from '../../models/me-view-model';

describe('SideNavComponent.Template', () => {
  let component: SideNavComponent;
  let fixture: ComponentFixture<SideNavComponent>;
  let meService: MeService;
  let injector: TestBed;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [
        PermissionsService,
        MeService,
        WindowService,
        MeRepository,
        {
          provide: Router,
          useValue: { navigate: jasmine.createSpy('navigate') }
        }
      ],
      declarations: [SideNavComponent, PermittedDirective],
      imports: [
        HttpClientTestingModule,
        FeatherModule.pick({ Archive, Users, Share2 }),
        MatSidenavModule,
        BrowserAnimationsModule
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SideNavComponent);
    injector = getTestBed();
    meService = injector.get(MeService);
    component = fixture.componentInstance;
    component.loggedIn = false;

    fixture.detectChanges();
  });

  describe('not logged in', () => {
    it('should not display the side-nav', () => {
      expect(fixture.debugElement.query(By.css('.side-nav'))).toBeFalsy();
    });
  });

  describe('logged in', () => {
    beforeEach(() => {
      component.loggedIn = true;
      fixture.detectChanges();
    });

    it('should display the side navigation', () => {
      expect(fixture.debugElement.query(By.css('.side-nav'))).toBeTruthy();
    });

    it('should display the members navigation item', () => {
      expect(fixture.debugElement.query(By.css('.nav-item.members'))).toBeTruthy();
    });
  });
});
