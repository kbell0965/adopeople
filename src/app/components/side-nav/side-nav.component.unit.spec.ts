import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MatSidenavModule } from '@angular/material';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { Router } from '@angular/router';
import { FeatherModule } from 'angular-feather';
import { Archive, Users, Share2 } from 'angular-feather/icons';
import { SideNavComponent } from './side-nav.component';
import { PermittedDirective } from 'src/app/shared/directives/permitted/permitted.directive';
import { Me, MeRepository } from '@pplsi-core/datalayer';
import { WindowService } from '@pplsi-core/shared-modules/dist/src/services/window.service';
import { PermissionsService } from 'src/app/services/permissions/permissions.service';
import { MeService } from 'src/app/services/me/me.service';

describe('SideNavComponent.Unit', () => {
  let component: SideNavComponent;
  let fixture: ComponentFixture<SideNavComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [
        PermissionsService,
        MeService,
        WindowService,
        MeRepository,
        {
          provide: Router,
          useValue: { navigate: jasmine.createSpy('navigate') }
        }
      ],
      declarations: [ SideNavComponent, PermittedDirective ],
      imports: [
        HttpClientTestingModule,
        FeatherModule.pick({ Archive, Users, Share2 }),
        MatSidenavModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SideNavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
