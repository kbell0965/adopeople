import { async, ComponentFixture, getTestBed, TestBed } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of, throwError } from 'rxjs';
import { MatIconRegistry } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AuthenticationService } from '../../services/authentication/authentication.service';
import { LoginComponent } from './login.component';
import { MeRepository } from '@pplsi-core/datalayer';
import { MeService } from 'src/app/services/me/me.service';
import { WindowService } from '@pplsi-core/shared-modules/dist/src/services/window.service';

describe('LoginComponent.Unit', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  let injectedRouter: any;
  let injector: TestBed;
  let authenticationService: AuthenticationService;
  let matIconRegistry: MatIconRegistry;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        LoginComponent
      ],
      imports: [
        ReactiveFormsModule,
        HttpClientTestingModule,
        MatInputModule,
        BrowserAnimationsModule
      ],
      providers: [
        { provide: Router, useValue: { navigate: jasmine.createSpy('navigate') } },
        AuthenticationService,
        WindowService,
        MeService,
        MeRepository
      ],
      schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    injector = getTestBed();
    injectedRouter = injector.get(Router);
    authenticationService = injector.get(AuthenticationService);
    matIconRegistry = injector.get(MatIconRegistry);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  describe('constructor', () => {
    it('adds the custom svgs to the registry', () => {
      expect(
        matIconRegistry.getNamedSvgIcon('do_not_show_password')
      ).toBeDefined();
      expect(
        matIconRegistry.getNamedSvgIcon('show_password')
      ).toBeDefined();
    });
  });

  describe('ngOnInit', () => {
    it('sets the loginForm', () => {
      component.ngOnInit();

      expect(component.loginForm).toBeTruthy();
    });
  });

  describe('#login', () => {
    it('redirects to the members component when the user successfully logs in', () => {
      spyOn(authenticationService, 'authenticate').and.returnValue(of({}));

      component.loginForm.controls['username'].setValue('johndoe@example.com');
      component.loginForm.controls['password'].setValue('somepassword');

      component.login();

      expect(injectedRouter.navigate).toHaveBeenCalledWith([ 'members' ]);
    });

    it('sets an error on the form when the user unsuccessfully logs in', () => {
      spyOn(authenticationService, 'authenticate').and.returnValue(throwError('uh oh'));

      component.loginForm.controls['username'].setValue('johndoe@example.com');
      component.loginForm.controls['password'].setValue('somepassword');

      component.login();

      expect(component.loginForm.getError('unauthorized')).toBe(true);
    });
  });

  describe('#togglePassword', () => {
    it('switches the showPassword value', () => {
      component.showPassword = false;

      component.togglePassword();

      expect(component.showPassword).toEqual(true);

      component.togglePassword();

      expect(component.showPassword).toEqual(false);
    });
  });
});
