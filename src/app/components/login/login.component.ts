import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ErrorStateMatcher } from '@angular/material/core';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';

import { AuthenticationService } from '../../services/authentication/authentication.service';

export class UnauthorizedStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    const unauthorized = form.getError('unauthorized');
    return !!(form.invalid && isSubmitted && unauthorized);
  }
}

@Component({
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  showPassword = false;
  errorStateMatcher = new UnauthorizedStateMatcher();

  constructor(
    private formBuilder: FormBuilder,
    private authenticationService: AuthenticationService,
    private router: Router,
    private matIconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer
  ) {
    this.matIconRegistry.addSvgIcon(
      'do_not_show_password',
      this.domSanitizer.bypassSecurityTrustResourceUrl('assets/DoNotShowPassword.svg')
    );
    this.matIconRegistry.addSvgIcon(
      'show_password',
      this.domSanitizer.bypassSecurityTrustResourceUrl('assets/ShowPassword.svg')
    );
  }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      username: [ null, Validators.compose([ Validators.email, Validators.required ]) ],
      password: [ null, Validators.required ]
    });
  }

  login(): void {
    this.authenticationService.authenticate(this.loginForm.value).subscribe(
      () => this.router.navigate([ 'members' ]),
      () => this.loginForm.setErrors({ 'unauthorized': true })
    );
  }

  togglePassword(): void {
    this.showPassword = !this.showPassword;
  }
}
