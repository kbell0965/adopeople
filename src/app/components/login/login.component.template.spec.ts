import { By } from '@angular/platform-browser';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { MatDialogModule, MatInputModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';

import { AuthenticationService } from '../../services/authentication/authentication.service';
import { LoginComponent } from './login.component';
import { MeRepository } from '@pplsi-core/datalayer';
import { MeService } from 'src/app/services/me/me.service';
import { WindowService } from '@pplsi-core/shared-modules/dist/src/services/window.service';

describe('LoginComponent.Template', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoginComponent ],
      imports: [
        ReactiveFormsModule,
        HttpClientTestingModule,
        MatInputModule,
        MatDialogModule,
        BrowserAnimationsModule,
        RouterTestingModule
      ],
      providers: [
        AuthenticationService,
        WindowService,
        MeService,
        MeRepository
      ],
      schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('has an email input box', () => {
    const el = fixture.debugElement.query(By.css('.email'));

    expect(el.nativeElement).not.toBeNull();
  });

  it('has a password input box', () => {
    const el = fixture.debugElement.query(By.css('.password'));

    expect(el.nativeElement).toBeTruthy();
  });

  it('can toggle password visibility', () => {
    const toggle = fixture.debugElement.query(By.css('.password-action')).nativeElement;
    const passwordInput = fixture.debugElement.query(By.css('.password')).nativeElement;

    toggle.click();

    fixture.detectChanges();

    expect(passwordInput.type).toEqual(('text'));

    toggle.click();

    fixture.detectChanges();

    expect(passwordInput.type).toEqual(('password'));
  });

  describe('login button', () => {
    it('has a login button', () => {
      const el = fixture.debugElement.query(By.css('.button'));

      expect(el.nativeElement).toBeTruthy();
    });

    it('calls the login method when clicked', () => {
      spyOn(component, 'login');
      const el = fixture.debugElement.query(By.css('.button'));

      component.loginForm.controls['username'].setValue('test@example.com');
      component.loginForm.controls['password'].setValue('passw0rd');

      fixture.detectChanges();
      el.nativeElement.click();

      expect(component.login).toHaveBeenCalled();
    });

    it('is disabled when the email is invalid', () => {
      component.loginForm.controls['username'].setValue('notAValidEmail');
      fixture.detectChanges();
      const el = fixture.debugElement.query(By.css('.button'));

      expect(el.nativeElement.attributes['disabled']).toBeDefined();
    });
  });
});
