import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';

import { MeViewModel } from '../../models/me-view-model';
import { AuthenticationService } from '../../services/authentication/authentication.service';
import { WindowService } from '@pplsi-core/shared-modules/dist/src/services/window.service';

@Component({
  selector: 'top-bar',
  templateUrl: './top-bar.component.html',
  styleUrls: ['./top-bar.component.scss']
})
export class TopBarComponent {
  @Input() region: string;
  @Input() loggedIn: boolean;
  @Input() me: MeViewModel;

  constructor(
    private router: Router,
    private authenticationService: AuthenticationService,
    private windowService: WindowService
  ) {}

  logout(): void {
    this.authenticationService
      .logout()
      .subscribe(() => this.windowService.replace('https://legalshieldcorp.okta.com/'));
  }
}
