import {
  async,
  ComponentFixture,
  TestBed,
  getTestBed
} from '@angular/core/testing';
import {
  HttpClientTestingModule
} from '@angular/common/http/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { Router } from '@angular/router';
import { MatMenuModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { of } from 'rxjs';

import { WindowService } from '@pplsi-core/shared-modules/dist/src/services/window.service';
import { MeRepository } from '@pplsi-core/datalayer';
import { TopBarComponent } from './top-bar.component';
import { AuthenticationService } from '../../services/authentication/authentication.service';
import { MeService } from 'src/app/services/me/me.service';

describe('TopBarComponent.Unit', () => {
  let fixture: ComponentFixture<TopBarComponent>;
  let component: TopBarComponent;
  let authenticationService: AuthenticationService;
  let windowService: WindowService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TopBarComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      imports: [MatMenuModule, HttpClientTestingModule, BrowserAnimationsModule],
      providers: [
        AuthenticationService,
        { provide: Router, useValue: { navigate: jasmine.createSpy('navigate') } },
        MeRepository,
        MeService,
        WindowService
      ]
    }).compileComponents();

    const injector = getTestBed();
    authenticationService = injector.get(AuthenticationService);
    windowService = injector.get(WindowService);
    fixture = TestBed.createComponent(TopBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  describe('#logout', () => {
    beforeEach(() => {
      spyOn(authenticationService, 'logout').and.returnValue(of({}));
      spyOn(windowService, 'replace');

      component.logout();
    });

    it('calls logout on the authenticationService', () => {
      expect(authenticationService.logout).toHaveBeenCalled();
    });

    it('calls replace on the windowService', () => {
      expect(windowService.replace).toHaveBeenCalledWith('https://legalshieldcorp.okta.com/');
    });
  });
});
