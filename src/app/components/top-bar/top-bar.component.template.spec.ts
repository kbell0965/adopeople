import {
  async,
  ComponentFixture,
  TestBed,
  getTestBed
} from '@angular/core/testing';
import {
  HttpClientTestingModule
} from '@angular/common/http/testing';
import { By } from '@angular/platform-browser';
import { CUSTOM_ELEMENTS_SCHEMA, DebugElement } from '@angular/core';
import { Router } from '@angular/router';
import { MatMenuModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { WindowService } from '@pplsi-core/shared-modules/dist/src/services/window.service';
import { Me, MeRepository } from '@pplsi-core/datalayer';
import { TopBarComponent } from './top-bar.component';
import { AuthenticationService } from '../../services/authentication/authentication.service';
import { MeViewModel } from '../../models/me-view-model';
import { MeService } from 'src/app/services/me/me.service';

describe('TopBarComponent.Template', () => {
  let fixture: ComponentFixture<TopBarComponent>;
  let component: TopBarComponent;
  let authenticationService: AuthenticationService;
  let router: Router;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TopBarComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      imports: [MatMenuModule, HttpClientTestingModule, BrowserAnimationsModule],
      providers: [
        AuthenticationService,
        { provide: Router, useValue: { navigate: jasmine.createSpy('navigate') } },
        MeRepository,
        MeService,
        WindowService
      ]
    }).compileComponents();

    const injector = getTestBed();
    router = injector.get(Router);
    authenticationService = injector.get(AuthenticationService);
  }));

  describe('when unauthenticated', () => {
    beforeEach(() => {
      fixture = TestBed.createComponent(TopBarComponent);
      component = fixture.componentInstance;
      component.loggedIn = false;

      fixture.detectChanges();
    });

    it('does not display the mat-toolbar', () => {
      expect(fixture.debugElement.query(By.css('mat-toolbar'))).toBeNull();
    });
  });

  describe('when logged in', () => {
    const me: MeViewModel = new MeViewModel(new Me({ id: '1', full_name: 'John Doe' }));

    beforeEach(() => {
      fixture = TestBed.createComponent(TopBarComponent);

      component = fixture.componentInstance;

      component.region = 'NA';
      component.loggedIn = true;
      component.me = me;

      fixture.detectChanges();
    });

    it('displays the mat-toolbar', () => {
      expect(fixture.debugElement.query(By.css('mat-toolbar'))).toBeTruthy();
    });

    describe('logo', () => {
      it('should be the correct logo', () => {
        expect(fixture.debugElement.query(By.css('.logo img')).nativeElement.src).toContain('assets/NA_LegalShieldLogo.svg');
      });
    });

    describe('profile container', () => {
      it('should display the avatar component', () => {
        expect(fixture.debugElement.query(By.css('ls-avatar'))).toBeTruthy();
      });

      it('should display the user name', () => {
        const element: DebugElement = fixture.debugElement.query(By.css('.name'));

        expect(element).toBeTruthy();
        expect(element.nativeElement.innerHTML).toBe(me.fullName);
      });

      describe('profile menu', () => {
        let dom;
        let button;
        let menu;

        beforeEach(() => {
          dom = fixture.nativeElement;
          button = dom.querySelector('.profile-btn');
        });

        it('should open the menu when clicking on the profile button', async () => {
          button.click();
          menu = dom.parentNode.querySelector('.mat-menu-panel');
          expect(menu).toBeTruthy();
        });

        describe('is closed', () => {
          it('should not have the menu open', async () => {
            menu = dom.parentNode.querySelector('.mat-menu-panel');
            expect(menu).toBeFalsy();
          });
        });

        describe('is open', () => {
          let logout;

          beforeEach(() => {
            button.click();
            menu = dom.parentNode.querySelector('.mat-menu-panel');
            logout = menu.querySelector('.logout');
          });

          it('should display the logout button', () => {
            expect(logout).toBeTruthy();
          });

          describe('logout button', () => {
            it('should logout when clicked', (done) => {
              logout.click();
              fixture.detectChanges();
              authenticationService.hasLoggedIn.subscribe(authenticated => {
                expect(authenticated).toBeFalsy();
                done();
              });
            });
          });
        });
      });
    });
  });
});
