import { HttpClientTestingModule } from '@angular/common/http/testing';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { AuthenticationService } from '../../services/authentication/authentication.service';
import { DrawerService } from '../../services/drawer/drawer.service';
import { MeService } from '../../services/me/me.service';
import { LayoutComponent } from './layout.component';
import { Router, RouterEvent } from '@angular/router';
import { ReplaySubject } from 'rxjs';
import { MeRepository } from '@pplsi-core/datalayer';
import { WindowService } from '@pplsi-core/shared-modules/dist/src/services/window.service';

describe('LayoutComponent.Template', () => {
  let fixture: ComponentFixture<LayoutComponent>;
  const eventSubject = new ReplaySubject<RouterEvent>(1);
  const routerMock = {
    navigate: jasmine.createSpy('navigate'),
    events: eventSubject.asObservable(),
    url: 'test/url'
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LayoutComponent ],
      schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
      providers: [
        MeService,
        MeRepository,
        DrawerService,
        AuthenticationService,
        WindowService,
        { provide: Router, useValue: routerMock }
      ],
      imports: [ HttpClientTestingModule ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LayoutComponent);
    fixture.detectChanges();
  });

  it('includes the mat-drawer component', () => {
    expect(fixture.debugElement.query(By.css('mat-drawer'))).toBeTruthy();
  });

  it('includes the top-bar component', () => {
    expect(fixture.debugElement.query(By.css('top-bar'))).toBeTruthy();
  });

  it('includes the side-nav', () => {
    expect(fixture.debugElement.query(By.css('side-nav'))).toBeTruthy();
  });

  it('passes confidential-messaage to side-nav', () => {
    expect(fixture.debugElement.query(By.css('side-nav confidential-message'))).toBeTruthy();
  });

  it('passes router-outlet to side-nav', () => {
    expect(fixture.debugElement.query(By.css('side-nav router-outlet'))).toBeTruthy();
  });
});
