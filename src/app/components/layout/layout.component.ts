import { CdkPortalOutlet } from '@angular/cdk/portal';
import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatSidenav } from '@angular/material';
import { environment } from '../../../environments/environment';
import { MeViewModel } from '../../models/me-view-model';
import { MeService } from '../../services/me/me.service';
import { DrawerService } from '../../services/drawer/drawer.service';
import { Event, Router, NavigationStart, NavigationEnd, NavigationCancel, NavigationError } from '@angular/router';
import { PermissionNamespaceType } from '@pplsi-core/datalayer';
import { AuthenticationService } from 'src/app/services/authentication/authentication.service';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'people-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit, AfterViewInit {
  region: string;
  me: MeViewModel;
  hasLoggedIn: boolean;
  loading: boolean;
  loadingColor: string;
  _hasAccess = false;

  @ViewChild('drawer', { static: false }) drawer: MatSidenav;
  @ViewChild(CdkPortalOutlet, { static: false }) drawerContentReference;

  constructor(private meService: MeService,
    private authenticationService: AuthenticationService,
    private drawerService: DrawerService,
    private router: Router) {
    this.subscribeToRouterEvents();
  }

  ngOnInit(): void {
    this.region = environment.region;

    this.authenticationService.authenticated().subscribe(isAuthenticated => {
      this.hasLoggedIn = isAuthenticated;

      this.meService.$currentUser().subscribe((me: MeViewModel) => {
        this.me = me;

        if (!!me && this.me.hasPermission('view_people_application', PermissionNamespaceType.Admin)) {
          this._hasAccess = true;
        } else {
          this.router.navigate(['/unauthorized']);
        }
      });

      this.loading = false;
      this.loadingColor = 'primary';
    });
  }

  ngAfterViewInit(): void {
    this.drawerService.setDrawer(this.drawer);
    this.drawerService.setDrawerContentReference(this.drawerContentReference);
  }

  updateLoadingMask(color: string, loading: boolean): void {
    this.loadingColor = color;
    this.loading = loading;
  }

  subscribeToRouterEvents(): void {
    this.router.events.subscribe((event: Event) => {
      switch (true) {
        case event instanceof NavigationStart: {
          this.updateLoadingMask('primary', true);
          break;
        }
        case event instanceof NavigationEnd:
        case event instanceof NavigationCancel: {
          this.updateLoadingMask('primary', false);
          break;
        }
        case event instanceof NavigationError: {
          this.updateLoadingMask('warn', true);
          break;
        }
        default: {
          break;
        }
      }
    });
  }

  get hasAccess(): boolean {
    return (this.hasLoggedIn || !!this.me) && this._hasAccess;
  }
}
