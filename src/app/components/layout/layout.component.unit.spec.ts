import { HttpClientTestingModule } from '@angular/common/http/testing';
import { async, ComponentFixture, getTestBed, TestBed } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { of, ReplaySubject } from 'rxjs';
import { Me } from '@pplsi-core/datalayer';
import { DrawerService } from '../../services/drawer/drawer.service';
import { LayoutComponent } from './layout.component';
import { RouterEvent, Router, NavigationStart, NavigationEnd, NavigationCancel, NavigationError } from '@angular/router';
import { MeViewModel } from '../../models/me-view-model';
import { MeService } from '../../services/me/me.service';
import { MeRepository } from '@pplsi-core/datalayer';
import { RouterTestingModule } from '@angular/router/testing';
import { PermissionNamespaceType } from '@pplsi-core/datalayer';
import { MeFactory, PeopleFactories } from '@pplsi-core/factories';
import { AuthenticationService } from 'src/app/services/authentication/authentication.service';
import { WindowService } from '@pplsi-core/shared-modules/dist/src/services/window.service';

describe('LayoutComponent.Unit', () => {
  let component: LayoutComponent;
  let fixture: ComponentFixture<LayoutComponent>;
  let meService: MeService;
  let meRepo: MeRepository;
  let drawerService: DrawerService;
  let methodSpy: jasmine.Spy;
  let router: Router;
  let auth: AuthenticationService;

  const eventSubject = new ReplaySubject<RouterEvent>(1);
  const routerMock = {
    navigate: jasmine.createSpy('navigate'),
    events: eventSubject.asObservable(),
    url: 'test/url'
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LayoutComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      providers: [
        AuthenticationService,
        MeRepository,
        MeService,
        DrawerService,
        RouterTestingModule,
        WindowService,
        { provide: Router, useValue: routerMock }
      ],
      imports: [HttpClientTestingModule]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LayoutComponent);
    component = fixture.componentInstance;
    const injector: TestBed = getTestBed();

    auth = injector.get(AuthenticationService);
    meRepo = injector.get(MeRepository);
    meService = injector.get(MeService);
    drawerService = injector.get(DrawerService);
    router = injector.get(Router);

    fixture.detectChanges();
  });

  describe('#ngOnInit', () => {
    let me: MeViewModel;

    describe('when user has permission to view people application', () => {
      beforeEach(() => {
        me = new MeViewModel(MeFactory.build({
          name: 'John Doe', permissions: [
            PeopleFactories.PermissionFactory.build({ name: 'view_people_application', namespace: PermissionNamespaceType.Admin })]
        }));
        spyOn(auth, 'authenticated').and.returnValue(of(true));
        spyOn(meService, '$currentUser').and.returnValue(of(me));

        component.ngOnInit();
      });

      it('should set region', () => {
        expect(component.region).toBe('NA');
      });

      it('should set me', () => {
        expect(component.me).toEqual(me);
      });

      it('should set hasLoggedIn', () => {
        expect(component.hasLoggedIn).toBe(true);
      });

      it('sets _hasAccess', () => {
        expect(component._hasAccess).toBe(true);
      });

      it('initializes the loading mask as hidden and primary style', () => {
        expect(component.loading).toEqual(false);
        expect(component.loadingColor).toEqual('primary');
      });
    });

    describe('when user lacks permission to view people application', () => {
      it('should route to /unauthorized', () => {

        spyOn(auth, 'authenticated').and.returnValue(of(true));
        spyOn(meService, '$currentUser').and.returnValue(of(new MeViewModel(MeFactory.build({
          name: 'John Doe', permissions: []
        }))));

        component.ngOnInit();
        fixture.whenStable().then(() => {
          fixture.detectChanges();
          expect(router.navigate).toHaveBeenCalledWith(['/unauthorized']);
        });
      });
    });
  });

  describe('#ngAfterViewInit', () => {
    it('sets the drawer on the drawer service', () => {
      spyOn(drawerService, 'setDrawer');

      component.ngAfterViewInit();

      expect(drawerService.setDrawer).toHaveBeenCalledWith(component.drawer);
    });

    it('sets the drawer content reference on the drawer service', () => {
      spyOn(drawerService, 'setDrawerContentReference');

      component.ngAfterViewInit();

      expect(drawerService.setDrawerContentReference).toHaveBeenCalledWith(component.drawerContentReference);
    });
  });

  describe('#subscribeToRouterEvents', () => {
    beforeEach(() => {
      methodSpy = spyOn(component, 'updateLoadingMask');
    });

    it('shows the loading mask on navigation start', () => {
      eventSubject.next(new NavigationStart(1, 'regular', 'imperative'));
      expect(methodSpy).toHaveBeenCalledWith('primary', true);
    });

    it('hides the loading mask on navigation end', () => {
      eventSubject.next(new NavigationEnd(1, 'regular', 'redirectUrl'));
      expect(methodSpy).toHaveBeenCalledWith('primary', false);
    });

    it('hides the loading mask on navigation cancel', () => {
      eventSubject.next(new NavigationCancel(1, 'regular', 'redirectUrl'));
      expect(methodSpy).toHaveBeenCalledWith('primary', false);
    });

    it('shows the loading mask with warning color on navigation error', () => {
      eventSubject.next(new NavigationError(1, 'regular', 'redirectUrl'));
      expect(methodSpy).toHaveBeenCalledWith('warn', true);
    });
  });

  describe('#updateLoadingMask', () => {
    it('sets component.loading to provided loading value', () => {
      component.updateLoadingMask('color', true);
      expect(component.loading).toEqual(true);
    });

    it('sets component.loadingColor to the provided color value', () => {
      component.updateLoadingMask('warn', false);
      expect(component.loadingColor).toEqual('warn');
    });
  });

  describe('#hasAccess', () => {
    it('returns false when hasLoggedIn is false and me is not set and _hasAccess is true', () => {
      component.hasLoggedIn = false;
      component.me = null;
      component._hasAccess = true;

      expect(component.hasAccess).toEqual(false);
    });

    it('returns false when _hasAccess is false but hasLoggedIn is true', () => {
      component.hasLoggedIn = true;
      component.me = null;
      component._hasAccess = false;

      expect(component.hasAccess).toEqual(false);
    });

    it('returns true when me is set and _hasAccess is true', () => {
      component.hasLoggedIn = false;
      component.me = new MeViewModel(new Me({}));
      component._hasAccess = true;

      expect(component.hasAccess).toEqual(true);
    });

    it('returns true when hasLoggedIn is true and _hasAccess is true', () => {
      component.hasLoggedIn = true;
      component.me = null;
      component._hasAccess = true;

      expect(component.hasAccess).toEqual(true);
    });
  });
});
