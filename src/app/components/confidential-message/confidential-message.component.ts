import { Component, Input } from '@angular/core';

@Component({
  selector: 'confidential-message',
  templateUrl: './confidential-message.component.html',
  styleUrls: ['./confidential-message.component.scss']
})
export class ConfidentialMessageComponent {
  @Input() loggedIn = false;
}
