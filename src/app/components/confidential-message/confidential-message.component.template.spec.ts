import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { ConfidentialMessageComponent } from './confidential-message.component';

describe('ConfidentialMessageComponent.Template', () => {
  let component: ConfidentialMessageComponent;
  let fixture: ComponentFixture<ConfidentialMessageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfidentialMessageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfidentialMessageComponent);

    component = fixture.componentInstance;
    component.loggedIn = false;

    fixture.detectChanges();
  });

  describe('not logged in', () => {
    it('should not display the confidential message', () => {
      expect(fixture.debugElement.query(By.css('div'))).toBeFalsy();
    });
  });

  describe('logged in', () => {
    beforeEach(() => {
      component.loggedIn = true;
      fixture.detectChanges();
    });

    it('should display the confidential message', () => {
      expect(fixture.debugElement.query(By.css('div'))).toBeTruthy();
    });

    it('displays a caution image', () => {
      const debugElement: DebugElement = fixture.debugElement.query(By.css('img'));

      expect(debugElement).toBeTruthy();
      expect(debugElement.nativeElement.src).toContain('assets/caution.png');
    });

    it('displays a caution message', () => {
      const debugElement: DebugElement = fixture.debugElement.query(By.css('h4'));

      expect(debugElement).toBeTruthy();
      expect(debugElement.nativeElement.innerHTML).toEqual('Confidential information present');
    });
  });
});
