import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { ResolutionSearchResultComponent } from './resolution-search-result.component';
import { DatePipe } from '@angular/common';
import { ResolutionViewModel } from '../../../modules/resolutions/models/resolution-view-model';
import { PartnerFactories } from '@pplsi-core/factories';
import { FeatherModule } from 'angular-feather';
import { Activity, CheckCircle, Layers, UserPlus } from 'angular-feather/icons';
import { FulfillmentPartnerBranch } from '@pplsi-core/datalayer/dist/@pplsi-core/datalayer/lib/models/partner/fulfillment-partner-branch';

const getResolution = (cfg) => new ResolutionViewModel(PartnerFactories.ResolutionFactory.build(cfg));

describe('ResolutionSearchResultComponent.Unit', () => {
  let component: ResolutionSearchResultComponent;
  let fixture: ComponentFixture<ResolutionSearchResultComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ResolutionSearchResultComponent],
      imports: [
        FeatherModule.pick({ Activity, CheckCircle, Layers, UserPlus }),
        RouterTestingModule
      ],
      providers: [DatePipe]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResolutionSearchResultComponent);
    component = fixture.componentInstance;
    component.resolution = getResolution({});
    fixture.detectChanges();
  });

  describe('#ngOnInit', () => {
    beforeEach(() => component.ngOnInit());

    it('sets the actionTaken prop', () => expect(component.actionTaken).toBeDefined());
    it('sets the approvedToContinue prop', () => expect(component.approvedToContinue).toBeDefined());
    it('sets the assignee prop', () => expect(component.assignee).toBeDefined());
    it('sets the createdAt prop', () => expect(component.createdAt).toBeDefined());
    it('sets the createdLabel prop', () => expect(component.createdLabel).toBeDefined());
    it('sets the displayId prop', () => expect(component.displayId).toBeDefined());
    it('sets the fullUserName prop', () => expect(component.fullUserName).toBeDefined());
    it('sets the nationalPlan prop', () => expect(component.nationalPlan).toBeDefined());
    it('sets the resolutionType prop', () => expect(component.resolutionType).toBeDefined());
    it('sets the status prop', () => expect(component.status).toBeDefined());
    it('sets the userCityState prop', () => expect(component.userCityState).toBeDefined());

    describe('when addresses is null', () => {
      it('it does not set userCityState prop', () => {
        component.userCityState = undefined;
        component.resolution = getResolution({ user: PartnerFactories.UserFactory.build({ addresses: null }) });
        component.ngOnInit();

        expect(component.userCityState).toBeUndefined();
      });
    });

    describe('when addresses array is empty', () => {
      it('it does not set userCityState prop', () => {
        component.userCityState = undefined;
        component.resolution = getResolution({ user: PartnerFactories.UserFactory.build({ addresses: [] }) });
        component.ngOnInit();

        expect(component.userCityState).toBeUndefined();
      });
    });
  });

  describe('#getCreatedLabel', () => {
    it('renders the created label from openedAt', () => {
      component.openedAt = new Date();
      const openedAt = component.openedAt;
      const month = openedAt.toLocaleString('default', { month: 'long' }),
        day = openedAt.getDate(),
        year = openedAt.getFullYear(),
        time = openedAt.toLocaleTimeString('default', { hour: '2-digit', minute: '2-digit' });
      expect(component.getCreatedLabel(openedAt)).toBe(`Created - ${month} ${day}, ${year} ${time}`);
    });
  });

  describe('#actionTakenClass', () => {
    it('returns ok when resolution type equals provider action taken', () => {
      component.resolution = new ResolutionViewModel(
        PartnerFactories.ResolutionFactory.build({ action_taken: 'provider action taken' })
      );

      expect(component.actionTakenClass).toBe('ok');
    });

    it('returns warning when resolution type is not equal to provider action taken', () => {
      component.resolution = new ResolutionViewModel(
        PartnerFactories.ResolutionFactory.build({ action_taken: 'anything else' })
      );

      expect(component.actionTakenClass).toBe('warn');
    });
  });
});
