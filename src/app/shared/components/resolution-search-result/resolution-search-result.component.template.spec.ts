import { DebugElement } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { RouterTestingModule } from '@angular/router/testing';
import { ResolutionSearchResultComponent } from './resolution-search-result.component';
import { PartnerFactories } from '@pplsi-core/factories';
import { ResolutionViewModel } from '../../../modules/resolutions/models/resolution-view-model';
import { DatePipe } from '@angular/common';
import { FeatherModule } from 'angular-feather';
import { Activity, CheckCircle, Layers, UserPlus } from 'angular-feather/icons';

const getResolution = (cfg) => new ResolutionViewModel(PartnerFactories.ResolutionFactory.build(cfg));

describe('ResolutionSearchResultComponent.Template', () => {
  let component: ResolutionSearchResultComponent;
  let fixture: ComponentFixture<ResolutionSearchResultComponent>;
  let datePipe: DatePipe;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ResolutionSearchResultComponent],
      imports: [
        FeatherModule.pick({ Activity, CheckCircle, Layers, UserPlus }),
        RouterTestingModule
      ],
      providers: [DatePipe]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResolutionSearchResultComponent);
    component = fixture.componentInstance;
    component.resolution = getResolution({ opened_at: '2019-11-15T22:00:30.906Z' });
    datePipe = new DatePipe('en_us');
    fixture.detectChanges();
  });

  it('has a router link to /resolutions/id', () => {
    const debugElement: DebugElement = fixture.debugElement.query(By.css('a'));

    expect(debugElement.nativeElement.getAttribute('href')).toEqual(`/resolutions/${component.resolution.id}`);
  });

  describe('national plan shield icon', () => {
    it('does not display when national plan is false', () => {
      component.resolution.national_plan = false;
      component.ngOnInit();
      fixture.detectChanges();
      expect(fixture.debugElement.query(By.css('.result-national-plan'))).toBeNull();
    });

    it('displays when national plan is truthy', () => {
      component.resolution.national_plan = true;
      component.ngOnInit();
      fixture.detectChanges();
      expect(fixture.debugElement.query(By.css('.result-national-plan'))).toBeTruthy();
    });
  });

  describe('group icon', () => {
    it('should not display if resolution group is true and national plan is true', () => {
      component.resolution.group = true;
      component.resolution.national_plan = true;
      component.ngOnInit();
      fixture.detectChanges();
      expect(fixture.debugElement.query(By.css('.resolution-is-member'))).toBeNull();
      expect(fixture.debugElement.query(By.css('.result-national-plan'))).toBeTruthy();
    });

    it('does not display when group is false', () => {
      component.resolution.group = false;
      component.ngOnInit();
      fixture.detectChanges();
      expect(fixture.debugElement.query(By.css('.resolution-is-member'))).toBeNull();
    });

    it('displays when group is truthy', () => {
      component.resolution.national_plan = false;
      component.resolution.group = true;
      component.ngOnInit();
      fixture.detectChanges();
      expect(fixture.debugElement.query(By.css('.resolution-is-member'))).toBeTruthy();
    });
  });

  describe('resolution action', () => {
    it('includes the result action text', () => {
      const el = fixture.debugElement.query(By.css('.result-action'));
      expect(el.nativeElement.innerHTML).toContain(component.resolution.action_taken);
    });

    it('is not displayed if actionTaken is undefined', () => {
      component.resolution.action_taken = undefined;
      component.ngOnInit();
      fixture.detectChanges();
      expect(fixture.debugElement.query(By.css('.result-action'))).toBeNull();
    });

    describe('provider action taken', () => {
      beforeEach(() => {
        component.resolution.action_taken = 'provider action taken';
        component.ngOnInit();
        fixture.detectChanges();
      });

      it('has the "ok" class', () => {
        expect(fixture.debugElement.query(By.css('.result-action.ok'))).toBeDefined();
      });

      it('does not have the "warn" class', () => {
        expect(fixture.debugElement.query(By.css('.result-action.warn'))).toBeNull();
      });
    });

    describe('waiting on provider', () => {
      beforeEach(() => {
        component.resolution.action_taken = 'waiting on provider';
        component.ngOnInit();
        fixture.detectChanges();
      });

      it('has the "warn" class', () => {
        expect(fixture.debugElement.query(By.css('.result-action.warn'))).toBeDefined();
      });

      it('does not have the "ok" class', () => {
        expect(fixture.debugElement.query(By.css('.result-action.ok'))).toBeNull();
      });
    });
  });

  it('includes the resolution user full name', () => {
    const el = fixture.debugElement.query(By.css('.result-name'));
    const testFullName = `${component.resolution.user.first_name} ${component.resolution.user.last_name}`;
    expect(el.nativeElement.innerHTML).toContain(testFullName);
  });

  it('includes the resolution user city and state', () => {
    const el = fixture.debugElement.query(By.css('.result-city'));
    const { locality, administrative_area } = component.resolution.user.addresses[0];
    const testCityState = `${locality}, ${administrative_area}`;
    expect(el.nativeElement.innerHTML).toContain(testCityState);
  });

  it('includes the result display id', () => {
    const el = fixture.debugElement.query(By.css('.result-display-id'));
    expect(el.nativeElement.innerHTML).toBe(component.resolution.display_id);
  });

  it('includes the result assignee', () => {
    const el = fixture.debugElement.query(By.css('.result-assignee'));
    expect(el.nativeElement.innerHTML).toContain(component.resolution.assignee.full_name);
  });

  it('includes the result status', () => {
    const el = fixture.debugElement.query(By.css('.result-status'));
    expect(el.nativeElement.innerHTML).toContain(component.resolution.status);
  });

  it('includes the created at info if created_at is present', () => {
    const el = fixture.debugElement.query(By.css('.result-created-at'));
    const openedAt = new Date(component.resolution.opened_at);
    const month = openedAt.toLocaleString('default', { month: 'long' }),
      day = openedAt.getDate(),
      year = openedAt.getFullYear(),
      time = openedAt.toLocaleTimeString('default', { hour: '2-digit', minute: '2-digit' });
    expect(el.nativeElement.innerHTML).toBe(`Created - ${month} ${day}, ${year} ${time}`);
  });

  it('includes the resolution status', () => {
    const el = fixture.debugElement.query(By.css('.result-status'));
    expect(el.nativeElement.innerHTML).toContain(component.resolution.status);
  });

  it('includes the resolution type', () => {
    const el = fixture.debugElement.query(By.css('.result-type'));
    expect(el.nativeElement.innerHTML).toContain(component.resolution.resolution_type);
  });
});
