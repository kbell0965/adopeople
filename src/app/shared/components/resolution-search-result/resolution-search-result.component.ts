import { Component, Input, OnInit } from '@angular/core';
import { ResolutionViewModel } from '../../../modules/resolutions/models/resolution-view-model';
import { DatePipe } from '@angular/common';
import { SafeHtml } from '@angular/platform-browser';

@Component({
  selector: 'ls-resolution-search-result',
  styleUrls: ['./resolution-search-result.scss'],
  templateUrl: './resolution-search-result.component.html'
})
export class ResolutionSearchResultComponent implements OnInit {
  readonly PROVIDER_ACTION_TAKEN = 'provider action taken';
  @Input() resolution: ResolutionViewModel;

  actionTaken: string;
  approvedToContinue: boolean;
  assignee: string;
  createdAt: Date;
  openedAt: Date;
  createdLabel: string;
  displayId: string;
  fullUserName: string;
  nationalPlan: boolean;
  resolutionType: string;
  status: string;
  userCityState: SafeHtml;

  constructor(private datePipe: DatePipe) {}

  ngOnInit() {
    if (this.resolution) {
      const {
        action_taken,
        approved_to_continue,
        assignee,
        created_at,
        opened_at,
        display_id,
        national_plan,
        resolution_type,
        status,
        user
      } = this.resolution;
      this.actionTaken = action_taken ? action_taken : null;
      this.approvedToContinue = approved_to_continue ? approved_to_continue : false;
      this.assignee = assignee.full_name ? assignee.full_name : '';
      this.createdAt = created_at;
      this.openedAt = opened_at;
      this.createdLabel = this.getCreatedLabel(this.openedAt);
      this.displayId = display_id ? display_id : '';
      this.fullUserName = this.resolution.fullUserName(user.first_name, user.last_name);
      this.nationalPlan = national_plan ? national_plan : false;
      this.resolutionType = resolution_type ? resolution_type : '';
      this.status = status ? status : '';
      if (user.addresses && user.addresses.length) {
        const { locality, administrative_area } = user.addresses[0];
        this.userCityState = this.resolution.userCityState(locality, administrative_area);
      }
    }
  }

  getCreatedLabel(date: Date): string {
    return date ? `Created - ${this.datePipe.transform(date, 'longDate')} ${this.datePipe.transform(date, 'hh:mm a')}` : '';
  }

  get actionTakenClass(): string {
    return this.resolution.action_taken === this.PROVIDER_ACTION_TAKEN ? 'ok' : 'warn';
  }
}
