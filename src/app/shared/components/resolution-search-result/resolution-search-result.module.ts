import { NgModule } from '@angular/core';
import { ResolutionSearchResultComponent } from './resolution-search-result.component';
import { FeatherModule } from 'angular-feather';
import { Activity, CheckCircle, Layers, UserPlus } from 'angular-feather/icons';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [ResolutionSearchResultComponent],
  exports: [ResolutionSearchResultComponent],
  imports: [
    CommonModule,
    RouterModule,
    FeatherModule.pick({ Activity, CheckCircle, Layers, UserPlus })
  ]
})
export class ResolutionSearchResultModule { }
