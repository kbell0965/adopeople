import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MatFormFieldModule, MAT_DIALOG_DATA, MatDialogModule, MatDialogRef } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { EditTextDialogComponent } from './edit-text-dialog.component';
import { EditTextDialogModule } from './edit-text-dialog.module';
import { EditTextDialogConfig } from './edit-text-dialog-config';

describe('EditTextDialogComponent', () => {
  let component: EditTextDialogComponent;
  let fixture: ComponentFixture<EditTextDialogComponent>;
  const config: EditTextDialogConfig = {
    title: 'test',
    cancelButtonText: 'cancel',
    submitButtonText: 'submit',
    maxRows: 1,
    value: 'value in',
    textLength: 10
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        MatFormFieldModule,
        EditTextDialogModule,
        MatDialogModule,
        BrowserAnimationsModule
      ],
      providers: [
        { provide: MAT_DIALOG_DATA, useValue: config },
        { provide: MatDialogRef, useValue: {} }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditTextDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
