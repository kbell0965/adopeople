import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatDialogModule, MatInputModule, MatFormFieldModule, MatButtonModule } from '@angular/material';
import { EditTextDialogComponent } from './edit-text-dialog.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [EditTextDialogComponent],
  imports: [
    CommonModule,
    FormsModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatDialogModule
  ],
  entryComponents: [EditTextDialogComponent]
})
export class EditTextDialogModule { }
