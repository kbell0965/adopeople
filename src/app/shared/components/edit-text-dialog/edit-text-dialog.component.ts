import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { EditTextDialogConfig } from './edit-text-dialog-config';

@Component({
  selector: 'app-edit-text-dialog',
  templateUrl: './edit-text-dialog.component.html',
  styleUrls: ['./edit-text-dialog.component.scss']
})
export class EditTextDialogComponent {
  constructor(@Inject(MAT_DIALOG_DATA) public data: EditTextDialogConfig) {
  }
}
