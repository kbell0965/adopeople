import { Observable } from 'rxjs';

export interface EditTextDialogConfig {
    title: string;
    cancelButtonText: string;
    submitButtonText: string;
    value: string;
    maxRows?: number;
    textLength?: number;
}
