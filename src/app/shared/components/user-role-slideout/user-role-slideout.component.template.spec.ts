import { async, ComponentFixture, TestBed, getTestBed } from '@angular/core/testing';

import { UserRoleSlideoutComponent } from './user-role-slideout.component';
import { DrawerService, DRAWER_DATA } from 'src/app/services/drawer/drawer.service';
import { PeopleModels, PeopleRoleRepository, UserRepository } from '@pplsi-core/datalayer';
import { PeopleFactories } from '@pplsi-core/factories';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { MatFormFieldModule, MatSidenavModule } from '@angular/material';
import { UserRoleSlideoutModule } from './user-role-slideout.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CUSTOM_ELEMENTS_SCHEMA, DebugElement } from '@angular/core';
import { By } from '@angular/platform-browser';
import { of } from 'rxjs';

describe('UserRoleSlideoutComponent.Template', () => {
  let component: UserRoleSlideoutComponent;
  let fixture: ComponentFixture<UserRoleSlideoutComponent>;
  let injector: TestBed;
  let drawer: DrawerService;

  const fpmStub: PeopleModels.FulfillmentPartnerMember =
    new PeopleModels.FulfillmentPartnerMember(PeopleFactories.FulfillmentPartnerMemberFactory.build({}));

  const roles: PeopleModels.Role[] = [
    new PeopleModels.Role(PeopleFactories.RoleFactory.build({})),
    new PeopleModels.Role(PeopleFactories.RoleFactory.build({})),
    new PeopleModels.Role(PeopleFactories.RoleFactory.build({}))
  ];

  const rolePage: PeopleModels.RolesWithPagination = new PeopleModels.RolesWithPagination({
    roles: roles,
    total_length: roles.length
  });

  fpmStub.fpm_roles = roles.slice(0, 2);

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [
        { provide: DRAWER_DATA, useValue: fpmStub },
        {
          provide: PeopleRoleRepository, useValue: {
            findAll: (page: number, per_page: number, search?: string) => of(rolePage),
            find: (roleId: string) => { },
            create: (role: PeopleModels.Role) => { },
            update: (roleId: string, role: PeopleModels.Role) => { }
          }
        },
        {
          provide: DrawerService, useValue: {
            close: () => { }
          }
        },
        {
          provide: UserRepository, useValue: {}
        }
      ], imports: [
        FormsModule,
        ReactiveFormsModule,
        HttpClientTestingModule,
        MatFormFieldModule,
        MatSidenavModule,
        UserRoleSlideoutModule,
        BrowserAnimationsModule
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserRoleSlideoutComponent);
    component = fixture.componentInstance;
    injector = getTestBed();
    drawer = injector.get(DrawerService);

    spyOn(drawer, 'close');
    fixture.detectChanges();

    component.ngOnInit();
    fixture.detectChanges();
  });

  describe('Header', () => {
    it('has user name in title', () => {
      const el = fixture.debugElement.query(By.css('header h3'));
      expect(el.nativeElement.innerHTML).toContain(fpmStub.first_name);
      expect(el.nativeElement.innerHTML).toContain(fpmStub.last_name);
    });

    it('clicking close icon closes edit', () => {
      spyOn(component, 'close');
      fixture.debugElement.query(By.css('i-feather[name="x"]')).nativeElement.click();
      expect(component.close).toHaveBeenCalled();
    });
  });

  describe('Form - User Information', () => {
    it('has user first name', () => {
      const el = fixture.debugElement.query(By.css('input.user-first-name'));
      expect(el.nativeElement.value).toBe(fpmStub.first_name);
    });

    it('has user last name', () => {
      const el = fixture.debugElement.query(By.css('input.user-last-name'));
      expect(el.nativeElement.value).toBe(fpmStub.last_name);
    });

    it('has user email address', () => {
      const el = fixture.debugElement.query(By.css('input.user-email'));
      expect(el.nativeElement.value).toBe(fpmStub.email);
    });
  });

  describe('Form - User Roles', () => {
    it('can change roles', () => {
      const originalRoleId = component.formRoles.at(0).value.roleId;
      const differentRole = roles.filter(role => role.id !== originalRoleId)[0];

      component.formRoles.at(0).setValue({ roleId: differentRole.id });
      const expectedDifferentRoleId = component.formRoles.at(0).value.roleId;

      expect(originalRoleId === expectedDifferentRoleId).toBeFalsy();
    });
  });

  describe('Form - Actions', () => {
    it('can cancel', () => {
      spyOn(component, 'close');
      const el = fixture.debugElement.query(By.css('button.cancel-button'));
      el.triggerEventHandler('click', {});
      expect(component.close).toHaveBeenCalled();
    });

    it('submit disabled if form is pristine', () => {
      const submitButton = fixture.debugElement.query(By.css('button.submit-button'));
      expect(component.userRolesForm.pristine).toBeTruthy();
      expect(submitButton.nativeElement.disabled).toBeTruthy();
    });

    it('submit enabled if form is dirty', () => {
      const submitButton = fixture.debugElement.query(By.css('button.submit-button'));
      component.formRoles.at(0).setValue({ roleId: roles[2] });
      component.formRoles.at(0).markAsDirty();
      expect(component.userRolesForm.dirty).toBeTruthy();

      fixture.detectChanges();
      expect(submitButton.nativeElement.disabled).toBeFalsy();
    });

    it('submit disabled after form is dirty but becomes invalid ', () => {
      const submitButton = fixture.debugElement.query(By.css('button.submit-button'));
      component.formRoles.at(0).markAsDirty();
      component.addAdditionalRole();

      fixture.detectChanges();
      expect(submitButton.nativeElement.disabled).toBeTruthy();
    });

    it('can click submit when enabled', () => {
      component.formRoles.at(0).markAsDirty();
      fixture.detectChanges();

      spyOn(component, 'submit');
      fixture.debugElement.query(By.css('button.submit-button')).nativeElement.click();

      expect(component.submit).toHaveBeenCalled();
    });
  });

  describe('Interpolated Component Properties', () => {
    it('title contains first name',
      () => expect(component.title).toContain(fpmStub.first_name));

    it('title contains last name',
      () => expect(component.title).toContain(fpmStub.last_name));

    it('userRolesForm first name form fields match fpm specified',
      () => expect(component.userRolesForm.get('firstName').value).toBe(fpmStub.first_name));

    it('userRolesForm lastName form fields match fpm specified',
      () => expect(component.userRolesForm.get('lastName').value).toBe(fpmStub.last_name));

    it('userRolesForm email form fields match fpm specified',
      () => expect(component.userRolesForm.get('email').value).toBe(fpmStub.email));
  });

  describe('Adding role', () => {
    it('Clicking add-role-text adds an additional role to the form', () => {
      const initialRoleCount = component.formRoles.length;
      fixture.debugElement.query(By.css('.add-role-text')).nativeElement.click();
      const withAdditionalRoleCount = component.formRoles.length;

      expect(withAdditionalRoleCount).toBe(initialRoleCount + 1);
    });
  });

  describe('Removing role', () => {
    it('First role has no removal icon', () => {
      const firstRemovalIcon = fixture.debugElement.queryAll(By.css('.roles-container'))[0].query(By.css('[name=x-circle]'));
      expect(firstRemovalIcon).toBeFalsy();
    });

    it('Second role has a removal icon', () => {
      const secondRemovalIcon = fixture.debugElement.queryAll(By.css('.roles-container'))[1].query(By.css('[name=x-circle]'));
      expect(secondRemovalIcon).toBeTruthy();
    });

    it('Clicking second role\'s removal icon removes role', () => {
      const secondRemovalIcon = fixture.debugElement.queryAll(By.css('.roles-container'))[1].query(By.css('[name=x-circle]'));
      const initialRoleCount = component.formRoles.length;
      secondRemovalIcon.nativeElement.click();
      const afterRemovalCount = component.formRoles.length;

      expect(afterRemovalCount).toBe(initialRoleCount - 1);
    });
  });
});
