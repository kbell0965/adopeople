import { async, ComponentFixture, TestBed, getTestBed } from '@angular/core/testing';
import { UserRoleSlideoutComponent } from './user-role-slideout.component';
import { DRAWER_DATA, DrawerService } from 'src/app/services/drawer/drawer.service';
import { PeopleModels, PeopleRoleRepository, UserRepository } from '@pplsi-core/datalayer';
import { PeopleFactories } from '@pplsi-core/factories';
import { FormsModule, ReactiveFormsModule, FormBuilder } from '@angular/forms';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { MatSidenavModule, MatFormFieldModule } from '@angular/material';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { of } from 'rxjs';
import { UserRoleSlideoutModule } from './user-role-slideout.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

describe('UserRoleSlideoutComponent', () => {
  let component: UserRoleSlideoutComponent;
  let fixture: ComponentFixture<UserRoleSlideoutComponent>;
  let injector: TestBed;
  let drawer: DrawerService;
  let userRepo: UserRepository;

  const fpmStub: PeopleModels.FulfillmentPartnerMember =
    new PeopleModels.FulfillmentPartnerMember(PeopleFactories.FulfillmentPartnerMemberFactory.build({}));

  const roles: PeopleModels.Role[] = [
    new PeopleModels.Role(PeopleFactories.RoleFactory.build({})),
    new PeopleModels.Role(PeopleFactories.RoleFactory.build({})),
    new PeopleModels.Role(PeopleFactories.RoleFactory.build({}))
  ];

  const rolePage: PeopleModels.RolesWithPagination = new PeopleModels.RolesWithPagination({
    roles: roles,
    total_length: roles.length
  });

  fpmStub.fpm_roles = roles.slice(0, 2);

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [
        { provide: DRAWER_DATA, useValue: fpmStub },
        {
          provide: PeopleRoleRepository, useValue: {
            findAll: (page: number, per_page: number, search?: string) => of(rolePage),
            find: (roleId: string) => { },
            create: (role: PeopleModels.Role) => { },
            update: (roleId: string, role: PeopleModels.Role) => { }
          }
        },
        {
          provide: DrawerService, useValue: {
            close: () => { }
          }
        },
        {
          provide: UserRepository, useValue: {
            add_role: jasmine.createSpy('add_role').and.returnValue(of({})),
            revoke_role: jasmine.createSpy('revoke_role').and.returnValue(of({}))
          }
        }
      ], imports: [
        FormsModule,
        ReactiveFormsModule,
        HttpClientTestingModule,
        MatFormFieldModule,
        MatSidenavModule,
        UserRoleSlideoutModule,
        BrowserAnimationsModule
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserRoleSlideoutComponent);
    component = fixture.componentInstance;
    injector = getTestBed();

    drawer = injector.get(DrawerService);
    userRepo = injector.get(UserRepository);

    fixture.detectChanges();
  });

  describe('ngOnInit', () => {
    beforeEach(() => {
      component.ngOnInit();
    });

    it('roles have been initialized', () => {
      expect(component.rolesPage$).toBeDefined();
      expect(component.rolesPage.roles.length).toBe(3);
    });

    it('formRoles.length initially matches fpm_roles.length',
      () => expect(component.formRoles.length).toBe(fpmStub.fpm_roles.length));
  });

  describe('#addAdditionalRole', () => {
    it('adds an empty role to formRoles', () => {
      const originalLength = component.formRoles.length;
      component.addAdditionalRole();

      expect(component.formRoles.length).toBe(originalLength + 1);
      expect(component.formRoles.controls[originalLength].value.roleId).toBeFalsy();
    });

    it('multiple function calls adds a single role if newly added role values are unset', () => {
      component.addAdditionalRole();
      expect(component.formRoles.length).toBe(3);
      component.addAdditionalRole();
      expect(component.formRoles.length).toBe(3);
    });

    it('multiple function calls adds additional roles if newly added role values are set', () => {
      component.addAdditionalRole();
      const firstLength = component.formRoles.length;
      component.formRoles.at(firstLength - 1).setValue({ roleId: 'is set' });

      component.addAdditionalRole();
      const secondLength = component.formRoles.length;

      expect(firstLength).toBe(secondLength - 1);
    });
  });

  describe('#removeRole', () => {
    it('can remove role', () => {
      const previousFirstRoleId = component.formRoles.value[0].roleId;
      expect(component.formRoles.length).toBe(2);
      component.removeRole(0);

      const currentFirstRoleId = component.formRoles.value[0].roleId;
      expect(component.formRoles.length).toBe(1);
      expect(previousFirstRoleId === currentFirstRoleId).toBeFalsy();
    });

    it('removing role sets pristine userRolesForm to dirty', () => {
      expect(component.userRolesForm.pristine).toBeTruthy();
      component.removeRole(0);
      expect(component.userRolesForm.dirty).toBeTruthy();
    });
  });

  describe('#filterRoles', () => {
    it('index 0 does not include selected role at index 1', () => {
      expect(component.filterRoles(0).indexOf(roles[1])).toBe(-1);
    });
  });

  describe('#submit', () => {
    it('no changed roles closes drawer', () => {
      spyOn(drawer, 'close');
      component.submit();

      expect(drawer.close).toHaveBeenCalled();
    });

    it('no changed roles result in no persisted role changes', () => {
      component.submit();

      expect(userRepo.add_role).toHaveBeenCalledTimes(0);
      expect(userRepo.revoke_role).toHaveBeenCalledTimes(0);
    });

    it('added roles are persisted', () => {
      component.addAdditionalRole();
      component.formRoles.at(component.formRoles.length - 1).setValue(
        { roleId: roles[2].id }
      );
      component.submit();

      expect(userRepo.add_role).toHaveBeenCalled();
    });

    it('revoked roles are persisted', () => {
      component.removeRole(component.formRoles.length - 1);
      component.submit();

      expect(userRepo.revoke_role).toHaveBeenCalled();
    });

    it('changing a role to another persists newly added role and revokes updated role', () => {
      component.formRoles.at(component.formRoles.length - 1).setValue(
        { roleId: roles[2].id }
      );
      component.submit();

      expect(userRepo.add_role).toHaveBeenCalledTimes(1);
      expect(userRepo.revoke_role).toHaveBeenCalledTimes(1);
    });
  });

  describe('#close', () => {
    it('calls DrawerService.close', () => {
      spyOn(drawer, 'close');
      component.close();

      expect(drawer.close).toHaveBeenCalled();
    });
  });
});
