import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserRoleSlideoutComponent } from './user-role-slideout.component';
import { FeatherModule } from 'angular-feather';
import { X, XCircle } from 'angular-feather/icons';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatListModule, MatSelectModule, MatInputModule } from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [UserRoleSlideoutComponent],
  imports: [
    CommonModule,
    FeatherModule.pick({ X, XCircle }),
    MatFormFieldModule,
    MatListModule,
    MatSelectModule,
    MatInputModule,
    FormsModule,
    ReactiveFormsModule
  ],
  entryComponents: [UserRoleSlideoutComponent]
})
export class UserRoleSlideoutModule { }
