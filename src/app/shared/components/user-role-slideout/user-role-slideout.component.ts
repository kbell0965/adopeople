import { Component, Inject, OnInit } from '@angular/core';
import { DrawerService, DRAWER_DATA } from 'src/app/services/drawer/drawer.service';
import { PeopleModels, PeopleRoleRepository, UserRepository } from '@pplsi-core/datalayer';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { Observable, forkJoin } from 'rxjs';
import { tap, finalize } from 'rxjs/operators';

class RoleAssignment {
  constructor(public role_id: string) { }
  public readonly roleable_type: string = 'FulfillmentPartnerMember';
}

@Component({
  selector: 'app-user-role-slideout',
  templateUrl: './user-role-slideout.component.html',
  styleUrls: ['./user-role-slideout.component.scss']
})
export class UserRoleSlideoutComponent implements OnInit {
  title: string;
  userRolesForm: FormGroup;
  filteredRoles: PeopleModels.Role[];
  rolesPage: PeopleModels.RolesWithPagination;
  rolesPage$: Observable<PeopleModels.RolesWithPagination>;
  readonly ROLE_DEFAULT = '';
  readonly USER_REPO_BASE_URL = '/a';

  constructor(private drawerService: DrawerService,
    private fb: FormBuilder,
    private roleRepository: PeopleRoleRepository,
    private userRepository: UserRepository,
    @Inject(DRAWER_DATA) private fpm: PeopleModels.FulfillmentPartnerMember
  ) {
    const selectedRoles = Object.assign([], fpm.fpm_roles.map(role => role.id));
    this.title = `Edit ${fpm.first_name} ${fpm.last_name}`;

    this.userRolesForm = fb.group({
      firstName: [{ value: fpm.first_name, disabled: true }, Validators.required],
      lastName: [{ value: fpm.last_name, disabled: true }, Validators.required],
      email: [{ value: fpm.email, disabled: true }, Validators.required],
      roles: fb.array(selectedRoles.map(roleId => this.mapRoleToFormGroup(roleId)))
    });
  }

  ngOnInit(): void {
    this.rolesPage$ = this.roleRepository.findAll(1, 50).pipe(tap(page => this.rolesPage = page));
  }

  addAdditionalRole(): void {
    if (this.formRoles.controls[this.formRoles.length - 1].value.roleId) {
      this.formRoles.push(this.mapRoleToFormGroup());
    }
  }

  removeRole(index: number): void {
    this.userRolesForm.markAsDirty();
    this.formRoles.removeAt(index);
  }

  filterRoles(groupIndex: number): PeopleModels.Role[] {
    const selectedRoles = this.formRoles.value.map(group => group.roleId);
    return this.rolesPage
      ? this.rolesPage.roles.filter(role =>
        selectedRoles.indexOf(role.id) === -1 || role.id === this.formRoles.at(groupIndex).value.roleId)
      : [];
  }

  get formRoles(): FormArray {
    return (this.userRolesForm.get('roles') || []) as FormArray;
  }

  private mapRoleToFormGroup(roleId: string = this.ROLE_DEFAULT): FormGroup {
    return this.fb.group({ roleId: [roleId, Validators.required] });
  }

  submit(): void {
    const originalRoleIds = this.fpm.fpm_roles.map(role => role.id);
    const formRoleIds = this.formRoles.value.map(group => group.roleId);

    const createRoleRequests = formRoleIds.filter(roleId => originalRoleIds.indexOf(roleId) === -1)
      .map(roleId => this.userRepository
        .add_role(this.USER_REPO_BASE_URL, this.fpm.id, new RoleAssignment(roleId)));

    const revokeRoleRequests = originalRoleIds.filter(roleId => formRoleIds.indexOf(roleId) === -1)
      .map(roleId => this.userRepository
        .revoke_role(this.USER_REPO_BASE_URL, this.fpm.id, new RoleAssignment(roleId)));

    const requests = [...createRoleRequests, ...revokeRoleRequests];

    if (createRoleRequests.length || revokeRoleRequests.length) {
      const subscription = forkJoin(requests).pipe(finalize(() => {
        subscription.unsubscribe();
        this.drawerService.close(true);
      })).subscribe();
    } else {
      this.drawerService.close(true);
    }
  }

  close(): void {
    this.drawerService.close(false);
  }
}
