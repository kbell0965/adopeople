import { Component, Input, OnInit, Output, EventEmitter, TemplateRef } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'ls-search-layout',
  templateUrl: './search-layout.component.html',
  styleUrls: ['./search-layout.component.scss']
})
export class SearchLayoutComponent implements OnInit {
  @Input() placeholder: string;
  @Input() submitResultTemplate: TemplateRef<any>;
  @Input() submitResultPagerTemplate: TemplateRef<any>;
  @Output() searchSubmit = new EventEmitter();

  searchForm: FormGroup;

  constructor(private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.searchForm = this.formBuilder.group({ query: '' });
  }

  formSubmit() {
    this.searchSubmit.emit();
  }
}
