import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { SearchLayoutComponent } from './search-layout.component';
import { ReactiveFormsModule } from '@angular/forms';
import { FeatherModule } from 'angular-feather';
import { Search } from 'angular-feather/icons';

describe('SearchLayoutComponent.Unit', () => {
  let component: SearchLayoutComponent;
  let fixture: ComponentFixture<SearchLayoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SearchLayoutComponent],
      imports: [
        FeatherModule.pick({ Search }),
        ReactiveFormsModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  describe('ngOnInit', () => {
    beforeEach(() => {
      component.ngOnInit();
    });

    it('sets the searchForm', () => {
      expect(component.searchForm.get('query').value).toBe('');
    });
  });

  describe('#formSubmit', () => {
    it('calls the searchSubmit output emitter', () => {
      spyOn(component.searchSubmit, 'emit');
      component.formSubmit();
      expect(component.searchSubmit.emit).toHaveBeenCalled();
    });
  });
});
