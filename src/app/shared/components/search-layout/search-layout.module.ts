import { NgModule } from '@angular/core';
import { SearchLayoutComponent } from './search-layout.component';
import { FeatherModule } from 'angular-feather';
import { Search } from 'angular-feather/icons';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [SearchLayoutComponent],
  exports: [SearchLayoutComponent],
  imports: [
    CommonModule,
    FeatherModule.pick({ Search }),
    ReactiveFormsModule
  ]
})
export class SearchLayoutModule { }
