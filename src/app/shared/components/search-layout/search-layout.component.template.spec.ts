import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { SearchLayoutComponent } from './search-layout.component';
import { ReactiveFormsModule } from '@angular/forms';
import { FeatherModule } from 'angular-feather';
import { Search } from 'angular-feather/icons';

describe('SearchLayoutComponent.Template', () => {
  let component: SearchLayoutComponent;
  let fixture: ComponentFixture<SearchLayoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SearchLayoutComponent],
      imports: [
        FeatherModule.pick({ Search }),
        ReactiveFormsModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  describe('Search Input', () => {
    it('has a search input', () => {
      expect(fixture.debugElement.query(By.css('.search-input')).name).toEqual('input');
    });

    it('button has inner text contents', () => {
      expect(fixture.debugElement.query(By.css('.search-button i-feather'))).toBeDefined();
    });
  });

  describe('Search Submit', () => {
    it('has placeholder text explaining which terms to query with', () => {
      component.placeholder = 'Placeholder Text';
      fixture.detectChanges();
      const el = fixture.debugElement.query(By.css('.search-input'));
      expect(el.nativeElement.getAttribute('placeholder')).toEqual('Placeholder Text');
    });

    it('search button calls the form submit method when clicked', () => {
      spyOn(component, 'formSubmit');
      const el = fixture.debugElement.query(By.css('.search-button'));
      el.nativeElement.click();
      expect(component.formSubmit).toHaveBeenCalled();
    });
  });
});
