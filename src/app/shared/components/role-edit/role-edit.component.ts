import { Component, OnInit, OnDestroy } from '@angular/core';
import { DrawerService } from 'src/app/services/drawer/drawer.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PeopleModels, PeopleRoleRepository, PermissionNamespaceType } from '@pplsi-core/datalayer';
import { Router } from '@angular/router';
import { DataService } from 'src/app/services/data/data.service';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-role-edit',
  templateUrl: './role-edit.component.html',
  styleUrls: ['./role-edit.component.scss']
})
export class RoleEditComponent implements OnInit, OnDestroy {
  readonly DESC_MAXLENGTH = 80;
  roleForm: FormGroup;
  submitFoundMatch = false;
  private _destroyed = new Subject<void>();

  constructor(
    private drawerService: DrawerService,
    private formBuilder: FormBuilder,
    private peopleRoleRepository: PeopleRoleRepository,
    private dataService: DataService,
    private router: Router) {
  }

  ngOnInit() {
    this.roleForm = this.formBuilder.group({
      name: [null, Validators.required],
      description: [null, [Validators.required, Validators.maxLength(this.DESC_MAXLENGTH)]],
      namespace: [PermissionNamespaceType.Admin]
    });
  }

  ngOnDestroy() {
    this._destroyed.next();
    this._destroyed.complete();
  }

  cancel(): void {
    this.drawerService.close();
  }

  submit(): void {
    this.peopleRoleRepository.findAll(1, 1, this.roleForm.controls.name.value)
      .pipe(takeUntil(this._destroyed))
      .subscribe((rolesWithPagination: PeopleModels.RolesWithPagination) => {
        if (rolesWithPagination.roles.length === 0) {
          this.dataService.setRole(this.roleForm.value as PeopleModels.Role);
          this.drawerService.close();
          this.router.navigate(['/roles/new']);
        } else {
          this.submitFoundMatch = true;
        }
      });
  }
}
