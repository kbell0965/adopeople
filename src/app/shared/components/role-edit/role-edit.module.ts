import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FeatherModule } from 'angular-feather';
import { X } from 'angular-feather/icons';
import { RoleEditComponent } from './role-edit.component';
import { ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule, MatInputModule } from '@angular/material';

@NgModule({
  declarations: [RoleEditComponent],
  imports: [
    FeatherModule.pick({ X }),
    CommonModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule
  ],
  entryComponents: [RoleEditComponent]
})
export class RoleEditModule { }
