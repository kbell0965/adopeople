import { async, ComponentFixture, TestBed, getTestBed } from '@angular/core/testing';
import { Router } from '@angular/router';

import { RoleEditComponent } from './role-edit.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DrawerService } from 'src/app/services/drawer/drawer.service';
import { PeopleRoleRepository, PeopleModels } from '@pplsi-core/datalayer';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { DataService } from 'src/app/services/data/data.service';
import { of } from 'rxjs';
import { MatSidenavModule } from '@angular/material';
import { RouterTestingModule } from '@angular/router/testing';
import { By } from '@angular/platform-browser';

describe('RoleEditComponent.Unit', () => {
  let component: RoleEditComponent;
  let fixture: ComponentFixture<RoleEditComponent>;
  let router: Router;
  let injector: TestBed;
  let roleRepository: PeopleRoleRepository;
  let dataService: DataService;
  let drawerService: DrawerService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RoleEditComponent],
      imports: [
        FormsModule,
        ReactiveFormsModule,
        HttpClientTestingModule,
        MatSidenavModule,
        RouterTestingModule
      ],
      providers: [
        {
          provide: Router,
          useValue: { navigate: jasmine.createSpy('navigate') }
        },
        DrawerService,
        DataService,
        PeopleRoleRepository
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RoleEditComponent);

    injector = getTestBed();
    router = injector.get(Router);

    roleRepository = injector.get(PeopleRoleRepository);
    spyOn(roleRepository, 'findAll').and.returnValue(of({ roles: [], total_length: 0 }));

    dataService = injector.get(DataService);
    spyOn(dataService, 'setRole');

    drawerService = injector.get(DrawerService);
    spyOn(drawerService, 'close');

    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  describe('#ngOnInit', () => {
    it('assigns roleForm', () => {
      component.ngOnInit();
      fixture.detectChanges();
      expect(component.roleForm).toBeTruthy();
    });
  });

  describe('#cancel', () => {
    it('drawerService should close when cancel is called', () => {
      component.cancel();
      expect(drawerService.close).toHaveBeenCalled();
    });
  });

  describe('#submit', () => {
    it('findAll should be called', () => {
      component.submit();
      expect(roleRepository.findAll).toHaveBeenCalledWith(1, 1, component.roleForm.controls.name.value);
    });

    it('submitFoundMatch true when name field already in use', () => {
      roleRepository.findAll = jasmine.createSpy().and.returnValue(of({ roles: [{} as PeopleModels.Role], total_length: 1 }));
      component.submit();
      expect(component.submitFoundMatch).toBe(true);
    });

    it('submitFoundMatch false when name field not already in use', () => {
      component.submit();
      expect(component.submitFoundMatch).toBe(false);
    });

    it('dataService should setRole with roleForm value when field name passes', () => {
      component.submit();
      expect(dataService.setRole).toHaveBeenCalledWith(component.roleForm.value);
    });

    it('drawerService should close when field name passes', () => {
      component.submit();
      expect(drawerService.close).toHaveBeenCalled();
    });

    it('navigate to finish making a new role when field name passes', () => {
      component.submit();
      expect(router.navigate).toHaveBeenCalledWith(['/roles/new']);
    });
  });
});
