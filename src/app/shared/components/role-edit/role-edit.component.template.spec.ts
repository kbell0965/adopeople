import { async, ComponentFixture, TestBed, getTestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RoleEditComponent } from './role-edit.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ReactiveFormsModule } from '@angular/forms';
import {
  MatInputModule
} from '@angular/material';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { Router } from '@angular/router';
import { PeopleRoleRepository } from '@pplsi-core/datalayer';
import { DrawerService } from 'src/app/services/drawer/drawer.service';
import { DataService } from 'src/app/services/data/data.service';
import { By } from '@angular/platform-browser';

describe('RoleEditComponent.Template', () => {
  let component: RoleEditComponent;
  let fixture: ComponentFixture<RoleEditComponent>;
  let injector: TestBed;
  let router: Router;
  let drawerService: DrawerService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        ReactiveFormsModule,
        RouterTestingModule,
        MatInputModule,
        BrowserAnimationsModule
      ],
      declarations: [
        RoleEditComponent
      ],
      providers: [
        {
          provide: Router,
          useValue: { navigate: jasmine.createSpy('navigate') }
        },
        DrawerService,
        DataService,
        PeopleRoleRepository
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RoleEditComponent);
    injector = getTestBed();
    router = injector.get(Router);

    drawerService = injector.get(DrawerService);
    spyOn(drawerService, 'close');

    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  describe('Created', () => {
    it('Header has text', () => {
      const header = fixture.debugElement.query(By.css('h3')).nativeElement;
      expect(header.innerText).toBe('Add Role');
    });

    it('Name input exists', () => {
      const name = fixture.debugElement.query(By.css('input[formControlName=name]')).nativeElement;
      expect(name).toBeTruthy();
    });

    it('Description input exists', () => {
      const description = fixture.debugElement.query(By.css('textarea[formControlName=description]')).nativeElement;
      expect(description).toBeTruthy();
    });

    it('Cancel button has text', () => {
      const cancel = fixture.debugElement.query(By.css('.cancel-button')).nativeElement;
      expect(cancel.innerText).toBe('CANCEL');
    });

    it('Submit button has text', () => {
      const submit = fixture.debugElement.query(By.css('.submit-button')).nativeElement;
      expect(submit.innerText).toBe('CREATE ROLE');
    });
  });

  describe('Text Length Indicator', () => {
    let descriptionTextarea: any;

    const sendDescriptionInput = (text: string) => {
      descriptionTextarea.value = text;
      descriptionTextarea.dispatchEvent(new Event('input'));
      fixture.detectChanges();
      return fixture.whenStable();
    };

    beforeEach(() => descriptionTextarea = fixture.debugElement.query(By.css('textarea')).nativeElement);

    it('displays appropriate count when text length zero', () => {
      sendDescriptionInput('');
      expect(fixture.debugElement.query(By.css('.length-indicator-text')).nativeElement.innerText).toBe(`0/${component.DESC_MAXLENGTH}`);
    });

    it('displays appropriate count when text length greater than zero', () => {
      sendDescriptionInput('beep boop');
      expect(fixture.debugElement.query(By.css('.length-indicator-text')).nativeElement.innerText)
        .toBe(`${'beep boop'.length}/${component.DESC_MAXLENGTH}`);
    });
  });

  describe('Cancel Button', () => {
    it('close drawerService when clicked', () => {
      const cancelButton = fixture.debugElement.query(By.css('.btn-secondary')).nativeElement;
      cancelButton.click();
      expect(drawerService.close).toHaveBeenCalled();
    });
  });

  describe('X Button', () => {
    it('close drawerService when clicked', () => {
      const cancelButton = fixture.debugElement.query(By.css('i-feather')).nativeElement;
      cancelButton.click();
      expect(drawerService.close).toHaveBeenCalled();
    });
  });

  describe('Submit Button', () => {
    it('should not have error message when submitFoundMatch is false', () => {
      component.submitFoundMatch = false;
      fixture.detectChanges();
      const errorMessage = fixture.debugElement.query(By.css('.invalid-role-name'));
      expect(errorMessage).toBeNull();
    });

    it('should have error message when submitFoundMatch is true', () => {
      component.submitFoundMatch = true;
      fixture.detectChanges();
      const errorMessage = fixture.debugElement.query(By.css('.invalid-role-name'));
      expect(errorMessage).not.toBeNull();
    });

    describe('When role form is invalid', () => {
      it('button should be disabled', () => {
        const submitButton = fixture.debugElement.query(By.css('.btn-action'));
        submitButton.nativeElement.click();
        expect(submitButton.nativeElement.disabled).toBeTruthy();
      });
    });

    describe('When role form is valid', () => {
      beforeEach(() => {
        component.roleForm.controls['name'].setValue('Test Name');
        component.roleForm.controls['description'].setValue('Description');
        fixture.detectChanges();
      });

      it('Submit button is enabled', () => {
        const submitButton = fixture.debugElement.query(By.css('.btn-action'));
        expect(submitButton.nativeElement.disabled).not.toBeTruthy();
      });

      it('#submit is called when button is clicked', () => {
        spyOn(component, 'submit');
        fixture.debugElement.query(By.css('.btn-action')).nativeElement.click();
        expect(component.submit).toHaveBeenCalled();
      });
    });
  });
});
