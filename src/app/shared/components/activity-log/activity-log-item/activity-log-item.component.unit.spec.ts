import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivityLogItemComponent } from './activity-log-item.component';
import { FeatherModule } from 'angular-feather';
import { FilePlus, Flag, PlusCircle, AlertTriangle, ArrowRightCircle } from 'angular-feather/icons';
import { DatePipe } from '@angular/common';
import { PartnerModels } from '@pplsi-core/datalayer';

describe('ActivityLogItemComponent.Unit', () => {
  let component: ActivityLogItemComponent;
  let fixture: ComponentFixture<ActivityLogItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ActivityLogItemComponent],
      imports: [FeatherModule.pick({ FilePlus, Flag, PlusCircle, AlertTriangle, ArrowRightCircle })],
      providers: [DatePipe]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActivityLogItemComponent);
    component = fixture.componentInstance;
    component.activity = new PartnerModels.Activity({
      activity_type: 'create',
      fulfillment_partner_member: 'John Doe',
      created_at: new Date('10-1-1929 12:00 AM'),
      comment: 'YouTube commercials are getting too long'
    });
    fixture.detectChanges();
  });

  describe('#ngOnInit', () => {
    it('sets the member name', () => {
      expect(component.member).toBe('John Doe');
    });

    it('sets the created date', () => {
      expect(component.created).toBe('October 1, 1929 12:00 AM');
    });

    it('sets the comment', () => {
      expect(component.comment).toBe('YouTube commercials are getting too long');
    });

    it('sets the isCreateItem boolean', () => {
      expect(component.isCreateItem).toBe(true);
    });

    it('isCreateItem is false if activity type is anything but "create"', () => {
      component.activity.activity_type = 'anything else';
      component.ngOnInit();
      expect(component.isCreateItem).toBe(false);
    });

    it('sets the alert boolean', () => {
      expect(component.alert).toBe(false);
    });

    describe('when alert is true', () => {
      beforeEach(() => {
        component.activity.alert = true;
        component.ngOnInit();
        fixture.detectChanges();
      });

      it('sets the alert boolean', () => {
        expect(component.alert).toBe(true);
      });
    });

    it('sets the homeOffice boolean', () => {
      expect(component.homeOffice).toBe(false);
    });

    describe('when home office is true', () => {
      beforeEach(() => {
        component.activity.home_office = true;
        component.ngOnInit();
        fixture.detectChanges();
      });

      it('sets the homeOffice boolean', () => {
        expect(component.homeOffice).toBe(true);
      });
    });
  });

  describe('#getFormattedCommentDate', () => {
    it('returns the expected long date and short time format', () => {
      const testDate = component.getFormattedCommentDate(new Date('12-1-1969 10:00 AM'));
      expect(testDate).toBe('December 1, 1969 10:00 AM');
    });
  });

  describe('#getIconName', () => {
    it('returns "plus-circle" for the create activity type', () => {
      component.activity.activity_type = 'create';
      expect(component.getIconName()).toBe('plus-circle');
    });

    it('returns "arrow-right-circle" for the update activity type', () => {
      component.activity.activity_type = 'update';
      expect(component.getIconName()).toBe('arrow-right-circle');
    });

    it('returns "file-plus" as the default icon', () => {
      component.activity.activity_type = 'anything else';
      expect(component.getIconName()).toBe('file-plus');
    });

    it('returns "alert-triangle" if the activity is an alert', () => {
      component.alert = true;
      expect(component.getIconName()).toBe('alert-triangle');
    });
  });
});
