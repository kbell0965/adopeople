import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { DatePipe } from '@angular/common';
import { PartnerModels } from '@pplsi-core/datalayer';

@Component({
  selector: 'ls-activity-log-item',
  templateUrl: './activity-log-item.component.html',
  styleUrls: ['./activity-log-item.component.scss']
})
export class ActivityLogItemComponent implements OnInit {
  @Input() activity: PartnerModels.Activity;
  @Input() resolutionDisplayId: string;
  @Output() activityAlertToggle = new EventEmitter();

  activityType: string;
  comment: string;
  created: string;
  isCreateItem: boolean;
  member: string;
  homeOffice: boolean;
  alert: boolean;

  constructor(private datePipe: DatePipe) { }

  ngOnInit() {
    const { activity_type, alert, home_office, comment, created_at, fulfillment_partner_member } = this.activity;

    this.activityType = activity_type ? activity_type : 'comment';
    this.activityType = home_office && !alert ? this.activityType + ' home-office' : this.activityType;
    this.activityType = alert ? this.activityType + ' alert' : this.activityType;
    this.comment = comment ? comment : '';
    this.created = created_at ? this.getFormattedCommentDate(created_at) : null;
    this.isCreateItem = (activity_type === 'create') ? true : false;
    this.member = fulfillment_partner_member ? fulfillment_partner_member : '';
    this.homeOffice = home_office ? home_office : false;
    this.alert = alert ? alert : false;
  }

  getFormattedCommentDate(commentDate: Date): string {
    return this.datePipe.transform(commentDate, 'longDate') + ' ' + this.datePipe.transform(commentDate, 'shortTime');
  }

  getIconName(): string {
    let iconName: string;
    switch (this.activity.activity_type) {
      case 'create':
        iconName = 'plus-circle';
        break;
      case 'update':
        iconName = 'arrow-right-circle';
        break;
      default:
        iconName = 'file-plus';
    }
    return this.alert ? 'alert-triangle' : iconName;
  }
}
