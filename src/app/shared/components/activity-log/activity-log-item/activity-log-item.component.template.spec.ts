import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivityLogItemComponent } from './activity-log-item.component';
import { FeatherModule } from 'angular-feather';
import { FilePlus, Flag, PlusCircle, AlertTriangle, ArrowRightCircle } from 'angular-feather/icons';
import { DatePipe } from '@angular/common';
import { PartnerModels } from '@pplsi-core/datalayer';
import { By } from '@angular/platform-browser';

describe('ActivityLogItemComponent.Template', () => {
  let component: ActivityLogItemComponent;
  let fixture: ComponentFixture<ActivityLogItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ActivityLogItemComponent],
      imports: [FeatherModule.pick({ FilePlus, Flag, PlusCircle, AlertTriangle, ArrowRightCircle })],
      providers: [DatePipe]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActivityLogItemComponent);
    component = fixture.componentInstance;
    component.activity = new PartnerModels.Activity({
      fulfillment_partner_member: 'John Doe',
      created_at: new Date('12-1-1969 10:00 AM'),
      comment: 'YouTube commercials are getting too long'
    });
    fixture.detectChanges();
  });

  describe('root container', () => {
    it('includes the comment class as the default', () => {
      const el = fixture.debugElement.query(By.css('.activity-log-item.comment'));
      expect(el).toBeTruthy();
    });

    it('includes the update class as for update activity type', () => {
      component.activity.activity_type = 'update';
      component.ngOnInit();
      fixture.detectChanges();
      const el = fixture.debugElement.query(By.css('.activity-log-item.update'));
      expect(el).toBeTruthy();
    });

    it('includes the create class as for create activity type', () => {
      component.activity.activity_type = 'create';
      component.ngOnInit();
      fixture.detectChanges();
      const el = fixture.debugElement.query(By.css('.activity-log-item.create'));
      expect(el).toBeTruthy();
    });

    describe('alert type', () => {
      beforeEach(() => {
        component.activity.alert = true;
        component.ngOnInit();
        fixture.detectChanges();
      });

      it('includes the alert class', () => {
        const el = fixture.debugElement.query(By.css('.activity-log-item.alert'));
        expect(el).toBeTruthy();
      });

      describe('flag icon', () => {
        it('is present', () => {
          const el = fixture.debugElement.query(By.css('.activity-log-item.alert i-feather[name="flag"]'));
          expect(el).toBeTruthy();
        });

        it('on click calls the activty alert toggle emitter', () => {
          spyOn(component.activityAlertToggle, 'emit');
          const el = fixture.debugElement.query(By.css('.activity-log-item.alert i-feather[name="flag"]'));
          el.nativeElement.click();
          expect(component.activityAlertToggle.emit).toHaveBeenCalled();
        });
      });
    });

    describe('LegalShield activity', () => {
      beforeEach(() => {
        component.activity.home_office = true;
        component.ngOnInit();
        fixture.detectChanges();
      });

      it('includes the home-office class', () => {
        const el = fixture.debugElement.query(By.css('.activity-log-item.home-office'));
        expect(el).toBeTruthy();
      });

      describe('LegalShield icon', () => {
        it('is present', () => {
          const el = fixture.debugElement.query(By.css('.activity-log-item.home-office img[src="assets/NA_LegalShieldLogo.svg"]'));
          expect(el).toBeTruthy();
        });
      });
    });

    describe('LegalShield activity with alert class', () => {
      beforeEach(() => {
        component.activity.home_office = true;
        component.activity.alert = true;
        component.ngOnInit();
        fixture.detectChanges();
      });

      it('does not include the home-office class', () => {
        const el = fixture.debugElement.query(By.css('.activity-log-item.home-office'));
        expect(el).toBeFalsy();
      });

      it('includes the alert class', () => {
        const el = fixture.debugElement.query(By.css('.activity-log-item.alert'));
        expect(el).toBeTruthy();
      });

      describe('flag icon', () => {
        it('is present', () => {
          const el = fixture.debugElement.query(By.css('.activity-log-item.alert i-feather[name="flag"]'));
          expect(el).toBeTruthy();
        });

        it('on click calls the activty alert toggle emitter', () => {
          spyOn(component.activityAlertToggle, 'emit');
          const el = fixture.debugElement.query(By.css('.activity-log-item.alert i-feather[name="flag"]'));
          el.nativeElement.click();
          expect(component.activityAlertToggle.emit).toHaveBeenCalled();
        });
      });
    });
  });

  describe('icon', () => {
    it('is the default icon', () => {
      const el = fixture.debugElement.query(By.css('.log-item-icon i-feather'));
      expect(el.attributes['ng-reflect-name']).toBe('file-plus');
    });

    it('shows a plus-circle icon for the "create" log item type', () => {
      component.activity.activity_type = 'create';
      fixture.detectChanges();
      const el = fixture.debugElement.query(By.css('.log-item-icon i-feather'));
      expect(el.attributes['ng-reflect-name']).toBe('plus-circle');
    });

    it('shows an arrow-right-circle icon for the "update" log item type', () => {
      component.activity.activity_type = 'update';
      fixture.detectChanges();
      const el = fixture.debugElement.query(By.css('.log-item-icon i-feather'));
      expect(el.attributes['ng-reflect-name']).toBe('arrow-right-circle');
    });
  });

  describe('note contents', () => {
    describe('title', () => {
      it('displays the member name', () => {
        const el = fixture.debugElement.query(By.css('.log-item-title'));
        expect(el.nativeElement.innerHTML).toContain('John Doe');
      });

      it('includes "Created Resolution" and the resolution display ID for create log item type', () => {
        component.activity.activity_type = 'create';
        component.resolutionDisplayId = 'ABC-123';
        component.ngOnInit();
        fixture.detectChanges();
        const el = fixture.debugElement.query(By.css('.log-item-title'));
        expect(el.nativeElement.innerHTML).toContain('Created');
        expect(el.nativeElement.innerHTML).toContain('Resolution');
        expect(el.nativeElement.innerHTML).toContain('ABC-123');
      });
    });

    it('displays the formatted note date', () => {
      const el = fixture.debugElement.query(By.css('.log-item-date'));
      expect(el.nativeElement.innerHTML).toContain('December 1, 1969 10:00 AM');
    });

    it('displays the formatted note date', () => {
      const el = fixture.debugElement.query(By.css('.log-item-comment'));
      expect(el.nativeElement.innerHTML).toContain('YouTube commercials are getting too long');
    });
  });
});
