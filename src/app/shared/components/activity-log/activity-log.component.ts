import { Component, EventEmitter, Input, OnInit, Output, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { Observable, Subscription } from 'rxjs';
import { PartnerModels } from '@pplsi-core/datalayer';
@Component({
  selector: 'ls-activity-log',
  templateUrl: './activity-log.component.html',
  styleUrls: ['./activity-log.component.scss']
})
export class ActivityLogComponent implements OnInit, OnDestroy {
  @Input() activityLog$: Observable<PartnerModels.Activity[]>;
  @Input() placeholder: string;
  @Input() note$: Observable<string>;
  @Input() resolutionDisplayId: string;
  @Output() activityLogSubmit = new EventEmitter();
  @Output() activityAlertToggle = new EventEmitter();

  activityLogForm: FormGroup;
  noteSubcription: Subscription;

  constructor(private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.activityLogForm = this.formBuilder.group({ note: '', alert: new FormControl(false) });
    this.noteSubcription = this.note$.subscribe(note => this.activityLogForm.setValue({ note, alert: false }));
  }

  ngOnDestroy() {
    this.noteSubcription.unsubscribe();
  }

  formSubmit() {
    this.activityLogSubmit.emit({
      comment: this.activityLogForm.get('note').value,
      alert: this.activityLogForm.get('alert').value
    });
  }
}
