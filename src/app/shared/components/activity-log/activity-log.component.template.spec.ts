import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivityLogComponent } from './activity-log.component';
import { ActivityLogItemComponent } from './activity-log-item/activity-log-item.component';
import { By } from '@angular/platform-browser';
import { ReactiveFormsModule } from '@angular/forms';
import { FeatherModule } from 'angular-feather';
import { AlertTriangle, ArrowRightCircle, FilePlus, Flag, PlusCircle } from 'angular-feather/icons';
import { PartnerModels } from '@pplsi-core/datalayer';
import { of } from 'rxjs';
import { DatePipe, CommonModule } from '@angular/common';
import { MatButtonModule, MatCheckboxModule, MatInputModule } from '@angular/material';

const getActivity = (cfg) => new PartnerModels.Activity(cfg);
const fakeActivities = Array.from(Array(10), (x, i) => i + 1).map(i => getActivity({}));

describe('ActivityLogComponent.Template', () => {
  let component: ActivityLogComponent;
  let fixture: ComponentFixture<ActivityLogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ActivityLogComponent, ActivityLogItemComponent],
      imports: [
        CommonModule,
        FeatherModule.pick({ AlertTriangle, ArrowRightCircle, FilePlus, Flag, PlusCircle }),
        MatButtonModule,
        MatCheckboxModule,
        MatInputModule,
        ReactiveFormsModule
      ],
      providers: [DatePipe]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActivityLogComponent);
    component = fixture.componentInstance;
    component.note$ = of('');
    fixture.detectChanges();
  });

  describe('inputs', () => {
    it('has a cancel (reset) button', () => {
      expect(fixture.debugElement.query(By.css('.log-controls button[type="reset"]'))).not.toBeNull();
    });

    it('has a submit button', () => {
      expect(fixture.debugElement.query(By.css('.log-controls button[type="submit"]'))).not.toBeNull();
    });

    it('has an alert checkbox', () => {
      expect(fixture.debugElement.query(By.css('mat-checkbox input'))).toBeTruthy();
    });

    it('has an input field', () => {
      const el = fixture.debugElement.query(By.css('mat-checkbox .mat-checkbox-label'));
      expect(el.nativeElement.innerHTML).toContain('Flag as alert');
    });

    it('has expected placeholder text', () => {
      component.placeholder = 'test placeholder';
      fixture.detectChanges();
      const el = fixture.debugElement.query(By.css('.log-input textarea'));
      expect(el.nativeElement.getAttribute('placeholder')).toContain('test placeholder');
    });
  });

  describe('messages', () => {
    it('displays all of the expected activity notes', done => {
      component.activityLog$ = of(fakeActivities);
      fixture.detectChanges();
      setTimeout(() => {
        const el = fixture.debugElement.queryAll(By.css('ls-activity-log-item'));
        expect(el.length).toBe(10);
        done();
      });
    });
  });
});
