import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { ActivityLogComponent } from './activity-log.component';
import { ReactiveFormsModule } from '@angular/forms';
import { ActivityLogItemComponent } from './activity-log-item/activity-log-item.component';
import { FeatherModule } from 'angular-feather';
import { AlertTriangle, ArrowRightCircle, FilePlus, Flag, PlusCircle } from 'angular-feather/icons';
import { MatButtonModule, MatCheckboxModule, MatInputModule } from '@angular/material';

@NgModule({
  declarations: [ActivityLogComponent, ActivityLogItemComponent],
  exports: [
    ActivityLogComponent,
    ActivityLogItemComponent,
    FeatherModule,
    MatButtonModule,
    MatCheckboxModule,
    MatInputModule
  ],
  imports: [
    CommonModule,
    FeatherModule.pick({ AlertTriangle, ArrowRightCircle, FilePlus, Flag, PlusCircle }),
    MatButtonModule,
    MatCheckboxModule,
    MatInputModule,
    ReactiveFormsModule
  ],
  providers: [DatePipe]
})
export class ActivityLogModule { }
