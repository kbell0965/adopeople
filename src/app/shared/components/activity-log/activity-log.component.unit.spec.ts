import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivityLogComponent } from './activity-log.component';
import { ReactiveFormsModule } from '@angular/forms';
import { ActivityLogItemComponent } from './activity-log-item/activity-log-item.component';
import { FeatherModule } from 'angular-feather';
import { AlertTriangle, ArrowRightCircle, FilePlus, Flag, PlusCircle } from 'angular-feather/icons';
import { of } from 'rxjs';
import { MatInputModule, MatCheckboxModule, MatButtonModule } from '@angular/material';
import { CommonModule } from '@angular/common';

describe('ActivityLogComponent.Unit', () => {
  let component: ActivityLogComponent;
  let fixture: ComponentFixture<ActivityLogComponent>;
  let noteSubscriptionSpy: jasmine.Spy;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ActivityLogComponent, ActivityLogItemComponent],
      imports: [
        CommonModule,
        FeatherModule.pick({ AlertTriangle, ArrowRightCircle, FilePlus, Flag, PlusCircle }),
        MatButtonModule,
        MatCheckboxModule,
        MatInputModule,
        ReactiveFormsModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActivityLogComponent);
    component = fixture.componentInstance;
    component.note$ = of('');
    noteSubscriptionSpy = spyOn(component.note$, 'subscribe').and.callThrough();
    fixture.detectChanges();
  });

  describe('ngOnInit', () => {
    beforeEach(() => {
      noteSubscriptionSpy.calls.reset();
      component.ngOnInit();
    });

    describe('sets activityLogForm', () => {
      it('note', () => expect(component.activityLogForm.get('note').value).toBe(''));
      it('alert', () => expect(component.activityLogForm.get('alert').value).toBe(false));
    });

    it('subscribes to the note observable', () => {
      expect(noteSubscriptionSpy).toHaveBeenCalledTimes(1);
    });
  });

  describe('#ngOnDestroy', () => {
    it('unsubscribes the create resolution subscription', () => {
      component.noteSubcription = of(true).subscribe();
      component.ngOnDestroy();
      expect(component['noteSubcription'].closed).toBeTruthy();
    });
  });

  describe('#formSubmit', () => {
    it('calls the searchSubmit output emitter', () => {
      spyOn(component.activityLogSubmit, 'emit');
      component.formSubmit();
      expect(component.activityLogSubmit.emit).toHaveBeenCalledWith({ comment: '', alert: false });
    });
  });
});
