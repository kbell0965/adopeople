import { Component } from '@angular/core';
import { WindowService } from '@pplsi-core/shared-modules/dist/src/services/window.service';

@Component({
  selector: 'ls-unauthorized',
  templateUrl: './unauthorized.component.html',
  styleUrls: ['./unauthorized.component.scss']
})
export class UnauthorizedComponent {
  constructor(private windowService: WindowService) { }

  login(): void {
    this.windowService.replace('/auth/legalshield?strategy=okta');
  }
}
