import { DebugElement } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { WindowService } from '@pplsi-core/shared-modules/dist/src/services/window.service';
import { UnauthorizedComponent } from './unauthorized.component';

describe('UnauthorizedComponent.Template', () => {
  let component: UnauthorizedComponent;
  let fixture: ComponentFixture<UnauthorizedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [UnauthorizedComponent],
      imports: [],
      providers: [
        WindowService
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnauthorizedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  describe('refresh button', () => {
    it('calls login when clicked', () => {
      spyOn(component, 'login');

      const debugElement: DebugElement = fixture.debugElement.query(By.css('button'));
      debugElement.nativeElement.click();

      expect(component.login).toHaveBeenCalled();
    });
  });
});
