import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { WindowService } from '@pplsi-core/shared-modules/dist/src/services/window.service';
import { UnauthorizedComponent } from './unauthorized.component';

describe('UnauthorizedComponent.Unit', () => {
  let component: UnauthorizedComponent;
  let fixture: ComponentFixture<UnauthorizedComponent>;
  let windowService: WindowService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [UnauthorizedComponent],
      imports: [],
      providers: [
        WindowService
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnauthorizedComponent);
    component = fixture.componentInstance;
    windowService = TestBed.get(WindowService);
    fixture.detectChanges();
  });

  describe('#login', () => {
    it('calls replace on the windowService', () => {
      spyOn(windowService, 'replace');

      component.login();

      expect(windowService.replace).toHaveBeenCalledWith('/auth/legalshield?strategy=okta');
    });
  });
});
