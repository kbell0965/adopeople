import { NgModule } from '@angular/core';
import { UnauthorizedComponent } from './unauthorized.component';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [UnauthorizedComponent],
  exports: [UnauthorizedComponent],
  imports: [
    CommonModule
  ]
})
export class UnauthorizedModule { }
