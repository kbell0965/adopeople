import { KeyValue } from '@angular/common';
import { Component, Inject, OnInit, OnDestroy, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AccountViewModel } from '../../../models/account-view-model';
import { FulfillmentPartnerMemberViewModel } from '../../../models/fulfillment-partner-member-view-model';
import { MembershipViewModel } from '../../../models/membership-view-model';
import { PartnerFulfillmentPartnerMemberViewModel } from '../../../models/partner-fulfillment-partner-member-view-model';
import { ResolutionViewModel } from '../../../modules/resolutions/models/resolution-view-model';
import { DRAWER_DATA, DrawerService } from '../../../services/drawer/drawer.service';
import {
  PartnerModels,
  PeopleFulfillmentPartnerRepository,
  PeopleServiceRequestRepository,
  PartnerResolutionRepository,
  PeopleModels,
  PeopleFulfillmentPartnerMembersRepository
} from '@pplsi-core/datalayer';
import { DataService } from '../../../services/data/data.service';
import { takeUntil, map } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  templateUrl: './resolution-edit.component.html',
  styleUrls: ['./resolution-edit.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ResolutionEditComponent implements OnInit, OnDestroy {
  private _destroyed = new Subject<void>();
  resolution: ResolutionViewModel;
  account: AccountViewModel;
  fulfillmentPartnerBranches: PeopleModels.FulfillmentPartnerBranch[];
  fulfillmentPartnerMembers: PartnerFulfillmentPartnerMemberViewModel[];
  fulfillmentPartners: PeopleModels.FulfillmentPartner[];
  targetMembers: FulfillmentPartnerMemberViewModel[];
  editForm: FormGroup;
  conflictServiceRequests: PeopleModels.ServiceRequest[] = [];
  serviceRequests: PeopleModels.ServiceRequest[] = [];
  showOtherContactMethodInput = true;
  stopInfiniteScroll = false;
  readonly CONCERNS = [
    'Untimely Callback',
    'Disagree with Legal Advice/Opinion',
    'Has not received will, letter etc',
    'Attorney/Staff Attitude',
    'Retainer or Hourly Rate',
    'Referral Attorney',
    'Coverage Advised',
    'Disposition of Case',
    'Attorney did not appear',
    'Miscellaneous/Unknown',
    'Has not been refereed to an Attorney',
    'Improper PPL Procedure',
    'Improper use of Software',
    'Survey Scored Incorrectly',
    'No Callback from Attorney',
    'Unable to Speak with Attorney',
    'Insufficient Advice',
    'Wrong Area/Distance too far',
    'Attorney has Conflict',
    'Attorney does not handle the Area of Law',
    'Attorney would not take the Case',
    'Member requests a different Attorney',
    'Attorney would not provide Discount'
  ];
  readonly STATUSES = ResolutionViewModel.STATUSES;
  readonly ITEMS_PER_PAGE = 10;
  originalOrder = (a: KeyValue<string, string>, b: KeyValue<string, string>): number => {
    return 0;
  }

  constructor(@Inject(DRAWER_DATA) private data: any,
              private drawerService: DrawerService,
              private formBuilder: FormBuilder,
              private peopleFulfillmentPartnerRepository: PeopleFulfillmentPartnerRepository,
              private peopleServiceRequestRepository: PeopleServiceRequestRepository,
              private partnerResolutionRepository: PartnerResolutionRepository,
              private dataService: DataService,
              private peopleFulfillmentPartnerMembersRepository: PeopleFulfillmentPartnerMembersRepository) { }

  ngOnInit(): void {
    this.resolution = this.data.resolution;
    this.account = this.data.account;
    this.fulfillmentPartnerMembers = this.data.fulfillmentPartnerMembers;
    this.fulfillmentPartners = this.data.fulfillmentPartners;
    this.fulfillmentPartnerBranches = this.data.fulfillmentPartnerBranches;
    this.targetMembers = this.data.targetMembers || [];

    this.editForm = this.formBuilder.group({
      membership: [ this.account.memberships[this.resolutionMembershipIndex()], Validators.required ],
      assignee: [ this.fulfillmentPartnerMembers[this.resolutionAssigneeIndex()], Validators.required ],
      status: [ this.resolution.status, Validators.required ],
      concern: [ this.resolution.concern, Validators.required ],
      resolutionType: [ this.resolution.resolution_type, Validators.required ],
      source: [ this.resolution.source, Validators.required ],
      target: [ (this.fulfillmentPartners) ? this.fulfillmentPartners[this.resolutionTargetIndex()] : null ],
      targetBranch: [ this.resolutionTargetBranchValue() ],
      targetMember: [ this.resolutionTargetMemberValue() ],
      serviceRequest: [ null ],
      contactMethod: [ 'other', Validators.required ],
      providerAction: [ this.resolution.action_taken ],
      contactMethodOther: [ this.resolution.preferred_contact_method.value ],
      referral: [ this.resolution.referral ],
      approved: [ this.resolution.approved_to_continue ]
    });

    this.loadServiceRequests(this.resolutionType);
    this.updateRequiredFields(this.resolutionType);
  }

  ngOnDestroy() {
    this._destroyed.next();
    this._destroyed.complete();
  }

  loadServiceRequests(resolutionType: string): void {
    if (resolutionType === 'conflict') {
      this.loadConflictServiceRequests();
    } else {
      this.loadTargetBranchServiceRequests();
    }
  }

  loadFulfillmentPartners(): void {
    const targetBranchId = this.editForm.controls['targetBranch'].value.id;
    if (targetBranchId) {
      this.peopleFulfillmentPartnerRepository.findAll(targetBranchId)
        .pipe(takeUntil(this._destroyed), map((results: PeopleModels.FulfillmentPartner[]) =>
          results.map(fulfillmentPartners => new PartnerModels.FulfillmentPartner(fulfillmentPartners))
        ))
        .subscribe((fulfillmentPartners: PartnerModels.FulfillmentPartner[]) => {
          this.fulfillmentPartners = fulfillmentPartners;
        });
    }
  }

  get membership(): MembershipViewModel {
    return this.editForm.controls['membership'].value;
  }

  get resolutionType(): string {
    return this.editForm.controls['resolutionType'].value;
  }

  get target(): PartnerModels.FulfillmentPartner {
    return this.editForm.controls['target'].value;
  }

  get targetBranch(): PeopleModels.FulfillmentPartnerBranch {
    return this.editForm.controls['targetBranch'].value;
  }

  get contactMethods(): PartnerModels.ContactMethod[] {
    return this.membership.user.contact_methods;
  }

  get email(): string {
    return this.membership.user.email;
  }

  setServiceRequestValue(): void {
    if (this.resolution.resolution_type === 'conflict') {
      this.editForm.controls['serviceRequest'].setValue(this.conflictServiceRequests[this.resolutionConflictServiceRequestIndex()]);
    } else {
      this.editForm.controls['serviceRequest'].setValue(this.serviceRequests[this.resolutionServiceRequestIndex()]);
    }
  }

  resolutionTargetBranchValue(): PeopleModels.FulfillmentPartnerBranch | null {
    const resolutionTargetBranch: PeopleModels.FulfillmentPartnerBranch = this
      .fulfillmentPartnerBranches[this.resolutionTargetBranchIndex()];
    return resolutionTargetBranch ? resolutionTargetBranch : null;
  }

  resolutionTargetMemberValue(): FulfillmentPartnerMemberViewModel | null {
    return this.targetMembers[this.resolutionTargetMemberIndex()];
  }

  close(): void {
    this.drawerService.close();
  }

  resolutionMembershipIndex(): number {
    return this.account.memberships.findIndex((membership: MembershipViewModel) => membership.id === this.resolution.membership_id);
  }

  resolutionAssigneeIndex(): number {
    return this.fulfillmentPartnerMembers.findIndex(
      (fulfillmentPartnerMember: PartnerFulfillmentPartnerMemberViewModel) => fulfillmentPartnerMember.id === this.resolution.assignee.id
    );
  }

  resolutionTargetIndex(): number {
    return this.fulfillmentPartners.findIndex(
      (fulfillmentPartner: PartnerModels.FulfillmentPartner) => fulfillmentPartner.id === this.resolution.target.id
    );
  }

  resolutionTargetBranchIndex(): number {
    return this.fulfillmentPartnerBranches.findIndex((fulfillmentPartnerBranch: PeopleModels.FulfillmentPartnerBranch) => {
      return fulfillmentPartnerBranch.id === this.resolution.target_branch.id;
    });
  }

  resolutionTargetMemberIndex(): number {
    return this.targetMembers.findIndex((fulfillmentPartnerMember: FulfillmentPartnerMemberViewModel) => {
      return fulfillmentPartnerMember.id === this.resolution.target_member.id;
    });
  }

  resolutionConflictServiceRequestIndex(): number {
    return this.conflictServiceRequests.findIndex(
      (serviceRequest: PeopleModels.ServiceRequest) => serviceRequest.id === this.resolution.service_request.id
    );
  }

  resolutionServiceRequestIndex(): number {
    return this.serviceRequests.findIndex(
      (serviceRequest: PeopleModels.ServiceRequest) => serviceRequest.id === this.resolution.service_request.id
    );
  }

  updateRequiredFields(resolutionType): void {
    if (resolutionType === 'conflict') {
      this.editForm.controls['target'].setValidators(null);
      this.editForm.controls['targetBranch'].setValidators(null);
      this.editForm.controls['referral'].setValidators(Validators.required);
    } else {
      this.editForm.controls['target'].setValidators(Validators.required);
      this.editForm.controls['targetBranch'].setValidators(Validators.required);
      this.editForm.controls['referral'].setValidators(null);
    }

    this.editForm.controls['target'].updateValueAndValidity();
    this.editForm.controls['targetBranch'].updateValueAndValidity();
    this.editForm.controls['referral'].updateValueAndValidity();
  }

  loadConflictServiceRequests(): void {
    const page = Math.ceil(this.conflictServiceRequests.length / this.ITEMS_PER_PAGE) + 1;
    this.peopleServiceRequestRepository
      .findAllByMembershipId(this.membership.id, { page: page.toString() })
      .subscribe((serviceRequests: PeopleModels.ServiceRequest[]) => {
        this.conflictServiceRequests = this.conflictServiceRequests.concat(serviceRequests);
        if (serviceRequests.length < this.ITEMS_PER_PAGE) {
          this.stopInfiniteScroll = true;
        }
        this.setServiceRequestValue();
      });
  }

  loadTargetBranchServiceRequests(): void {
    const page = Math.ceil(this.serviceRequests.length / this.ITEMS_PER_PAGE) + 1;
    this.peopleServiceRequestRepository
      .findAllByMembershipId(this.membership.id, {
        fulfillment_partner_branch_id: this.targetBranch.id,
        page: page.toString()
      })
      .subscribe((serviceRequests: PeopleModels.ServiceRequest[]) => {
        this.serviceRequests = this.serviceRequests.concat(serviceRequests);
        if (serviceRequests.length < this.ITEMS_PER_PAGE) {
          this.stopInfiniteScroll = true;
        }
        this.setServiceRequestValue();
      });
  }

  loadTargetMembers(): void {
    this.peopleFulfillmentPartnerMembersRepository
      .findAllByFulfillmentPartnerBranch(this.targetBranch.id)
      .pipe(takeUntil(this._destroyed))
      .subscribe((fulfillmentPartnerMembers: PeopleModels.FulfillmentPartnerMember[]) => {
        this.targetMembers = fulfillmentPartnerMembers.map((fulfillmentPartnerMember: PeopleModels.FulfillmentPartnerMember) => {
          return new FulfillmentPartnerMemberViewModel(fulfillmentPartnerMember);
        });
      });
  }

  showOtherContactMethodField(event): void {
    this.showOtherContactMethodInput = event.value === 'other';
  }

  submit(): void {
    const body = {
      source: this.editForm.controls['source'].value,
      status: this.editForm.controls['status'].value,
      membership_id: this.membership.id,
      concern: this.editForm.controls['concern'].value,
      resolution_type: this.resolutionType,
      approved_to_continue: this.editForm.controls['approved'].value,
      assignee_id: this.editForm.controls['assignee'].value.id,
      action_taken: this.editForm.controls['providerAction'].value
    };

    if (this.editForm.controls['contactMethod'].value === 'other') {
      body['preferred_contact_method'] = this.editForm.controls['contactMethodOther'].value;
    } else if (this.editForm.controls['contactMethod'].value === 'email') {
      body[ 'preferred_contact_method' ] = this.email;
    } else {
      body['preferred_contact_method'] = this.editForm.controls['contactMethod'].value.value;
    }

    if (this.resolutionType === 'conflict') {
      body['referral'] = this.editForm.controls['referral'].value;

      if (this.editForm.controls['serviceRequest'].value) {
        body['service_request_id'] = this.editForm.controls['serviceRequest'].value.id;
      }
    } else {
      body['target_id'] = this.target.id;
      body['target_branch_id'] = this.targetBranch.id;

      if (this.editForm.controls['serviceRequest'].value) {
        body['service_request_id'] = this.editForm.controls['serviceRequest'].value.id;
      }

      if (this.resolutionType === 'provider') {
        if (this.editForm.controls['targetMember'].value) {
          body['target_member_id'] = this.editForm.controls['targetMember'].value.id;
        }
      } else {
        if (this.editForm.controls['referral'].value) {
          body['referral'] = this.editForm.controls['referral'].value;
        }
      }
    }

    this.partnerResolutionRepository.update(this.resolution.id, body).subscribe((resolution: PartnerModels.Resolution) => {
      this.dataService.setResolution(new ResolutionViewModel(resolution));
      this.close();
    });
  }

  targetBranchChanged(): void {
    this.loadTargetBranchServiceRequests();
    this.loadFulfillmentPartners();
    this.loadTargetMembers();
  }

  onScroll(): void {
    this.loadServiceRequests(this.resolutionType);
  }
}
