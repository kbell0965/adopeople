import { HttpClientTestingModule } from '@angular/common/http/testing';
import { DebugElement, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import {
  MatCheckboxModule,
  MatFormFieldModule,
  MatInputModule,
  MatRadioModule,
  MatSelectModule,
  MatProgressSpinnerModule
} from '@angular/material';
import { By } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FeatherModule } from 'angular-feather';
import { X } from 'angular-feather/icons';
import { MatSelectInfiniteScrollModule } from 'ng-mat-select-infinite-scroll';
import { PartnerFactories, PeopleFactories } from '@pplsi-core/factories';
import {
  PartnerModels,
  PeopleServiceRequestRepository,
  PartnerResolutionRepository,
  PeopleFulfillmentPartnerRepository,
  PeopleModels,
  PeopleFulfillmentPartnerMembersRepository
} from '@pplsi-core/datalayer';
import { AccountViewModel } from '../../../models/account-view-model';
import { PartnerFulfillmentPartnerMemberViewModel } from '../../../models/partner-fulfillment-partner-member-view-model';
import { ResolutionViewModel } from '../../../modules/resolutions/models/resolution-view-model';
import { DataService } from '../../../services/data/data.service';
import { DRAWER_DATA, DrawerService } from '../../../services/drawer/drawer.service';
import { ResolutionEditComponent } from './resolution-edit.component';

const fulfillmentPartnerBranch: object = PeopleFactories.FulfillmentPartnerBranchFactory.build();
const fulfillmentPartnerBranches: PeopleModels.FulfillmentPartnerBranch[] = [
  new PeopleModels.FulfillmentPartnerBranch(new PeopleModels.FulfillmentPartnerBranch(fulfillmentPartnerBranch))
];
const fulfillmentPartner: object = PeopleFactories.FulfillmentPartnerFactory.build();
const fulfillmentPartners: PartnerModels.FulfillmentPartner[] = [
  new PartnerModels.FulfillmentPartner(fulfillmentPartner)
];

describe('ResolutionEditComponent.Template', () => {
  let component: ResolutionEditComponent;
  let fixture: ComponentFixture<ResolutionEditComponent>;
  const accountFake: object = PartnerFactories.AccountFactory.build();
  const account: AccountViewModel = new AccountViewModel(accountFake);
  const fulfillmentPartnerMembersFake: object[] = PartnerFactories.FulfillmentPartnerMemberFactory.buildList(1);
  const fulfillmentPartnerMembers: PartnerFulfillmentPartnerMemberViewModel[] = fulfillmentPartnerMembersFake
    .map((fulfillmentPartnerMemberFake: object) => new PartnerFulfillmentPartnerMemberViewModel(
      new PartnerModels.FulfillmentPartnerMember(fulfillmentPartnerMemberFake))
    );
  const resolutionFake: object = PartnerFactories.ResolutionFactory.build({
    assignee: fulfillmentPartnerMembersFake[0],
    membership_id: accountFake['memberships'][0].id,
    target: fulfillmentPartner,
    target_branch: fulfillmentPartnerBranch,
    target_member: fulfillmentPartnerMembersFake
  });
  const resolution: ResolutionViewModel = new ResolutionViewModel(resolutionFake);
  const dataStub = {
    resolution: resolution,
    account: account,
    fulfillmentPartnerMembers: fulfillmentPartnerMembers,
    fulfillmentPartners: fulfillmentPartners,
    fulfillmentPartnerBranches: fulfillmentPartnerBranches
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResolutionEditComponent ],
      imports: [
        FeatherModule.pick({ X }),
        ReactiveFormsModule,
        HttpClientTestingModule,
        MatFormFieldModule,
        MatSelectModule,
        MatRadioModule,
        MatInputModule,
        MatCheckboxModule,
        MatProgressSpinnerModule,
        BrowserAnimationsModule,
        MatSelectInfiniteScrollModule
      ],
      providers: [
        { provide: DRAWER_DATA, useValue: dataStub },
        DrawerService,
        PeopleFulfillmentPartnerRepository,
        PeopleServiceRequestRepository,
        PartnerResolutionRepository,
        DataService,
        PeopleFulfillmentPartnerMembersRepository
      ],
      schemas: [
        CUSTOM_ELEMENTS_SCHEMA,
        NO_ERRORS_SCHEMA
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResolutionEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  describe('header', () => {
    it('displays the edit resolution header', () => {
      const debugElement: DebugElement = fixture.debugElement.query(By.css('h3'));

      expect(debugElement.nativeElement.innerHTML).toEqual(`Edit Resolution ${resolution.display_id}`);
    });

    it('displays the close button', () => {
      const debugElement: DebugElement = fixture.debugElement.query(By.css('.header i-feather'));

      expect(debugElement).not.toBeNull();
    });
  });

  describe('contents', () => {
    it('displays the user input', () => {
      const debugElement: DebugElement = fixture.debugElement.query(By.css('.user-input'));

      expect(debugElement).not.toBeNull();
    });

    it('displays the assignee input', () => {
      const debugElement: DebugElement = fixture.debugElement.query(By.css('.assignee-input'));

      expect(debugElement).not.toBeNull();
    });

    it('displays the status input', () => {
      const debugElement: DebugElement = fixture.debugElement.query(By.css('.status-input'));

      expect(debugElement).not.toBeNull();
    });

    describe('resolution type', () => {
      it('displays the resolution type input', () => {
        const debugElement: DebugElement = fixture.debugElement.query(By.css('.resolution-type-input'));

        expect(debugElement).not.toBeNull();
      });

      it('calls the loadConflictServiceRequests method when the conflict option is clicked', () => {
        spyOn(component, 'loadConflictServiceRequests');
        const debugElement: DebugElement = fixture.debugElement.query(By.css('.conflict-option'));
        debugElement.nativeElement.click();

        expect(component.loadConflictServiceRequests).toHaveBeenCalled();
      });
    });

    it('displays the concern input', () => {
      const debugElement: DebugElement = fixture.debugElement.query(By.css('.concern-input'));

      expect(debugElement).not.toBeNull();
    });


    describe('source of concern input', () => {
      it('radio group exists', () => {
        expect(fixture.debugElement.query(By.css('mat-radio-group.source-input'))).not.toBeNull();
      });

      it('has an Email/Letter option', () => {
        expect(fixture.debugElement.query(By.css('.source-input mat-radio-button[value="letter"]'))).not.toBeNull();
      });

      it('has an Call/Chat option', () => {
        expect(fixture.debugElement.query(By.css('.source-input mat-radio-button[value="call"]'))).not.toBeNull();
      });

      it('has an Survey option', () => {
        expect(fixture.debugElement.query(By.css('.source-input mat-radio-button[value="survey"]'))).not.toBeNull();
      });

      it('has an Associate option', () => {
        expect(fixture.debugElement.query(By.css('.source-input mat-radio-button[value="associate"]'))).not.toBeNull();
      });

      it('has an Clarification option', () => {
        expect(fixture.debugElement.query(By.css('.source-input mat-radio-button[value="clarification"]'))).not.toBeNull();
      });
    });

    describe('not a conflict resolution type', () => {
      beforeEach(() => {
        component.editForm.controls['resolutionType'].setValue('provider');
        fixture.detectChanges();
      });

      it('displays the target input', () => {
        const debugElement: DebugElement = fixture.debugElement.query(By.css('.target-input'));

        expect(debugElement).not.toBeNull();
      });

      describe('target branch', () => {
        it('displays the target branch input', () => {
          const debugElement: DebugElement = fixture.debugElement.query(By.css('.target-branch-input'));

          expect(debugElement).not.toBeNull();
        });

        it('calls loadTargetBranchServiceRequests when selection changes', () => {
          spyOn(component, 'loadTargetBranchServiceRequests');
          const debugElement: DebugElement = fixture.debugElement.query(By.css('.target-branch-input'));
          debugElement.triggerEventHandler('selectionChange', {});

          expect(component.loadTargetBranchServiceRequests).toHaveBeenCalled();
        });
      });

      it('displays the service request input', () => {
        const debugElement: DebugElement = fixture.debugElement.query(By.css('.service-request-input'));

        expect(debugElement).not.toBeNull();
      });

      describe('provider resolution type', () => {
        it('displays the target member input', () => {
          const debugElement: DebugElement = fixture.debugElement.query(By.css('.target-member-input'));

          expect(debugElement).not.toBeNull();
        });
      });

      describe('referral resolution type', () => {
        it('displays the referral input', () => {
          component.editForm.controls['resolutionType'].setValue('referral');
          fixture.detectChanges();
          const debugElement: DebugElement = fixture.debugElement.query(By.css('.referral-input'));

          expect(debugElement).not.toBeNull();
        });
      });
    });

    describe('conflict resolution type', () => {
      beforeEach(() => {
        component.editForm.controls['resolutionType'].setValue('conflict');
        fixture.detectChanges();
      });

      it('displays the service request input', () => {
        const debugElement: DebugElement = fixture.debugElement.query(By.css('.service-request-input'));

        expect(debugElement).not.toBeNull();
      });

      it('displays the referral input', () => {
        const debugElement: DebugElement = fixture.debugElement.query(By.css('.referral-input'));

        expect(debugElement).not.toBeNull();
      });
    });

    describe('provider action', () => {
      it('displays the provider action input', () => {
        const debugElement: DebugElement = fixture.debugElement.query(By.css('.provider-action'));

        expect(debugElement).not.toBeNull();
      });

      it('displays two options for selection', () => {
        const debugElement: DebugElement = fixture.debugElement.query(By.css('.provider-action'));
        const optionsCount = debugElement.queryAll(By.css('mat-radio-button')).length;

        expect(optionsCount).toBe(2);
      });
    });

    describe('contact method', () => {
      it('displays the contact method input', () => {
        const debugElement: DebugElement = fixture.debugElement.query(By.css('.contact-methods'));

        expect(debugElement).not.toBeNull();
      });

      it('calls showOtherContactMethodField on selection change', () => {
        spyOn(component, 'showOtherContactMethodField');
        const debugElement: DebugElement = fixture.debugElement.query(By.css('.contact-methods'));
        debugElement.triggerEventHandler('change', {});

        expect(component.showOtherContactMethodField).toHaveBeenCalled();
      });
    });

    describe('other contact method', () => {
      describe('when showOtherContactMethodInput is true', () => {
        it('displays the other contact method input', () => {
          component.showOtherContactMethodInput = true;
          fixture.detectChanges();
          const debugElement: DebugElement = fixture.debugElement.query(By.css('.other-contact-method-input'));

          expect(debugElement).not.toBeNull();
        });
      });

      describe('when showOtherContactMethodInput is false', () => {
        it('does not display the other contact method input', () => {
          component.showOtherContactMethodInput = false;
          fixture.detectChanges();
          const debugElement: DebugElement = fixture.debugElement.query(By.css('.other-contact-method-input'));

          expect(debugElement).toBeNull();
        });
      });
    });

    it('displays the approved to continue input', () => {
      const debugElement: DebugElement = fixture.debugElement.query(By.css('.approval-input'));

      expect(debugElement).not.toBeNull();
    });

    describe('cancel button', () => {
      it('displays the cancel button', () => {
        const debugElement: DebugElement = fixture.debugElement.query(By.css('.cancel-button'));

        expect(debugElement).not.toBeNull();
      });

      it('calls close on the component when clicked', () => {
        spyOn(component, 'close');
        const debugElement: DebugElement = fixture.debugElement.query(By.css('.cancel-button'));
        debugElement.nativeElement.click();

        expect(component.close).toHaveBeenCalled();
      });
    });

    describe('save changes button', () => {
      it('displays the save changes button', () => {
        const debugElement: DebugElement = fixture.debugElement.query(By.css('.submit-button'));

        expect(debugElement).not.toBeNull();
      });

      it('is disabled until form is valid', () => {
        spyOnProperty(component.editForm, 'invalid', 'get').and.returnValue(true);
        fixture.detectChanges();
        const debugElement: DebugElement = fixture.debugElement.query(By.css('.submit-button'));

        expect(debugElement.nativeElement.attributes['disabled']).toBeDefined();
      });

      it('is not disabled when form is valid', () => {
        spyOnProperty(component.editForm, 'invalid', 'get').and.returnValue(false);
        fixture.detectChanges();
        const debugElement: DebugElement = fixture.debugElement.query(By.css('.submit-button'));

        expect(debugElement.nativeElement.attributes['disabled']).toBeUndefined();
      });

      it('calls submit on the component when clicked', () => {
        spyOn(component, 'submit');
        const debugElement: DebugElement = fixture.debugElement.query(By.css('.submit-button'));
        debugElement.nativeElement.click();

        expect(component.submit).toHaveBeenCalled();
      });
    });

    describe('intakes dropdown', () => {
      it('displays the loading spinner icon', () => {
        component.stopInfiniteScroll = false;
        fixture.detectChanges();
        const dropDownElement: DebugElement = fixture.debugElement.query(By.css('.service-request-input'));
        expect(dropDownElement).not.toBeNull();
        dropDownElement.nativeElement.click();
        fixture.detectChanges();

        const debugElement: DebugElement = fixture.debugElement.query(By.css('.loader'));
        expect(debugElement).not.toBeNull();
      });
    });
  });
});
