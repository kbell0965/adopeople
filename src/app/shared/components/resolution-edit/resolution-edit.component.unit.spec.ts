import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, getTestBed, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule, Validators } from '@angular/forms';
import {
  MatCheckboxModule,
  MatFormFieldModule,
  MatInputModule,
  MatRadioModule,
  MatSelectModule,
  MatProgressSpinnerModule
} from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FeatherModule } from 'angular-feather';
import { X } from 'angular-feather/icons';
import { of } from 'rxjs';
import { MatSelectInfiniteScrollModule } from 'ng-mat-select-infinite-scroll';
import Spy = jasmine.Spy;
import { PartnerFactories, PeopleFactories } from '@pplsi-core/factories';
import {
  PartnerModels,
  PeopleServiceRequestRepository,
  PartnerResolutionRepository,
  PeopleModels,
  PeopleFulfillmentPartnerRepository,
  PeopleFulfillmentPartnerMembersRepository
} from '@pplsi-core/datalayer';
import { AccountViewModel } from '../../../models/account-view-model';
import { FulfillmentPartnerMemberViewModel } from '../../../models/fulfillment-partner-member-view-model';
import { MembershipViewModel } from '../../../models/membership-view-model';
import { PartnerFulfillmentPartnerMemberViewModel } from '../../../models/partner-fulfillment-partner-member-view-model';
import { ResolutionViewModel } from '../../../modules/resolutions/models/resolution-view-model';
import { DataService } from '../../../services/data/data.service';
import { DRAWER_DATA, DrawerService } from '../../../services/drawer/drawer.service';
import { ResolutionEditComponent } from './resolution-edit.component';

const getFulfillmentPartnerBranch = (config = {}) => PeopleFactories.FulfillmentPartnerBranchFactory.build(config);
const getFulfillmentPartnerMember = (config = {}) => PartnerFactories.FulfillmentPartnerMemberFactory.build(config);
const getFulfillmentPartner = (config = {}) => PeopleFactories.FulfillmentPartnerFactory.build(config);

const fulfillmentPartnerBranches: PeopleModels.FulfillmentPartnerBranch[] = [
  new PeopleModels.FulfillmentPartnerBranch(new PeopleModels.FulfillmentPartnerBranch(getFulfillmentPartnerBranch({ id: '1' }))),
  new PeopleModels.FulfillmentPartnerBranch(new PeopleModels.FulfillmentPartnerBranch(getFulfillmentPartnerBranch({ id: '2' })))
];
const fulfillmentPartners: PartnerModels.FulfillmentPartner[] = [
  new PartnerModels.FulfillmentPartner(getFulfillmentPartner())
];
const fulfillmentPartnerMembers: PartnerFulfillmentPartnerMemberViewModel[] = [
  new PartnerFulfillmentPartnerMemberViewModel(getFulfillmentPartnerMember({
    fulfillment_partner_branches: [fulfillmentPartnerBranches[1]]
  })),
  new PartnerFulfillmentPartnerMemberViewModel(getFulfillmentPartnerMember({
    fulfillment_partner_branches: [fulfillmentPartnerBranches[0]]
  }))
];

describe('ResolutionEditComponent.Unit', () => {
  let component: ResolutionEditComponent;
  let fixture: ComponentFixture<ResolutionEditComponent>;
  let injector: TestBed;
  const accountFake: object = PartnerFactories.AccountFactory.build();
  const account: AccountViewModel = new AccountViewModel(accountFake);
  const resolutionFake: object = PartnerFactories.ResolutionFactory.build({
    assignee: fulfillmentPartnerMembers[0],
    membership_id: accountFake['memberships'][0].id,
    target: fulfillmentPartners[0],
    target_branch: fulfillmentPartnerBranches[1],
    target_member: fulfillmentPartnerMembers[1]
  });
  const resolution: ResolutionViewModel = new ResolutionViewModel(resolutionFake);
  const dataStub = {
    resolution: resolution,
    account: account,
    fulfillmentPartnerMembers: fulfillmentPartnerMembers,
    fulfillmentPartners: fulfillmentPartners,
    fulfillmentPartnerBranches: fulfillmentPartnerBranches
  };
  let loadServiceRequestSpy: Spy;
  let drawerService: DrawerService;
  let peopleServiceRequestRepository: PeopleServiceRequestRepository;
  let partnerResolutionRepository: PartnerResolutionRepository;
  let peopleFulfillmentPartnerRepository: PeopleFulfillmentPartnerRepository;
  let dataService: DataService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResolutionEditComponent ],
      imports: [
        FeatherModule.pick({ X }),
        ReactiveFormsModule,
        HttpClientTestingModule,
        MatFormFieldModule,
        MatSelectModule,
        MatRadioModule,
        MatInputModule,
        MatCheckboxModule,
        MatProgressSpinnerModule,
        BrowserAnimationsModule,
        MatSelectInfiniteScrollModule
      ],
      providers: [
        { provide: DRAWER_DATA, useValue: dataStub },
        DrawerService,
        PeopleFulfillmentPartnerRepository,
        PeopleServiceRequestRepository,
        PartnerResolutionRepository,
        DataService,
        PeopleFulfillmentPartnerMembersRepository
      ],
      schemas: [
        CUSTOM_ELEMENTS_SCHEMA,
        NO_ERRORS_SCHEMA
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResolutionEditComponent);
    injector = getTestBed();
    component = fixture.componentInstance;
    drawerService = injector.get(DrawerService);
    peopleServiceRequestRepository = injector.get(PeopleServiceRequestRepository);
    partnerResolutionRepository = injector.get(PartnerResolutionRepository);
    peopleFulfillmentPartnerRepository = injector.get(PeopleFulfillmentPartnerRepository);
    dataService = injector.get(DataService);
    loadServiceRequestSpy = spyOn(component, 'loadServiceRequests');
    fixture.detectChanges();
    component.ngOnInit();
  });

  afterAll(() => {
    TestBed.resetTestingModule();
  });

  describe('#ngOnInit', () => {
    beforeEach(() => {
      spyOn(component, 'updateRequiredFields');
    });

    it('sets the resolution', () => {
      expect(component.resolution).toEqual(resolution);
    });

    it('sets the account', () => {
      expect(component.account).toEqual(account);
    });

    it('sets the fulfillment partner members', () => {
      expect(component.fulfillmentPartnerMembers).toEqual(fulfillmentPartnerMembers);
    });

    it('sets the fulfillment partners', () => {
      expect(component.fulfillmentPartners).toEqual(fulfillmentPartners);
    });

    describe('editForm', () => {
      beforeEach(() => {
        component.ngOnInit();
      });

      it('sets up the form', () => {
        expect(component.editForm).toBeDefined();
      });

      it('creates a membership control', () => {
        expect(component.editForm.controls['membership']).toBeDefined();
        expect(component.editForm.controls['membership'].value)
          .toEqual(component.account.memberships[component.resolutionMembershipIndex()]);
        expect(component.editForm.controls['membership'].validator).toEqual(Validators.required);
      });

      it('creates an assignee control', () => {
        spyOn(component, 'resolutionAssigneeIndex').and.returnValue(0);
        component.ngOnInit();

        expect(component.editForm.controls['assignee']).toBeDefined();
        expect(component.editForm.controls['assignee'].value).toEqual(component.fulfillmentPartnerMembers[0]);
        expect(component.editForm.controls['assignee'].validator).toEqual(Validators.required);
      });

      it('creates a status control', () => {
        expect(component.editForm.controls['status']).toBeDefined();
        expect(component.editForm.controls['status'].value).toEqual(component.resolution.status);
        expect(component.editForm.controls['status'].validator).toEqual(Validators.required);
      });

      it('creates a concern control', () => {
        expect(component.editForm.controls['concern']).toBeDefined();
        expect(component.editForm.controls['concern'].value).toEqual(component.resolution.concern);
        expect(component.editForm.controls['concern'].validator).toEqual(Validators.required);
      });

      it('creates a resolutionType control', () => {
        expect(component.editForm.controls['resolutionType']).toBeDefined();
        expect(component.editForm.controls['resolutionType'].value).toEqual(component.resolution.resolution_type);
        expect(component.editForm.controls['resolutionType'].validator).toEqual(Validators.required);
      });

      it('creates a source control', () => {
        expect(component.editForm.controls['source']).toBeDefined();
        expect(component.editForm.controls['source'].value).toEqual(component.resolution.source);
        expect(component.editForm.controls['source'].validator).toEqual(Validators.required);
      });

      it('creates a target control', () => {
        spyOn(component, 'resolutionTargetIndex').and.returnValue(0);
        component.ngOnInit();

        expect(component.editForm.controls['target']).toBeDefined();
        expect(component.editForm.controls['target'].value).toEqual(component.fulfillmentPartners[0]);
        expect(component.editForm.controls['target'].validator).not.toEqual(Validators.required);
      });

      it('creates a targetBranch control', () => {
        const targetBranch = new PeopleModels.FulfillmentPartnerBranch(
          new PeopleModels.FulfillmentPartnerBranch(PeopleFactories.FulfillmentPartnerBranchFactory.build())
        );
        spyOn(component, 'resolutionTargetBranchValue').and.returnValue(targetBranch);
        component.ngOnInit();

        expect(component.editForm.controls['targetBranch']).toBeDefined();
        expect(component.editForm.controls['targetBranch'].value).toEqual(targetBranch);
        expect(component.editForm.controls['targetBranch'].validator).not.toEqual(Validators.required);
      });

      it('creates a targetMember control', () => {
        const targetMemberValue = new FulfillmentPartnerMemberViewModel(new PeopleModels.FulfillmentPartnerMember(
          PeopleFactories.FulfillmentPartnerMemberFactory.build()
        ));
        spyOn(component, 'resolutionTargetMemberValue').and.returnValue(targetMemberValue);
        component.ngOnInit();

        expect(component.editForm.controls['targetMember']).toBeDefined();
        expect(component.editForm.controls['targetMember'].value).toEqual(targetMemberValue);
        expect(component.editForm.controls['targetMember'].validator).not.toEqual(Validators.required);
      });

      it('creates a serviceRequest control', () => {
        expect(component.editForm.controls['serviceRequest']).toBeDefined();
        expect(component.editForm.controls['serviceRequest'].value).toBeNull();
        expect(component.editForm.controls['serviceRequest'].validator).not.toEqual(Validators.required);
      });

      it('creates a contactMethod control', () => {
        expect(component.editForm.controls['contactMethod']).toBeDefined();
        expect(component.editForm.controls['contactMethod'].value).toEqual('other');
        expect(component.editForm.controls['contactMethod'].validator).toEqual(Validators.required);
      });

      it('creates a contactMethodOther control', () => {
        expect(component.editForm.controls['contactMethodOther']).toBeDefined();
        expect(component.editForm.controls['contactMethodOther'].value).toEqual(component.resolution.preferred_contact_method.value);
        expect(component.editForm.controls['contactMethodOther'].validator).not.toEqual(Validators.required);
      });

      it('creates a referral control', () => {
        expect(component.editForm.controls['referral']).toBeDefined();
        expect(component.editForm.controls['referral'].value).toEqual(component.resolution.referral);
        expect(component.editForm.controls['referral'].validator).not.toEqual(Validators.required);
      });

      it('creates a approved control', () => {
        expect(component.editForm.controls['approved']).toBeDefined();
        expect(component.editForm.controls['approved'].value).toEqual(component.resolution.approved_to_continue);
        expect(component.editForm.controls['approved'].validator).not.toEqual(Validators.required);
      });
    });

    it('calls loadServiceRequests', () => {
      component.ngOnInit();

      expect(component.loadServiceRequests).toHaveBeenCalledWith(component.resolutionType);
    });

    it('calls updateRequiredFields', () => {
      component.ngOnInit();

      expect(component.updateRequiredFields).toHaveBeenCalledWith(component.resolutionType);
    });
  });

  describe('#ngOnDestroy', () => {
    beforeEach(() => {
      spyOn((component as any)._destroyed, 'next');
      spyOn((component as any)._destroyed, 'complete');
      component.ngOnDestroy();
    });

    it('calls #next on `_destroyed`', () => expect((component as any)._destroyed.next).toHaveBeenCalled());
    it('calls #complete on `_destroyed`', () => expect((component as any)._destroyed.complete).toHaveBeenCalled());
  });

  describe('#loadServiceRequests', () => {
    beforeEach(() => {
      loadServiceRequestSpy.and.callThrough();
    });

    describe('when resolution type is conflict', () => {
      it('calls loadConflictServiceRequests', () => {
        spyOn(component, 'loadConflictServiceRequests');
        component.loadServiceRequests('conflict');

        expect(component.loadConflictServiceRequests).toHaveBeenCalled();
      });
    });

    describe('when resolution type is not conflict', () => {
      it('calls loadTargetBranchServiceRequests', () => {
        spyOn(component, 'loadTargetBranchServiceRequests');
        component.loadServiceRequests('something else');

        expect(component.loadTargetBranchServiceRequests).toHaveBeenCalled();
      });
    });
  });

  describe('getters', () => {
    beforeEach(() => {
      component.ngOnInit();
    });

    describe('#membership', () => {
      it('returns the control value', () => {
        component.editForm.controls['membership'].setValue(account.memberships[0]);

        expect(component.membership).toEqual(account.memberships[0]);
      });
    });

    describe('#resolutionType', () => {
      it('returns the control value', () => {
        component.editForm.controls['resolutionType'].setValue('some value');

        expect(component.resolutionType).toEqual('some value');
      });
    });

    describe('#target', () => {
      it('returns the control value', () => {
        component.editForm.controls['target'].setValue(fulfillmentPartners[0]);

        expect(component.target).toEqual(fulfillmentPartners[0]);
      });
    });

    describe('#targetBranch', () => {
      it('returns the control value', () => {
        const targetBranchValue = new PeopleModels.FulfillmentPartnerBranch(
          new PeopleModels.FulfillmentPartnerBranch(PeopleFactories.FulfillmentPartnerBranchFactory.build())
        );
        component.editForm.controls['targetBranch'].setValue(targetBranchValue);

        expect(component.targetBranch).toEqual(targetBranchValue);
      });
    });

    describe('#contactMethods', () => {
      it('returns the contactMethods on the membership', () => {
        component.editForm.controls['membership'].setValue(account.memberships[0]);
        expect(component.contactMethods).toEqual(account.memberships[0].user.contact_methods);
      });
    });

    describe('#email', () => {
      it('returns the email on the membership', () => {
        component.editForm.controls['membership'].setValue(account.memberships[0]);
        expect(component.email).toEqual(account.memberships[0].user.email);
      });
    });
  });

  describe('#setServiceRequestValue', () => {
    describe('when the resolution type is conflict', () => {
      it('sets the conflict service request value', () => {
        component.conflictServiceRequests = PeopleFactories.ServiceRequestFactory.buildList(1)
          .map((serviceRequest: object) => new PeopleModels.ServiceRequest(serviceRequest));
        component.resolution.resolution_type = 'conflict';
        component.editForm.controls['serviceRequest'].setValue(null);
        component.setServiceRequestValue();

        expect(component.editForm.controls['serviceRequest'].value)
          .toEqual(component.conflictServiceRequests[component.resolutionConflictServiceRequestIndex()]);
      });
    });

    describe('when the resolution type is not conflict', () => {
      it('sets the service request value', () => {
        component.serviceRequests = PeopleFactories.ServiceRequestFactory.buildList(1)
          .map((serviceRequest: object) => new PeopleModels.ServiceRequest(serviceRequest));
        component.resolution.resolution_type = 'something else';
        component.editForm.controls['serviceRequest'].setValue(null);
        component.setServiceRequestValue();

        expect(component.editForm.controls['serviceRequest'].value)
          .toEqual(component.serviceRequests[component.resolutionServiceRequestIndex()]);
      });
    });
  });

  describe('#resolutionTargetBranchValue', () => {
    describe('when the target is found', () => {
      it('returns the target branch on the resolution', () => {
        spyOn(component, 'resolutionTargetIndex').and.returnValue(0);
        spyOn(component, 'resolutionTargetBranchIndex').and.returnValue(0);

        expect(component.resolutionTargetBranchValue()).toEqual(component.fulfillmentPartnerBranches[0]);
      });
    });

    describe('when the target is not found', () => {
      it('returns null', () => {
        component.resolution.target_branch = getFulfillmentPartnerBranch({ id: 'nope' });

        expect(component.resolutionTargetBranchValue()).toEqual(null);
      });
    });
  });

  describe('#resolutionTargetMemberValue', () => {
    it('returns the target member on the resolution', () => {
      const targetMember = new FulfillmentPartnerMemberViewModel(
        new PeopleModels.FulfillmentPartnerMember(PeopleFactories.FulfillmentPartnerMemberFactory.build())
      );
      spyOn(component, 'resolutionTargetMemberIndex').and.returnValue(0);
      component.targetMembers = [targetMember];

      expect(component.resolutionTargetMemberValue()).toEqual(targetMember);
    });
  });

  describe('#close', () => {
    it('calls close on the drawer service', () => {
      spyOn(drawerService, 'close');
      component.close();

      expect(drawerService.close).toHaveBeenCalled();
    });
  });

  describe('#resolutionMembershipIndex', () => {
    it('returns the index of the resolution membership on the account', () => {
      const correctMembership: MembershipViewModel = new MembershipViewModel(PartnerFactories.MembershipFactory.build());
      const otherMembership: MembershipViewModel = new MembershipViewModel(PartnerFactories.MembershipFactory.build());
      component.account.memberships = [otherMembership, correctMembership];
      component.resolution.membership_id = correctMembership.id;

      expect(component.resolutionMembershipIndex()).toEqual(1);
    });
  });

  describe('#resolutionAssigneeIndex', () => {
    it('returns the index of the resolution assignee from the fulfillment partner members', () => {
      const correctFulfillmentPartnerMember: PartnerFulfillmentPartnerMemberViewModel = new PartnerFulfillmentPartnerMemberViewModel(
        PartnerFactories.FulfillmentPartnerMemberFactory.build()
      );
      const otherFulfillmentPartnerMember: PartnerFulfillmentPartnerMemberViewModel = new PartnerFulfillmentPartnerMemberViewModel(
        PartnerFactories.FulfillmentPartnerMemberFactory.build()
      );
      component.fulfillmentPartnerMembers = [otherFulfillmentPartnerMember, correctFulfillmentPartnerMember];
      component.resolution.assignee.id = correctFulfillmentPartnerMember.id;

      expect(component.resolutionAssigneeIndex()).toEqual(1);
    });
  });

  describe('#resolutionTargetIndex', () => {
    it('returns the index of the resolution target from the fulfillment partners', () => {
      const correctFulfillmentPartner: PartnerModels.FulfillmentPartner = new PartnerModels.FulfillmentPartner(
        PartnerFactories.FulfillmentPartnerFactory.build()
      );
      const otherFulfillmentPartner: PartnerModels.FulfillmentPartner = new PartnerModels.FulfillmentPartner(
        PartnerFactories.FulfillmentPartnerFactory.build()
      );
      component.fulfillmentPartners = [otherFulfillmentPartner, correctFulfillmentPartner];
      component.resolution.target.id = correctFulfillmentPartner.id;

      expect(component.resolutionTargetIndex()).toEqual(1);
    });
  });

  describe('#resolutionTargetBranchIndex', () => {
    it('returns the index of the resolution target branch from the fulfillment partners', () => {
      const targetBranch = new PeopleModels.FulfillmentPartnerBranch(
        new PeopleModels.FulfillmentPartnerBranch(PeopleFactories.FulfillmentPartnerBranchFactory.build())
      );
      component.fulfillmentPartnerBranches = [targetBranch];
      component.resolution.target_branch.id = targetBranch.id;
      spyOn(component, 'resolutionTargetIndex').and.returnValue(0);

      expect(component.resolutionTargetBranchIndex()).toEqual(0);
    });
  });

  describe('#resolutionTargetMemberIndex', () => {
    it('returns the index of the resolution target member from the fulfillment partners', () => {
      const targetMember = new FulfillmentPartnerMemberViewModel(
        new PeopleModels.FulfillmentPartnerMember(PeopleFactories.FulfillmentPartnerMemberFactory.build())
      );
      component.targetMembers = [targetMember];
      component.resolution.target_member.id = targetMember.id;
      expect(component.resolutionTargetMemberIndex()).toEqual(0);
    });
  });

  describe('#resolutionConflictServiceRequestIndex', () => {
    it('returns the index of the resolution conflict service request from the conflict service requests', () => {
      const correctServiceRequest: PeopleModels.ServiceRequest = new PeopleModels.ServiceRequest(
        PeopleFactories.ServiceRequestFactory.build()
      );
      const otherServiceRequest: PeopleModels.ServiceRequest = new PeopleModels.ServiceRequest(
        PeopleFactories.ServiceRequestFactory.build()
      );
      component.conflictServiceRequests = [otherServiceRequest, correctServiceRequest];
      component.resolution.service_request.id = correctServiceRequest.id;

      expect(component.resolutionConflictServiceRequestIndex()).toEqual(1);
    });
  });

  describe('#resolutionServiceRequestIndex', () => {
    it('returns the index of the resolution service request from the service requests', () => {
      const correctServiceRequest: PeopleModels.ServiceRequest = new PeopleModels.ServiceRequest(
        PeopleFactories.ServiceRequestFactory.build()
      );
      const otherServiceRequest: PeopleModels.ServiceRequest = new PeopleModels.ServiceRequest(
        PeopleFactories.ServiceRequestFactory.build()
      );
      component.serviceRequests = [otherServiceRequest, correctServiceRequest];
      component.resolution.service_request.id = correctServiceRequest.id;

      expect(component.resolutionServiceRequestIndex()).toEqual(1);
    });
  });

  describe('#updateRequiredFields', () => {
    beforeEach(() => {
      component.ngOnInit();
    });

    describe('conflict resolution type', () => {
      it('updates the required fields', () => {
        component.updateRequiredFields('conflict');

        expect(component.editForm.controls['target'].validator).not.toEqual(Validators.required);
        expect(component.editForm.controls['targetBranch'].validator).not.toEqual(Validators.required);
        expect(component.editForm.controls['referral'].validator).toEqual(Validators.required);
      });
    });

    describe('any other resolution type', () => {
      it('updates the required fields', () => {
        component.updateRequiredFields('anything else');

        expect(component.editForm.controls['target'].validator).toEqual(Validators.required);
        expect(component.editForm.controls['targetBranch'].validator).toEqual(Validators.required);
        expect(component.editForm.controls['referral'].validator).not.toEqual(Validators.required);
      });
    });
  });

  describe('#loadTargetBranchServiceRequests', () => {
    const serviceRequests = [new PeopleModels.ServiceRequest(PeopleFactories.ServiceRequestFactory.build())];

    beforeEach(() => {
      component.editForm.controls['targetBranch'].setValue(new PartnerModels.FulfillmentPartnerBranch({ id: 2 }));

      spyOn(peopleServiceRequestRepository, 'findAllByMembershipId').and
        .returnValue(of(serviceRequests));
      spyOn(component, 'setServiceRequestValue');
    });

    it('calls the people service request repository', () => {
      component.loadTargetBranchServiceRequests();

      expect(peopleServiceRequestRepository.findAllByMembershipId)
        .toHaveBeenCalledWith(component.membership.id, {
          fulfillment_partner_branch_id: component.targetBranch.id,
          page: '1'
        });
    });

    it('sets the service requests on the component', () => {
      component.loadTargetBranchServiceRequests();

      expect(component.serviceRequests).toEqual(serviceRequests);
    });
  });

  describe('#loadConflictServiceRequests', () => {
    const serviceRequests = [new PeopleModels.ServiceRequest(PeopleFactories.ServiceRequestFactory.build())];

    beforeEach(() => {
      spyOn(peopleServiceRequestRepository, 'findAllByMembershipId').and
        .returnValue(of(serviceRequests));
    });

    it('calls the people service request repository', () => {
      component.loadConflictServiceRequests();

      expect(peopleServiceRequestRepository.findAllByMembershipId)
        .toHaveBeenCalledWith(component.membership.id, { page: '1' });
    });

    it('sets the service requests on the component', () => {
      component.loadConflictServiceRequests();

      expect(component.conflictServiceRequests).toEqual(serviceRequests);
    });
  });

  describe('#loadFulfillmentPartners', () => {
    beforeEach(() => {
      component.editForm.controls['targetBranch'].setValue(new PartnerModels.FulfillmentPartnerBranch({ id: 2 }));
      spyOn(peopleFulfillmentPartnerRepository, 'findAll').and.returnValue(of(fulfillmentPartners));
      component.loadFulfillmentPartners();
    });

    it('calls the people fulfillment partner service repository', () => {
      expect(peopleFulfillmentPartnerRepository.findAll).toHaveBeenCalledTimes(1);
    });

    it('sets the fulfillment partners on the component', () => {
      const expected = fulfillmentPartners.map((fulfillmentPartner: PeopleModels.FulfillmentPartner) => {
        return new PeopleModels.FulfillmentPartner(fulfillmentPartner);
      });

      expect(component.fulfillmentPartners).toEqual(expected);
    });
  });

  describe('#showOtherContactMethodField', () => {
    it('returns false when other is not the value', () => {
      component.showOtherContactMethodField({ value: 'something else' });

      expect(component.showOtherContactMethodInput).toBe(false);
    });

    it('returns true when other is the value', () => {
      component.showOtherContactMethodField({ value: 'other' });

      expect(component.showOtherContactMethodInput).toBe(true);
    });
  });

  describe('#submit', () => {
    let body: object;
    let resolutionResponse: PartnerModels.Resolution;

    beforeEach(() => {
      component.ngOnInit();

      component.editForm.controls['source'].setValue('some source');
      component.editForm.controls['concern'].setValue('some concern');
      component.editForm.controls['resolutionType'].setValue('conflict');
      component.editForm.controls['approved'].setValue(true);
      component.editForm.controls['assignee'].setValue(fulfillmentPartnerMembers[0]);
      component.editForm.controls['status'].setValue('unassigned');
      component.editForm.controls['providerAction'].setValue('provider action taken');

      body = {
        source: 'some source',
        status: 'unassigned',
        membership_id: component.membership.id,
        concern: 'some concern',
        resolution_type: 'conflict',
        approved_to_continue: true,
        assignee_id: fulfillmentPartnerMembers[0].id,
        action_taken: 'provider action taken'
      };

      resolutionResponse = new PartnerModels.Resolution(PartnerFactories.ResolutionFactory.build());

      spyOn(partnerResolutionRepository, 'update').and.returnValue(of(resolutionResponse));
      spyOn(dataService, 'setResolution');
      spyOn(component, 'close');
    });

    describe('contact method not other', () => {
      it('updates a resolutions with the correct body', () => {
        const contactMethod = new PartnerModels.ContactMethod(PartnerFactories.ContactMethodFactory.build());

        component.editForm.controls['contactMethod'].setValue(contactMethod);
        component.editForm.controls['referral'].setValue('some referral');

        body['preferred_contact_method'] = contactMethod.value;
        body['referral'] = 'some referral';

        component.submit();

        expect(partnerResolutionRepository.update).toHaveBeenCalledWith(component.resolution.id, body);
        expect(dataService.setResolution).toHaveBeenCalledWith(new ResolutionViewModel(resolutionResponse));
        expect(component.close).toHaveBeenCalled();
      });
    });

    describe('contact method other', () => {
      it('creates a resolutions with the correct body', () => {
        component.editForm.controls['contactMethod'].setValue('other');
        component.editForm.controls['contactMethodOther'].setValue('some other contact method');
        component.editForm.controls['referral'].setValue('some referral');

        body['preferred_contact_method'] = 'some other contact method';
        body['referral'] = 'some referral';

        component.submit();

        expect(partnerResolutionRepository.update).toHaveBeenCalledWith(component.resolution.id, body);
        expect(dataService.setResolution).toHaveBeenCalledWith(new ResolutionViewModel(resolutionResponse));
        expect(component.close).toHaveBeenCalled();
      });
    });

    describe('contact method email', () => {
      it('creates a resolutions with the correct body', () => {
        component.editForm.controls['contactMethod'].setValue('email');
        component.editForm.controls['referral'].setValue('some referral');

        body['preferred_contact_method'] = component.email;
        body['referral'] = 'some referral';

        component.submit();

        expect(partnerResolutionRepository.update).toHaveBeenCalledWith(component.resolution.id, body);
        expect(dataService.setResolution).toHaveBeenCalledWith(new ResolutionViewModel(resolutionResponse));
        expect(component.close).toHaveBeenCalled();
      });
    });

    describe('conflict resolution type', () => {
      describe('when a service request is not set', () => {
        it('creates a resolutions with the correct body', () => {
          const contactMethod = new PartnerModels.ContactMethod(PartnerFactories.ContactMethodFactory.build());

          component.editForm.controls['contactMethod'].setValue(contactMethod);
          component.editForm.controls['referral'].setValue('some referral');
          component.editForm.controls['resolutionType'].setValue('conflict');

          body['preferred_contact_method'] = contactMethod.value;
          body['referral'] = 'some referral';

          component.submit();

          expect(partnerResolutionRepository.update).toHaveBeenCalledWith(component.resolution.id, body);
          expect(dataService.setResolution).toHaveBeenCalledWith(new ResolutionViewModel(resolutionResponse));
          expect(component.close).toHaveBeenCalled();
        });
      });

      describe('when a service request is set', () => {
        it('creates a resolutions with the correct body', () => {
          const contactMethod = new PartnerModels.ContactMethod(PartnerFactories.ContactMethodFactory.build());
          const serviceRequest = new PeopleModels.ServiceRequest(PeopleFactories.ServiceRequestFactory.build());

          component.editForm.controls['contactMethod'].setValue(contactMethod);
          component.editForm.controls['referral'].setValue('some referral');
          component.editForm.controls['resolutionType'].setValue('conflict');
          component.editForm.controls['serviceRequest'].setValue(serviceRequest);

          body['preferred_contact_method'] = contactMethod.value;
          body['referral'] = 'some referral';
          body['service_request_id'] = serviceRequest.id;

          component.submit();

          expect(partnerResolutionRepository.update).toHaveBeenCalledWith(component.resolution.id, body);
          expect(dataService.setResolution).toHaveBeenCalledWith(new ResolutionViewModel(resolutionResponse));
          expect(component.close).toHaveBeenCalled();
        });
      });
    });

    describe('any other resolution type', () => {
      describe('when a service request is not set', () => {
        it('creates a resolutions with the correct body', () => {
          const contactMethod = new PartnerModels.ContactMethod(PartnerFactories.ContactMethodFactory.build());
          const target = new PartnerModels.FulfillmentPartner(PartnerFactories.FulfillmentPartnerFactory.build());
          const targetBranch = new PartnerModels.FulfillmentPartnerBranch(PartnerFactories.FulfillmentPartnerBranchFactory.build());

          component.editForm.controls['contactMethod'].setValue(contactMethod);
          component.editForm.controls['resolutionType'].setValue('some other type');
          component.editForm.controls['target'].setValue(target);
          component.editForm.controls['targetBranch'].setValue(targetBranch);

          body['preferred_contact_method'] = contactMethod.value;
          body['target_id'] = target.id;
          body['target_branch_id'] = targetBranch.id;
          body['resolution_type'] = 'some other type';
          body['referral'] = component.editForm.controls['referral'].value;

          component.submit();

          expect(partnerResolutionRepository.update).toHaveBeenCalledWith(component.resolution.id, body);
          expect(dataService.setResolution).toHaveBeenCalledWith(new ResolutionViewModel(resolutionResponse));
          expect(component.close).toHaveBeenCalled();
        });
      });

      describe('when a service request is set', () => {
        it('creates a resolutions with the correct body', () => {
          const contactMethod = new PartnerModels.ContactMethod(PartnerFactories.ContactMethodFactory.build());
          const serviceRequest = new PeopleModels.ServiceRequest(PeopleFactories.ServiceRequestFactory.build());
          const target = new PartnerModels.FulfillmentPartner(PartnerFactories.FulfillmentPartnerFactory.build());
          const targetBranch = new PartnerModels.FulfillmentPartnerBranch(PartnerFactories.FulfillmentPartnerBranchFactory.build());

          component.editForm.controls['contactMethod'].setValue(contactMethod);
          component.editForm.controls['resolutionType'].setValue('some other type');
          component.editForm.controls['target'].setValue(target);
          component.editForm.controls['targetBranch'].setValue(targetBranch);
          component.editForm.controls['serviceRequest'].setValue(serviceRequest);

          body['preferred_contact_method'] = contactMethod.value;
          body['target_id'] = target.id;
          body['target_branch_id'] = targetBranch.id;
          body['resolution_type'] = 'some other type';
          body['service_request_id'] = serviceRequest.id;
          body['referral'] = component.editForm.controls['referral'].value;

          component.submit();

          expect(partnerResolutionRepository.update).toHaveBeenCalledWith(component.resolution.id, body);
          expect(dataService.setResolution).toHaveBeenCalledWith(new ResolutionViewModel(resolutionResponse));
          expect(component.close).toHaveBeenCalled();
        });
      });

      describe('resolution type is provider', () => {
        describe('when a target member is set', () => {
          it('creates a resolutions with the correct body', () => {
            const contactMethod = new PartnerModels.ContactMethod(PartnerFactories.ContactMethodFactory.build());
            const target = new PartnerModels.FulfillmentPartner(PartnerFactories.FulfillmentPartnerFactory.build());
            const targetBranch = new PartnerModels.FulfillmentPartnerBranch(PartnerFactories.FulfillmentPartnerBranchFactory.build());
            const targetMember = new PartnerModels.FulfillmentPartnerMember(PartnerFactories.FulfillmentPartnerMemberFactory.build());

            component.editForm.controls['contactMethod'].setValue(contactMethod);
            component.editForm.controls['resolutionType'].setValue('provider');
            component.editForm.controls['target'].setValue(target);
            component.editForm.controls['targetBranch'].setValue(targetBranch);
            component.editForm.controls['targetMember'].setValue(targetMember);

            body['preferred_contact_method'] = contactMethod.value;
            body['target_id'] = target.id;
            body['target_branch_id'] = targetBranch.id;
            body['target_member_id'] = targetMember.id;
            body['resolution_type'] = 'provider';

            component.submit();

            expect(partnerResolutionRepository.update).toHaveBeenCalledWith(component.resolution.id, body);
            expect(dataService.setResolution).toHaveBeenCalledWith(new ResolutionViewModel(resolutionResponse));
            expect(component.close).toHaveBeenCalled();
          });
        });

        describe('when a target member is not set', () => {
          it('creates a resolutions with the correct body', () => {
            const contactMethod = new PartnerModels.ContactMethod(PartnerFactories.ContactMethodFactory.build());
            const target = new PartnerModels.FulfillmentPartner(PartnerFactories.FulfillmentPartnerFactory.build());
            const targetBranch = new PartnerModels.FulfillmentPartnerBranch(PartnerFactories.FulfillmentPartnerBranchFactory.build());

            component.editForm.controls['contactMethod'].setValue(contactMethod);
            component.editForm.controls['resolutionType'].setValue('provider');
            component.editForm.controls['target'].setValue(target);
            component.editForm.controls['targetBranch'].setValue(targetBranch);
            component.editForm.controls['targetMember'].setValue(null);

            body['preferred_contact_method'] = contactMethod.value;
            body['target_id'] = target.id;
            body['target_branch_id'] = targetBranch.id;
            body['resolution_type'] = 'provider';

            component.submit();

            expect(partnerResolutionRepository.update).toHaveBeenCalledWith(component.resolution.id, body);
            expect(dataService.setResolution).toHaveBeenCalledWith(new ResolutionViewModel(resolutionResponse));
            expect(component.close).toHaveBeenCalled();
          });
        });
      });

      describe('resolution type is not provider', () => {
        describe('when a referral is set', () => {
          it('creates a resolutions with the correct body', () => {
            const contactMethod = new PartnerModels.ContactMethod(PartnerFactories.ContactMethodFactory.build());
            const target = new PartnerModels.FulfillmentPartner(PartnerFactories.FulfillmentPartnerFactory.build());
            const targetBranch = new PartnerModels.FulfillmentPartnerBranch(PartnerFactories.FulfillmentPartnerBranchFactory.build());

            component.editForm.controls['contactMethod'].setValue(contactMethod);
            component.editForm.controls['resolutionType'].setValue('some other type');
            component.editForm.controls['target'].setValue(target);
            component.editForm.controls['targetBranch'].setValue(targetBranch);
            component.editForm.controls['referral'].setValue('some referral');

            body['preferred_contact_method'] = contactMethod.value;
            body['target_id'] = target.id;
            body['target_branch_id'] = targetBranch.id;
            body['referral'] = 'some referral';
            body['resolution_type'] = 'some other type';

            component.submit();

            expect(partnerResolutionRepository.update).toHaveBeenCalledWith(component.resolution.id, body);
            expect(dataService.setResolution).toHaveBeenCalledWith(new ResolutionViewModel(resolutionResponse));
            expect(component.close).toHaveBeenCalled();
          });
        });

        describe('when a referral is not set', () => {
          it('creates a resolutions with the correct body', () => {
            const contactMethod = new PartnerModels.ContactMethod(PartnerFactories.ContactMethodFactory.build());
            const target = new PartnerModels.FulfillmentPartner(PartnerFactories.FulfillmentPartnerFactory.build());
            const targetBranch = new PartnerModels.FulfillmentPartnerBranch(PartnerFactories.FulfillmentPartnerBranchFactory.build());

            component.editForm.controls['contactMethod'].setValue(contactMethod);
            component.editForm.controls['resolutionType'].setValue('some other type');
            component.editForm.controls['target'].setValue(target);
            component.editForm.controls['targetBranch'].setValue(targetBranch);
            component.editForm.controls['referral'].setValue(null);

            body['preferred_contact_method'] = contactMethod.value;
            body['target_id'] = target.id;
            body['target_branch_id'] = targetBranch.id;
            body['resolution_type'] = 'some other type';

            component.submit();

            expect(partnerResolutionRepository.update).toHaveBeenCalledWith(component.resolution.id, body);
            expect(dataService.setResolution).toHaveBeenCalledWith(new ResolutionViewModel(resolutionResponse));
            expect(component.close).toHaveBeenCalled();
          });
        });
      });
    });
  });

  describe('#targetBranchChanged', () => {
    beforeEach(() => {
      spyOn(component, 'loadTargetBranchServiceRequests');
      spyOn(component, 'loadFulfillmentPartners');
      spyOn(component, 'loadTargetMembers');
      component.targetBranchChanged();
    });

    it('calls the loadTargetBranchServiceRequest method', () => {
      expect(component.loadTargetBranchServiceRequests).toHaveBeenCalledTimes(1);
    });

    it('calls the loadFulfillmentPartners method', () => {
      expect(component.loadFulfillmentPartners).toHaveBeenCalledTimes(1);
    });

    it('calls the loadTargetMembers method', () => {
      expect(component.loadTargetMembers).toHaveBeenCalledTimes(1);
    });
  });
});
