import { NgModule } from '@angular/core';
import {
  MatCheckboxModule,
  MatFormFieldModule,
  MatInputModule,
  MatRadioModule,
  MatSelectModule,
  MatProgressSpinnerModule
} from '@angular/material';
import { BrowserModule } from '@angular/platform-browser';
import { FeatherModule } from 'angular-feather';
import { X } from 'angular-feather/icons';
import { MatSelectInfiniteScrollModule } from 'ng-mat-select-infinite-scroll';

import { ResolutionEditComponent } from './resolution-edit.component';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [ ResolutionEditComponent ],
  exports: [ ResolutionEditComponent ],
  imports: [
    FeatherModule.pick({ X }),
    MatFormFieldModule,
    MatSelectModule,
    BrowserModule,
    MatRadioModule,
    MatInputModule,
    MatCheckboxModule,
    MatProgressSpinnerModule,
    ReactiveFormsModule,
    MatSelectInfiniteScrollModule
  ],
  entryComponents: [ ResolutionEditComponent ]
})
export class ResolutionEditModule { }
