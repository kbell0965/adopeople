import { TestBed, ComponentFixture, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { PermittedDirective } from './permitted.directive';
import { Router } from '@angular/router';
import { Me, MeRepository } from '@pplsi-core/datalayer';
import { WindowService } from '@pplsi-core/shared-modules/dist/src/services/window.service';
import { PermissionsService } from '../../../services/permissions/permissions.service';
import { MeService } from '../../../services/me/me.service';
import { MeViewModel } from '../../../models/me-view-model';
import { FeatherModule } from 'angular-feather';
import { Archive, Users, Share2 } from 'angular-feather/icons';
import { MatSidenavModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SideNavComponent } from '../../../components/side-nav/side-nav.component';
import { ViewContainerRef, TemplateRef } from '@angular/core';
import { PermissionNamespaceType } from '@pplsi-core/datalayer';

describe('PermittedDirective', () => {
  let component: SideNavComponent;
  let fixture: ComponentFixture<SideNavComponent>;
  let meService: MeService;
  let me: MeViewModel;
  let meResponse: Me;
  let injector: TestBed;
  let viewContainerRef: ViewContainerRef;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        PermissionsService,
        MeService,
        WindowService,
        MeRepository,
        {
          provide: Router,
          useValue: { navigate: jasmine.createSpy('navigate') }
        },
        ViewContainerRef
      ],
      declarations: [
        SideNavComponent,
        PermittedDirective
      ],
      imports: [
        HttpClientTestingModule,
        FeatherModule.pick({ Archive, Users, Share2 }),
        MatSidenavModule,
        BrowserAnimationsModule]
    }).compileComponents();

    fixture = TestBed.createComponent(SideNavComponent);
    injector = getTestBed();
    meService = injector.get(MeService);
    viewContainerRef = injector.get(ViewContainerRef);
    component = fixture.componentInstance;
    component.loggedIn = false;
  });

  describe('#canViewContainer', () => {
    it('is true when current user has specified permission', () => {
      meResponse = new Me({
        name: 'John Doe', permissions: [{
          name: 'view_people_application',
          namespace: PermissionNamespaceType.Admin
        }]
      });
      me = new MeViewModel(meResponse);

      const permitted = new PermittedDirective(meService, {} as TemplateRef<any>, {} as ViewContainerRef);
      permitted.permissionName = 'view_people_application';
      expect(permitted.canViewContainer(me)).toBe(true);
    });

    it('is false when current user lacks specified permission', () => {
      meResponse = new Me({ name: 'John Doe', permissions: [{ name: 'view_role', namespace: PermissionNamespaceType.Admin }] });
      me = new MeViewModel(meResponse);

      const permitted = new PermittedDirective(meService, {} as TemplateRef<any>, {} as ViewContainerRef);
      permitted.permissionName = 'view_people_application';
      expect(permitted.canViewContainer(me)).toBe(false);
    });
  });
});

