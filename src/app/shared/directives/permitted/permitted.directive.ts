import { Directive, Input, TemplateRef, ViewContainerRef, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { MeService } from '../../../services/me/me.service';
import { MeViewModel } from '../../../models/me-view-model';
import { PermissionNamespaceType } from '@pplsi-core/datalayer';

@Directive({ selector: '[permission]' })
export class PermittedDirective implements OnInit {
  meSubscription: Subscription;
  readonly PEOPLE_APP_NAMESPACE = PermissionNamespaceType.Admin;

  @Input('permission') permissionName: string;

  constructor(private meService: MeService,
    private templateRef: TemplateRef<any>,
    private viewContainer: ViewContainerRef) { }

  ngOnInit(): void {
    this.meSubscription = this.meService.$currentUser().subscribe((me: MeViewModel) => {
      if (this.canViewContainer(me)) {
        this.viewContainer.createEmbeddedView(this.templateRef);
      } else {
        this.viewContainer.clear();
      }
    });
  }

  public canViewContainer(me: MeViewModel) {
    return me.hasPermission(this.permissionName, this.PEOPLE_APP_NAMESPACE);
  }
}
