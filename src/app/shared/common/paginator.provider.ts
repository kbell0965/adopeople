import { MatPaginatorIntl } from '@angular/material';

const getPaginatorRangeLabel = (pageIndex: number, pageSize: number, length: number): string => {
  if (length === 0 || pageSize === 0) { return `Displaying 0 out of ${length}`; }
  const startIndex = pageIndex * pageSize;
  const endIndex = startIndex < length ? Math.min(startIndex + pageSize, length) : startIndex + pageSize;
  return `Displaying ${startIndex + 1} - ${endIndex} out of ${length}`;
};

const getPaginatorIntl = () => {
  const paginatorIntl = new MatPaginatorIntl();
  paginatorIntl.itemsPerPageLabel = 'Results per page:';
  paginatorIntl.getRangeLabel = getPaginatorRangeLabel;
  return paginatorIntl;
};

export const MaterialPaginatorProvider = { provide: MatPaginatorIntl, useValue: getPaginatorIntl() };
