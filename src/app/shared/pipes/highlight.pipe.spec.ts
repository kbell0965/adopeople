import { TestBed, getTestBed } from '@angular/core/testing';
import { DomSanitizer } from '@angular/platform-browser';

import { HighlightPipe } from './highlight.pipe';

describe('HighlightPipe', () => {
  let pipe: HighlightPipe;
  let injector: TestBed;
  const text = 'The quick brown fox jumps over the lazy dog';

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        {
          provide: DomSanitizer,
          useValue: {
            bypassSecurityTrustHtml: v => v
          }
        },
        HighlightPipe
      ]
    });
  });

  beforeEach(() => {
    injector = getTestBed();
    pipe = injector.get(HighlightPipe);
  });

  it('is created', () => {
    expect(pipe).toBeTruthy();
  });

  it('highlights query term in search text', () => {
    const result = pipe.transform(text, 'brown');
    expect(result).toBe('The quick <span class="highlight">brown</span> fox jumps over the lazy dog');
  });

  it('does not highlight when query term is not in search text', () => {
    const result = pipe.transform(text, 'black');
    expect(result).toBe(text);
  });

  it('returns empty string when text is null', () => {
    const result = pipe.transform(null, 'text');
    expect(result).toBeNull();
  });
});
