import { Pipe, PipeTransform } from '@angular/core';
import { MeService } from 'src/app/services/me/me.service';
import { MeViewModel } from 'src/app/models/me-view-model';
import { PermissionNamespaceType } from '@pplsi-core/datalayer';

@Pipe({
  name: 'permissionDisabled'
})
export class PermissionDisabledPipe implements PipeTransform {
  meVm: MeViewModel;
  readonly NAMESPACE = PermissionNamespaceType.Admin;

  constructor(meService: MeService) {
    meService.$currentUser().subscribe(me => this.meVm = Object.freeze(me));
  }

  transform(value: string, namespace: string = this.NAMESPACE): boolean {
    return !this.hasPermission(value, namespace);
  }

  hasPermission(value: string, namespace: string) {
    return this.meVm.hasPermission(value, namespace);
  }
}
