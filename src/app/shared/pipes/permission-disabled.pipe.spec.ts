import { PermissionDisabledPipe } from './permission-disabled.pipe';
import { TestBed, getTestBed } from '@angular/core/testing';
import { MeService } from 'src/app/services/me/me.service';
import { MeFactory, PeopleFactories } from '@pplsi-core/factories';
import { MeRepository, PermissionNamespaceType } from '@pplsi-core/datalayer';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';

let meRepo: MeRepository;
let meService: MeService;
let injector: TestBed;
let pipe: PermissionDisabledPipe;
const getMe = (cfg) => MeFactory.build(cfg);
const getPermission = (cfg) => PeopleFactories.PermissionFactory.build(cfg);
const expectedPermissionName = 'view:people_app';

describe('PermissionDisabledPipe', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                MeService,
                MeRepository
            ],
            imports: [HttpClientTestingModule]
        }).compileComponents();

        injector = getTestBed();
        meRepo = injector.get(MeRepository);

        spyOn(meRepo, 'get').and.returnValue(of(getMe({
            permissions: [getPermission({
                name: expectedPermissionName,
                namespace: PermissionNamespaceType.Admin
            })]
        })));

        meService = injector.get(MeService);
        pipe = new PermissionDisabledPipe(meService);
    });

    describe('#transform', () => {
        const unexpectedPermissionName = 'view:partner_app';
        const unexpectedNamespace = 'partner';

        it('returns false, ie. not disabled, when permission is on current user', () => {
            expect(pipe.transform(expectedPermissionName)).toBe(false);
        });

        it('returns true, ie. is disabled, when permission not on current user', () => {
            expect(pipe.transform(unexpectedPermissionName)).toBe(true);
        });

        it('returns false, ie. not disabled, when namespace is specified and otherwise matches permission', () => {
            expect(pipe.transform(expectedPermissionName, PermissionNamespaceType.Admin)).toBe(false);
        });

        it('returns true, ie. is disabled, when namespace is specified and doesn\'t match permission', () => {
            expect(pipe.transform(expectedPermissionName, unexpectedNamespace)).toBe(true);
        });
    });

    describe('#hasPermission', () => {
        const unexpectedPermissionName = 'view:partner_app';

        it('returns false when permission is on current user', () => {
            expect(pipe.hasPermission(expectedPermissionName, PermissionNamespaceType.Admin));
        });

        it('returns true when permission is missing current user', () => {
            expect(pipe.hasPermission(unexpectedPermissionName, PermissionNamespaceType.Admin));
        });
    });
});
