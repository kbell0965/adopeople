import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { AvatarModule } from 'ng-components.adonis';
import { MaterialPaginatorProvider } from '../shared/common/paginator.provider';
import { MatButtonModule, MatInputModule } from '@angular/material';
import { PermissionDisabledPipe } from './pipes/permission-disabled.pipe';
import { PermittedDirective } from './directives/permitted/permitted.directive';

@NgModule({
  imports: [
    AvatarModule,
    CommonModule,
    MatButtonModule,
    MatInputModule,
    ReactiveFormsModule
  ],
  exports: [
    AvatarModule,
    CommonModule,
    MatButtonModule,
    MatInputModule,
    ReactiveFormsModule,
    PermissionDisabledPipe,
    PermittedDirective
  ],
  providers: [MaterialPaginatorProvider],
  declarations: [PermissionDisabledPipe, PermittedDirective]
})
export class SharedModule { }
