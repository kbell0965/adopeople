import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { Router } from '@angular/router';
import { of, Observable } from 'rxjs';
import { AuthGuard } from './auth.guard';
import { WindowService } from '@pplsi-core/shared-modules/dist/src/services/window.service';
import { AuthenticationService } from 'src/app/services/authentication/authentication.service';

describe('AuthGuard', () => {
  let authGuard: AuthGuard;
  let router: Router;
  let authService: MockAuthenticationService;
  let windowService: WindowService;

  class MockAuthenticationService {
    authenticated(): Observable<boolean> { return of(true); }
  }

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        AuthGuard,
        WindowService,
        { provide: Router, useValue: { navigate: jasmine.createSpy('navigate') } },
        { provide: AuthenticationService, useValue: new MockAuthenticationService() }
      ],
      imports: [HttpClientTestingModule]
    });

    const injector = getTestBed();
    authGuard = injector.get(AuthGuard);
    router = injector.get(Router);
    authService = injector.get(AuthenticationService);
    windowService = injector.get(WindowService);
  });

  describe('canActivate', () => {
    it('if authenticated can go to authenticated route', done => {
      authGuard.canActivate().subscribe(access => {
        expect(access).toBe(true);
        done();
      });
    });

    it('if unauthenticated redirect to login', done => {
      spyOn(authService, 'authenticated').and.returnValue(of(false));

      authGuard.canActivate().subscribe(access => {
        expect(access).toBe(false);
        expect(router.navigate).toHaveBeenCalledWith(['/unauthorized']);
        done();
      });
    });
  });
});
