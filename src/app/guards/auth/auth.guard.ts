import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthenticationService } from 'src/app/services/authentication/authentication.service';
import { tap } from 'rxjs/operators';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private authenticationService: AuthenticationService, private router: Router) { }

  canActivate(): Observable<boolean> {
    return this.authenticationService.authenticated().pipe(
      tap(isAuthenticated => !isAuthenticated && this.router.navigate(['/unauthorized'])));
  }
}
