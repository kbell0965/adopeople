import { Injectable } from '@angular/core';
import {
  CanActivate,
  ActivatedRouteSnapshot
} from '@angular/router';
import { Observable, of, forkJoin } from 'rxjs';
import { map } from 'rxjs/operators';
import { PermissionsService } from '../../services/permissions/permissions.service';
import { PermissionNamespaceType } from '@pplsi-core/datalayer';

@Injectable()
export class PermissionsGuard implements CanActivate {
  readonly VIEW_APP = 'view_people_application';
  constructor(
    private permissionsService: PermissionsService
  ) { }

  canActivate(activatedRouteSnapshot: ActivatedRouteSnapshot): Observable<boolean> {
    const permission = activatedRouteSnapshot.data.permission || this.VIEW_APP;
    const namespace = activatedRouteSnapshot.data.namespace || PermissionNamespaceType.Admin;
    return this.permissionsService.validate(permission, namespace);
  }
}
