import { async, TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { Router } from '@angular/router';
import { of } from 'rxjs';

import { WindowService } from '@pplsi-core/shared-modules/dist/src/services/window.service';
import { PermissionsGuard } from './permissions.guard';
import { PermissionsService } from '../../services/permissions/permissions.service';
import { MeService } from '../../services/me/me.service';
import { MeRepository } from '@pplsi-core/datalayer';
import { PermissionNamespaceType } from '@pplsi-core/datalayer';

describe('PermissionsGuard', () => {
  let permissionsGuard: PermissionsGuard;
  let meService: MeService;
  let router: Router;
  let windowService: WindowService;
  let meRepository: MeRepository;
  const activatedRouteSnapshotWithViewRolesPermission: any = {
    snapshot: {},
    data: { permission: 'view_roles_and_users', namespace: PermissionNamespaceType.Admin }
  };
  const activatedRouteSnapshotFakeWithoutPermissions: any = { snapshot: {}, data: {} };
  let permissionsService: PermissionsService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [
        PermissionsService,
        PermissionsGuard,
        MeService,
        MeRepository,
        WindowService,
        MeRepository,
        {
          provide: Router,
          useValue: { navigate: jasmine.createSpy('navigate') }
        }
      ],
      imports: [HttpClientTestingModule]
    });

    const injector = getTestBed();
    permissionsGuard = injector.get(PermissionsGuard);
    permissionsService = injector.get(PermissionsService);
    meService = injector.get(MeService);
    router = injector.get(Router);
    meRepository = injector.get(MeRepository);
    windowService = injector.get(WindowService);
  }));

  describe('canActivate', () => {
    describe('when checking access to specific route', () => {
      it('returns true when has permission', () => {

        spyOn(permissionsService, 'validate').and.returnValue(of(true));
        permissionsGuard.canActivate(activatedRouteSnapshotWithViewRolesPermission)
          .subscribe(result => expect(result).toBe(true));
      });

      it('returns false without permission', () => {

        spyOn(permissionsService, 'validate').and.returnValue(of(false));
        permissionsGuard.canActivate(activatedRouteSnapshotFakeWithoutPermissions)
          .subscribe(result => expect(result).toBe(false));
      });
    });
  });
});
