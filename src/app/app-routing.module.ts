import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { UnauthorizedComponent } from './shared/components/unauthorized/unauthorized.component';
import { AuthGuard } from './guards/auth/auth.guard';

const routes: Routes = [
  {
    path: '',
    canActivate: [AuthGuard],
    redirectTo: 'members',
    pathMatch: 'full'
  },
  {
    path: 'accounts',
    canActivate: [AuthGuard],
    loadChildren: () => import('./modules/accounts/accounts.module').then(a => a.AccountsModule)
  },
  {
    path: 'password-login',
    component: LoginComponent
  },
  {
    path: 'unauthorized',
    component: UnauthorizedComponent
  },
  {
    path: 'members',
    canActivate: [AuthGuard],
    loadChildren: () => import('./modules/members/members.module').then(m => m.MembersModule)
  },
  {
    path: 'resolutions',
    canActivate: [AuthGuard],
    loadChildren: () => import('./modules/resolutions/resolutions.module').then(m => m.ResolutionsModule)
  },
  {
    path: 'roles',
    canActivate: [AuthGuard],
    loadChildren: () => import('./modules/roles/roles.module').then(m => m.RolesModule)
  },
  {
    path: 'users',
    canActivate: [AuthGuard],
    loadChildren: () => import('./modules/users/users.module').then(m => m.UsersModule)
  },
  {
    path: '**',
    redirectTo: '',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
