import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { MeViewModel } from '../../models/me-view-model';
import { MeService } from '../me/me.service';

@Injectable()
export class PermissionsService {

  constructor(private meService: MeService) { }

  validate(permission, namespace): Observable<boolean> {
    return this.meService.$currentUser().pipe(
      map((me: MeViewModel) => {
        return me && me.hasPermission(permission, namespace);
      })
    );
  }
}
