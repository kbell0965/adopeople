import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { WindowService } from '@pplsi-core/shared-modules/dist/src/services/window.service';
import { of } from 'rxjs';

import { Me, MeRepository, PermissionNamespaceType } from '@pplsi-core/datalayer';
import { PermissionsService } from './permissions.service';
import { MeViewModel } from '../../models/me-view-model';
import { MeService } from '../me/me.service';
import { MeFactory } from '@pplsi-core/factories';

describe('PermissionsService', () => {
  let meRepository: MeRepository;
  let meService: MeService;
  let permissionsService: PermissionsService;
  let me: MeViewModel;
  let meResponse: Me;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        {
          provide: MeService, useValue: {
            $currentUser: () => of(new MeViewModel(MeFactory.build({
              name: 'John Doe',
              permissions: [{ name: 'view_stuff', namespace: PermissionNamespaceType.Admin }]
            })))
          }
        },
        MeRepository,
        PermissionsService,
        WindowService
      ],
      imports: [
        HttpClientTestingModule
      ]
    });

    const injector = getTestBed();
    meService = injector.get(MeService);
    permissionsService = injector.get(PermissionsService);
    meRepository = injector.get(MeRepository);
  });

  describe('#validate', () => {
    it('returns true when user has valid permissions', (done) => {
      meResponse = new Me({ name: 'John Doe', permissions: [{ name: 'view_stuff', namespace: PermissionNamespaceType.Admin }] });
      me = new MeViewModel(meResponse);
      spyOn(me, 'hasPermission').and.returnValue(true);

      permissionsService.validate('view_stuff', PermissionNamespaceType.Admin).subscribe((access) => {
        expect(access).toBeTruthy();
        done();
      });
    });

    it('calls false when user has invalid permissions', (done) => {
      meResponse = new Me({
        name: 'John Doe', permissions: [{
          name: 'view_people_application',
          namespace: PermissionNamespaceType.Admin
        }]
      });
      me = new MeViewModel(meResponse);
      spyOn(meService, '$currentUser').and.returnValue(of(me));

      permissionsService.validate('view_stuff', PermissionNamespaceType.Admin).subscribe((access) => {
        expect(access).toBeFalsy();
        done();
      });
    });
  });
});
