import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';

import { Me, MeRepository } from '@pplsi-core/datalayer';
import { MeService } from './me.service';
import { MeViewModel } from '../../models/me-view-model';
import { MeFactory } from '@pplsi-core/factories';
import { of } from 'rxjs';

describe('MeService', () => {
  let meRepository: MeRepository;
  let meService: MeService;
  const me: Me = MeFactory.build({ name: 'John Doe' });

  class MockMeRepo {
    get() { return of(me); }
  }

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [{ provide: MeRepository, useValue: new MockMeRepo() }, MeService],
      imports: [
        HttpClientTestingModule
      ]
    });

    const injector = getTestBed();
    meRepository = injector.get(MeRepository);
    meService = injector.get(MeService);
  });

  describe('#$currentUser', () => {
    it('currentUser stream response is of type MeViewModel and is equal to service\'s source MeViewModel', done => {
      meService.$currentUser().subscribe((resp) => {
        expect(resp instanceof MeViewModel).toBe(true);
        expect(resp.areEqual(meService['_base'])).toBe(true);
        done();
      });
    });
  });

  describe('#ngOnDestroy', () => {
    beforeEach(() => {
      spyOn((meService as any)._destroyed, 'next');
      spyOn((meService as any)._destroyed, 'complete');
      meService.ngOnDestroy();
    });

    it('calls #next on `_destroyed`', () => expect((meService as any)._destroyed.next).toHaveBeenCalled());
    it('calls #complete on `_destroyed`', () => expect((meService as any)._destroyed.complete).toHaveBeenCalled());
  });
});
