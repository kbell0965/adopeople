import { Injectable, OnDestroy } from '@angular/core';
import { Observable, Subject, of, from } from 'rxjs';
import { map, takeUntil } from 'rxjs/operators';
import { MeRepository, Me } from '@pplsi-core/datalayer';
import { MeViewModel } from '../../models/me-view-model';

@Injectable()
export class MeService implements OnDestroy {
  private _base: MeViewModel;
  private _me: Promise<MeViewModel>;
  private _destroyed = new Subject<void>();

  constructor(private meRepository: MeRepository) {
    this._me = this.meRepository.get().pipe(
      map(me => {
        this._base = !!me ? new MeViewModel(me) : null;
        return this._base;
      }),
      takeUntil(this._destroyed)
    ).toPromise();
  }

  public $currentUser(): Observable<MeViewModel> {
    return (this._base ? of(this._base) : from(this._me))
      .pipe(takeUntil(this._destroyed));
  }

  ngOnDestroy() {
    this._destroyed.next();
    this._destroyed.complete();
  }
}
