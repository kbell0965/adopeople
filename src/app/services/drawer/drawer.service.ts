import { Injectable, InjectionToken, Injector } from '@angular/core';
import { MatSidenav } from '@angular/material';
import { PortalInjector, ComponentPortal } from '@angular/cdk/portal';
import { Observable, BehaviorSubject } from 'rxjs';
import { skip } from 'rxjs/operators';

export const DRAWER_DATA = new InjectionToken<{}>('DrawerData');

@Injectable()
export class DrawerService {
  private drawer: MatSidenav;
  private drawerContentReference: any;
  private drawerClosedSubject: BehaviorSubject<any>;

  constructor(private injector: Injector) { }

  setDrawer(drawer: MatSidenav): void {
    this.drawer = drawer;
  }

  setDrawerContentReference(drawerContentReference): void {
    this.drawerContentReference = drawerContentReference;
  }

  close(payload: any = null): void {
    this.drawer.close();
    this.drawerContentReference.detach();

    if (this.drawerClosedSubject) {
      this.drawerClosedSubject.next(payload);
    }
  }

  open(component: any, data?: any): void {
    this.drawerContentReference.detach();
    const componentPortal = new ComponentPortal(component, null, this.createInjector(data || {}));
    this.drawerContentReference.attach(componentPortal);

    this.drawer.open();
  }

  open$(component: any, data?: any): Observable<any> {
    this.open(component, data);
    return (this.drawerClosedSubject = new BehaviorSubject<any>(null)).asObservable().pipe(skip(1));
  }

  private createInjector(data): PortalInjector {
    const injectorTokens = new WeakMap<any, any>([
      [DRAWER_DATA, data]
    ]);

    return new PortalInjector(this.injector, injectorTokens);
  }
}
