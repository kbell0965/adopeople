import { TestBed, getTestBed } from '@angular/core/testing';
import { MatSidenavModule, MatSidenav } from '@angular/material';

import { DrawerService } from './drawer.service';
import { Observable, Subscription } from 'rxjs';

describe('DrawerService', () => {
  let injector: TestBed;
  let drawerService: DrawerService;
  const fakeDrawer = {
    open: () => { },
    close: () => { }
  } as MatSidenav;
  const fakeDrawerContentReference = {
    attach: () => { },
    detach: () => { }
  };
  class FakeComponent { }

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        MatSidenavModule
      ],
      providers: [
        DrawerService
      ]
    });

    injector = getTestBed();
    drawerService = injector.get(DrawerService);
    drawerService.setDrawer(fakeDrawer);
    drawerService.setDrawerContentReference(fakeDrawerContentReference);
  });

  describe('#open', () => {
    beforeEach(() => {
      spyOn(fakeDrawer, 'open');
      spyOn(fakeDrawerContentReference, 'attach');
      spyOn(fakeDrawerContentReference, 'detach');
      drawerService.open(FakeComponent);
    });

    it('calls #open on the drawer 1 time', () => {
      expect(fakeDrawer.open).toHaveBeenCalledTimes(1);
    });

    it('calls #detach on the drawerContentReference 1 time', () => {
      expect(fakeDrawerContentReference.detach).toHaveBeenCalledTimes(1);
    });

    it('calls #attach on the drawerContentReference 1 time', () => {
      expect(fakeDrawerContentReference.attach).toHaveBeenCalledTimes(1);
    });
  });

  describe('#open$', () => {
    let drawerSubscription: Subscription;
    let openResult: any = 'initial value';

    beforeEach(() => {
      spyOn(fakeDrawer, 'open');
      spyOn(fakeDrawerContentReference, 'attach');
      spyOn(fakeDrawerContentReference, 'detach');
      drawerSubscription = drawerService.open$(FakeComponent).subscribe(result => openResult = result);
    });

    it('calls #open on the drawer 1 time', () => {
      expect(fakeDrawer.open).toHaveBeenCalledTimes(1);
    });

    it('calls #detach on the drawerContentReference 1 time', () => {
      expect(fakeDrawerContentReference.detach).toHaveBeenCalledTimes(1);
    });

    it('calls #attach on the drawerContentReference 1 time', () => {
      expect(fakeDrawerContentReference.attach).toHaveBeenCalledTimes(1);
    });

    it('observable does not emit a value when drawer is opened', () => {
      expect(openResult).toBe('initial value');
    });
  });

  describe('#close', () => {
    beforeEach(() => {
      spyOn(fakeDrawer, 'close');
    });

    it('calls #close on the drawer', () => {
      drawerService.close();
      expect(fakeDrawer.close).toHaveBeenCalled();
    });

    it('can #close after opening with #open', () => {
      drawerService.open(FakeComponent);
      drawerService.close();
      expect(fakeDrawer.close).toHaveBeenCalled();
    });

    it('calls #close and callback occurs with non-null payload following #open$', () => {
      const wasAssigned = jasmine.createSpy('wasAssigned');

      drawerService.open$(FakeComponent).subscribe(result => wasAssigned(result));
      drawerService.close(true);

      expect(fakeDrawer.close).toHaveBeenCalled();
      expect(wasAssigned).toHaveBeenCalledWith(true);
    });
  });
});
