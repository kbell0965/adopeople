import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { map, tap, catchError } from 'rxjs/operators';
import { Credentials } from '../../models/credentials';
import { MeService } from '../me/me.service';
import { WindowService } from '@pplsi-core/shared-modules/dist/src/services/window.service';

@Injectable()
export class AuthenticationService {
  public hasLoggedIn: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  constructor(private http: HttpClient, private meService: MeService, private windowService: WindowService) { }

  authenticate(credentials: Credentials): Observable<object> {
    return this.http.post('/auth/tokens', credentials).pipe(
      tap(() => this.hasLoggedIn.next(true))
    );
  }

  logout(): Observable<object> {
    return this.http.get('/auth/logout').pipe(
      tap(() => this.hasLoggedIn.next(false))
    );
  }

  authenticated(): Observable<boolean> {
    return this.meService.$currentUser().pipe(
      catchError(() => {
        this.windowService.replace('/auth/legalshield?strategy=okta');
        return of(false);
      }),
      map(me => !!me));
  }
}
