import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpTestingController, HttpClientTestingModule } from '@angular/common/http/testing';
import { flatMap } from 'rxjs/operators';
import { of } from 'rxjs';

import { Credentials } from '../../models/credentials';
import { AuthenticationService } from './authentication.service';
import { MeService } from '../me/me.service';
import { MeFactory } from '@pplsi-core/factories';
import { MeViewModel } from 'src/app/models/me-view-model';
import { WindowService } from '@pplsi-core/shared-modules/dist/src/services/window.service';

describe('AuthenticationService', () => {
  let http: HttpTestingController;
  let authenticationService: AuthenticationService;
  let meService: MeService;

  class MockMeService {
    $currentUser() { return of(new MeViewModel(MeFactory.build({ full_name: 'John Doe' }))); }
  }

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        AuthenticationService,
        WindowService,
        { provide: MeService, useValue: new MockMeService() }
      ],
      imports: [
        HttpClientTestingModule
      ]
    });

    const injector = getTestBed();
    http = injector.get(HttpTestingController);
    authenticationService = injector.get(AuthenticationService);
    meService = injector.get(MeService);
  });

  describe('#authenticate', () => {
    const credentials: Credentials = new Credentials({
      username: 'test@test.com',
      password: 'P@sSw0rD'
    });
    const response: object = {
      'accessToken': 'some access token',
      'idToken': {
        'some-id-token-fields': 'some id token field data'
      }
    };

    it('makes a POST request to the auth/tokens endpoint', done => {
      authenticationService.authenticate(credentials).subscribe(returnedResponse => {
        expect(returnedResponse).toEqual(response);
        done();
      });

      const request = http.expectOne('/auth/tokens');
      expect(request.request.method).toBe('POST');
      expect(request.request.body).toEqual(credentials);

      request.flush(response);
    });

    it('marks hasLoggedIn as true', done => {
      authenticationService.authenticate(credentials).pipe(
        flatMap(() => authenticationService.hasLoggedIn)
      ).subscribe((hasLoggedIn: boolean) => {
        expect(hasLoggedIn).toBe(true);
        done();
      });

      http.expectOne('/auth/tokens').flush(response);
    });
  });

  describe('#logout', () => {
    it('makes a GET request to the auth/logout endpoint', done => {
      authenticationService.hasLoggedIn.next(true);

      authenticationService.logout().subscribe(() => {
        authenticationService.hasLoggedIn.subscribe(value =>
          expect(value).toBe(false)
        );
        done();
      });

      const request = http.expectOne('/auth/logout');
      expect(request.request.method).toBe('GET');

      request.flush({});
    });
  });

  describe('#authenticated', () => {
    it('returns true if current user is truthy', done => {
      authenticationService.authenticated().subscribe((authenticated: boolean) => {
        expect(authenticated).toBe(true);
        done();
      });
    });

    it('returns false if current user is falsy', done => {
      spyOn(meService, '$currentUser').and.returnValue(of(null));

      authenticationService.authenticated().subscribe((authenticated: boolean) => {
        expect(authenticated).toBe(false);
        done();
      });
    });
  });
});
