import { TestBed, getTestBed } from '@angular/core/testing';

import { ResolutionViewModel } from '../../modules/resolutions/models/resolution-view-model';
import { PartnerFactories, PeopleFactories } from '@pplsi-core/factories';
import { DataService } from './data.service';
import { PeopleModels } from '@pplsi-core/datalayer';

describe('DataService', () => {
  let injector: TestBed;
  let dataService: DataService;
  const fakeRole: object = PeopleFactories.RoleFactory.build();
  const role: PeopleModels.Role = new PeopleModels.Role(fakeRole);

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DataService]
    });

    injector = getTestBed();
    dataService = injector.get(DataService);
  });

  describe('getters', () => {
    describe('#resolution', () => {
      it('returns the resolution behavior subject', () => {
        dataService.resolution.subscribe((resolution: ResolutionViewModel) => expect(resolution).toBeUndefined());
      });
    });

    describe('#role', () => {
      it('unset role is undefined', () => {
        dataService.role.subscribe((dataRole: PeopleModels.Role) => expect(dataRole).toBeUndefined());
      });

      it('role subscription returns a role', () => {
        dataService.setRole(role);
        dataService.role.subscribe((dataRole: PeopleModels.Role) => expect(dataRole).toBe(role));
      });
    });
  });

  describe('#setResolution', () => {
    it('updates the resolution behavior subject', done => {
      const resolution: ResolutionViewModel = new ResolutionViewModel(PartnerFactories.ResolutionFactory.build());
      dataService.setResolution(resolution);
      dataService.resolution.subscribe((updatedResolution: ResolutionViewModel) => {
        expect(updatedResolution).toEqual(resolution);
        done();
      });
    });
  });

  describe('#setRole', () => {
    it('updates the role behavior subject', done => {
      dataService.setRole(role);
      dataService.role.subscribe((updatedRole: PeopleModels.Role) => {
        expect(updatedRole).toEqual(role);
        done();
      });
    });
  });
});
