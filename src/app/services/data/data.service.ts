import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

import { ResolutionViewModel } from '../../modules/resolutions/models/resolution-view-model';
import { PeopleModels } from '@pplsi-core/datalayer/dist/@pplsi-core/datalayer/public_api';

@Injectable()
export class DataService {
  private _resolution: BehaviorSubject<ResolutionViewModel> = new BehaviorSubject(undefined);
  private _role: BehaviorSubject<PeopleModels.Role> = new BehaviorSubject(undefined);

  constructor() { }

  get resolution(): BehaviorSubject<ResolutionViewModel> {
    return this._resolution;
  }

  setResolution(resolution: ResolutionViewModel): void {
    this._resolution.next(resolution);
  }

  get role(): BehaviorSubject<PeopleModels.Role> {
    return this._role;
  }

  setRole(role: PeopleModels.Role): void {
    this._role.next(role);
  }
}
