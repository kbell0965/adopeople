import { async, ComponentFixture, TestBed, getTestBed } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { RolesPermissionsComponent } from './roles-permissions.component';
import { DrawerService } from 'src/app/services/drawer/drawer.service';
import { RoleEditComponent } from 'src/app/shared/components/role-edit/role-edit.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import {
  MatTabsModule,
  MatIconModule
} from '@angular/material';
import { RouterTestingModule } from '@angular/router/testing';
import { Router } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';
import { MeService } from 'src/app/services/me/me.service';
import { of } from 'rxjs';

describe('RolePermissionsComponent', () => {
  let component: RolesPermissionsComponent;
  let fixture: ComponentFixture<RolesPermissionsComponent>;
  let injector: TestBed;
  let drawerService: DrawerService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        RolesPermissionsComponent,
        RoleEditComponent
      ],
      imports: [
        RouterTestingModule,
        ReactiveFormsModule,
        FormsModule,
        MatTabsModule,
        MatIconModule,
        SharedModule
      ],
      providers: [
        DrawerService,
        {
          provide: MeService,
          useValue: {
            get: () => of({ hasPermission: () => false })
          }
        }
      ],
      schemas: [
        CUSTOM_ELEMENTS_SCHEMA
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RolesPermissionsComponent);

    injector = getTestBed();

    drawerService = injector.get(DrawerService);

    component = fixture.componentInstance;
    fixture.detectChanges();

    spyOn(drawerService, 'open');
  });

  describe('#ngOnDestroy', () => {
    it('unsubscribes routerEventSubscription', () => {
      spyOn(component.routerEventSubscription, 'unsubscribe');
      component.ngOnDestroy();

      expect(component.routerEventSubscription.unsubscribe).toHaveBeenCalled();
    });
  });

  describe('#loadCreate', () => {
    it('should call openDrawer when loadCreate is called', () => {
      spyOn(component, 'openDrawer');
      component.loadCreate();
      expect(component.openDrawer).toHaveBeenCalled();
    });
  });

  describe('#openDrawer', () => {
    it('should open drawer and supply RoleEditComponent', () => {
      component.openDrawer();
      expect(drawerService.open).toHaveBeenCalledWith(RoleEditComponent);
    });
  });

  describe('#syncActiveTabIndex', () => {
    it('updates activeTabIndex to tab corresponding to router url', () => {
      const router = injector.get(Router);
      component.tabData[1].link = router.url;
      component.syncActiveTabIndex();

      expect(component.activeTabIndex).toBe(1);
    });
  });
});
