import { async, ComponentFixture, TestBed, getTestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { By } from '@angular/platform-browser';
import { RolesPermissionsComponent } from './roles-permissions.component';
import { RolesIndexComponent } from '../roles-index/roles-index.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import {
  MatButtonModule,
  MatInputModule,
  MatToolbarModule,
  MatTabsModule,
  MatIconModule
} from '@angular/material';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTableModule } from '@angular/material/table';
import { of } from 'rxjs';
import { MatGridListModule } from '@angular/material/grid-list';
import { PeopleRoleRepository, PplsiCoreDatalayerModule, PeopleModels } from '@pplsi-core/datalayer';
import { LocalizationService } from '@pplsi-core/shared-modules/dist/src/services/localization.service';
import { SearchLayoutModule } from 'src/app/shared/components/search-layout/search-layout.module';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { DrawerService } from 'src/app/services/drawer/drawer.service';
import { DataService } from 'src/app/services/data/data.service';
import { SharedModule } from 'src/app/shared/shared.module';
import { MeService } from 'src/app/services/me/me.service';

describe('RolesPermissionsComponent.Template', () => {
  let component: RolesPermissionsComponent;
  let fixture: ComponentFixture<RolesPermissionsComponent>;
  let injector: TestBed;
  let roleRepository: PeopleRoleRepository;
  let meService: MeService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        ReactiveFormsModule,
        RouterTestingModule.withRoutes([
          { path: 'roles/users', component: RolesPermissionsComponent }
        ]),
        MatTabsModule,
        MatIconModule,
        MatPaginatorModule,
        MatButtonModule,
        MatIconModule,
        MatInputModule,
        MatToolbarModule,
        MatTableModule,
        MatToolbarModule,
        MatGridListModule,
        MatInputModule,
        BrowserAnimationsModule,
        SearchLayoutModule,
        SharedModule,
        PplsiCoreDatalayerModule.forRoot()
      ],
      declarations: [
        RolesPermissionsComponent,
        RolesIndexComponent
      ],
      providers: [
        LocalizationService,
        {
          provide: 'Window',
          useValue: window
        },
        {
          provide: 'localizations',
          useValue: [
            { locale: 'en-US', currencyCode: 'USD', dateFormat: 'MM/dd/yyyy' },
            { locale: 'en-GB', currencyCode: 'GBP', dateFormat: 'dd/MM/yyyy' },
            { locale: 'en-CA', currencyCode: 'CAD', dateFormat: 'yyyy-MM-dd' }
          ]
        },
        {
          provide: 'defaultLocale',
          useValue: 'en-US'
        },
        DrawerService,
        DataService,
        {
          provide: MeService,
          useValue: {
            get: () => of({ hasPermission: () => true })
          }
        }
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
      .compileComponents();
  }));

  describe('With all permissions', () => {
    beforeEach(() => {
      fixture = TestBed.createComponent(RolesPermissionsComponent);
      injector = getTestBed();

      roleRepository = injector.get(PeopleRoleRepository);
      spyOn(roleRepository, 'findAll').and.returnValue(of(
        <PeopleModels.RolesWithPagination> { roles: [], total_length: 0 }));

      component = fixture.componentInstance;
      meService = injector.get(MeService);
      fixture.detectChanges();
    });

    describe('Header', () => {
      it('has inner text contents', () => {
        const el = fixture.debugElement.query(By.css('h2'));
        expect(el.nativeElement.innerText).toBe('Roles and Permissions');
      });
    });

    describe('secondary button', () => {
      it('should call loadCreate when clicked', () => {
        spyOn(component, 'loadCreate');
        component.activeTabIndex = 0;
        fixture.detectChanges();
        fixture.debugElement.query(By.css('.secondary')).nativeElement.click();
        expect(component.loadCreate).toHaveBeenCalled();
      });

      it('should be visible if tab data has addButtonClick', () => {
        component.activeTabIndex = 0;
        component.tabData[component.activeTabIndex].addButtonClick = () => { };

        fixture.detectChanges();
        expect(fixture.debugElement.query(By.css('.secondary'))).toBeTruthy();
      });

      it('should be hidden if tab data without addButtonClick', () => {
        component.activeTabIndex = 0;
        component.tabData[component.activeTabIndex].addButtonClick = null;

        fixture.detectChanges();
        expect(fixture.debugElement.query(By.css('.secondary'))).toBeFalsy();
      });

      it('text should match button text specified within tab data', () => {
        component.activeTabIndex = 0;
        component.tabData[component.activeTabIndex].addButtonClick = () => { };
        component.tabData[component.activeTabIndex].addButtonText = '123456';

        fixture.detectChanges();
        expect(fixture.debugElement.query(By.css('.secondary')).nativeElement.innerText).toBe('123456');
      });
    });

    describe('Navigation Tabs', () => {
      it('are all rendered with labels matching tab text', () => {
        const elements = fixture.debugElement.queryAll(By.css('.container nav a'));
        expect(elements.length).toBe(component.tabData.length);

        elements.forEach((el, index) => {
          expect(el.nativeElement.innerText).toBe(component.tabData[index].label);
        });
      });

      it('clicking tab updates active tab index', () => {
        const elements = fixture.debugElement.queryAll(By.css('.container nav a'));
        elements[1].nativeElement.click();
        expect(component.activeTabIndex).toBe(1);
      });
    });
  });
});
