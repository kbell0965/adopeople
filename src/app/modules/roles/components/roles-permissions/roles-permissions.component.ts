import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, NavigationStart } from '@angular/router';
import { DrawerService } from 'src/app/services/drawer/drawer.service';
import { RoleEditComponent } from 'src/app/shared/components/role-edit/role-edit.component';
import { Subscription } from 'rxjs';

@Component({
  selector: 'roles-permissions-component',
  templateUrl: './roles-permissions.component.html',
  styleUrls: ['roles-permissions.component.scss']
})
export class RolesPermissionsComponent implements OnDestroy {
  readonly DEFAULT_ADD_TEXT = 'ADD';

  activeTabIndex: number;
  showButton: boolean;
  routerEventSubscription: Subscription;

  tabData = [
    {
      label: 'Roles',
      link: '/roles/list',
      addButtonClick: () => this.loadCreate(),
      addButtonText: 'ADD ROLE'
    },
    { label: 'Users', link: '/roles/users' }
  ];

  constructor(private router: Router,
    private drawerService: DrawerService) {
    this.routerEventSubscription = this.router.events.subscribe(event => {
      if (event instanceof NavigationStart) {
        this.syncActiveTabIndex();
      }
    });

    this.syncActiveTabIndex();
  }

  ngOnDestroy() {
    this.routerEventSubscription.unsubscribe();
  }

  loadCreate = (): void => this.openDrawer();

  openDrawer = (): void => this.drawerService.open(RoleEditComponent);

  syncActiveTabIndex = (): void => {
    this.activeTabIndex = this.tabData.indexOf(this.tabData.find(tab => tab.link === this.router.url));
  }
}
