import { Component, Input } from '@angular/core';

@Component({
  selector: 'role-expansion-panel',
  templateUrl: './role-expansion-panel.component.html',
  styleUrls: ['./role-expansion-panel.component.scss']
})
export class RoleExpansionPanelComponent {
  @Input() public title: string;
  @Input() public description: string;
}
