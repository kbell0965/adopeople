import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RoleExpansionPanelComponent } from './role-expansion-panel.component';
import { By } from '@angular/platform-browser';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { MatExpansionModule } from '@angular/material';

describe('RoleExpansionPanelComponent', () => {
  let component: RoleExpansionPanelComponent;
  let fixture: ComponentFixture<RoleExpansionPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [MatExpansionModule],
      declarations: [RoleExpansionPanelComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
      .compileComponents();
  }));


  beforeEach(() => {
    fixture = TestBed.createComponent(RoleExpansionPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should properly render expansion header title', () => {
    component.title = 'Title Test';
    fixture.detectChanges();

    const el = fixture.debugElement.query(By.css('.role-expansion-title'));
    expect(el.nativeElement.textContent).toBe('Title Test');
  });

  it('should properly render expansion header description', () => {
    component.description = 'Title Description';
    fixture.detectChanges();

    const el = fixture.debugElement.query(By.css('.role-expansion-description'));
    expect(el.nativeElement.textContent).toBe('Title Description');
  });

});
