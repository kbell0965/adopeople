import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RolesDetailsFooterComponent } from './roles-details-footer.component';
import { MatToolbarModule } from '@angular/material';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

describe('RolesDetailsFooterComponent', () => {
  let component: RolesDetailsFooterComponent;
  let fixture: ComponentFixture<RolesDetailsFooterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RolesDetailsFooterComponent],
      imports: [MatToolbarModule, BrowserAnimationsModule],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RolesDetailsFooterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should toggle state', () => {
    component.toggled = false;
    component.toggle();
    expect(component.toggled).toBe(true);
  });
});
