import { Component, Input } from '@angular/core';
import { trigger, state, style, transition, animate } from '@angular/animations';

@Component({
  selector: 'roles-details-footer',
  templateUrl: './roles-details-footer.component.html',
  styleUrls: ['./roles-details-footer.component.scss'],
  animations: [
    trigger('openClose', [
      state('false', style({
        height: '0px',
        opacity: '0'
      })),
      state('true', style({
        height: '75px',
        opacity: '1'
      })),
      transition('true => false', [
        animate('.3s')
      ]),
      transition('false => true', [
        animate('.3s')
      ])
    ])
  ]
})
export class RolesDetailsFooterComponent {
  @Input() public toggled = true;

  public toggle() {
    this.toggled = !this.toggled;
  }
}
