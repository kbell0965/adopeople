import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-grouped-permission-header',
  templateUrl: './grouped-permission-header.component.html',
  styleUrls: ['./grouped-permission-header.component.scss']
})
export class GroupedPermissionHeaderComponent {
  @Input() headings: string[] = [];
  @Input() title: string;

  constructor() { }
}
