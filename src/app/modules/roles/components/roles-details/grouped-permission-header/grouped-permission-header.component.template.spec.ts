import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GroupedPermissionHeaderComponent } from './grouped-permission-header.component';
import { By } from '@angular/platform-browser';

describe('GroupedPermissionHeaderComponent.Template', () => {
  let component: GroupedPermissionHeaderComponent;
  let fixture: ComponentFixture<GroupedPermissionHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [GroupedPermissionHeaderComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GroupedPermissionHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  describe('headings', () => {
    it('does not display column headers if headings are undefined', () => {
      component.headings = undefined;
      expect(fixture.debugElement.query(By.css('.grouped-permission-heading span'))).toBeNull();
    });

    it('does not display column headers if headings are undefined', () => {
      component.headings = [];
      expect(fixture.debugElement.query(By.css('.grouped-permission-heading span'))).toBeNull();
    });

    it('displays heading if one is specified', () => {
      component.headings = ['test_heading'];
      fixture.detectChanges();
      const el = fixture.debugElement.query(By.css('.grouped-permission-heading span'));

      expect(el.nativeElement.innerText).toBe('TEST_HEADING');
    });

    it('displays multiple headings in order if multiple headings are specified', () => {
      component.headings = ['1', '2', '3', 'lets go'];
      fixture.detectChanges();
      const els = fixture.debugElement.queryAll(By.css('.grouped-permission-heading span'));

      expect(els[0].nativeElement.innerText).toBe('1');
      expect(els[1].nativeElement.innerText).toBe('2');
      expect(els[2].nativeElement.innerText).toBe('3');
      expect(els[3].nativeElement.innerText).toBe('LETS GO');
    });
  });

  describe('title', () => {
    it('displays permission group title if specified', () => {
      component.title = 'hello title';
      fixture.detectChanges();
      expect(fixture.debugElement.query(By.css('.grouped-permission-title')).nativeElement.innerText).toBe('HELLO TITLE');
    });

    it('does not display permission group title if empty string', () => {
      component.title = '';
      expect(fixture.debugElement.query(By.css('.grouped-permission-title'))).toBeNull();
    });

    it('does not display permission group title if undefined', () => {
      component.title = undefined;
      expect(fixture.debugElement.query(By.css('.grouped-permission-title'))).toBeNull();
    });
  });
});
