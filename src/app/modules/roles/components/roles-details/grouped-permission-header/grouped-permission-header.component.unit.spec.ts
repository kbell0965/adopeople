import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GroupedPermissionHeaderComponent } from './grouped-permission-header.component';

describe('GroupedPermissionHeaderComponent', () => {
  let component: GroupedPermissionHeaderComponent;
  let fixture: ComponentFixture<GroupedPermissionHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [GroupedPermissionHeaderComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GroupedPermissionHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  describe('headings', () => {
    it('are mutable/accessible', () => {
      component.headings = ['hello'];
      expect(component.headings[0]).toBe('hello');
    });

    it('no exceptions when headings are undefined', () => {
      component.headings = undefined;
      fixture.detectChanges();
      expect(fixture).toBeTruthy();
    });
  });
});
