import { Component, Input, Output, EventEmitter } from '@angular/core';
import { PeopleModels } from '@pplsi-core/datalayer';

@Component({
  selector: 'app-grouped-permission-row',
  templateUrl: './grouped-permission-row.component.html',
  styleUrls: ['./grouped-permission-row.component.scss']
})
export class GroupedPermissionRowComponent {
  @Input() public permissions: string[] = [];
  @Input() public title = '';
  @Input() public rolePermissions: PeopleModels.Permission[] = [];
  @Output() public permissionChange: EventEmitter<string> = new EventEmitter<string>();

  constructor() { }

  public hasPermission(permission: string): boolean {
    return !!(this.rolePermissions && !!this.rolePermissions.map(p => p.name).includes(permission));
  }
}
