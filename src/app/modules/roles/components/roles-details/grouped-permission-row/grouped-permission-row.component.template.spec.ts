import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GroupedPermissionRowComponent } from './grouped-permission-row.component';
import { MatCheckboxModule } from '@angular/material';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { By } from '@angular/platform-browser';
import { PeopleFactories } from '@pplsi-core/factories';

describe('GroupedPermissionRowComponent.Template', () => {
  let component: GroupedPermissionRowComponent;
  let fixture: ComponentFixture<GroupedPermissionRowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [GroupedPermissionRowComponent],
      imports: [
        MatCheckboxModule
      ],
      schemas: [
        CUSTOM_ELEMENTS_SCHEMA
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GroupedPermissionRowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  describe('title', () => {
    it('displays permission group title if specified', () => {
      component.title = 'hello title';
      fixture.detectChanges();
      expect(fixture.debugElement.query(By.css('.col-dk-4 span')).nativeElement.innerText).toBe('hello title');
    });

    it('does not display permission group title if empty string', () => {
      component.title = '';
      expect(fixture.debugElement.query(By.css('.col-dk-4 span')).nativeElement.innerText).toBe('');
    });

    it('does not display permission group title if undefined', () => {
      component.title = undefined;
      expect(fixture.debugElement.query(By.css('.col-dk-4 span')).nativeElement.innerText).toBe('');
    });
  });

  describe('permission checkboxes', () => {
    it('does not display if permissions are not specified', () => {
      expect(fixture.debugElement.query(By.css('mat-checkbox'))).toBeNull();
    });

    it('displays checkbox if one permission is specified', () => {
      component.permissions = ['foo'];
      fixture.detectChanges();

      const el = fixture.debugElement.query(By.css('.grouped-permission-checkbox mat-checkbox'));
      expect(el).toBeTruthy();
    });

    it('displays multiple checkboxes if multiple controls are specified', () => {
      component.permissions = ['ladadee', 'ladadaa'];
      fixture.detectChanges();

      const els = fixture.debugElement.queryAll(By.css('.grouped-permission-checkbox mat-checkbox'));
      expect(els.length).toBe(2);
    });

    it('is checked if hasPermission', () => {
      component.permissions = ['asdf'];
      component.rolePermissions = [PeopleFactories.PermissionFactory.build({ name: 'asdf' })];

      fixture.detectChanges();
      expect(fixture.debugElement.query(By.css('mat-checkbox')).classes['mat-checkbox-checked']).toBeTruthy();
    });

    it('is unchecked if not hasPermission', () => {
      component.permissions = ['asdf'];
      component.rolePermissions = [PeopleFactories.PermissionFactory.build({ name: 'qwerty' })];

      fixture.detectChanges();
      expect(fixture.debugElement.query(By.css('mat-checkbox')).classes['mat-checkbox-checked']).toBeFalsy();
    });
  });
});
