import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GroupedPermissionRowComponent } from './grouped-permission-row.component';
import { MatCheckboxModule } from '@angular/material';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { PeopleFactories } from '@pplsi-core/factories';

describe('GroupedPermissionRowComponent', () => {
  let component: GroupedPermissionRowComponent;
  let fixture: ComponentFixture<GroupedPermissionRowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [GroupedPermissionRowComponent],
      imports: [
        MatCheckboxModule
      ],
      schemas: [
        CUSTOM_ELEMENTS_SCHEMA
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GroupedPermissionRowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  describe('title', () => {
    it('are mutable/accessible', () => {
      component.title = 'hello';
      expect(component.title).toBe('hello');
    });

    it('default is an empty string', () => { expect(component.title).toBe(''); });
  });

  describe('#hasPermssion', () => {
    it('returns false when no permissions are specified', () => {
      component.rolePermissions = [];
      expect(component.hasPermission('view:np_incomplete')).toBe(false);
    });

    it('returns false when no permissions are undefined', () => {
      component.rolePermissions = undefined;
      expect(component.hasPermission('view:np_incomplete')).toBe(false);
    });

    it('returns true when no one permission is specified', () => {
      component.rolePermissions = [PeopleFactories.PermissionFactory.build({ name: 'read:the_newspaper' })];
      expect(component.hasPermission('read:the_newspaper')).toBe(true);
    });

    it('returns true when greater than one permission is specified', () => {
      component.rolePermissions = [
        PeopleFactories.PermissionFactory.build({ name: 'view:morning_news' }),
        PeopleFactories.PermissionFactory.build({ name: 'view:weather_forecast' })
      ];
      expect(component.hasPermission('view:morning_news')).toBe(true);
    });
  });
});
