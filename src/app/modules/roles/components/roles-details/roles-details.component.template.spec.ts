import { async, ComponentFixture, TestBed, getTestBed } from '@angular/core/testing';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { By } from '@angular/platform-browser';
import { RoleDetailsComponent } from './roles-details.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import {
  MatButtonModule,
  MatInputModule,
  MatToolbarModule,
  MatTabsModule,
  MatIconModule,
  MatCheckboxModule,
  MatDialogModule,
  MAT_DIALOG_DATA
} from '@angular/material';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTableModule } from '@angular/material/table';
import { of } from 'rxjs';
import { MatGridListModule } from '@angular/material/grid-list';
import { DrawerService } from '../../../../services/drawer/drawer.service';
import { PeopleModels, PeopleRoleRepository, PplsiCoreDatalayerModule } from '@pplsi-core/datalayer';
import { LocalizationService } from '@pplsi-core/shared-modules/dist/src/services/localization.service';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RolesDetailsFooterComponent } from './roles-details-footer/roles-details-footer.component';
import { RoleExpansionPanelComponent } from './role-expansion-panel/role-expansion-panel.component';
import { PeopleFactories } from '@pplsi-core/factories';
import { DataService } from 'src/app/services/data/data.service';

const getRole = (cfg) => new PeopleModels.Role(PeopleFactories.RoleFactory.build(cfg));
const getPermission = (cfg) => new PeopleModels.Permission(PeopleFactories.PermissionFactory.build(cfg));

describe('RoleDetailsComponent.Template', () => {
  let component: RoleDetailsComponent;
  let fixture: ComponentFixture<RoleDetailsComponent>;
  let injector: TestBed;
  let roleRepository: PeopleRoleRepository;
  let dataService: DataService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        ReactiveFormsModule,
        RouterTestingModule,
        MatTabsModule,
        MatIconModule,
        MatPaginatorModule,
        MatButtonModule,
        MatIconModule,
        MatInputModule,
        MatToolbarModule,
        MatTableModule,
        MatToolbarModule,
        MatGridListModule,
        MatInputModule,
        MatCheckboxModule,
        BrowserAnimationsModule,
        FormsModule,
        MatDialogModule,
        MatCheckboxModule,
        PplsiCoreDatalayerModule.forRoot()
      ],
      declarations: [
        RoleDetailsComponent, RolesDetailsFooterComponent, RoleExpansionPanelComponent
      ],
      providers: [
        LocalizationService,
        {
          provide: 'Window',
          useValue: window
        },
        {
          provide: 'localizations',
          useValue: [
            { locale: 'en-US', currencyCode: 'USD', dateFormat: 'MM/dd/yyyy' },
            { locale: 'en-GB', currencyCode: 'GBP', dateFormat: 'dd/MM/yyyy' },
            { locale: 'en-CA', currencyCode: 'CAD', dateFormat: 'yyyy-MM-dd' }
          ]
        },
        {
          provide: 'defaultLocale',
          useValue: 'en-US'
        },
        { provide: MAT_DIALOG_DATA, useValue: {} },
        DrawerService,
        DataService
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RoleDetailsComponent);
    injector = getTestBed();
    roleRepository = injector.get(PeopleRoleRepository);
    spyOn(roleRepository, 'find').and.returnValue(of(
      getRole({ name: 'test' })));

    dataService = injector.get(DataService);
    dataService.setRole(getRole({ name: 'test' }));

    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  describe('Header', () => {
    it('role name appears as h2', () => {
      const el = fixture.debugElement.query(By.css('header h2'));
      expect(el.nativeElement.innerText).toContain('test');
    });
  });

  describe('Footer', () => {
    it('footer should be visible', () => {
      expect(fixture.debugElement.query(By.css('.mat-toolbar-footer'))).not.toBeNull();
    });
  });

  describe('Breadcrumb', () => {
    it('role name appears in breadcrump', () => {
      const el = fixture.debugElement.query(By.css('.breadcrumb-text-active'));
      expect(el.nativeElement.innerText).toBe('TEST');
    });
  });
});
