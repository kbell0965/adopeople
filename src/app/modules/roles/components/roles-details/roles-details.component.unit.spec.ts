import { async, ComponentFixture, TestBed, getTestBed } from '@angular/core/testing';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RoleDetailsComponent } from './roles-details.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import {
    MatButtonModule,
    MatInputModule,
    MatToolbarModule,
    MatTabsModule,
    MatIconModule,
    MatCheckboxModule,
    MatDialogModule,
    MAT_DIALOG_DATA,
    MatDialog,
    MatDialogRef
} from '@angular/material';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTableModule } from '@angular/material/table';
import { of } from 'rxjs';
import { MatGridListModule } from '@angular/material/grid-list';
import { PeopleModels, PeopleRoleRepository, PplsiCoreDatalayerModule } from '@pplsi-core/datalayer';
import { LocalizationService } from '@pplsi-core/shared-modules/dist/src/services/localization.service';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { DrawerService } from '../../../../services/drawer/drawer.service';
import { RolesDetailsFooterComponent } from './roles-details-footer/roles-details-footer.component';
import { RoleExpansionPanelComponent } from './role-expansion-panel/role-expansion-panel.component';
import { PeopleFactories } from '@pplsi-core/factories';
import { ActivatedRoute, Router } from '@angular/router';
import { SaveDialogComponent } from '../save-dialog/save-dialog.component';
import { EditTextDialogComponent } from 'src/app/shared/components/edit-text-dialog/edit-text-dialog.component';
import { EditTextDialogConfig } from 'src/app/shared/components/edit-text-dialog/edit-text-dialog-config';
import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';
import { DataService } from 'src/app/services/data/data.service';

const getRole = (cfg) => new PeopleModels.Role(PeopleFactories.RoleFactory.build(cfg));
const getPermission = (cfg) => new PeopleModels.Permission(PeopleFactories.PermissionFactory.build(cfg));

describe('RoleDetailsComponent', () => {
    let component: RoleDetailsComponent;
    let fixture: ComponentFixture<RoleDetailsComponent>;
    let injector: TestBed;
    let roleRepository: PeopleRoleRepository;
    let dialog: MatDialog;
    let router: Router;
    let ds: DataService;

    const activatedRouteStub = {
        snapshot: {
            params: { 'role_id': 'test' }
        }
    };

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                HttpClientTestingModule,
                ReactiveFormsModule,
                RouterTestingModule,
                MatTabsModule,
                MatIconModule,
                MatPaginatorModule,
                MatButtonModule,
                MatIconModule,
                MatInputModule,
                MatToolbarModule,
                MatTableModule,
                MatToolbarModule,
                MatGridListModule,
                MatInputModule,
                MatCheckboxModule,
                BrowserAnimationsModule,
                FormsModule,
                MatDialogModule,
                MatCheckboxModule,
                PplsiCoreDatalayerModule.forRoot()
            ],
            declarations: [
                RoleDetailsComponent,
                RolesDetailsFooterComponent,
                RoleExpansionPanelComponent,
                SaveDialogComponent,
                EditTextDialogComponent
            ],
            providers: [
                LocalizationService,
                {
                    provide: 'Window',
                    useValue: window
                },
                {
                    provide: 'localizations',
                    useValue: [
                        { locale: 'en-US', currencyCode: 'USD', dateFormat: 'MM/dd/yyyy' },
                        { locale: 'en-GB', currencyCode: 'GBP', dateFormat: 'dd/MM/yyyy' },
                        { locale: 'en-CA', currencyCode: 'CAD', dateFormat: 'yyyy-MM-dd' }
                    ]
                },
                {
                    provide: 'defaultLocale',
                    useValue: 'en-US'
                },
                { provide: MAT_DIALOG_DATA, useValue: {} },
                {
                    provide: ActivatedRoute, useValue: activatedRouteStub
                },
                {
                    provide: Router,
                    useValue: { navigate: jasmine.createSpy('navigate') }
                },
                DrawerService,
                DataService
            ],
            schemas: [CUSTOM_ELEMENTS_SCHEMA]
        }).overrideModule(BrowserDynamicTestingModule, {
            set: {
                entryComponents: [SaveDialogComponent, EditTextDialogComponent]
            }
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(RoleDetailsComponent);
        injector = getTestBed();

        router = injector.get(Router);
        roleRepository = injector.get(PeopleRoleRepository);

        dialog = TestBed.get(MatDialog);

        ds = injector.get(DataService);
        ds.setRole(getRole({ name: 'test', description: 'test' }));

        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    describe('#ngOnInit', () => {
        it('assigns role', () => {
            component.ngOnInit();
            fixture.detectChanges();
            expect(component.role).toBeTruthy();
        });
    });

    describe('#handlePermissionChanged', () => {
        it('increments permission count when assigned', () => {
            component.permissions = [getPermission({ name: 'hi' })];
            fixture.detectChanges();

            const footer = new RolesDetailsFooterComponent();

            component.handlePermissionChanged('hi', footer);
            expect(component.count).toBe(1);
        });
    });

    describe('#handleSaveChanges', () => {
        it('opens save dialog with count data', () => {
            component.ngOnInit();
            fixture.detectChanges();

            const ref: MatDialogRef<SaveDialogComponent> = dialog.open(SaveDialogComponent, { data: { count: 1 } });
            expect(ref.componentInstance.data.count).toBe(1);
            ref.close();
        });
    });

    describe('#handleRoleNameEdit', () => {
        it('open dialog with edited name', () => {
            const editConfig: EditTextDialogConfig = {
                title: 'Rename role',
                value: 'new role name',
                submitButtonText: 'RENAME',
                cancelButtonText: 'CANCEL'
            };

            const ref: MatDialogRef<EditTextDialogComponent> = dialog.open(EditTextDialogComponent, { data: editConfig });
            expect(ref.componentInstance.data.value).toBe('new role name');
            ref.close();
        });
    });

    describe('#handleDescriptionEdit', () => {
        it('open dialog with edited description', () => {
            const editConfig: EditTextDialogConfig = {
                title: 'Rename role',
                value: 'new role description',
                submitButtonText: 'RENAME',
                cancelButtonText: 'CANCEL'
            };

            const ref: MatDialogRef<EditTextDialogComponent> = dialog.open(EditTextDialogComponent, { data: editConfig });
            expect(ref.componentInstance.data.value).toBe('new role description');
            ref.close();
        });
    });

    describe('#handleNewRole', () => {
        it('role should be assigned to the newRole', done => {
            const role = getRole({ name: 'test' });
            const newRole = of(getRole({ name: 'new role' }));


            spyOn(roleRepository, 'create').and.returnValue(newRole);
            spyOn(component, 'mapPermissionsOntoRole').and.returnValue(role);

            component.role = of(role);
            component.handleNewRole();


            fixture.whenStable().then(() => {
                fixture.detectChanges();
                expect(roleRepository.create).toHaveBeenCalledWith(role);
                expect(router.navigate).toHaveBeenCalledWith(['/roles/list']);
                done();
            });
        });
    });

    describe('#handleRoleUpdate', () => {
        it('role should be assigned to the updated role', done => {
            const role = getRole({ id: '123', name: 'test' });
            const updatedRole = of(getRole({ id: '123', name: 'updated role' }));

            spyOn(roleRepository, 'update').and.returnValue(updatedRole);
            spyOn(component, 'mapPermissionsOntoRole').and.returnValue(role);

            component.role = of(role);
            component.handleRoleUpdate();

            fixture.whenStable().then(() => {
                fixture.detectChanges();

                expect(roleRepository.update).toHaveBeenCalledWith(role.id, role);
                expect(router.navigate).toHaveBeenCalledWith(['/roles/list']);
                done();
            });
        });
    });

    describe('#hasSectionPermission', () => {
        it('no errors when permissions are empty', () => {
            component.permissions = [];
            component.hasSectionPermission('any:permission', component.permissions);
        });

        it('no errors when undefined', () => {
            component.permissions = undefined;
            component.hasSectionPermission('any:permission', component.permissions);
        });

        it('returns false if permission not on role', () => {
            expect(component.hasSectionPermission('view:obscurely_named_section_not_to_be_found', component.permissions));
        });

        it('returns true if permission is on role', () => {
            component.permissions.push(getPermission({ name: 'found:permission' }));
            expect(component.hasSectionPermission('found:permission', component.permissions));
        });
    });

    describe('#mapPermissionsOntoRole', () => {
        it('returns role with empty component permissions', () => {
            let role = getRole({ permissions: [getPermission({ name: 'some:permission' })] });
            component.permissions = [];
            role = component.mapPermissionsOntoRole(role);
            expect(role.permissions.length).toBe(0);
        });

        it('returns role with component 1 permission', () => {
            let role = getRole({ permissions: [] });
            component.permissions = [getPermission({ name: 'some:permission' })];
            role = component.mapPermissionsOntoRole(role);
            expect(role.permissions.length).toBe(1);
            expect(role.permissions[0].name).toBe('some:permission');
        });

        it('returns role with greater than component 1 permissions', () => {
            let role = getRole({ permissions: [] });
            component.permissions = [getPermission({ name: 'some:permission' }), getPermission({ name: 'other:permission' })];
            role = component.mapPermissionsOntoRole(role);
            expect(role.permissions.length).toBe(2);
            expect(role.permissions[0].name).toBe('some:permission');
            expect(role.permissions[1].name).toBe('other:permission');
        });
    });
});
