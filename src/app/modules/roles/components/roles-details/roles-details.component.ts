import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PeopleRoleRepository, PeopleModels, PermissionNamespaceType } from '@pplsi-core/datalayer';
import { Observable, of, Subject } from 'rxjs';
import { RolesDetailsFooterComponent } from './roles-details-footer/roles-details-footer.component';
import { finalize, map, switchMap, takeUntil } from 'rxjs/operators';
import { MatDialog } from '@angular/material';
import { SaveDialogComponent } from '../save-dialog/save-dialog.component';
import { EditTextDialogComponent } from 'src/app/shared/components/edit-text-dialog/edit-text-dialog.component';
import { DataService } from 'src/app/services/data/data.service';

@Component({
  selector: 'roles-details-component',
  templateUrl: './roles-details.component.html',
  styleUrls: ['roles-details.component.scss']
})
export class RoleDetailsComponent implements OnInit {
  title: string;
  roleId: string;
  role: Observable<PeopleModels.Role>;
  content: any[];
  count = 0;
  loading = true;
  private _destroyed = new Subject<void>();
  permissions: PeopleModels.Permission[] = [];
  changedPermissions: Record<string, boolean> = {};

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private roleRepository: PeopleRoleRepository,
    private dataService: DataService,
    public dialog: MatDialog
  ) { this.content = []; }

  ngOnInit() {
    this.dataService.role
      .pipe(takeUntil(this._destroyed))
      .subscribe((role: PeopleModels.Role) => {
        this.permissions = role.permissions || [];
        this.role = of(role);
      });
  }

  handleDescriptionEdit(value: string): void {
    const editConfig = {
      title: 'Edit role description',
      maxRows: 2,
      value: value,
      submitButtonText: 'RENAME',
      cancelButtonText: 'CANCEL',
      textLength: 80
    };

    const dialogRef = this.dialog.open(EditTextDialogComponent, { data: editConfig, width: '380px' });
    dialogRef.afterClosed()
      .pipe(takeUntil(this._destroyed))
      .subscribe((desc: string) => {
        if (desc) {
          this.role = this.role.pipe(
            switchMap(role => this.roleRepository.update(role.id, { ...role, description: desc } as PeopleModels.Role)));
        }
      });
  }

  handleRoleNameEdit(value: string): void {
    const editConfig = {
      title: 'Rename role',
      value: value,
      submitButtonText: 'RENAME',
      cancelButtonText: 'CANCEL'
    };

    const dialogRef = this.dialog.open(EditTextDialogComponent, { data: editConfig, width: '380px' });
    dialogRef.afterClosed()
      .pipe(takeUntil(this._destroyed))
      .subscribe((name: string) => {
        if (name) {
          this.role = this.role.pipe(
            switchMap(role => this.roleRepository.update(role.id, { ...role, name: name } as PeopleModels.Role)));
        }
      });
  }

  handlePermissionChanged(permission: string, footer: RolesDetailsFooterComponent): void {
    this.changedPermissions[permission] = !this.changedPermissions[permission];
    let count = 0;

    for (const key in this.changedPermissions) {
      count += this.changedPermissions[key] ? 1 : 0;
    }

    this.count = count;
    footer.toggled = !!this.count;
  }

  handleSaveChanges(): void {
    this.dialog.open(SaveDialogComponent, {
      data: { count: this.count }, width: '380px'
    }).afterClosed()
      .pipe(switchMap(result => result ? this.role : null),
        takeUntil(this._destroyed))
      .subscribe(role => {
        if (!role) { return; }
        this.syncRolePermissions();

        if (role.id) {
          this.handleRoleUpdate();
        } else {
          this.handleNewRole();
        }
      });
  }

  handleNewRole() {
    this.role = this.role.pipe(
      switchMap(newRole =>
        this.roleRepository.create(this.mapPermissionsOntoRole(newRole))
          .pipe(finalize(() => this.router.navigate(['/roles/list']))))
    );
  }


  handleRoleUpdate() {
    this.role = this.role.pipe(
      switchMap(role => {
        const updatedRole = this.mapPermissionsOntoRole(role);

        return this.roleRepository.update(role.id, updatedRole).pipe(finalize(() => this.router.navigate(['/roles/list'])));
      }));
  }

  hasSectionPermission(permission: string, permissions: PeopleModels.Permission[]): boolean {
    return permissions && permissions.map(p => p.name).includes(permission);
  }

  mapPermissionsOntoRole(role: PeopleModels.Role): PeopleModels.Role {
    return { ...role, permissions: this.permissions } as PeopleModels.Role;
  }

  private syncRolePermissions(): void {
    const permissionDeltas = [];

    for (const key in this.changedPermissions) {
      if (this.changedPermissions[key]) {
        permissionDeltas.push(key);
      }
    }

    let permissionCount = this.permissions.length;
    permissionDeltas.forEach(delta => {
      this.permissions = this.permissions.filter(p => p.name !== delta);

      if (permissionCount === this.permissions.length) {
        this.permissions.push({ name: delta, namespace: PermissionNamespaceType.Admin } as PeopleModels.Permission);
      }

      permissionCount = this.permissions.length;
    });
  }
}
