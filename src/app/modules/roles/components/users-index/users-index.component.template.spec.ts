import { async, ComponentFixture, TestBed, getTestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { ActivatedRoute } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { By } from '@angular/platform-browser';
import { UsersIndexComponent } from './users-index.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import {
  MatButtonModule,
  MatInputModule,
  MatToolbarModule,
  MatTabsModule,
  MatIconModule,
  MatPaginatorModule,
  MatTableModule,
  MatMenuModule,
  MatSelectModule,
  MatGridListModule
} from '@angular/material';
import { of } from 'rxjs';
import { PeopleModels, PeopleFulfillmentPartnerMembersRepository, PplsiCoreDatalayerModule, MeRepository } from '@pplsi-core/datalayer';
import { LocalizationService } from '@pplsi-core/shared-modules/dist/src/services/localization.service';
import { PeopleFactories, MeFactory } from '@pplsi-core/factories';
import { SearchLayoutModule } from 'ng-components.adonis';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { DrawerService } from 'src/app/services/drawer/drawer.service';
import { PermissionDisabledPipe } from 'src/app/shared/pipes/permission-disabled.pipe';
import { MeService } from 'src/app/services/me/me.service';
import { MeViewModel } from 'src/app/models/me-view-model';
import { PermissionNamespaceType } from '@pplsi-core/datalayer';

describe('UsersIndexComponent.Template', () => {
  let component: UsersIndexComponent;
  let fixture: ComponentFixture<UsersIndexComponent>;
  let injector: TestBed;
  let userRepository: PeopleFulfillmentPartnerMembersRepository;
  let drawerService: DrawerService;
  let meService: MeService;

  const getUser = (cfg) => PeopleFactories.FulfillmentPartnerMemberFactory.build(cfg);
  const fakeUsers = Array.from(Array(10), (x, i) => i + 1).map(i => getUser({}));
  const activatedRouteStub = {
    snapshot: {
      data: {
        initialQueryResult: { fulfillment_partner_members: fakeUsers, total_length: 10 }
      }
    }
  };

  const getMeViewModel = (permissions: string[]) => {
    const meResponse = MeFactory.build({ permissions: permissions.map(x => ({ name: x, namespace: PermissionNamespaceType.Admin })) });
    return new MeViewModel(meResponse);
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        ReactiveFormsModule,
        RouterTestingModule,
        MatTabsModule,
        MatIconModule,
        MatPaginatorModule,
        MatButtonModule,
        MatIconModule,
        MatInputModule,
        MatToolbarModule,
        MatTableModule,
        MatToolbarModule,
        MatGridListModule,
        MatInputModule,
        MatSelectModule,
        MatMenuModule,
        BrowserAnimationsModule,
        SearchLayoutModule,
        PplsiCoreDatalayerModule.forRoot()
      ],
      declarations: [UsersIndexComponent, PermissionDisabledPipe],
      providers: [
        { provide: ActivatedRoute, useValue: activatedRouteStub },
        LocalizationService,
        {
          provide: 'Window',
          useValue: window
        },
        {
          provide: 'localizations',
          useValue: [
            { locale: 'en-US', currencyCode: 'USD', dateFormat: 'MM/dd/yyyy' },
            { locale: 'en-GB', currencyCode: 'GBP', dateFormat: 'dd/MM/yyyy' },
            { locale: 'en-CA', currencyCode: 'CAD', dateFormat: 'yyyy-MM-dd' }
          ]
        },
        {
          provide: 'defaultLocale',
          useValue: 'en-US'
        },
        {
          provide: DrawerService,
          useValue: {
            open: () => { },
            open$: () => {
              return of({});
            }
          }
        },
        {
          provide: MeService,
          useValue: {
            get: () => of(getMeViewModel([]))
          }
        },
        MeRepository
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsersIndexComponent);
    injector = getTestBed();
    userRepository = injector.get(PeopleFulfillmentPartnerMembersRepository);
    spyOn(userRepository, 'findAll').and.returnValue(of(
      <PeopleModels.FulfillmentPartnerMembersWithPagination> { fulfillment_partner_members: fakeUsers, total_length: 10 }));
    component = fixture.componentInstance;
    drawerService = injector.get(DrawerService);
    meService = injector.get(MeService);
    component.ngOnInit();
  });

  describe('Header', () => {
    it('button has inner text contents', () => {
      fixture.detectChanges();
      const el = fixture.debugElement.query(By.css('.secondary'));
      expect(el.nativeElement.innerText).toContain('Role');
    });
  });

  describe('Table', () => {
    beforeEach(() => fixture.detectChanges());

    it('displays empty message', () => {
      component.fulfillment_partner_members = [];
      fixture.detectChanges();
      const el = fixture.debugElement;
      expect(el.nativeElement.innerText).toContain('No records to display');
    });

    it('displays the users', () => {
      component.fulfillment_partner_members = [fakeUsers[0]];
      fixture.detectChanges();

      const rowHtmlElements = fixture.debugElement.nativeElement.querySelectorAll('tbody tr');
      expect(rowHtmlElements.length).toBe(1);
    });

    it('displays the users information', () => {
      const user = fakeUsers[0];
      component.fulfillment_partner_members = [user];
      fixture.detectChanges();

      const rowHtmlElements = fixture.debugElement.nativeElement.querySelectorAll('tbody tr');
      const row = rowHtmlElements[0];
      expect(row.cells[0].innerText).toEqual(user.first_name + ' ' + user.last_name);
      expect(row.cells[2].innerText).toEqual(user.email);
    });

    it('displays edit user row', () => {
      spyOn(drawerService, 'open$');

      const openMenus = fixture.debugElement.nativeElement.querySelectorAll('.user-management-open');
      openMenus[0].click();

      fixture.nativeElement.parentElement.querySelectorAll('.user-management-edit')[0].click();
      expect(drawerService.open$).toHaveBeenCalled();
    });
  });

  describe('Manage Table Column', () => {
    it('has user row column to manage users', () => {
      component.ngOnInit();
      fixture.detectChanges();
      fixture.whenStable().then(() => {
        expect(fixture.debugElement.nativeElement.querySelectorAll('.cdk-column-manage').length).toBeGreaterThan(0);
      });
    });
  });
});
