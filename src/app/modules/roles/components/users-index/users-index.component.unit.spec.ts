import { async, ComponentFixture, TestBed, getTestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { UsersIndexComponent } from './users-index.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import {
  MatButtonModule,
  MatInputModule,
  MatToolbarModule,
  MatTabsModule,
  MatIconModule,
  MatPaginatorModule,
  MatTableModule,
  MatMenuModule,
  MatSelectModule,
  MatGridListModule
} from '@angular/material';
import { of } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import {
  PeopleModels,
  PeopleRoleRepository,
  PeopleFulfillmentPartnerMembersRepository,
  PplsiCoreDatalayerModule,
  MeRepository
} from '@pplsi-core/datalayer';
import { LocalizationService } from '@pplsi-core/shared-modules/dist/src/services/localization.service';
import { PeopleFactories } from '@pplsi-core/factories';
import { SearchLayoutModule } from 'src/app/shared/components/search-layout/search-layout.module';
import { CUSTOM_ELEMENTS_SCHEMA, Component } from '@angular/core';
import { DrawerService } from 'src/app/services/drawer/drawer.service';
import { PermissionDisabledPipe } from 'src/app/shared/pipes/permission-disabled.pipe';
import { MeService } from 'src/app/services/me/me.service';

describe('UsersIndexComponent', () => {
  let component: UsersIndexComponent;
  let fixture: ComponentFixture<UsersIndexComponent>;
  let injector: TestBed;
  let userRepository: PeopleFulfillmentPartnerMembersRepository;
  let userRepositorySpy: jasmine.Spy;
  let roleRepository: PeopleRoleRepository;
  let roleRepositorySpy: jasmine.Spy;
  let drawerService: DrawerService;

  const getUser = (cfg) => PeopleFactories.FulfillmentPartnerMemberFactory.build(cfg);
  const fakeUsers = Array.from(Array(10), (x, i) => i + 1).map(i => getUser({}));
  const activatedRouteStub = {
    snapshot: {
      data: {
        initialQueryResult: { fulfillment_partner_members: fakeUsers, total_length: 10 }
      }
    }
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        ReactiveFormsModule,
        RouterTestingModule,
        MatTabsModule,
        MatIconModule,
        MatPaginatorModule,
        MatButtonModule,
        MatIconModule,
        MatInputModule,
        MatToolbarModule,
        MatTableModule,
        MatToolbarModule,
        MatGridListModule,
        MatInputModule,
        MatSelectModule,
        BrowserAnimationsModule,
        SearchLayoutModule,
        MatMenuModule,
        PplsiCoreDatalayerModule.forRoot()
      ],
      declarations: [UsersIndexComponent, PermissionDisabledPipe],
      providers: [
        { provide: ActivatedRoute, useValue: activatedRouteStub },
        LocalizationService,
        {
          provide: 'Window',
          useValue: window
        },
        {
          provide: 'localizations',
          useValue: [
            { locale: 'en-US', currencyCode: 'USD', dateFormat: 'MM/dd/yyyy' },
            { locale: 'en-GB', currencyCode: 'GBP', dateFormat: 'dd/MM/yyyy' },
            { locale: 'en-CA', currencyCode: 'CAD', dateFormat: 'yyyy-MM-dd' }
          ]
        },
        {
          provide: 'defaultLocale',
          useValue: 'en-US'
        },
        {
          provide: DrawerService,
          useValue: {
            open: () => { },
            open$: jasmine.createSpy('open$').and.returnValue(of({}))
          }
        },
        {
          provide: MeService,
          useValue: {
            get: () => of({ hasPermission: () => false })
          }
        }
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsersIndexComponent);
    injector = getTestBed();
    userRepository = injector.get(PeopleFulfillmentPartnerMembersRepository);
    userRepositorySpy = spyOn(userRepository, 'findAll');
    userRepositorySpy.and.returnValue(of(
      <PeopleModels.FulfillmentPartnerMembersWithPagination> { fulfillment_partner_members: fakeUsers, total_length: 10 }));
    roleRepository = injector.get(PeopleRoleRepository);
    roleRepositorySpy = spyOn(roleRepository, 'findAll');
    roleRepositorySpy.and.returnValue(of(
      <PeopleModels.RolesWithPagination> { roles: [], total_length: 0 }));
    drawerService = injector.get(DrawerService);
    component = fixture.componentInstance;

    fixture.detectChanges();
  });

  describe('#ngOnInit', () => {
    it('calls refresh', () => {
      spyOn(component, 'refresh');
      component.ngOnInit();
      expect(component.fulfillment_partner_members.length).toEqual(10);
    });
  });

  describe('#search', () => {
    it('sets query to null and calls refresh', () => {
      userRepositorySpy.calls.reset();
      component.search('');

      expect(component.query).toEqual(null);
      expect(userRepositorySpy).toHaveBeenCalledTimes(1);
    });

    it('calls userRepositorySpy', () => {
      userRepositorySpy.calls.reset();
      component.search('t');

      expect(component.query).toEqual('t');
      expect(userRepositorySpy).toHaveBeenCalledTimes(1);
    });
  });

  describe('#onPaginateChange', () => {
    it('calls userRepositorySpy', () => {
      userRepositorySpy.calls.reset();
      const pageEvent = { pageIndex: 0, pageSize: 10, length: 100 };
      component.onPaginateChange(pageEvent);
      expect(component.pageEvent).toEqual(pageEvent);
      expect(userRepositorySpy).toHaveBeenCalledTimes(1);
    });
  });

  describe('#getRoles', () => {
    it('calls getRoles', () => {
      roleRepositorySpy.calls.reset();
      component.getRoles();
      expect(roleRepositorySpy).toHaveBeenCalledTimes(1);
    });
  });

  describe('#selectChange', () => {
    it('sets the roleIds and calls refresh', () => {
      userRepositorySpy.calls.reset();
      component.selectChange({ value: ['1', '2', '3'] });
      expect(userRepositorySpy).toHaveBeenCalledTimes(1);
    });
  });

  describe('#refresh', () => {
    it('calls refresh', () => {
      userRepositorySpy.calls.reset();
      component.refresh();
      expect(userRepositorySpy).toHaveBeenCalledTimes(1);
    });
  });

  describe('#OnDestroy', () => {
    it('unsubscribes the create fulfillmentPartnerMemberSubscription subscription', () => {
      component.fulfillmentPartnerMemberSubscription = of(true).subscribe();
      component.roleSubscription = of(true).subscribe();
      component.OnDestroy();
      expect(component['fulfillmentPartnerMemberSubscription'].closed).toBeTruthy();
      expect(component['roleSubscription'].closed).toBeTruthy();
    });
  });

  describe('#openUserEdit', () => {
    it('drawer service opens drawer', () => {
      component.ngOnInit();
      component.refresh();
      component.openUserEdit(fakeUsers[0]);
      expect(drawerService.open$).toHaveBeenCalled();
    });
  });

  describe('#getColumns', () => {
    const isReadonly = true;

    it('manage column included if user table is not readonly', () => {
      const columns = component.getColumns(!isReadonly);
      expect(columns).toContain('manage');
    });


    it('manage column not included when user table is readonly', () => {
      const columns = component.getColumns(isReadonly);
      expect(columns).not.toContain('manage');
    });
  });
});
