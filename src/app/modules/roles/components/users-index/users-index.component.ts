import { Component, OnInit } from '@angular/core';
import { PageEvent } from '@angular/material/paginator';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { PeopleModels, PeopleFulfillmentPartnerMembersRepository, PeopleRoleRepository } from '@pplsi-core/datalayer';
import { DrawerService } from 'src/app/services/drawer/drawer.service';
import { UserRoleSlideoutComponent } from 'src/app/shared/components/user-role-slideout/user-role-slideout.component';

@Component({
  selector: 'users-index-component',
  templateUrl: './users-index.component.html',
  styleUrls: ['users-index.component.scss']
})
export class UsersIndexComponent implements OnInit {
  isReadonly = false;
  displayedColumns: string[] = ['first_name', 'fpm_roles', 'email', 'manage'];
  readOnlyColumns: string[] = ['first_name', 'fpm_roles', 'email'];
  fulfillment_partner_members: PeopleModels.FulfillmentPartnerMember[] = [];
  length = 100;
  pageSize = 10;
  userFilterForm: FormGroup;
  pageSizeOptions: number[] = [5, 10, 25, 100];
  fulfillmentPartnerMemberSubscription: Subscription;
  pageEvent: PageEvent = { pageIndex: 0, pageSize: 10, length: 100 };
  roleIds: string[];
  roles: PeopleModels.Role[];
  roleSubscription: Subscription;
  query: string;

  constructor(
    private formBuilder: FormBuilder,
    private fulfillmentPartnerMemberRepository: PeopleFulfillmentPartnerMembersRepository,
    private roleRepository: PeopleRoleRepository,
    private activatedRoute: ActivatedRoute,
    private drawerService: DrawerService
  ) { }

  ngOnInit(): void {
    this.userFilterForm = this.formBuilder.group({
      user_roles: [null, null]
    });
    this.roleIds = [];

    const { fulfillment_partner_members, total_length } = this.activatedRoute.snapshot.data['initialQueryResult'];
    if (fulfillment_partner_members) {
      this.fulfillment_partner_members = fulfillment_partner_members.map(
        fulfillment_partner_member => {
          return new PeopleModels.FulfillmentPartnerMember(fulfillment_partner_member);
        }
      );
    }
    this.pageEvent.length = total_length;
    this.getRoles();
  }

  refresh(): void {
    this.fulfillmentPartnerMemberSubscription = this.fulfillmentPartnerMemberRepository.findAll(
      this.pageEvent.pageIndex + 1, this.pageEvent.pageSize, this.query, this.roleIds)
      .subscribe((FulfillmentPartnerMembersWithPagination: PeopleModels.FulfillmentPartnerMembersWithPagination) => {
        const fulfillment_partner_members = FulfillmentPartnerMembersWithPagination.fulfillment_partner_members;
        this.pageEvent.length = FulfillmentPartnerMembersWithPagination.total_length;
        this.fulfillment_partner_members = fulfillment_partner_members.map(
          (FulfillmentPartnerMember: PeopleModels.FulfillmentPartnerMember) =>
            new PeopleModels.FulfillmentPartnerMember(FulfillmentPartnerMember));
      });
  }

  getRoles(): void {
    this.roleSubscription = this.roleRepository.findAll(1, 50)
      .subscribe((rolesWithPagination: PeopleModels.RolesWithPagination) => {
        const roles = rolesWithPagination.roles;
        this.roles = roles.map((role: PeopleModels.Role) => new PeopleModels.Role(role));
      });
  }

  selectChange($event): void {
    this.roleIds = $event.value;
    this.refresh();
  }

  search(query: string = ''): void {
    if (query && query.length > 0) {
      this.query = query;
      this.refresh();
    } else if (query === '') {
      this.query = null;
      this.refresh();
    }
  }

  onPaginateChange($event): void {
    this.pageEvent = $event;
    this.refresh();
  }

  OnDestroy(): void {
    this.fulfillmentPartnerMemberSubscription.unsubscribe();
    this.roleSubscription.unsubscribe();
  }

  openUserEdit(fpm: PeopleModels.FulfillmentPartnerMember): void {
    const ref = this.drawerService.open$(UserRoleSlideoutComponent, fpm);

    const subscription = ref.subscribe(closeResult => {
      if (closeResult) {
        this.refresh();
      }

      if (subscription) {
        subscription.unsubscribe();
      }
    });
  }

  getColumns(isReadOnly: boolean): string[] {
    return isReadOnly ? this.readOnlyColumns : this.displayedColumns;
  }
}
