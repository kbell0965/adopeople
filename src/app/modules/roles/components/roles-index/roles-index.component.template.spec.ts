import { async, ComponentFixture, TestBed, getTestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { ActivatedRoute } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { By } from '@angular/platform-browser';
import { RolesIndexComponent } from './roles-index.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import {
  MatButtonModule,
  MatInputModule,
  MatToolbarModule,
  MatTabsModule,
  MatIconModule
} from '@angular/material';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTableModule } from '@angular/material/table';
import { of } from 'rxjs';
import { MatGridListModule } from '@angular/material/grid-list';
import { PeopleModels, PeopleRoleRepository, PplsiCoreDatalayerModule, MeRepository } from '@pplsi-core/datalayer';
import { LocalizationService } from '@pplsi-core/shared-modules/dist/src/services/localization.service';
import { PartnerFactories } from '@pplsi-core/factories';
import { SearchLayoutModule } from 'ng-components.adonis';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FeatherModule } from 'angular-feather';
import { Download } from 'angular-feather/icons';
import { MeService } from 'src/app/services/me/me.service';
import { MeViewModel } from 'src/app/models/me-view-model';
import { SharedModule } from 'src/app/shared/shared.module';

describe('RolesIndexComponent.Template', () => {
  let component: RolesIndexComponent;
  let fixture: ComponentFixture<RolesIndexComponent>;
  let injector: TestBed;
  let roleRepository: PeopleRoleRepository;

  const getRole = (cfg) => PartnerFactories.RoleFactory.build(cfg);
  const fakeRoles = Array.from(Array(10), (x, i) => i + 1).map(i => getRole({}));
  const activatedRouteStub = {
    snapshot: {
      data: {
        initialQueryResult: of({ roles: fakeRoles, total_length: 10 })
      }
    }
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        FeatherModule.pick({ Download }),
        HttpClientTestingModule,
        ReactiveFormsModule,
        RouterTestingModule,
        MatTabsModule,
        MatIconModule,
        MatPaginatorModule,
        MatButtonModule,
        MatIconModule,
        MatInputModule,
        MatToolbarModule,
        MatTableModule,
        MatToolbarModule,
        MatGridListModule,
        MatInputModule,
        BrowserAnimationsModule,
        SearchLayoutModule,
        PplsiCoreDatalayerModule.forRoot(),
        SharedModule
      ],
      declarations: [
        RolesIndexComponent
      ],
      providers: [
        { provide: ActivatedRoute, useValue: activatedRouteStub },
        LocalizationService,
        {
          provide: 'Window',
          useValue: window
        },
        {
          provide: 'localizations',
          useValue: [
            { locale: 'en-US', currencyCode: 'USD', dateFormat: 'MM/dd/yyyy' },
            { locale: 'en-GB', currencyCode: 'GBP', dateFormat: 'dd/MM/yyyy' },
            { locale: 'en-CA', currencyCode: 'CAD', dateFormat: 'yyyy-MM-dd' }
          ]
        },
        {
          provide: 'defaultLocale',
          useValue: 'en-US'
        },
        MeService,
        MeRepository
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RolesIndexComponent);
    injector = getTestBed();
    roleRepository = injector.get(PeopleRoleRepository);
    spyOn(roleRepository, 'findAll').and.returnValue(of(
      <PeopleModels.RolesWithPagination> { roles: [], total_length: 0 }));
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  describe('export button', () => {
    it('has inner text contents', () => {
      expect(fixture.debugElement.query(By.css('.export-roles-button')).nativeElement.innerText).toBe('EXPORT');
    });

    it('calls exportRoles when clicked', () => {
      spyOn(component, 'exportRoles');
      fixture.debugElement.query(By.css('.export-roles-button')).nativeElement.click();
      expect(component.exportRoles).toHaveBeenCalled();
    });
  });
});
