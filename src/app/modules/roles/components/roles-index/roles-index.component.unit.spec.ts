import { async, ComponentFixture, TestBed, getTestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RolesIndexComponent } from './roles-index.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import {
  MatButtonModule,
  MatInputModule,
  MatToolbarModule,
  MatTabsModule,
  MatIconModule
} from '@angular/material';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTableModule } from '@angular/material/table';
import { of } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { MatGridListModule } from '@angular/material/grid-list';
import { PeopleModels, PeopleRoleRepository, PplsiCoreDatalayerModule, PermissionNamespaceType } from '@pplsi-core/datalayer';
import { LocalizationService } from '@pplsi-core/shared-modules/dist/src/services/localization.service';
import { PartnerFactories } from '@pplsi-core/factories';
import { SearchLayoutModule } from 'src/app/shared/components/search-layout/search-layout.module';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FeatherModule } from 'angular-feather';
import { Download } from 'angular-feather/icons';
import { MeService } from 'src/app/services/me/me.service';
import { PermittedDirective } from 'src/app/shared/directives/permitted/permitted.directive';
import { PermissionDisabledPipe } from 'src/app/shared/pipes/permission-disabled.pipe';

describe('RolesIndexComponent', () => {
  let component: RolesIndexComponent;
  let fixture: ComponentFixture<RolesIndexComponent>;
  let injector: TestBed;
  let roleRepository: PeopleRoleRepository;
  let roleRepositorySpy: jasmine.Spy;

  const getRole = (cfg) => PartnerFactories.RoleFactory.build(cfg);
  const fakeRoles = Array.from(Array(10), (x, i) => i + 1).map(i => getRole({}));
  const activatedRouteStub = {
    snapshot: {
      data: {
        initialQueryResult: of({ roles: fakeRoles, total_length: 10 })
      }
    }
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        FeatherModule.pick({ Download }),
        HttpClientTestingModule,
        ReactiveFormsModule,
        RouterTestingModule,
        MatTabsModule,
        MatIconModule,
        MatPaginatorModule,
        MatButtonModule,
        MatIconModule,
        MatInputModule,
        MatToolbarModule,
        MatTableModule,
        MatToolbarModule,
        MatGridListModule,
        MatInputModule,
        BrowserAnimationsModule,
        SearchLayoutModule,
        PplsiCoreDatalayerModule.forRoot()
      ],
      declarations: [RolesIndexComponent, PermissionDisabledPipe, PermittedDirective],
      providers: [
        { provide: ActivatedRoute, useValue: activatedRouteStub },
        LocalizationService,
        {
          provide: 'Window',
          useValue: window
        },
        {
          provide: 'localizations',
          useValue: [
            { locale: 'en-US', currencyCode: 'USD', dateFormat: 'MM/dd/yyyy' },
            { locale: 'en-GB', currencyCode: 'GBP', dateFormat: 'dd/MM/yyyy' },
            { locale: 'en-CA', currencyCode: 'CAD', dateFormat: 'yyyy-MM-dd' }
          ]
        },
        {
          provide: 'defaultLocale',
          useValue: 'en-US'
        },
        {
          provide: MeService,
          useValue: {
            $currentUser: () => of({ hasPermission: () => { } })
          }
        }
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RolesIndexComponent);
    injector = getTestBed();
    roleRepository = injector.get(PeopleRoleRepository);
    roleRepositorySpy = spyOn(roleRepository, 'findAll');
    roleRepositorySpy.and.returnValue(of(
      <PeopleModels.RolesWithPagination> { roles: [], total_length: 0 }));
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  describe('#ngOnInit', () => {
    it('calls refresh', () => {
      spyOn(component, 'refresh');
      component.ngOnInit();
      expect(component.roles.length).toEqual(0);
    });
  });

  describe('#search', () => {
    it('calls roleRepositorySpy', () => {
      roleRepositorySpy.calls.reset();
      component.search('test');

      expect(component.query).toEqual('test');
      expect(roleRepositorySpy).toHaveBeenCalledTimes(1);
    });
  });

  describe('#onPaginateChange', () => {
    it('calls roleRepositorySpy', () => {
      roleRepositorySpy.calls.reset();
      const pageEvent = { pageIndex: 0, pageSize: 10, length: 100 };
      component.onPaginateChange(pageEvent);
      expect(component.pageEvent).toEqual(pageEvent);
      expect(roleRepositorySpy).toHaveBeenCalledTimes(1);
    });
  });

  describe('#refresh', () => {
    it('calls refresh', () => {
      roleRepositorySpy.calls.reset();
      component.refresh();
      expect(roleRepositorySpy).toHaveBeenCalledTimes(1);
    });
  });

  describe('#OnDestroy', () => {
    it('unsubscribes the create role subscription', () => {
      component.roleSubscription = of(true).subscribe();
      component.OnDestroy();
      expect(component['roleSubscription'].closed).toBeTruthy();
    });
  });

  describe('#exportRoles', () => {
    it('calls roleRepository.export', done => {
      const blob: Blob = new Blob(['CSV Content']);
      const url = window.URL.createObjectURL(blob);

      spyOn(roleRepository, 'export').and.returnValue(of(blob));
      spyOn(window.URL, 'createObjectURL').and.returnValue(url);
      spyOn(window, 'open');

      component.exportRoles();

      fixture.whenStable().then(() => {
        fixture.detectChanges();
        expect(roleRepository.export).toHaveBeenCalledWith(PermissionNamespaceType.Admin);
        expect(window.open).toHaveBeenCalledWith(url);
        done();
      });
    });
  });
});
