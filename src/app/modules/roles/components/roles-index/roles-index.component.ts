import { Component, OnInit, OnDestroy } from '@angular/core';
import { PageEvent } from '@angular/material/paginator';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Subscription, Subject } from 'rxjs';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { PeopleModels, PeopleRoleRepository, PermissionNamespaceType } from '@pplsi-core/datalayer';
import { takeUntil } from 'rxjs/operators';
import { PermissionDisabledPipe } from 'src/app/shared/pipes/permission-disabled.pipe';

const INPUT_DEBOUNCE = 200;

@Component({
  selector: 'roles-index-component',
  templateUrl: './roles-index.component.html',
  styleUrls: ['roles-index.component.scss'],
  providers: [PermissionDisabledPipe]
})
export class RolesIndexComponent implements OnInit, OnDestroy {

  private _destroyed = new Subject<void>();
  displayedColumns: string[] = ['name', 'description', 'created_at', 'number_of_users'];
  roles: PeopleModels.Role[] = [];
  searchForm: FormGroup;
  length = 100;
  pageSize = 10;
  pageSizeOptions: number[] = [5, 10, 25, 100];
  roleSubscription: Subscription;
  pageEvent: PageEvent = { pageIndex: 0, pageSize: 10, length: 100 };

  constructor(
    private formBuilder: FormBuilder,
    private roleRepository: PeopleRoleRepository,
    private matIconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private permissionDisabled: PermissionDisabledPipe
  ) {
    this.matIconRegistry.addSvgIcon(
      'search',
      this.domSanitizer.bypassSecurityTrustResourceUrl('assets/search.svg')
    );
  }

  get query() {
    return this.searchForm.get('query').value;
  }

  set query(query) {
    this.searchForm.controls['query'].setValue(query);
  }

  ngOnInit(): void {
    this.searchForm = this.formBuilder.group({
      query: ''
    });

    this.searchForm.controls['query'].valueChanges
      .pipe(takeUntil(this._destroyed), debounceTime(INPUT_DEBOUNCE), distinctUntilChanged())
      .subscribe((value: string) => this.search(value));

    const { roles, total_length } = this.activatedRoute.snapshot.data['initialQueryResult'];
    if (roles) {
      this.roles = roles.map(role => new PeopleModels.Role(role));
    }
    this.pageEvent.length = total_length;
  }

  ngOnDestroy(): void {
    this._destroyed.next();
    this._destroyed.complete();
  }

  refresh(): void {
    this.roleSubscription = this.roleRepository.findAll(this.pageEvent.pageIndex + 1, this.pageEvent.pageSize, this.query)
      .pipe(takeUntil(this._destroyed))
      .subscribe((rolesWithPagination: PeopleModels.RolesWithPagination) => {
        const roles = rolesWithPagination.roles;
        this.pageEvent.length = rolesWithPagination.total_length;
        this.roles = roles.map((role: PeopleModels.Role) => new PeopleModels.Role(role));
      });
  }

  search(query: string = ''): void {
    if (query && query.length > 0) {
      this.query = query;
      this.refresh();
    } else if (query === '') {
      this.query = null;
      this.refresh();
    }
  }

  navigateRow(role: PeopleModels.Role): void {
      this.router.navigate(['/roles', role.id]);
  }

  onPaginateChange($event: PageEvent): void {
    this.pageEvent = $event;
    this.refresh();
  }

  OnDestroy(): void {
    this.roleSubscription.unsubscribe();
  }

  exportRoles(): void {
    this.roleRepository.export(PermissionNamespaceType.Admin)
      .pipe(takeUntil(this._destroyed))
      .subscribe((response: Blob) => {
        const url = window.URL.createObjectURL(response);
        window.open(url);
      });
  }
}
