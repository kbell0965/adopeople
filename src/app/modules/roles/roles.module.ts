import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { MatProgressSpinnerModule, PageEvent, MatTabsModule, MatDialogModule } from '@angular/material';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatDividerModule } from '@angular/material/divider';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatChipsModule } from '@angular/material/chips';
import { MatFormFieldModule } from '@angular/material';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatSelectModule } from '@angular/material/select';
import { MatMenuModule } from '@angular/material/menu';
import { PageLayoutModule, SearchLayoutModule } from 'ng-components.adonis';
import { RolesRoutingModule } from './roles-routing.module';
import { RolesPermissionsComponent } from './components/roles-permissions/roles-permissions.component';
import { RolesIndexComponent } from './components/roles-index/roles-index.component';
import { RoleDetailsComponent } from './components/roles-details/roles-details.component';
import { UsersIndexComponent } from './components/users-index/users-index.component';
import { PplsiCoreDatalayerModule } from '@pplsi-core/datalayer';
import { LocalizationService } from '@pplsi-core/shared-modules/dist/src/services/localization.service';
import { RoleFromPathResolver } from './resolvers/role-from-path/role-from-path.resolver';
import { RolesInitialQueryResolver } from './resolvers/roles-initial-query/roles-initial-query.resolver';
import { FulfillmentPartnerMemberFromPathResolver } from './resolvers/user-from-path/user-from-path.resolver';
import { FulfillmentPartnerMembersInitialQueryResolver } from './resolvers/users-initial-query/users-initial-query.resolver';
import { SharedModule } from 'src/app/shared/shared.module';
import { RoleExpansionPanelComponent } from './components/roles-details/role-expansion-panel/role-expansion-panel.component';
import { RolesDetailsFooterComponent } from './components/roles-details/roles-details-footer/roles-details-footer.component';
import { SaveDialogComponent } from './components/save-dialog/save-dialog.component';
import { FeatherModule } from 'angular-feather';
import { Download, Edit3, MoreVertical } from 'angular-feather/icons';
import { EditTextDialogModule } from 'src/app/shared/components/edit-text-dialog/edit-text-dialog.module';
import { GroupedPermissionRowComponent } from './components/roles-details/grouped-permission-row/grouped-permission-row.component';
import { GroupedPermissionHeaderComponent } from './components/roles-details/grouped-permission-header/grouped-permission-header.component';

@NgModule({
  declarations: [
    RolesPermissionsComponent,
    RolesIndexComponent,
    RoleDetailsComponent,
    UsersIndexComponent,
    RoleExpansionPanelComponent,
    RolesDetailsFooterComponent,
    SaveDialogComponent,
    GroupedPermissionRowComponent,
    GroupedPermissionHeaderComponent
  ],
  imports: [
    EditTextDialogModule,
    MatProgressSpinnerModule,
    RolesRoutingModule,
    MatTableModule,
    MatPaginatorModule,
    MatDividerModule,
    MatToolbarModule,
    MatGridListModule,
    MatTabsModule,
    MatInputModule,
    MatIconModule,
    MatListModule,
    MatChipsModule,
    MatFormFieldModule,
    MatSelectModule,
    MatCheckboxModule,
    MatSlideToggleModule,
    FormsModule,
    MatExpansionModule,
    PageLayoutModule,
    PplsiCoreDatalayerModule.forRoot(),
    SearchLayoutModule,
    SharedModule,
    MatDialogModule,
    MatMenuModule,
    FeatherModule.pick({ Download, Edit3, MoreVertical })
  ],
  providers: [
    PageEvent,
    RoleFromPathResolver,
    RolesInitialQueryResolver,
    FulfillmentPartnerMemberFromPathResolver,
    FulfillmentPartnerMembersInitialQueryResolver,
    LocalizationService,
    {
      provide: 'Window',
      useValue: window
    },
    {
      provide: 'localizations',
      useValue: [
        { locale: 'en-US', currencyCode: 'USD', dateFormat: 'MM/dd/yyyy' },
        { locale: 'en-GB', currencyCode: 'GBP', dateFormat: 'dd/MM/yyyy' },
        { locale: 'en-CA', currencyCode: 'CAD', dateFormat: 'yyyy-MM-dd' }
      ]
    },
    {
      provide: 'defaultLocale',
      useValue: 'en-US'
    }
  ], entryComponents: [SaveDialogComponent]
})
export class RolesModule { }
