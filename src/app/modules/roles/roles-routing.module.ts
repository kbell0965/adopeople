import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RolesPermissionsComponent } from './components/roles-permissions/roles-permissions.component';
import { RoleDetailsComponent } from './components/roles-details/roles-details.component';
import { RolesIndexComponent } from './components/roles-index/roles-index.component';
import { UsersIndexComponent } from './components/users-index/users-index.component';
import { RolesInitialQueryResolver } from './resolvers/roles-initial-query/roles-initial-query.resolver';
import { RoleFromPathResolver } from './resolvers/role-from-path/role-from-path.resolver';
import { FulfillmentPartnerMembersInitialQueryResolver } from './resolvers/users-initial-query/users-initial-query.resolver';
import { PermissionsGuard } from '../../guards/permissions/permissions.guard';

const routes: Routes = [{
  path: '', component: RolesPermissionsComponent,
  children: [{
    path: 'list', component: RolesIndexComponent,
    resolve: { initialQueryResult: RolesInitialQueryResolver }
  }, {
    path: 'users', component: UsersIndexComponent,
    resolve: { initialQueryResult: FulfillmentPartnerMembersInitialQueryResolver }
  }]
}, {
  path: ':role_id',
  component: RoleDetailsComponent,
  resolve: { role: RoleFromPathResolver },
  pathMatch: 'full'
},
{ path: '**', redirectTo: '', pathMatch: 'full' }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RolesRoutingModule { }
