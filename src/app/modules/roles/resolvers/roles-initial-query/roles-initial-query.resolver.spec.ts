import { TestBed, getTestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpClientTestingModule } from '@angular/common/http/testing';

import { PeopleRoleRepository, PeopleModels } from '@pplsi-core/datalayer';
import { PeopleFactories } from '@pplsi-core/factories';
import { RolesInitialQueryResolver } from './roles-initial-query.resolver';
import { ActivatedRouteSnapshot } from '@angular/router';

describe('RolesInitialQueryResolver', () => {
  let injector: TestBed;
  let rolesInitialQueryResolver: RolesInitialQueryResolver;
  let peopleRoleRepository: PeopleRoleRepository;
  let route: ActivatedRouteSnapshot;
  const getRole = (cfg) => PeopleFactories.RoleFactory.build(cfg);
  const getRoles = () => Array.from(Array(10), (x, i) => i + 1).map(i => getRole({}));
  const getRolesWithPagination = { roles: getRoles(), total_length: 10 };

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        RolesInitialQueryResolver,
        PeopleRoleRepository,
        { provide: ActivatedRouteSnapshot }
      ],
      imports: [HttpClientTestingModule]
    });

    route = TestBed.get(ActivatedRouteSnapshot);
    injector = getTestBed();
    rolesInitialQueryResolver = injector.get(RolesInitialQueryResolver);
    peopleRoleRepository = injector.get(PeopleRoleRepository);

    spyOn(peopleRoleRepository, 'findAll').withArgs(1, 10)
      .and.returnValue(of(getRolesWithPagination as any));
  });

  describe('#resolve', () => {
    it('gets roles with pagination from the roles initial query resolver', () => {
      rolesInitialQueryResolver.resolve(route).subscribe(
        (result: PeopleModels.RolesWithPagination) => {
          expect(result.roles.length).toBe(10);
        }
      );
    });
  });
});
