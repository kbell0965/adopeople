import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { PeopleRoleRepository, PeopleModels } from '@pplsi-core/datalayer';

@Injectable()
export class RolesInitialQueryResolver implements Resolve<PeopleModels.RolesWithPagination> {
  constructor(private roleRepository: PeopleRoleRepository) { }

  resolve(activatedRouteSnapshot: ActivatedRouteSnapshot): Observable<PeopleModels.RolesWithPagination> {
    return this.roleRepository.findAll(1, 10);
  }
}
