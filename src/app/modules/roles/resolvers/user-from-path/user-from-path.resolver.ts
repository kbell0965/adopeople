import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { PeopleModels, PeopleFulfillmentPartnerMembersRepository } from '@pplsi-core/datalayer';
import { DataService } from '../../../../services/data/data.service';

@Injectable()
export class FulfillmentPartnerMemberFromPathResolver implements Resolve<PeopleModels.FulfillmentPartnerMember> {
  constructor(private fulfillmentPartnerMembersRepository: PeopleFulfillmentPartnerMembersRepository, private dataService: DataService) {}

  resolve(activatedRouteSnapshot: ActivatedRouteSnapshot): Observable<PeopleModels.FulfillmentPartnerMember> {
    const id = activatedRouteSnapshot.params['fulfillment_partner_member_id'];

    return this.fulfillmentPartnerMembersRepository.find(id as string).pipe(
      map((fulfillmentPartnerMember: PeopleModels.FulfillmentPartnerMember) =>
        new PeopleModels.FulfillmentPartnerMember(fulfillmentPartnerMember))
    );
  }
}
