import { TestBed, getTestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { PeopleFulfillmentPartnerMembersRepository, PeopleModels } from '@pplsi-core/datalayer';
import { PartnerFactories } from '@pplsi-core/factories';
import { DataService } from '../../../../services/data/data.service';
import { FulfillmentPartnerMemberFromPathResolver } from './user-from-path.resolver';

describe('FulfillmentPartnerMemberFromPathResolver', () => {
  let injector: TestBed;
  let userFromPathResolver: FulfillmentPartnerMemberFromPathResolver;
  let userRepository: PeopleFulfillmentPartnerMembersRepository;
  let dataService: DataService;
  const fakeUser: object = PartnerFactories.FulfillmentPartnerMemberFactory.build();
  const user: PeopleModels.FulfillmentPartnerMember = new PeopleModels.FulfillmentPartnerMember(fakeUser);

  const activatedRouteSnapshotStub = {
    params: {
      fulfillment_partner_member_id: fakeUser['id']
    }
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        FulfillmentPartnerMemberFromPathResolver,
        PeopleFulfillmentPartnerMembersRepository,
        DataService
      ],
      imports: [
        HttpClientTestingModule
      ]
    });

    injector = getTestBed();
    userFromPathResolver = injector.get(FulfillmentPartnerMemberFromPathResolver);
    userRepository = injector.get(PeopleFulfillmentPartnerMembersRepository);
    dataService = injector.get(DataService);

    spyOn(userRepository, 'find').and.returnValue(of(user));
  });

  describe('#resolve', () => {
    it('gets user from the PeopleFulfillmentPartnerMembersRepository', (done) => {
      userFromPathResolver.resolve(activatedRouteSnapshotStub as any).subscribe((returnedUser: PeopleModels.FulfillmentPartnerMember) => {
        expect(returnedUser).toEqual(new PeopleModels.FulfillmentPartnerMember(user));
        done();
      });
    });

    it('gets user from the PeopleFulfillmentPartnerMembersRepository with the user id from the path', (done) => {
      userFromPathResolver.resolve(activatedRouteSnapshotStub as any).subscribe(() => {
        expect(userRepository.find).toHaveBeenCalledWith(user['id']);
        done();
      });
    });
  });
});
