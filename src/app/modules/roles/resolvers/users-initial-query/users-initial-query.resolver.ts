import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { PeopleFulfillmentPartnerMembersRepository, PeopleModels } from '@pplsi-core/datalayer';

@Injectable()
export class FulfillmentPartnerMembersInitialQueryResolver implements Resolve<PeopleModels.FulfillmentPartnerMembersWithPagination> {
  constructor(private fulfillmentPartnerMembersRepository: PeopleFulfillmentPartnerMembersRepository) { }

  resolve(activatedRouteSnapshot: ActivatedRouteSnapshot): Observable<PeopleModels.FulfillmentPartnerMembersWithPagination> {
    return this.fulfillmentPartnerMembersRepository.findAll(1, 10, '', []);
  }
}
