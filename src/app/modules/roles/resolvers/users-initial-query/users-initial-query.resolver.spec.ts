import { TestBed, getTestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpClientTestingModule } from '@angular/common/http/testing';

import { PeopleFulfillmentPartnerMembersRepository, PeopleModels } from '@pplsi-core/datalayer';
import { PeopleFactories } from '@pplsi-core/factories';
import { FulfillmentPartnerMembersInitialQueryResolver } from './users-initial-query.resolver';
import { ActivatedRouteSnapshot } from '@angular/router';

describe('UsersInitialQueryResolver', () => {
  let injector: TestBed;
  let usersInitialQueryResolver: FulfillmentPartnerMembersInitialQueryResolver;
  let peopleFulfillmentPartnerMembersRepository: PeopleFulfillmentPartnerMembersRepository;
  let route: ActivatedRouteSnapshot;
  const getUser = (cfg) => PeopleFactories.FulfillmentPartnerMemberFactory.build(cfg);
  const getUsers = () => Array.from(Array(10), (x, i) => i + 1).map(i => getUser({}));
  const getUsersWithPagination = { fulfillment_partner_members: getUsers(), total_length: 10 };

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        FulfillmentPartnerMembersInitialQueryResolver,
        PeopleFulfillmentPartnerMembersRepository,
        { provide: ActivatedRouteSnapshot }
      ],
      imports: [HttpClientTestingModule]
    });

    route = TestBed.get(ActivatedRouteSnapshot);
    injector = getTestBed();
    usersInitialQueryResolver = injector.get(FulfillmentPartnerMembersInitialQueryResolver);
    peopleFulfillmentPartnerMembersRepository = injector.get(PeopleFulfillmentPartnerMembersRepository);

    spyOn(peopleFulfillmentPartnerMembersRepository, 'findAll').withArgs(1, 10, '', [])
      .and.returnValue(of(getUsersWithPagination as any));
  });

  describe('#resolve', () => {
    it('gets users with pagination from the users initial query resolver', () => {
      usersInitialQueryResolver.resolve(route).subscribe(
        (result: PeopleModels.FulfillmentPartnerMembersWithPagination) => {
          expect(result.fulfillment_partner_members.length).toBe(10);
        }
      );
    });
  });
});
