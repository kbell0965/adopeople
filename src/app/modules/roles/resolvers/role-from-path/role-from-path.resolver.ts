import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Observable, of } from 'rxjs';
import { map, tap } from 'rxjs/operators';

import { PeopleModels, PeopleRoleRepository } from '@pplsi-core/datalayer';
import { DataService } from '../../../../services/data/data.service';
import { Router } from '@angular/router';

@Injectable()
export class RoleFromPathResolver implements Resolve<PeopleModels.Role> {
  constructor(private roleRepository: PeopleRoleRepository, private dataService: DataService, private router: Router) {}

  resolve(activatedRouteSnapshot: ActivatedRouteSnapshot): Observable<PeopleModels.Role> {
    const roleId = activatedRouteSnapshot.params['role_id'];

    if (roleId !== 'new') {
      return this.roleRepository.find(roleId as string).pipe(
        map((role: PeopleModels.Role) => new PeopleModels.Role(role)),
        tap((role: PeopleModels.Role) => this.dataService.setRole(role))
      );
    } else if (!this.dataService.role.value) {
      this.router.navigate(['/roles/list']);
    }
  }
}
