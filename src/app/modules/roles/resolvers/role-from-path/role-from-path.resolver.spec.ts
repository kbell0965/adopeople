import { TestBed, getTestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpClientTestingModule } from '@angular/common/http/testing';

import { PeopleRoleRepository, PeopleModels } from '@pplsi-core/datalayer';
import { PeopleFactories } from '@pplsi-core/factories';
import { DataService } from '../../../../services/data/data.service';
import { RoleFromPathResolver } from './role-from-path.resolver';
import { RouterTestingModule } from '@angular/router/testing';

describe('RoleFromPathResolver', () => {
  let injector: TestBed;
  let roleFromPathResolver: RoleFromPathResolver;
  let roleRepository: PeopleRoleRepository;
  let dataService: DataService;
  const fakeRole: object = PeopleFactories.RoleFactory.build();
  const role: PeopleModels.Role = new PeopleModels.Role(fakeRole);

  const activatedRouteSnapshotStub = {
    params: {
      role_id: fakeRole['id']
    }
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        RoleFromPathResolver,
        PeopleRoleRepository,
        DataService
      ],
      imports: [
        HttpClientTestingModule,
        RouterTestingModule
      ]
    });

    injector = getTestBed();
    roleFromPathResolver = injector.get(RoleFromPathResolver);
    roleRepository = injector.get(PeopleRoleRepository);
    dataService = injector.get(DataService);

    spyOn(roleRepository, 'find').and.returnValue(of(role));
  });

  describe('#resolve', () => {
    it('gets role from the PeopleRoleRepository', (done) => {
      roleFromPathResolver.resolve(activatedRouteSnapshotStub as any).subscribe((returnedRole: PeopleModels.Role) => {
        expect(returnedRole).toEqual(new PeopleModels.Role(role));
        done();
      });
    });

    it('gets role from the PeopleRoleRepository with the role id from the path', (done) => {
      roleFromPathResolver.resolve(activatedRouteSnapshotStub as any).subscribe(() => {
        expect(roleRepository.find).toHaveBeenCalledWith(role['id']);
        done();
      });
    });

    it('persists role state by calling dataService setRole', (done) => {
      spyOn(dataService, 'setRole');
      roleFromPathResolver.resolve(activatedRouteSnapshotStub as any).subscribe(() => {
        expect(dataService.setRole).toHaveBeenCalledWith(new PeopleModels.Role(role));
        done();
      });
    });
  });
});
