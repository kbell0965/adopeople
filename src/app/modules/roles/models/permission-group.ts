import { PermissionRow } from './permission-row';

export interface PermissionGroup {
    title: string;
    headings: string[];
    rows: PermissionRow[];
}
