import { PermissionGroup } from './permission-group';

export interface PermissionSection {
    description: string;
    groups: PermissionGroup[];
    permission;
    title: string;
}
