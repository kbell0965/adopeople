export interface PermissionRow {
    title: string;
    permissions: string[];
}
