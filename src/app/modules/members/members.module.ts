import { NgModule } from '@angular/core';
import { MatCheckboxModule, MatCardModule, MatProgressSpinnerModule, PageEvent } from '@angular/material';

import { HighlightPipe } from '../../shared/pipes/highlight.pipe';
import { MembersRoutingModule } from './members-routing.module';
import { MemberSearchComponent } from './components/member-search/member-search.component';
import {
  MemberPredictiveResultComponent
} from './components/member-predictive-result/member-predictive-result.component';
import { PplsiCoreDatalayerModule } from '@pplsi-core/datalayer';
import { MemberSearchResultComponent } from './components/member-search-result/member-search-result.component';
import { LocalizationService } from '@pplsi-core/shared-modules/dist/src/services/localization.service';
import { SharedModule } from 'src/app/shared/shared.module';
import { UserMergeDialogModule } from './components/member-search/user-merge-dialog/user-merge-dialog.module';

@NgModule({
  declarations: [
    HighlightPipe,
    MemberPredictiveResultComponent,
    MemberSearchComponent,
    MemberSearchResultComponent
  ],
  imports: [
    MatCardModule,
    MatCheckboxModule,
    MatProgressSpinnerModule,
    MembersRoutingModule,
    PplsiCoreDatalayerModule.forRoot(),
    SharedModule,
    UserMergeDialogModule
  ],
  providers: [
    HighlightPipe,
    LocalizationService,
    PageEvent,
    {
      provide: 'Window',
      useValue: window
    },
    {
      provide: 'localizations',
      useValue: [
        { locale: 'en-US', currencyCode: 'USD', dateFormat: 'MM/dd/yyyy' },
        { locale: 'en-GB', currencyCode: 'GBP', dateFormat: 'dd/MM/yyyy' },
        { locale: 'en-CA', currencyCode: 'CAD', dateFormat: 'yyyy-MM-dd' }
      ]
    },
    {
      provide: 'defaultLocale',
      useValue: 'en-US'
    }
  ]
})
export class MembersModule { }
