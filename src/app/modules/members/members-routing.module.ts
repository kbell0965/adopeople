import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MemberSearchComponent } from './components/member-search/member-search.component';

const routes: Routes = [
  { path: '', component: MemberSearchComponent },
  { path: '**', redirectTo: '', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MembersRoutingModule { }
