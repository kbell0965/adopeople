import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { PartnerFactories } from '@pplsi-core/factories/dist/@pplsi-core/factories';
import { MemberSearchResultComponent } from './member-search-result.component';
import { PartnerUserViewModel } from '../../../../models/partner-user-view-model';
import { RouterTestingModule } from '@angular/router/testing';
import { LocalizationService } from '@pplsi-core/shared-modules/dist/src/services/localization.service';
import { MatCheckboxModule } from '@angular/material';

describe('MemberSearchResultComponent.Template', () => {
  let component: MemberSearchResultComponent;
  let fixture: ComponentFixture<MemberSearchResultComponent>;

  const testUsers = ['jonny', 'jane', 'jonk', 'janet'].map(testUser => {
    return new PartnerUserViewModel(PartnerFactories.UserFactory.build({
      first_name: testUser,
      email: `${testUser}@example.com`,
      phone_number: `4053211234`
    }));
  });

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MemberSearchResultComponent ],
      imports: [
        RouterTestingModule,
        MatCheckboxModule
      ],
      providers: [
        LocalizationService,
        {
          provide: 'Window',
          useValue: window
        },
        {
          provide: 'localizations',
          useValue: [
            { locale: 'en-US', currencyCode: 'USD', dateFormat: 'MM/dd/yyyy' }
          ]
        },
        {
          provide: 'defaultLocale',
          useValue: 'en-US'
        }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MemberSearchResultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  describe('Search Results', () => {
    it('should have a search results container', () => {
      const el = fixture.debugElement.query(By.css('.search-results'));
      expect(el).toBeDefined();
    });

    describe('with no members', () => {
      it('should display the no results message', () => {
        const el = fixture.debugElement.query(By.css('.no-results'));
        expect(el).toBeDefined();
      });

      it('should display the query in the no results message', () => {
        component.query = 'testquery';
        fixture.detectChanges();
        const el = fixture.debugElement.query(By.css('.no-results'));
        expect(el.nativeElement.innerHTML).toContain(component.query);
      });
    });

    describe('with member results', () => {
      let elCards;

      beforeEach(() => {
        component.members = testUsers;
        component.query = testUsers[0].first_name;
        fixture.detectChanges();
        elCards = fixture.debugElement.queryAll(By.css('.member-card'));
      });

      it('should hide the no results container', () => {
        expect(fixture.debugElement.query(By.css('.no-results'))).toBeNull();
      });

      it('should include a header displaying the query', () => {
        const elHeader = fixture.debugElement.query(By.css('.search-results-header'));
        expect(elHeader.nativeElement.innerHTML).toContain(component.query);
      });

      it('search result cards should have a card for every member', () => {
        expect(elCards.length).toBe(component.members.length);
      });

      describe('search result card', () => {
        it('should have 6 child division elements regardless of null props', () => {
          component.members[1].first_name = null;
          component.members[1].last_name = null;
          component.members[1].phone_number = null;
          component.members[1].addresses = [];
          component.members[1].unique_product_names = [];
          fixture.detectChanges();
          const elCard = fixture.debugElement.queryAll(By.css('.member-card'))[1];
          const elCardDivs = elCard.queryAll(By.css('.card > div'));
          expect(elCardDivs.length).toBe(6);
        });

        it('should display the member name', () => {
          const elCardMemberName = fixture.debugElement.query(By.css('.member-card .member-name'));
          expect(elCardMemberName.nativeElement.innerHTML)
            .toContain(testUsers[0].fullName());
        });

        it('should display the member email', () => {
          const elCardEmail = fixture.debugElement.query(By.css('.member-card .member-email'));
          expect(elCardEmail.nativeElement.innerHTML).toContain(testUsers[0].email);
        });

        it('should display the member location', () => {
          const elCardEmail = fixture.debugElement.query(By.css('.member-card .member-location'));
          expect(elCardEmail.nativeElement.innerHTML).toContain(testUsers[0].formattedLocation());
        });

        it('should display the member plans', () => {
          const elCardPlans = fixture.debugElement.query(By.css('.member-card .member-active-plans'));
          expect(elCardPlans.nativeElement.innerHTML).toContain(testUsers[0].serviceablePlanNames());
        });

        describe('member phone numbers', () => {
          it('should display the phone field even when there are no phone numbers', () => {
            component.members[0].phone_number = null;
            component.members[0].formattedHomePhoneNumber = () => null;
            fixture.detectChanges();
            const elCard = fixture.debugElement.queryAll(By.css('.search-results .member-card'))[0];
            const elCardPhones = elCard.query(By.css('.member-phone'));
            expect(elCardPhones).toBeDefined();
          });

          it('should display the member primary phone number', () => {
            const elCardPhones = fixture.debugElement.query(By.css('.member-card .member-phone'));
            expect(elCardPhones.nativeElement.innerHTML)
              .toContain(testUsers[0].formattedPhoneNumber(component.locale, 'primary'));
          });

          it('should display the member secondary phone number', () => {
            const elCardPhones = fixture.debugElement.query(By.css('.member-card .member-phone'));
            expect(elCardPhones.nativeElement.innerHTML)
              .toContain(testUsers[0].formattedPhoneNumber(component.locale, 'secondary'));
          });

          it('should not display the comma when there is no secondary phone number', () => {
            component.members[0].contact_methods.pop();
            const elCardPhones = fixture.debugElement.query(By.css('.member-card .member-phone'));
            expect(elCardPhones.nativeElement.innerHTML).not.toContain(',');
          });
        });
      });
    });

    describe('user selection for merge', () => {
      let elCards;

      beforeEach(() => {
        component.members = testUsers;
        component.query = testUsers[0].first_name;
        fixture.detectChanges();
        elCards = fixture.debugElement.queryAll(By.css('.member-card'));
      });

      it('should not have a footer toolbar when we do not have a join', () => {
        const elFooter = fixture.debugElement.query(By.css('.footer'));

        expect(elFooter).toBe(null);
      });

      it('should display footer toolbar when we have a join', () => {
        component.selection.add(testUsers[0]);
        component.selection.add(testUsers[1]);
        fixture.detectChanges();
        const elFooter = fixture.debugElement.query(By.css('.footer'));

        expect(elFooter).toBeTruthy();
      });

      it('should not have any checkboxes checked by default', () => {
        const checks = fixture.debugElement.queryAll(By.css('.mat-checkbox-checked'));
        expect(checks.length).toBe(0);
      });

      it('should have equal checked boxes to selections', () => {
        component.selection.add(testUsers[0]);
        fixture.detectChanges();
        const checks = fixture.debugElement.queryAll(By.css('.mat-checkbox-checked'));

        expect(checks.length).toBe(component.selection.size);
      });

      it('should toggle checked boxes to match selections', () => {
        component.handleIncludeInJoinChanged(testUsers[0]);
        component.handleIncludeInJoinChanged(testUsers[1]);
        component.handleIncludeInJoinChanged(testUsers[0]);
        fixture.detectChanges();
        const checks = fixture.debugElement.queryAll(By.css('.mat-checkbox-checked'));
        expect(checks.length).toBe(1);
      });

      it('should have checkboxes for each user when making selections', () => {
        fixture.detectChanges();
        const checks = fixture.debugElement.queryAll(By.css('.mat-checkbox'));
        expect(checks.length).toEqual(testUsers.length);
      });

      it('should remove other checkboxes when have total selections', () => {
        component.selection.add(testUsers[0]);
        component.selection.add(testUsers[1]);

        fixture.detectChanges();
        const checks = fixture.debugElement.queryAll(By.css('.mat-checkbox'));
        expect(checks.length).toEqual(testUsers.length - component.selection.size);
      });
    });
  });
});
