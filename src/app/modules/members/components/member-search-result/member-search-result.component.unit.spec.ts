import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MemberSearchResultComponent } from './member-search-result.component';
import { RouterTestingModule } from '@angular/router/testing';
import { LocalizationService } from '@pplsi-core/shared-modules/dist/src/services/localization.service';
import { PartnerFactories } from '@pplsi-core/factories/dist/@pplsi-core/factories';
import { PartnerUserViewModel } from '../../../../models/partner-user-view-model';
import { MatCheckboxModule } from '@angular/material';

describe('MemberSearchResultComponent.Unit', () => {
  let component: MemberSearchResultComponent;
  let fixture: ComponentFixture<MemberSearchResultComponent>;
  let localizationService: LocalizationService;
  const testUsers = ['jonny', 'jane', 'jonk', 'janet'].map(testUser => {
    return new PartnerUserViewModel(PartnerFactories.UserFactory.build({
      first_name: testUser,
      email: `${testUser}@example.com`,
      phone_number: `4053211234`
    }));
  });

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MemberSearchResultComponent ],
      imports: [
        RouterTestingModule,
        MatCheckboxModule
      ],
      providers: [
        LocalizationService,
        {
          provide: 'Window',
          useValue: window
        },
        {
          provide: 'localizations',
          useValue: [
            { locale: 'en-US', currencyCode: 'USD', dateFormat: 'MM/dd/yyyy' }
          ]
        },
        {
          provide: 'defaultLocale',
          useValue: 'en-US'
        }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MemberSearchResultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    localizationService = TestBed.get(LocalizationService);
  });

  it('should set the locale on creation', () => {
    expect(component.locale).toBe(localizationService.getLocale().slice(-2));
  });

  it('should set an empty members array if members are not provided', () => {
    expect(component.members).toEqual([]);
  });

  describe('#handleIncludeInJoinChanged', () => {
    it('should set selection when ', () => {
      component.handleIncludeInJoinChanged(testUsers[0]);

      expect(component.selection.size).toEqual(1);
    });

    it('should toggle existing selection', () => {
      component.selection.add(testUsers[0]);
      component.handleIncludeInJoinChanged(testUsers[0]);

      expect(component.selection.size).toEqual(0);
    });
  });

  describe('#handleCancelClick', () => {
    it('should set selection when ', () => {
      component.selection.add(testUsers[0]);
      component.handleCancelClick();

      expect(component.selection.size).toEqual(0);
    });

    it('should clear existing selection', () => {
      const selection = component.selection;
      const clearSpy = spyOn(selection, 'clear');
      component.handleCancelClick();

      expect(clearSpy).toHaveBeenCalled();
    });

    it('should clear existing selection', () => {
      const selection = component.selection;
      const clearSpy = spyOn(selection, 'clear');
      component.handleCancelClick();

      expect(clearSpy).toHaveBeenCalled();
    });
  });
});
