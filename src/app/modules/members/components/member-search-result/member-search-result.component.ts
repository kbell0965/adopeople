import { Component, Input, Output, OnChanges, SimpleChanges, EventEmitter } from '@angular/core';
import { LocalizationService } from '@pplsi-core/shared-modules/dist/src/services/localization.service';
import { PartnerUserViewModel } from '../../../../models/partner-user-view-model';

@Component({
  selector: 'member-search-result',
  templateUrl: './member-search-result.component.html',
  styleUrls: ['./member-search-result.component.scss']
})
export class MemberSearchResultComponent implements OnChanges {
  locale: string;
  selection: Set<PartnerUserViewModel> = new Set<PartnerUserViewModel>();
  hasJoin = 2;

  @Input()
  members: PartnerUserViewModel[];

  @Input()
  query: string;

  @Output()
  merge: EventEmitter<string[]> = new EventEmitter<string[]>();

  constructor(private localizationService: LocalizationService) {
    this.locale = this.localizationService.getLocale().slice(-2);
    this.members = this.members ? this.members : [];
  }

  ngOnChanges() {
    this.selection.clear();
  }

  handleIncludeInJoinChanged(member: PartnerUserViewModel) {
    return this.selection.delete(member) ? this.selection : this.selection.add(member);
  }

  handleCancelClick(): void {
    this.selection.clear();
  }

  handleMergeClick(): void {
    const usersToMerge: string[] = Array.from(this.selection).map(user => user.id);
    this.selection.clear();
    this.merge.emit(usersToMerge);
  }
}
