import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { By } from '@angular/platform-browser';
import { MemberSearchComponent } from './member-search.component';
import { MemberSearchResultComponent } from '../member-search-result/member-search-result.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { MemberPredictiveResultComponent } from '../member-predictive-result/member-predictive-result.component';
import { MatProgressSpinnerModule } from '@angular/material';
import { PplsiCoreDatalayerModule } from '@pplsi-core/datalayer';
import { LocalizationService } from '@pplsi-core/shared-modules/dist/src/services/localization.service';
import { PartnerFactories } from '@pplsi-core/factories/dist/@pplsi-core/factories';
import { PartnerUserViewModel } from '../../../../models/partner-user-view-model';
import { UserMergeDialogModule } from './user-merge-dialog/user-merge-dialog.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatCheckboxModule } from '@angular/material';

describe('MemberSearchComponent.Template', () => {
  let component: MemberSearchComponent;
  let fixture: ComponentFixture<MemberSearchComponent>;

  const testUsers = ['jonny', 'jane', 'jonk', 'janet'].map(testUser => {
    return new PartnerUserViewModel(PartnerFactories.UserFactory.build({
      first_name: testUser,
      email: `${testUser}@example.com`,
      phone_number: `4053211234`
    }));
  });

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        BrowserAnimationsModule,
        HttpClientTestingModule,
        ReactiveFormsModule,
        RouterTestingModule,
        MatProgressSpinnerModule,
        PplsiCoreDatalayerModule.forRoot(),
        UserMergeDialogModule,
        MatCheckboxModule
      ],
      declarations: [
        MemberPredictiveResultComponent,
        MemberSearchComponent,
        MemberSearchResultComponent
      ],
      providers: [
        LocalizationService,
        {
          provide: 'Window',
          useValue: window
        },
        {
          provide: 'localizations',
          useValue: [
            { locale: 'en-US', currencyCode: 'USD', dateFormat: 'MM/dd/yyyy' },
            { locale: 'en-GB', currencyCode: 'GBP', dateFormat: 'dd/MM/yyyy' },
            { locale: 'en-CA', currencyCode: 'CAD', dateFormat: 'yyyy-MM-dd' }
          ]
        },
        {
          provide: 'defaultLocale',
          useValue: 'en-US'
        }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MemberSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  describe('Logo', () => {
    it('should be shown if search has not yet been submitted', () => {
      const el = fixture.debugElement.query(By.css('.logo img'));
      expect(el).toBeTruthy();
    });

    it('should not be shown if search has been submitted', () => {
      component.searchSubmitted = true;
      fixture.detectChanges();
      expect(fixture.debugElement.query(By.css('.logo img'))).toBeNull();
    });
  });

  describe('Search Input', () => {
    it('has a search input box', () => {
      expect(fixture.debugElement.query(By.css('.search-input')).name).toEqual('input');
    });

    it('button has inner text contents', () => {
      const el = fixture.debugElement.query(By.css('.search-button'));
      expect(el.nativeElement.innerText).toBe('\u00a0'); // &nbsp;
    });

    it('has a class .with-members when there are members', () => {
      component.members = testUsers;
      fixture.detectChanges();
      expect(fixture.debugElement.query(By.css('.form-wrapper.with-members'))).toBeTruthy();
    });

    describe('Search Submit', () => {
      it('has placeholder text explaining which terms to query with', () => {
        const el = fixture.debugElement.query(By.css('.search-input'));
        expect(el.nativeElement.getAttribute('placeholder')).toEqual('Search Member name, email or phone number');
      });

      it('search button calls the search method when clicked', () => {
        spyOn(component, 'updateResults');
        const el = fixture.debugElement.query(By.css('.search-button'));
        component.searchForm.controls['query'].setValue('smith');
        fixture.detectChanges();
        el.nativeElement.click();
        expect(component.updateResults).toHaveBeenCalled();
      });
    });
  });
});
