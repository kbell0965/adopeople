import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { tap, debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { PageEvent } from '@angular/material/paginator';
import { PartnerModels, PartnerUserRepository } from '@pplsi-core/datalayer';
import { PeopleModels, PeopleUserRepository } from '@pplsi-core/datalayer';
import { environment } from '@environment';
import { PartnerUserViewModel } from 'src/app/models/partner-user-view-model';
import { takeUntil } from 'rxjs/operators';
import { Subject, forkJoin } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { UserMergeDialogComponent } from './user-merge-dialog/user-merge-dialog.component';

const INPUT_DEBOUNCE = 200;

@Component({
  selector: 'member-search',
  templateUrl: './member-search.component.html',
  styleUrls: ['member-search.component.scss']
})
export class MemberSearchComponent implements OnInit, OnDestroy {
  latestSearch: string;
  logoPath: string;
  members: PartnerUserViewModel[] = [];
  membersPageEvent: PageEvent = { pageIndex: 1, pageSize: 12, length: 0 };
  pageEvent: PageEvent;
  predictedMembers: PartnerUserViewModel[] = [];
  predictedMembersPageEvent: PageEvent = { pageIndex: 1, pageSize: 8, length: 0 };
  searching = false;
  searchForm: FormGroup;
  searchSubmitted = false;

  private _destroyed = new Subject<void>();

  constructor(
    private formBuilder: FormBuilder,
    private userRepository: PeopleUserRepository,
    private partnerUserRepository: PartnerUserRepository,
    public dialog: MatDialog
  ) { }

  get query() {
    return this.searchForm.get('query').value;
  }

  set query(query) {
    this.searchForm.controls['query'].setValue(query);
  }

  get searchActive() {
    return this.query ? true : false;
  }

  get showPredictedResults() {
    return this.predictedMembers.length;
  }

  ngOnInit() {
    this.logoPath = `assets/${environment.region}_LegalShieldWithBackground.svg`;
    this.searchForm = this.formBuilder.group({
      query: ''
    });
    this.searchForm.controls['query'].valueChanges
      .pipe(debounceTime(INPUT_DEBOUNCE), distinctUntilChanged())
      .subscribe((value: string) => this.updatePredictions(value));
  }

  ngOnDestroy() {
    this._destroyed.next();
    this._destroyed.complete();
  }

  clearPredictions(): void {
    this.predictedMembers = [];
  }

  loadMergeUsersDialog(usersIds: string[]): void {
    forkJoin(usersIds.map(userId => this.userRepository.find(userId)))
      .pipe(takeUntil(this._destroyed))
      .subscribe((users: PeopleModels.User[]) => this.openMergeUsersDialog(users));
  }

  openMergeUsersDialog(users: PeopleModels.User[]): void {
    const mergeDialogConfig = {
      maxHeight: 'calc(100vh - 112px)',
      minWidth: 800
    };

    this.dialog.open(UserMergeDialogComponent, {
      data: { users },
      ...mergeDialogConfig
    });
  }

  updatePredictions(query: string = ''): void {
    if (query.length > 2) {
      this.partnerUserRepository
        .findAll(query, this.predictedMembersPageEvent.pageIndex, this.predictedMembersPageEvent.pageSize)
        .pipe(takeUntil(this._destroyed))
        .subscribe((usersWithPagination: PartnerModels.UsersWithPagination) => {
          if (query !== this.latestSearch) {
            const users = usersWithPagination.users.slice(0, 8);
            this.predictedMembersPageEvent.length = usersWithPagination.total_length;
            this.predictedMembers = users.map((user: PartnerModels.User) => new PartnerUserViewModel(user));
          }
        });
    } else {
      this.clearPredictions();
    }
  }

  updateResults(query: string = ''): void {
    if ((query.length > 2) && (query !== this.latestSearch)) {
      this.searching = true;
      this.partnerUserRepository
        .findAll(query, this.membersPageEvent.pageIndex, this.membersPageEvent.pageSize)
        .pipe(tap(() => {
          this.searchSubmitted = true;
        }))
        .pipe(takeUntil(this._destroyed))
        .subscribe((usersWithPagination: PartnerModels.UsersWithPagination) => {
          this.latestSearch = query;
          this.membersPageEvent.length = usersWithPagination.total_length;
          this.members = usersWithPagination.users.map((user: PartnerModels.User) => new PartnerUserViewModel(user));
          this.clearPredictions();
          this.searching = false;
        });
    } else {
      this.clearPredictions();
    }
  }
}
