import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MergeDialogUserProfileComponent } from './merge-dialog-user-profile.component';
import { FeatherModule } from 'angular-feather';
import { Check, Minus } from 'angular-feather/icons';
import { AvatarModule } from 'ng-components.adonis';
import { LocalizationService } from '@pplsi-core/shared-modules/dist/src/services/localization.service';
import { By } from '@angular/platform-browser';
import { PeopleFactories } from '@pplsi-core/factories';
import { UserViewModel } from 'src/app/modules/users/models/user-view-model';
import { PeopleModels } from '@pplsi-core/datalayer';
import { DebugElement } from '@angular/core';

interface Localization { locale: string; dateFormat: string; }
const fakeAddresses: PeopleModels.Address[] = [new PeopleModels.Address(PeopleFactories.AddressFactory.build())];
const fakeContactMethods: PeopleModels.ContactMethod[] = [
  PeopleFactories.ContactMethodFactory.build({ label: 'cell', value: '8004441111' }),
  PeopleFactories.ContactMethodFactory.build({ label: 'home', value: '8004441111' }),
  PeopleFactories.ContactMethodFactory.build({ label: 'work', value: '8004441111' })
];
const fakeProducts: PeopleModels.Product[] = [
  PeopleFactories.ProductFactory.build({ name: 'ProductA' }),
  PeopleFactories.ProductFactory.build({ name: 'ProductB' })
];
const fakeAccount: PeopleModels.Account = new PeopleModels.Account({ products: fakeProducts });

const fakeUser: PeopleModels.User = new PeopleModels.User(PeopleFactories.UserFactory.build({
  date_of_birth: '1912-06-23T12:58:01.347Z',
  first_name: 'Alan',
  last_name: 'Turing',
  truncated_national_id: '325114945',
  contact_methods: fakeContactMethods,
  addresses: fakeAddresses,
  accounts: [fakeAccount]
}));
const fakeUserNoData: PeopleModels.User = new PeopleModels.User(PeopleFactories.UserFactory.build({
  addresses: [],
  contact_methods: [],
  date_of_birth: null,
  email: null,
  groups: null,
  truncated_national_id: null,
  accounts: []
}));
const testUserNoData: UserViewModel = new UserViewModel(fakeUserNoData);
const testUser: UserViewModel = new UserViewModel(fakeUser);
const defaultLocaleProvider: Localization = { locale: 'en-US', dateFormat: 'MM/dd/yyyy' };

describe('MergeDialogUserProfileComponent.Template', () => {
  let component: MergeDialogUserProfileComponent;
  let fixture: ComponentFixture<MergeDialogUserProfileComponent>;

  const setupTestBed = (localization: Localization = defaultLocaleProvider, user: UserViewModel = testUser) => {
    TestBed.configureTestingModule({
      declarations: [MergeDialogUserProfileComponent],
      imports: [AvatarModule, FeatherModule.pick({ Check, Minus })],
      providers: [
        LocalizationService,
        { provide: 'defaultLocale', useValue: localization.locale },
        { provide: 'localizations', useValue: [localization] },
        { provide: 'Window', useValue: window }
      ]
    }).compileComponents();
    fixture = TestBed.createComponent(MergeDialogUserProfileComponent);
    component = fixture.componentInstance;
    component.user = user;
    fixture.detectChanges();
  };

  describe('when "en-US" (default) locale', () => {
    beforeEach(() => setupTestBed());

    describe('with no user input', () => {
      beforeEach(() => {
        component.user = undefined;
        fixture.detectChanges();
      });

      it('has no user', () => expect(component.user).toBeFalsy());
      it('still creates', () => expect(component).toBeTruthy());
    });

    describe('with user input', () => {
      describe('profile header', () => {
        describe('avatar', () => {
          it('is present', () => expect(fixture.debugElement.query(By.css('.profile-header ls-avatar'))).not.toBeNull());

          it('has the expected initials', () => {
            const el = fixture.debugElement.query(By.css('.profile-header ls-avatar'));

            expect(el.nativeElement.innerHTML).toContain('AT');
          });
        });

        it('displays the full name', () => {
          const el = fixture.debugElement.query(By.css('.profile-header h3'));

          expect(el.nativeElement.innerHTML).toEqual('Alan Turing');
        });

        it('displays the product names as csv', () => {
          const el = fixture.debugElement.query(By.css('.profile-header .header-labels p'));

          expect(el.nativeElement.innerHTML).toEqual(testUser.csvProductNames);
        });
      });

      describe('profile body', () => {
        describe('national id field', () => {
          it('displays label as "Social Security Number"', () => {
            const el = fixture.debugElement.query(By.css('.profile-body .national-id h4'));
            expect(el.nativeElement.innerHTML).toContain('Social Security Number');
          });

          it('displays national id as "••• •• 4945"', () => {
            const el = fixture.debugElement.query(By.css('.profile-body .national-id p'));
            expect(el.nativeElement.innerHTML).toEqual('••• •• 4945');
          });
        });

        describe('date of birth field', () => {
          it('has a label "Date of Birth"', () => {
            const el = fixture.debugElement.query(By.css('.profile-body .date-of-birth h4'));

            expect(el.nativeElement.innerHTML).toEqual('Date of Birth');
          });

          it('displays the date of birth as "06/23/1912"', () => {
            const el = fixture.debugElement.query(By.css('.profile-body .date-of-birth p'));

            expect(el.nativeElement.innerHTML).toEqual('06/23/1912');
          });
        });

        describe('cell phone field', () => {
          it('has a label "Cell Phone"', () => {
            const el = fixture.debugElement.query(By.css('.profile-body .cell-phone h4'));

            expect(el.nativeElement.innerHTML).toEqual('Cell Phone');
          });

          it('displays the formatted cell phone value', () => {
            const el = fixture.debugElement.query(By.css('.profile-body .cell-phone p'));

            expect(el.nativeElement.innerHTML).toEqual(component.user.formattedPhoneNumber(component.locale, 'cell'));
          });
        });

        describe('work phone field', () => {
          it('has a label "Work Phone"', () => {
            const el = fixture.debugElement.query(By.css('.profile-body .work-phone h4'));

            expect(el.nativeElement.innerHTML).toEqual('Work Phone');
          });

          it('displays the formatted work phone value', () => {
            const el = fixture.debugElement.query(By.css('.profile-body .work-phone p'));

            expect(el.nativeElement.innerHTML).toEqual(component.user.formattedPhoneNumber(component.locale, 'work'));
          });
        });

        describe('home phone field', () => {
          it('has a label "Home Phone"', () => {
            const el = fixture.debugElement.query(By.css('.profile-body .home-phone h4'));

            expect(el.nativeElement.innerHTML).toEqual('Home Phone');
          });

          it('displays the formatted home phone value', () => {
            const el = fixture.debugElement.query(By.css('.profile-body .home-phone p'));

            expect(el.nativeElement.innerHTML).toEqual(component.user.formattedPhoneNumber(component.locale, 'home'));
          });
        });

        describe('address field', () => {
          it('has a label "Address"', () => {
            const el = fixture.debugElement.query(By.css('.profile-body .address h4'));

            expect(el.nativeElement.innerHTML).toEqual('Address');
          });

          describe('displays address parts', () => {
            it('address 1', () => {
              const element: DebugElement = fixture.debugElement.query(By.css('.profile-body .address'));

              expect(element.nativeElement.innerHTML).toContain(fakeAddresses[0].address_1);
            });

            it('displays address 2', () => {
              const element: DebugElement = fixture.debugElement.query(By.css('.profile-body .address'));

              expect(element.nativeElement.innerHTML).toContain(fakeAddresses[0].address_2);
            });

            it('displays the city', () => {
              const element: DebugElement = fixture.debugElement.query(By.css('.profile-body .address'));

              expect(element.nativeElement.innerHTML).toContain(fakeAddresses[0].locality);
            });

            it('displays the region', () => {
              const element: DebugElement = fixture.debugElement.query(By.css('.profile-body .address'));

              expect(element.nativeElement.innerHTML).toContain(fakeAddresses[0].administrative_area);
            });

            it('displays the country', () => {
              const element: DebugElement = fixture.debugElement.query(By.css('.profile-body .address'));

              expect(element.nativeElement.innerHTML).toContain(fakeAddresses[0].country);
            });

            it('displays the postal code', () => {
              const element: DebugElement = fixture.debugElement.query(By.css('.profile-body .address'));

              expect(element.nativeElement.innerHTML).toContain(fakeAddresses[0].postal_code);
            });
          });
        });

        describe('email field', () => {
          it('has a label "Email"', () => {
            const el = fixture.debugElement.query(By.css('.profile-body .email h4'));

            expect(el.nativeElement.innerHTML).toEqual('Email');
          });

          describe('email address field value', () => {
            it('displays the email address', () => {
              const element: DebugElement = fixture.debugElement.query(By.css('.email p a'));

              expect(element.nativeElement.innerHTML).toEqual(component.user.email);
            });

            it('links to the email address', () => {
              const element: DebugElement = fixture.debugElement.query(By.css('.email p a'));

              expect(element.nativeElement.getAttribute('href')).toEqual(`mailto:${component.user.email}`);
            });
          });
        });
      });
    });
  });

  describe('when Canadian locale', () => {
    beforeEach(() => setupTestBed({ locale: 'en-CA', dateFormat: 'yyyy-MM-dd' }));

    describe('profile body', () => {
      describe('national id field', () => {
        it('displays label as "Social Insurance Number"', () => {
          const elNationalIdLabel = fixture.debugElement.query(By.css('.profile-body .national-id h4'));

          expect(elNationalIdLabel.nativeElement.innerHTML).toContain('Social Insurance Number');
        });
      });

      describe('date of birth field', () => {
        it('displays the date of birth as "1912-06-23"', () => {
          const elDateOfBirthLabel = fixture.debugElement.query(By.css('.profile-body .date-of-birth p'));

          expect(elDateOfBirthLabel.nativeElement.innerHTML).toEqual('1912-06-23');
        });
      });
    });
  });

  describe('when Great Britain locale', () => {
    beforeEach(() => setupTestBed({ locale: 'en-GB', dateFormat: 'dd/MM/yyyy' }));

    describe('profile body', () => {
      describe('national id field', () => {
        it('displays label as "National Insurance Number"', () => {
          const elNationalIdLabel = fixture.debugElement.query(By.css('.profile-body .national-id h4'));

          expect(elNationalIdLabel.nativeElement.innerHTML).toContain('National Insurance Number');
        });
      });

      describe('date of birth field', () => {
        it('displays the date of birth as "23/06/1912"', () => {
          const elDateOfBirthLabel = fixture.debugElement.query(By.css('.profile-body .date-of-birth p'));

          expect(elDateOfBirthLabel.nativeElement.innerHTML).toEqual('23/06/1912');
        });
      });
    });
  });

  describe('when user has no data', () => {
    beforeEach(() => setupTestBed(defaultLocaleProvider, testUserNoData));

    it('displays no text for product names if incoming products data is null', () => {
      expect(fixture.debugElement.query(By.css('.header-labels p')).nativeElement.innerHTML).toEqual('');
    });

    it('displays two minus icons when no date of birth is available', () => {
      expect(fixture.debugElement.queryAll(By.css('.date-of-birth i-feather')).length).toBe(2);
    });

    it('displays two minus icons when no national id is available', () => {
      expect(fixture.debugElement.queryAll(By.css('.national-id i-feather')).length).toBe(2);
    });

    it('displays two minus icons when no address is available', () => {
      expect(fixture.debugElement.queryAll(By.css('.address i-feather')).length).toBe(2);
    });

    it('displays two minus icons when no email is available', () => {
      expect(fixture.debugElement.queryAll(By.css('.email i-feather')).length).toBe(2);
    });

    it('displays two minus icons when no cell phone is available', () => {
      expect(fixture.debugElement.queryAll(By.css('.cell-phone i-feather')).length).toBe(2);
    });

    it('displays two minus icons when no work phone is available', () => {
      expect(fixture.debugElement.queryAll(By.css('.work-phone i-feather')).length).toBe(2);
    });

    it('displays two minus icons when no home phone is available', () => {
      expect(fixture.debugElement.queryAll(By.css('.home-phone i-feather')).length).toBe(2);
    });
  });
});
