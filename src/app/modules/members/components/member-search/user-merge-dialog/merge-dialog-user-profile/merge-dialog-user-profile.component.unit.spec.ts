import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MergeDialogUserProfileComponent } from './merge-dialog-user-profile.component';
import { FeatherModule } from 'angular-feather';
import { Check, Minus } from 'angular-feather/icons';
import { AvatarModule } from 'ng-components.adonis';
import { LocalizationService } from '@pplsi-core/shared-modules/dist/src/services/localization.service';
import { PeopleFactories } from '@pplsi-core/factories';
import { PeopleModels } from '@pplsi-core/datalayer';
import { UserViewModel } from 'src/app/modules/users/models/user-view-model';

interface Localization { locale: string; dateFormat: string; }
const fakeAddresses: PeopleModels.Address[] = [new PeopleModels.Address(PeopleFactories.AddressFactory.build())];
const fakeContactMethods: PeopleModels.ContactMethod[] = [
  PeopleFactories.ContactMethodFactory.build({ label: 'cell', value: '8004441111' }),
  PeopleFactories.ContactMethodFactory.build({ label: 'home', value: '8004441111' }),
  PeopleFactories.ContactMethodFactory.build({ label: 'work', value: '8004441111' })
];
const fakeUser: PeopleModels.User = new PeopleModels.User(PeopleFactories.UserFactory.build({
  date_of_birth: '1912-06-23T12:58:01.347Z',
  first_name: 'Alan',
  last_name: 'Turing',
  truncated_national_id: '325114945',
  contact_methods: fakeContactMethods,
  addresses: fakeAddresses
}));
const testUser: UserViewModel = new UserViewModel(fakeUser);
const defaultLocaleProvider: Localization = { locale: 'en-US', dateFormat: 'MM/dd/yyyy' };

describe('MergeDialogUserProfileComponent.Unit', () => {
  let component: MergeDialogUserProfileComponent;
  let fixture: ComponentFixture<MergeDialogUserProfileComponent>;

  const setupTestBed = (localization: Localization = defaultLocaleProvider, user: UserViewModel = testUser) => {
    TestBed.configureTestingModule({
      declarations: [MergeDialogUserProfileComponent],
      imports: [AvatarModule, FeatherModule.pick({ Check, Minus })],
      providers: [
        LocalizationService,
        { provide: 'defaultLocale', useValue: localization.locale },
        { provide: 'localizations', useValue: [localization] },
        { provide: 'Window', useValue: window }
      ]
    }).compileComponents();
    fixture = TestBed.createComponent(MergeDialogUserProfileComponent);
    component = fixture.componentInstance;
    component.user = user;
    fixture.detectChanges();
  };

  describe('when "en-US" (default) locale', () => {
    beforeEach(() => setupTestBed());

    describe('with no user input', () => {
      beforeEach(() => {
        component.user = undefined;
        fixture.detectChanges();
      });

      it('has no user', () => expect(component.user).toBeFalsy());
      it('still creates', () => expect(component).toBeTruthy());
    });

    describe('#ngOnInit', () => {
      it('sets the locale property to the component', () => expect(component.locale).toEqual('en-US'));
      it('sets the dateFormat property to the component', () => expect(component.dateFormat).toEqual('MM/dd/yyyy'));
      it('sets the address property to the component', () => expect(component.address).toEqual(fakeUser.addresses[0]));
      it('sets the cellPhone property to the component', () => expect(component.cellPhone).toEqual('(800) 444-1111'));
      it('sets the homePhone property to the component', () => expect(component.homePhone).toEqual('(800) 444-1111'));
      it('sets the workPhone property to the component', () => expect(component.workPhone).toEqual('(800) 444-1111'));
      it('sets the firstName property to the component', () => expect(component.firstName).toEqual('Alan'));
      it('sets the lastName property to the component', () => expect(component.lastName).toEqual('Turing'));
      it('sets the nationalId property to the component', () => expect(component.nationalId).toEqual('••• •• 4945'));
      it('sets the dateOfBirth property to the component', () => expect(component.dateOfBirth).toEqual('1912-06-23T12:58:01.347Z'));
      it('sets the email property to the component', () => expect(component.email).toEqual(fakeUser.email));
      it('sets the csv product names to the component', () => expect(component.csvProductNames).toEqual(testUser.csvProductNames));
    });
  });
});
