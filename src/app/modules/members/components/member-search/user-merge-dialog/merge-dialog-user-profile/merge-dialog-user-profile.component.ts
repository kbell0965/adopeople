import { Component, Input, OnInit } from '@angular/core';
import { LocalizationService } from '@pplsi-core/shared-modules/dist/src/services/localization.service';
import { UserViewModel } from 'src/app/modules/users/models/user-view-model';
import { PeopleModels } from '@pplsi-core/datalayer/dist/@pplsi-core/datalayer/public_api';

@Component({
  selector: 'ls-merge-dialog-user-profile',
  templateUrl: './merge-dialog-user-profile.component.html',
  styleUrls: ['./merge-dialog-user-profile.component.scss']
})
export class MergeDialogUserProfileComponent implements OnInit {
  @Input() user: UserViewModel;

  address: PeopleModels.Address;
  cellPhone: string;
  csvProductNames: string;
  dateFormat: string;
  dateOfBirth: string;
  email: string;
  firstName: string;
  homePhone: string;
  lastName: string;
  locale: string;
  nationalId: string;
  workPhone: string;

  constructor(private localizationService: LocalizationService) { }

  ngOnInit() {
    this.locale = this.localizationService.getLocale();
    this.dateFormat = this.localizationService.getDateFormat();

    if (this.user) {
      const { addresses, date_of_birth, email, first_name, last_name } = this.user;
      this.address = (addresses && addresses.length) ? addresses[0] : null;
      this.cellPhone = this.user.formattedPhoneNumber(this.locale, 'cell');
      this.homePhone = this.user.formattedPhoneNumber(this.locale, 'home');
      this.workPhone = this.user.formattedPhoneNumber(this.locale, 'work');
      this.firstName = first_name;
      this.lastName = last_name;
      this.nationalId = this.user.nationalId(this.locale);
      this.dateOfBirth = date_of_birth;
      this.email = email;
      this.csvProductNames = this.user.csvProductNames;
    }
  }
}
