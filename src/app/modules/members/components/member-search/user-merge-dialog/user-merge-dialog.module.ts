import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserMergeDialogComponent } from './user-merge-dialog.component';
import { FeatherModule } from 'angular-feather';
import { Check, Minus, ArrowLeft } from 'angular-feather/icons';
import { MatButtonModule, MatStepperModule, MatCheckboxModule, MatRadioModule } from '@angular/material';
import { MatDialogModule, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { MergeDialogUserProfileComponent } from './merge-dialog-user-profile/merge-dialog-user-profile.component';
import { AvatarModule } from 'ng-components.adonis';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MergeDialogUserInfoComponent } from './merge-dialog-user-info/merge-dialog-user-info/merge-dialog-user-info.component';

@NgModule({
  declarations: [UserMergeDialogComponent, MergeDialogUserProfileComponent, MergeDialogUserInfoComponent],
  imports: [
    AvatarModule,
    CommonModule,
    FeatherModule.pick({ Check, Minus, ArrowLeft }),
    FormsModule,
    MatButtonModule,
    MatCheckboxModule,
    MatDialogModule,
    MatRadioModule,
    MatStepperModule,
    ReactiveFormsModule
  ],
  exports: [
    AvatarModule,
    MatButtonModule,
    MatCheckboxModule,
    MatDialogModule,
    MatStepperModule,
    MergeDialogUserProfileComponent,
    UserMergeDialogComponent,
    MergeDialogUserInfoComponent
  ],
  providers: [
    { provide: MatDialogRef, useValue: {} },
    { provide: MAT_DIALOG_DATA, useValue: [] }
  ],
  entryComponents: [UserMergeDialogComponent]
})
export class UserMergeDialogModule { }
