import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { UserMergeDialogComponent } from './user-merge-dialog.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { By } from '@angular/platform-browser';
import { UserMergeDialogModule } from './user-merge-dialog.module';
import { LocalizationService } from '@pplsi-core/shared-modules/dist/src/services/localization.service';
import { FormsModule } from '@angular/forms';

describe('UserMergeDialogComponent.Template', () => {
  let component: UserMergeDialogComponent;
  let fixture: ComponentFixture<UserMergeDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        BrowserAnimationsModule,
        UserMergeDialogModule
      ],
      providers: [
        LocalizationService,
        { provide: 'Window', useValue: window },
        { provide: 'localizations', useValue: [{ locale: 'en-US', currencyCode: 'USD', dateFormat: 'MM/dd/yyyy' }] },
        { provide: 'defaultLocale', useValue: 'en-US' }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserMergeDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  describe('header', () => {
    it('has the expected title', () => {
      const el = fixture.debugElement.query(By.css('.dialog-header h2'));
      expect(el.nativeElement.innerHTML).toContain('Merge users');
    });

    it('has the expected sub title', () => {
      const el = fixture.debugElement.query(By.css('.dialog-header p'));
      expect(el.nativeElement.innerHTML).toContain('Completing this action will merge user information.');
    });

    describe('merge users warning', () => {
      it('does not display if not on the last step', () => {
        component.nextStepClick();
        fixture.detectChanges();
        const el = fixture.debugElement.query(By.css('.dialog-header p.warning'));
        expect(el).toBeNull();
      });

      it('displays if on the last step', () => {
        component.nextStepClick();
        component.nextStepClick();
        fixture.detectChanges();
        const el = fixture.debugElement.query(By.css('.dialog-header p.warning'));
        expect(el).not.toBeNull();
      });
    });
  });

  describe('body', () => {
    describe('stepper', () => {
      it('exists', () => {
        const el = fixture.debugElement.query(By.css('.dialog-body mat-horizontal-stepper'));
        expect(el).not.toBeNull();
      });

      it('has 3 steps', () => {
        const els = fixture.debugElement.queryAll(By.css('.dialog-body .mat-step-header'));
        expect(els.length).toBe(3);
      });

      describe('steps', () => {
        describe('selected users', () => {
          it('has a label of "Selected users"', () => {
            const els = fixture.debugElement.queryAll(By.css('.dialog-body .mat-step-header .mat-step-text-label'));
            expect(els[0].nativeElement.innerHTML).toContain('Selected users');
          });
        });

        describe('user information', () => {
          it('has a label of "User information"', () => {
            const els = fixture.debugElement.queryAll(By.css('.dialog-body .mat-step-header .mat-step-text-label'));
            expect(els[1].nativeElement.innerHTML).toContain('User information');
          });
        });

        describe('review', () => {
          it('has a label of "Review"', () => {
            const els = fixture.debugElement.queryAll(By.css('.dialog-body .mat-step-header .mat-step-text-label'));
            expect(els[2].nativeElement.innerHTML).toContain('Review');
          });

          it('has one merge dialog user profile', () => {
            const els = fixture.debugElement.queryAll(By.css('.dialog-body ls-merge-dialog-user-profile'));
            expect(els.length).toBe(1);
          });
        });
      });
    });
  });

  describe('footer', () => {
    it('has a cancel button', () => {
      const el = fixture.debugElement.query(By.css('.dialog-footer .merge-user-cancel button'));
      expect(el.nativeElement.innerHTML).toContain('Cancel');
    });

    describe('user merge acknowledgement', () => {
      beforeEach(() => {
        component.nextStepClick();
        component.nextStepClick();
        fixture.detectChanges();
      });

      it('exists when on the final step', () => {
        const el = fixture.debugElement.query(By.css('.dialog-footer .merge-user-acknowledge'));
        expect(el.nativeElement.innerHTML).toContain('I acknowledge the merge.');
      });

      it('has an unchecked checkbox by default', () => {
        const el = fixture.debugElement.query(By.css('.dialog-footer .merge-user-acknowledge mat-checkbox'));
        expect(el.nativeElement.checked).toBeFalsy();
      });
    });

    describe('previous step button', () => {
      beforeEach(() => {
        component.nextStepClick();
        fixture.detectChanges();
      });

      it('exists when stepper is past the first step', () => {
        const el = fixture.debugElement.query(By.css('.dialog-footer #merge-action-previous'));
        expect(el.nativeElement.innerHTML).toContain('Previous Step');
      });

      it('is a primary button', () => {
        const el = fixture.debugElement.query(By.css('.dialog-footer #merge-action-previous'));
        expect(el.nativeElement.getAttribute('color')).toContain('primary');
      });

      it('does not exist when on the first step', () => {
        component.previousStepClick();
        fixture.detectChanges();
        const el = fixture.debugElement.query(By.css('.dialog-footer #merge-action-previous'));
        expect(el).toBeNull();
      });
    });

    describe('next step button', () => {
      it('exists', () => {
        const el = fixture.debugElement.query(By.css('.dialog-footer #merge-action-next'));
        expect(el.nativeElement.innerHTML).toContain('Next Step');
      });

      it('is a primary button', () => {
        const el = fixture.debugElement.query(By.css('.dialog-footer #merge-action-next'));
        expect(el.nativeElement.getAttribute('class')).toContain('primary');
      });

      it('does not exist when on the final step', () => {
        component.nextStepClick();
        component.nextStepClick();
        fixture.detectChanges();
        const el = fixture.debugElement.query(By.css('.dialog-footer #merge-action-next'));
        expect(el).toBeNull();
      });
    });

    describe('final step merge button', () => {
      beforeEach(() => {
        component.nextStepClick();
        component.nextStepClick();
        fixture.detectChanges();
      });

      it('exists', () => {
        const el = fixture.debugElement.query(By.css('.dialog-footer #merge-action-final'));
        expect(el.nativeElement.innerHTML).toContain('Merge Users');
      });

      it('is a primary button', () => {
        const el = fixture.debugElement.query(By.css('.dialog-footer #merge-action-final'));
        expect(el.nativeElement.getAttribute('class')).toContain('primary');
      });

      it('does not exist when not on the final step', () => {
        component.previousStepClick();
        fixture.detectChanges();
        const el = fixture.debugElement.query(By.css('.dialog-footer #merge-action-final'));
        expect(el).toBeNull();
      });

      it('is disabled until the acknowledgement checkbox is checked', () => {
        const el = fixture.debugElement.query(By.css('.dialog-footer #merge-action-final'));
        expect(el.nativeElement.getAttribute('ng-reflect-disabled')).toBe('true');
      });

      it('is enabled when the acknowledgement checkbox is checked', () => {
        const elCheckbox = fixture.debugElement.query(By.css('.dialog-footer .merge-user-acknowledge mat-checkbox'));
        elCheckbox.nativeElement.click();
        fixture.detectChanges();
        const el = fixture.debugElement.query(By.css('.dialog-footer #merge-action-final'));
        expect(el.nativeElement.getAttribute('ng-reflect-disabled')).toBe('true');
      });
    });
  });
});
