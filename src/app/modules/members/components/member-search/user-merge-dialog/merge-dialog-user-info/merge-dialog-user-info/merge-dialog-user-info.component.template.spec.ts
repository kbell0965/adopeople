import { MergeDialogUserInfoComponent } from './merge-dialog-user-info.component';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FeatherModule } from 'angular-feather';
import { Check, Minus } from 'angular-feather/icons';
import { AvatarModule } from 'ng-components.adonis';
import { LocalizationService } from '@pplsi-core/shared-modules/dist/src/services/localization.service';
import { By } from '@angular/platform-browser';
import { PeopleFactories } from '@pplsi-core/factories';
import { UserViewModel } from 'src/app/modules/users/models/user-view-model';
import { PeopleModels } from '@pplsi-core/datalayer';
import { MatRadioModule } from '@angular/material';

interface Localization { locale: string; dateFormat: string; }
const fakeAddressesA: PeopleModels.Address[] = [new PeopleModels.Address(PeopleFactories.AddressFactory.build())];
const fakeAddressesB: PeopleModels.Address[] = [new PeopleModels.Address(PeopleFactories.AddressFactory.build())];
const fakeContactMethodsA: PeopleModels.ContactMethod[] = [
  PeopleFactories.ContactMethodFactory.build({ label: 'cell', value: '9999999991' }),
  PeopleFactories.ContactMethodFactory.build({ label: 'home', value: '8888888881' }),
  PeopleFactories.ContactMethodFactory.build({ label: 'work', value: '7777777771' })
];
const fakeContactMethodsB: PeopleModels.ContactMethod[] = [
  PeopleFactories.ContactMethodFactory.build({ label: 'cell', value: '9999999992' }),
  PeopleFactories.ContactMethodFactory.build({ label: 'home', value: '8888888882' }),
  PeopleFactories.ContactMethodFactory.build({ label: 'work', value: '7777777772' })
];
const fakeUserA: PeopleModels.User = new PeopleModels.User(PeopleFactories.UserFactory.build({
  date_of_birth: '1999-05-19T12:58:01.347Z',
  first_name: 'Mace',
  last_name: 'Windu',
  truncated_national_id: '444994444',
  contact_methods: fakeContactMethodsA,
  addresses: fakeAddressesA
}));

const fakeUserB: PeopleModels.User = new PeopleModels.User(PeopleFactories.UserFactory.build({
  date_of_birth: '1999-05-20T12:58:01.347Z',
  first_name: 'Master Mace',
  last_name: 'Windu',
  truncated_national_id: '444994444',
  contact_methods: fakeContactMethodsB,
  addresses: fakeAddressesB
}));

const testUserA: UserViewModel = new UserViewModel(fakeUserA);
const testUserB: UserViewModel = new UserViewModel(fakeUserB);

const testUsers: UserViewModel[] = [testUserA, testUserB];

const defaultLocaleProvider: Localization = { locale: 'en-US', dateFormat: 'MM/dd/yyyy' };

describe('MergeDialogUserInfoComponent.Template', () => {
  let component: MergeDialogUserInfoComponent;
  let fixture: ComponentFixture<MergeDialogUserInfoComponent>;

  const setupTestBed = (localization: Localization = defaultLocaleProvider, users: UserViewModel[] = testUsers) => {
    TestBed.configureTestingModule({
      declarations: [MergeDialogUserInfoComponent],
      imports: [MatRadioModule, AvatarModule, FeatherModule.pick({ Check, Minus })],
      providers: [
        LocalizationService,
        { provide: 'defaultLocale', useValue: localization.locale },
        { provide: 'localizations', useValue: [localization] },
        { provide: 'Window', useValue: window }
      ]
    }).compileComponents();
    fixture = TestBed.createComponent(MergeDialogUserInfoComponent);
    component = fixture.componentInstance;
    component.users = users;
    fixture.detectChanges();
  };

  describe('when "en-US" (default) locale', () => {
    beforeEach(() => setupTestBed());

    describe('with two users', () => {
      it('headers are present' , () => {
        const userCount = fixture.debugElement.queryAll(By.css('.profile-header')).length;
        expect(userCount).toEqual(2);
      });

      it('bodies are present' , () => {
        const userCount = fixture.debugElement.queryAll(By.css('.profile-body')).length;
        expect(userCount).toEqual(2);
      });

      it('displays both names', () => {
        const el = fixture.debugElement.queryAll(By.css('.profile-header h3'));

        const userA = el[0];
        const userB = el[1];

        expect(userA.nativeElement.innerHTML).toEqual(testUserA.first_name + ' ' + testUserA.last_name);
        expect(userB.nativeElement.innerHTML).toEqual(testUserB.first_name + ' ' + testUserB.last_name);
      });

      it('displays SSN', () => {
        const el = fixture.debugElement.queryAll(By.css('input[name="idList"]'));
        expect(el.length).toEqual(2);
      });

      it('displays first name', () => {
        const el = fixture.debugElement.queryAll(By.css('input[name="fNameList"]'));
        expect(el.length).toEqual(2);
      });


      it('displays last name', () => {
        const el = fixture.debugElement.queryAll(By.css('input[name="lNameList"]'));
        expect(el.length).toEqual(2);
      });

      it('displays primary phone', () => {
        const el = fixture.debugElement.queryAll(By.css('input[name="primaryPhonelist"]'));
        expect(el.length).toEqual(2);
      });

      it('displays secondary phone', () => {
        const el = fixture.debugElement.queryAll(By.css('input[name="secondaryPhonelist"]'));
        expect(el.length).toEqual(2);
      });

      it('displays address', () => {
        const el = fixture.debugElement.queryAll(By.css('input[name="addressList"]'));
        expect(el.length).toEqual(2);
      });

      it('displays email', () => {
        const el = fixture.debugElement.queryAll(By.css('input[name="email"]'));
        expect(el.length).toEqual(2);
      });
    });
  });
});
