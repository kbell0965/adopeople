import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { PeopleModels } from '@pplsi-core/datalayer';
import { UserViewModel } from 'src/app/modules/users/models/user-view-model';
import { LocalizationService } from '@pplsi-core/shared-modules/dist/src/services/localization.service';

@Component({
  selector: 'ls-merge-dialog-user-info',
  templateUrl: './merge-dialog-user-info.component.html',
  styleUrls: ['./merge-dialog-user-info.component.scss']
})
export class MergeDialogUserInfoComponent implements OnInit {
  @Input() users: UserViewModel[];
  @Output() updateMergeUser = new EventEmitter<UserViewModel>();

  sourceUsers: SourceUser[];
  mergeUser: SourceUser;
  dateFormat: string;
  locale: string;

  constructor(private localizationService: LocalizationService) { }

  ngOnInit() {
    this.mergeUser = {} as SourceUser;
    this.locale = this.localizationService.getLocale();
    this.dateFormat = this.localizationService.getDateFormat();

    if (this.users) {
      this.sourceUsers = this.users.map((user: UserViewModel) => {
        const sourceUser = {} as SourceUser;
        sourceUser.firstName = user.first_name;
        sourceUser.lastName = user.last_name;
        sourceUser.nationalId = user.nationalId(this.locale);
        sourceUser.primaryPhone = user.formattedPhoneNumber(this.locale, 'primary');
        sourceUser.secondaryPhone = user.formattedPhoneNumber(this.locale, 'secondary');
        sourceUser.email = user.email;
        sourceUser.address = (user.addresses && user.addresses.length) ? user.addresses[0] : null;
        return sourceUser;
      });
    }
  }

  valueSelected(selectedUser: UserViewModel, property: string): void {
  }
}

interface SourceUser {
  address: PeopleModels.Address;
  primaryPhone: string;
  secondaryPhone: string;
  dateFormat: string;
  dateOfBirth: string;
  email: string;
  firstName: string;
  lastName: string;
  locale: string;
  nationalId: string;
}
