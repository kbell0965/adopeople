import { MergeDialogUserInfoComponent } from './merge-dialog-user-info.component';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FeatherModule } from 'angular-feather';
import { Check, Minus } from 'angular-feather/icons';
import { AvatarModule } from 'ng-components.adonis';
import { LocalizationService } from '@pplsi-core/shared-modules/dist/src/services/localization.service';
import { By } from '@angular/platform-browser';
import { PeopleFactories } from '@pplsi-core/factories';
import { UserViewModel } from 'src/app/modules/users/models/user-view-model';
import { PeopleModels } from '@pplsi-core/datalayer';
import { MatRadioModule } from '@angular/material';

interface Localization { locale: string; dateFormat: string; }
const fakeAddressesA: PeopleModels.Address[] = [new PeopleModels.Address(PeopleFactories.AddressFactory.build())];
const fakeAddressesB: PeopleModels.Address[] = [new PeopleModels.Address(PeopleFactories.AddressFactory.build())];
const fakeContactMethodsA: PeopleModels.ContactMethod[] = [
  PeopleFactories.ContactMethodFactory.build({ label: 'primary', value: '9999999991' }),
  PeopleFactories.ContactMethodFactory.build({ label: 'secondary', value: '8888888881' })
];
const fakeContactMethodsB: PeopleModels.ContactMethod[] = [
  PeopleFactories.ContactMethodFactory.build({ label: 'primary', value: '9999999992' }),
  PeopleFactories.ContactMethodFactory.build({ label: 'secondary', value: '8888888882' })
];
const fakeUserA: PeopleModels.User = new PeopleModels.User(PeopleFactories.UserFactory.build({
  date_of_birth: '1999-05-19T12:58:01.347Z',
  first_name: 'Mace',
  last_name: 'Windu',
  truncated_national_id: '444994444',
  contact_methods: fakeContactMethodsA,
  addresses: fakeAddressesA
}));

const fakeUserB: PeopleModels.User = new PeopleModels.User(PeopleFactories.UserFactory.build({
  date_of_birth: '1999-05-20T12:58:01.347Z',
  first_name: 'Master Mace',
  last_name: 'Windu',
  truncated_national_id: '444994444',
  contact_methods: fakeContactMethodsB,
  addresses: fakeAddressesB
}));

const testUserA: UserViewModel = new UserViewModel(fakeUserA);
const testUserB: UserViewModel = new UserViewModel(fakeUserB);
const testUsers: UserViewModel[] = [testUserA, testUserB];
const defaultLocaleProvider: Localization = { locale: 'en-US', dateFormat: 'MM/dd/yyyy' };

describe('MergeDialogUserInfoComponent.Unit', () => {
  let component: MergeDialogUserInfoComponent;
  let fixture: ComponentFixture<MergeDialogUserInfoComponent>;

  const setupTestBed = (localization: Localization = defaultLocaleProvider, users: UserViewModel[] = testUsers) => {
    TestBed.configureTestingModule({
      declarations: [MergeDialogUserInfoComponent],
      imports: [MatRadioModule, AvatarModule, FeatherModule.pick({ Check, Minus })],
      providers: [
        LocalizationService,
        { provide: 'defaultLocale', useValue: localization.locale },
        { provide: 'localizations', useValue: [localization] },
        { provide: 'Window', useValue: window }
      ]
    }).compileComponents();
    fixture = TestBed.createComponent(MergeDialogUserInfoComponent);
    component = fixture.componentInstance;
    component.users = users;
    fixture.detectChanges();
  };

  describe('when "en-US" (default) locale', () => {
    beforeEach(() => setupTestBed());
    describe('#ngOnInit', () => {
      it('Sets the local property to the component', () => expect(component.locale).toEqual('en-US'));
      it('sets the dateFormat property to the component', () => expect(component.dateFormat).toEqual('MM/dd/yyyy'));
      it('Builds the source users property for the component', () => expect(component.sourceUsers.length).toEqual(2));
      it('sets the mergeUser property to the component', () => expect(component.mergeUser).toBeDefined());

      it('sets the primary phone for first user to the component', () => expect(component.sourceUsers[0].primaryPhone)
      .toEqual(testUserA.formattedPhoneNumber(component.locale, 'primary')));
      it('sets the secondary phone for first user to the component', () => expect(component.sourceUsers[0].secondaryPhone)
      .toEqual(testUserA.formattedPhoneNumber(component.locale, 'secondary')));

      it('sets the primary phone for second user to the component', () => expect(component.sourceUsers[1].primaryPhone)
      .toEqual(testUserB.formattedPhoneNumber(component.locale, 'primary')));
      it('sets the secondary phone for second user to the component', () => expect(component.sourceUsers[1].secondaryPhone)
      .toEqual(testUserB.formattedPhoneNumber(component.locale, 'secondary')));

      it('sets the firstName for first user to the component', () => expect(component.sourceUsers[0].firstName)
      .toEqual(testUserA.first_name));
      it('sets the firstName for second user to the component', () => expect(component.sourceUsers[1].firstName)
      .toEqual(testUserB.first_name));

      it('sets the lastName for first user to the component', () => expect(component.sourceUsers[0].lastName)
      .toEqual(testUserA.last_name));
      it('sets the lastName for second user to the component', () => expect(component.sourceUsers[1].lastName)
      .toEqual(testUserB.last_name));

      it('sets the nationalId for first user to the component', () => expect(component.sourceUsers[0].nationalId)
      .toEqual(testUserA.nationalId(component.locale)));
      it('sets the nationalId for second user to the component', () => expect(component.sourceUsers[1].nationalId)
      .toEqual(testUserB.nationalId(component.locale)));

      it('sets the address for the first user to the component', () => expect(component.sourceUsers[0].address)
      .toEqual(testUserA.addresses[0]));
      it('sets the address for the second user to the component', () => expect(component.sourceUsers[1].address)
      .toEqual(testUserB.addresses[0]));

      it('sets the email for first user to the component', () => expect(component.sourceUsers[0].email)
      .toEqual(testUserA.email));
      it('sets the email for second user to the component', () => expect(component.sourceUsers[1].email)
      .toEqual(testUserB.email));
    });
  });
});
