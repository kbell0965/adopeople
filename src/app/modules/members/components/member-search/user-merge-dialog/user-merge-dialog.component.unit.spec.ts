import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { UserMergeDialogComponent } from './user-merge-dialog.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { UserMergeDialogModule } from './user-merge-dialog.module';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { PeopleFactories } from '@pplsi-core/factories';
import { LocalizationService } from '@pplsi-core/shared-modules/dist/src/services/localization.service';
import { PeopleModels } from '@pplsi-core/datalayer';
import { UserViewModel } from 'src/app/modules/users/models/user-view-model';
import { FormsModule } from '@angular/forms';

const fakeUsers: PeopleModels.User[] = PeopleFactories.UserFactory.buildList(2);
const testUsers: PeopleModels.User[] = fakeUsers.map(user => new PeopleModels.User(user));

describe('UserMergeDialogComponent.Unit', () => {
  let component: UserMergeDialogComponent;
  let fixture: ComponentFixture<UserMergeDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [BrowserAnimationsModule, UserMergeDialogModule],
      providers: [
        LocalizationService,
        { provide: 'Window', useValue: window },
        { provide: 'localizations', useValue: [{ locale: 'en-US', currencyCode: 'USD', dateFormat: 'MM/dd/yyyy' }] },
        { provide: 'defaultLocale', useValue: 'en-US' },
        { provide: MAT_DIALOG_DATA, useValue: { users: testUsers }},
        { provide: MatDialogRef, useValue: {} }
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserMergeDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  describe('#ngOnInit', () => {
    it('sets users from provided data to the component', () => {
      expect(component.users.length).toBe(2);
    });

    it('maps incoming users data into user view models', () => {
      expect(component.users[0]).toEqual(new UserViewModel(testUsers[0]));
    });
  });

  describe('#nextStepClick', () => {
    it('increases the stepper selected index by 1', () => {
      component.nextStepClick();
      expect(component.stepper.selectedIndex).toBe(1);
    });

    it('only increases the stepper selected index to a maximum of 2', () => {
      component.nextStepClick();
      component.nextStepClick();
      component.nextStepClick();

      expect(component.stepper.selectedIndex).toBe(2);
    });
  });

  describe('#previousStepClick', () => {
    it('decreases the stepper selected index my 1', () => {
      component.stepper.selectedIndex = 1;
      component.previousStepClick();
      expect(component.stepper.selectedIndex).toBe(0);
    });

    it('only decreases the stepper selected index to a minimum of 0', () => {
      component.previousStepClick();
      expect(component.stepper.selectedIndex).toBe(0);
    });

    it('sets merge acknowledgement checkbox back to false (unchecked)', () => {
      component.mergeAcknowledged = true;
      component.previousStepClick();
      expect(component.mergeAcknowledged).toBe(false);
    });
  });
});
