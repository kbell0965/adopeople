import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatHorizontalStepper, MatCheckbox } from '@angular/material';
import { PeopleModels } from '@pplsi-core/datalayer';
import { UserViewModel } from 'src/app/modules/users/models/user-view-model';
import { StepperSelectionEvent } from '@angular/cdk/stepper';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'ls-member-merge-dialog',
  templateUrl: './user-merge-dialog.component.html',
  styleUrls: ['./user-merge-dialog.component.scss']
})
export class UserMergeDialogComponent implements OnInit {
  @ViewChild('stepper', { static: true }) stepper: MatHorizontalStepper;

  mergeAcknowledged: boolean;
  mergeUser: UserViewModel;
  users: UserViewModel[];

  constructor(
    public dialogRef: MatDialogRef<UserMergeDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
  }

  ngOnInit() {
    const { users } = this.data;
    this.users = users ? users.map((user: PeopleModels.User) => new UserViewModel(user)) : null;
  }

  nextStepClick(): void {
    if (this.stepper.selectedIndex < 2) {
      this.stepper.next();
    }
  }

  previousStepClick(): void {
    this.mergeAcknowledged = false;
    if (this.stepper.selectedIndex > 0) {
      this.stepper.previous();
    }
  }
}
