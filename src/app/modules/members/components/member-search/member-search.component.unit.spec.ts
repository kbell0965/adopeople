import { async, getTestBed, ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { PartnerUserViewModel } from '../../../../models/partner-user-view-model';
import { HighlightPipe } from '../../../../shared/pipes/highlight.pipe';
import { MemberSearchComponent } from './member-search.component';
import { MemberPredictiveResultComponent } from '../member-predictive-result/member-predictive-result.component';
import { MemberSearchResultComponent } from '../member-search-result/member-search-result.component';
import { MembersRoutingModule } from '../../members-routing.module';
import { MatProgressSpinnerModule, MatCheckboxModule, MatDialogModule } from '@angular/material';
import { PplsiCoreDatalayerModule, PartnerUserRepository, PartnerModels, PeopleUserRepository, PeopleModels } from '@pplsi-core/datalayer';
import { PartnerFactories, PeopleFactories } from '@pplsi-core/factories/dist/@pplsi-core/factories';
import { of } from 'rxjs';
import { LocalizationService } from '@pplsi-core/shared-modules/dist/src/services/localization.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { UserMergeDialogModule } from './user-merge-dialog/user-merge-dialog.module';
import { UserMergeDialogComponent } from './user-merge-dialog/user-merge-dialog.component';

const usersToMerge: PeopleModels.User[] = PeopleFactories.UserFactory.buildList(2);
const userIdsToMerge: string[] = usersToMerge.map(user => user.id);

describe('MemberSearchComponent.Unit', () => {
  let component: MemberSearchComponent;
  let fixture: ComponentFixture<MemberSearchComponent>;
  let injector: TestBed;
  let partnerUserRepository: PartnerUserRepository;
  let userRepository: PeopleUserRepository;

  const getSamplePredictedMembers = () => {
    return ['Janeu', 'Janexpisal', 'Janik', 'Janoea', 'Janou', 'Jany', 'Jang', 'Janedeu', 'Janc', 'Janue', 'Janstuear',
      'Janusti', 'Janige', 'Jano', 'Janoseb', 'Janeo', 'Janu', 'Janusude', 'Janet', 'Janki', 'Jann', 'Janaisa', 'Jandi',
      'Janome', 'Janai', 'Jantmopo', 'Janoei', 'Januda', 'Janx', 'Janunefo', 'Jana', 'Jane', 'Janseu'].map(name => {
      return new PartnerModels.User(PartnerFactories.UserFactory.build({
        first_name: name,
        email: `${name}@example.com`
      }));
    });
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        BrowserAnimationsModule,
        ReactiveFormsModule,
        MatProgressSpinnerModule,
        MembersRoutingModule,
        HttpClientTestingModule,
        RouterTestingModule,
        PplsiCoreDatalayerModule.forRoot(),
        UserMergeDialogModule,
        MatDialogModule,
        MatCheckboxModule
      ],
      declarations: [
        HighlightPipe,
        MemberSearchComponent,
        MemberSearchResultComponent,
        MemberPredictiveResultComponent
      ],
      providers: [
        HighlightPipe,
        LocalizationService,
        {
          provide: 'Window',
          useValue: window
        },
        {
          provide: 'localizations',
          useValue: [
            { locale: 'en-US', currencyCode: 'USD', dateFormat: 'MM/dd/yyyy' },
            { locale: 'en-GB', currencyCode: 'GBP', dateFormat: 'dd/MM/yyyy' },
            { locale: 'en-CA', currencyCode: 'CAD', dateFormat: 'yyyy-MM-dd' }
          ]
        },
        {
          provide: 'defaultLocale',
          useValue: 'en-US'
        }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MemberSearchComponent);
    component = fixture.componentInstance;
    injector = getTestBed();
    partnerUserRepository = injector.get(PartnerUserRepository);
    userRepository = injector.get(PeopleUserRepository);
    fixture.detectChanges();
  });

  describe('constructor', () => {
    it('should set a default member results page event', () => {
      expect(component.membersPageEvent).toEqual({ pageIndex: 1, pageSize: 12, length: 0 });
    });

    it('should set a default predicted member results page event', () => {
      expect(component.predictedMembersPageEvent).toEqual({ pageIndex: 1, pageSize: 8, length: 0 });
    });
  });

  describe('ngOnInit', () => {
    beforeEach(() => {
      component.ngOnInit();
    });

    it('sets the searchForm', () => {
      expect(component.searchForm.get('query').value).toBe('');
    });

    it('sets the logoPath', () => {
      expect(component.logoPath).toBe('assets/NA_LegalShieldWithBackground.svg');
    });
  });

  describe('#ngOnDestroy', () => {
    beforeEach(() => {
      spyOn((component as any)._destroyed, 'next');
      spyOn((component as any)._destroyed, 'complete');
      component.ngOnDestroy();
    });

    it('calls #next on `_destroyed`', () => expect((component as any)._destroyed.next).toHaveBeenCalled());
    it('calls #complete on `_destroyed`', () => expect((component as any)._destroyed.complete).toHaveBeenCalled());
  });

  describe('#get searchActive', () => {
    it('should be false when there is no query', () => {
      expect(component.searchActive).toBe(false);
    });

    it('should be true when there is a query', () => {
      component.searchForm.controls['query'].setValue('smith');
      expect(component.searchActive).toBe(true);
    });
  });

  describe('#showPredictedResults', () => {
    it('should be falsey when there are no predicted members', () => {
      expect(component.showPredictedResults).toBe(0);
    });

    it('should be truthy when there are predicted members', () => {
      component.predictedMembers = getSamplePredictedMembers().map((user: PartnerModels.User) => new PartnerUserViewModel(user));
      expect(component.showPredictedResults).toBeTruthy();
    });
  });

  describe('#get query', () => {
    it('should return the contents of the input', () => {
      expect(component.query).toBe('');
      component.searchForm.controls['query'].setValue('test');
      expect(component.query).toBe('test');
    });
  });

  describe('#set query', () => {
    it('should update the search form input value', () => {
      component.query = 'test';
      expect(component.searchForm.controls['query'].value).toBe('test');
    });
  });

  describe('#clearPredictions', () => {
    it('should clear the predicted members', () => {
      component.predictedMembers = getSamplePredictedMembers().map((user: PartnerModels.User) => new PartnerUserViewModel(user));
      component.clearPredictions();
      expect(component.predictedMembers.length).toBe(0);
    });
  });

  describe('#loadMergeUsersDialog', () => {
    beforeEach(() => {
      spyOn(userRepository, 'find').and.returnValue(of(usersToMerge[0]));
    });

    it('calls the user repository for each user id', () => {
      component.loadMergeUsersDialog(userIdsToMerge);

      expect(userRepository.find).toHaveBeenCalledTimes(2);
      expect(userRepository.find).toHaveBeenCalledWith(userIdsToMerge[0]);
      expect(userRepository.find).toHaveBeenCalledWith(userIdsToMerge[1]);
    });

    it('calls the open merge user dialog method', () => {
      spyOn(component, 'openMergeUsersDialog');
      component.loadMergeUsersDialog(userIdsToMerge);

      expect(component.openMergeUsersDialog).toHaveBeenCalledTimes(1);
    });
  });

  describe('#openMergeUsersDialog', () => {
    it('calls the dialog open method', () => {
      spyOn(component.dialog, 'open');
      component.openMergeUsersDialog(usersToMerge);

      expect(component.dialog.open).toHaveBeenCalledWith(UserMergeDialogComponent, {
        data: { users: usersToMerge }, maxHeight: 'calc(100vh - 112px)', minWidth: 800
      });
    });
  });

  describe('#updatePredictions', () => {
    beforeEach(() => {
      const query = 'thr';
      spyOn(partnerUserRepository, 'findAll').and.returnValue(of(
        <PartnerModels.UsersWithPagination> {
          users: getSamplePredictedMembers(),
          total_length: getSamplePredictedMembers().length
        }));
      component.searchForm.controls['query'].setValue(query);
      component.updatePredictions(query);
      fixture.detectChanges();
    });

    it('calls the user repository with the query', () => {
      expect(partnerUserRepository.findAll).toHaveBeenCalledTimes(1);
    });

    it('updates the predicted members prop with the user repository response', () => {
      expect(component.predictedMembers.length).toBe(8);
    });

    it('should clear existing predictions if the query is less than 2 characters', () => {
      const query = 'tw';
      component.predictedMembers = getSamplePredictedMembers().map((user: PartnerModels.User) => new PartnerUserViewModel(user));
      component.searchForm.controls['query'].setValue(query);
      component.updatePredictions(query);
      expect(component.predictedMembers.length).toBe(0);
    });

    it('should map the users to the app model view user', () => {
      component.predictedMembers.forEach((user: any) => {
        expect(user).toEqual(jasmine.any(PartnerUserViewModel));
      });
    });

    it('should not call the user repository if the last query is the same', () => {
      const el = fixture.nativeElement.querySelector('input');
      el.dispatchEvent(new Event('input'));
      fixture.detectChanges();
      expect(partnerUserRepository.findAll).toHaveBeenCalledTimes(1);
    });

    it('should call the user repository when the query is different from the last', () => {
      component.updatePredictions('four');
      expect(partnerUserRepository.findAll).toHaveBeenCalledTimes(2);
    });

    it('should not update predicted results if search results for the same query are present', () => {
      component.updatePredictions('fiver');
      component.updateResults('fiver');
      fixture.detectChanges();
      expect(component.predictedMembers).toEqual([]);
    });

    it('should clear the predicted results if submitting search for same as latest query', () => {
      component.updateResults('thr');
      component.updatePredictions('thr');
      component.updateResults('thr');
      fixture.detectChanges();
      expect(component.predictedMembers).toEqual([]);
    });
  });

  describe('#updateResults', () => {
    beforeEach(() => {
      component.searchForm.controls['query'].setValue('thr');
      spyOn(partnerUserRepository, 'findAll').and.returnValue(of(
        <PartnerModels.UsersWithPagination> {
          users: getSamplePredictedMembers(),
          total_length: getSamplePredictedMembers().length
      }));
      component.updateResults(component.query);
    });

    it('should call the user repository with the query', () => {
      expect(partnerUserRepository.findAll).toHaveBeenCalled();
    });

    it('should update members with the service response', () => {
      expect(component.members.length).toBe(getSamplePredictedMembers().length);
    });

    it('should set the searching property to false', () => {
      expect(component.searching).toBe(false);
    });

    it('should map the users to the app model view user', () => {
      component.members.forEach((user: any) => {
        expect(user).toEqual(jasmine.any(PartnerUserViewModel));
      });
    });
  });
});
