import { ComponentFixture, TestBed, getTestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { LocalizationService } from '@pplsi-core/shared-modules/dist/src/services/localization.service';
import { PartnerUserViewModel } from '../../../../models/partner-user-view-model';
import { HighlightPipe } from '../../../../shared/pipes/highlight.pipe';
import { MemberPredictiveResultComponent } from './member-predictive-result.component';
import { PartnerFactories } from '@pplsi-core/factories/dist/@pplsi-core/factories';
import { PhonePipe } from '@pplsi-core/shared-modules';

describe('MemberPredictiveResultComponent.Unit', () => {
  let injector: TestBed;
  let component: MemberPredictiveResultComponent;
  let fixture: ComponentFixture<MemberPredictiveResultComponent>;
  let pipe: HighlightPipe;
  let member;
  let localizationService: LocalizationService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        MemberPredictiveResultComponent
      ],
      imports: [
        RouterTestingModule
      ],
      providers: [
        HighlightPipe,
        PhonePipe,
        LocalizationService,
        {
          provide: 'Window',
          useValue: window
        },
        {
          provide: 'localizations',
          useValue: [
            { locale: 'en-US', currencyCode: 'USD', dateFormat: 'MM/dd/yyyy' }
          ]
        },
        {
          provide: 'defaultLocale',
          useValue: 'en-US'
        }
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    member = new PartnerUserViewModel(PartnerFactories.UserFactory.build({
      first_name: 'Johnny',
      email: 'johnny@example.com',
      phone_number: '9493211234'
    }));
    member.contact_methods[0].value = '9493211234';
    member.contact_methods[0].label = 'cell';
    injector = getTestBed();
    pipe = injector.get(HighlightPipe);
    localizationService = injector.get(LocalizationService);
    fixture = TestBed.createComponent(MemberPredictiveResultComponent);
    component = fixture.componentInstance;
    component.member = member;
    component.query = member.first_name;

    spyOn(component.member, 'formattedCellPhoneNumber').and.callThrough();
  });

  describe('constructor', () => {
    it('should set the local from the localization service', () => {
      expect(component.locale).toEqual('en-US');
    });
  });

  describe('#ngOnInit', () => {
    describe('when properties are not null', () => {
      beforeEach(() => {
        spyOn(component, 'highlightText').and.returnValues('email', 'name', 'phone');
        component.ngOnInit();
      });

      it('calls highlightText for each property', () => {
        expect(component.highlightText).toHaveBeenCalledTimes(3);
      });

      it('calls highlightText with the member email', () => {
        expect(component.highlightText).toHaveBeenCalledWith(member.email);
      });

      it('calls highlightText with the member name', () => {
        expect(component.highlightText).toHaveBeenCalledWith(member.fullName());
      });

      it('calls highlightTest with the member phone number', () => {
        expect(component.highlightText)
          .toHaveBeenCalledWith(
            member.formattedCellPhoneNumber(component.locale.slice(-2))
          );
      });

      it('sets the email property to the value returned by highlightText', () => {
        expect(component.email).toEqual('email');
      });

      it('sets the name property to the value returned by highlightText', () => {
        expect(component.name).toEqual('name');
      });

      it('sets the phone property to the value returned by highlightText', () => {
        expect(component.phone).toEqual('phone');
      });
    });

    describe('when phone number is null', () => {
      beforeEach(() => {
        spyOn(component, 'highlightText').and.returnValues('email', 'name', 'phone');
        component.member.contact_methods = [];
        component.ngOnInit();
      });

      it('does not call highlightText with phoneNumber', () => {
        expect(component.highlightText).toHaveBeenCalledTimes(2);
      });

      it('returns null for phone', () => {
        expect(component.phone).toBeNull();
      });
    });
  });

  describe('#highlightText', () => {
    it('calls highlight pipe to transform text', () => {
      spyOn(pipe, 'transform').and.returnValue('');
      component.highlightText('text');
      expect(pipe.transform).toHaveBeenCalledWith('text', member.first_name);
    });
  });
});
