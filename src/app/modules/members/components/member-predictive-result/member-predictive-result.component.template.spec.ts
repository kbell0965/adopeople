import { By } from '@angular/platform-browser';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { LocalizationService } from '@pplsi-core/shared-modules/dist/src/services/localization.service';
import { PartnerUserViewModel } from '../../../../models/partner-user-view-model';
import { MemberPredictiveResultComponent } from './member-predictive-result.component';
import { HighlightPipe } from '../../../../shared/pipes/highlight.pipe';
import { PartnerFactories } from '@pplsi-core/factories/dist/@pplsi-core/factories';
import { PhonePipe } from '@pplsi-core/shared-modules';

describe('MemberPredictiveResultComponent.Template', () => {
  let component: MemberPredictiveResultComponent;
  let fixture: ComponentFixture<MemberPredictiveResultComponent>;
  const member = new PartnerUserViewModel(PartnerFactories.UserFactory.build({
    first_name: 'Johnny',
    last_name: 'Cremin',
    email: 'johnny@example.com'
  }));
  member.contact_methods[0].value = '4053211234';
  member.contact_methods[0].label = 'cell';
  member.accountId = '1';

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        MemberPredictiveResultComponent,
        HighlightPipe
      ],
      imports: [
        RouterTestingModule
      ],
      providers: [
        HighlightPipe,
        PhonePipe,
        LocalizationService,
        {
          provide: 'Window',
          useValue: window
        },
        {
          provide: 'localizations',
          useValue: [
            { locale: 'en-US', currencyCode: 'USD', dateFormat: 'MM/dd/yyyy' }
          ]
        },
        {
          provide: 'defaultLocale',
          useValue: 'en-US'
        }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MemberPredictiveResultComponent);
    component = fixture.componentInstance;
    component.member = member;
    component.query = member.first_name;
    component.ngOnInit();

    fixture.detectChanges();
  });

  describe('when element has matching text', () => {
    it('highlights partial member name', () => {
      const el = fixture.debugElement.query(By.css('.member-name'));

      expect(el.nativeElement.innerHTML).toBe(`<span class="highlight">${member.first_name}</span> ${member.last_name}`);
    });

    it('highlights partial member email', () => {
      const el = fixture.debugElement.query(By.css('.member-email'));

      expect(el.nativeElement.innerHTML).toBe(`<span class="highlight">johnny</span>@example.com`);
    });

    it('highlights partial phone', () => {
      component.query = '405';
      component.ngOnInit();
      fixture.detectChanges();
      const el = fixture.debugElement.query(By.css('.member-phone'));

      expect(el.nativeElement.innerHTML).toBe(`(<span class="highlight">405</span>) 321-1234`);
    });
  });

  describe('when element does not have matching text', () => {
    beforeEach(() => {
      component.query = '';
      component.ngOnInit();
      fixture.detectChanges();
    });

    it('displays member name', () => {
      const el = fixture.debugElement.query(By.css('.member-name'));
      expect(el.nativeElement.innerHTML).toBe(member.fullName());
    });

    it('displays member email', () => {
      const el = fixture.debugElement.query(By.css('.member-email'));

      expect(el.nativeElement.innerHTML).toBe(`${member.email}`);
    });

    it('displays phone', () => {
      const el = fixture.debugElement.query(By.css('.member-phone'));
      expect(el.nativeElement.innerHTML)
        .toBe(member.formattedCellPhoneNumber(component.locale.slice(-2)));
    });
  });

  describe('predictive results', () => {
    it('includes the member name', () => {
      const el = fixture.debugElement.query(By.css('.predictive-results'));
      expect(el.query(By.css('.member-name')).nativeElement.innerHTML).toBe('<span class="highlight">Johnny</span> Cremin');
    });

    it('links to account member', () => {
      const el = fixture.debugElement.query(By.css('.predictive-results'));

      expect(el.nativeElement.getAttribute('href')).toEqual(`/users/${member.accountId}/user/${member.id}`);
    });
  });

  describe('when member has null data', () => {
    beforeEach(() => {
      const memberWithNulls = new PartnerUserViewModel(PartnerFactories.UserFactory.build({
        first_name: null,
        last_name: null,
        email: null,
        phone_number: null
      }));
      memberWithNulls.accountId = '1';
      memberWithNulls.contact_methods = [];

      component.member = memberWithNulls;
      component.ngOnInit();
      fixture.detectChanges();
    });

    it('does not display name', () => {
      const el = fixture.debugElement.query(By.css('.member-name'));
      expect(el).toBeNull();
    });

    it('does not display phone', () => {
      const el = fixture.debugElement.query(By.css('.member-phone'));
      expect(el).toBeNull();
    });

    it('does not display email', () => {
      const el = fixture.debugElement.query(By.css('.member-email'));
      expect(el).toBeNull();
    });
  });
});
