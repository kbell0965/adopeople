import { Component, Input, OnInit } from '@angular/core';
import { SafeHtml } from '@angular/platform-browser';

import { LocalizationService } from '@pplsi-core/shared-modules/dist/src/services/localization.service';
import { PartnerUserViewModel } from '../../../../models/partner-user-view-model';
import { HighlightPipe } from '../../../../shared/pipes/highlight.pipe';

@Component({
  selector: 'member-predictive-result',
  templateUrl: './member-predictive-result.component.html',
  styleUrls: ['./member-predictive-result.component.scss']
})
export class MemberPredictiveResultComponent implements OnInit {
  @Input()
  member: PartnerUserViewModel;

  @Input()
  query: string;

  email: SafeHtml;
  location: SafeHtml;
  name: SafeHtml;
  phone: SafeHtml;
  locale: string;

  constructor(
    private pipe: HighlightPipe,
    private localizationService: LocalizationService
  ) {
    this.locale = this.localizationService.getLocale();
  }

  ngOnInit() {
    const formattedPhoneNumber = this.member.formattedCellPhoneNumber(this.locale.slice(-2));
    this.email = this.highlightText(this.member.email);
    this.location = this.member.formattedLocation();
    this.name = this.highlightText(this.member.fullName());
    this.phone = formattedPhoneNumber ? this.highlightText(formattedPhoneNumber) : null;
  }

  highlightText(text: string): SafeHtml {
    return this.pipe.transform(text, this.query);
  }
}
