import { NgModule } from '@angular/core';
import { MatRadioModule, MatSelectModule, MatOptionModule, MatTabsModule, MatCheckboxModule } from '@angular/material';
import { FeatherModule } from 'angular-feather';
import { ChevronDown, ChevronUp } from 'angular-feather/icons';
import { LocalizationService } from '@pplsi-core/shared-modules/dist/src/services/localization.service';
import { FulfillmentPartnerBranchesResolver } from '../../resolvers/fulfillment-partner-branches/fulfillment-partner-branches.resolver';
import { AccountsRoutingModule } from './accounts-routing.module';
import { CreateResolutionComponent } from './components/create-resolution/create-resolution.component';
import { AccountFromPathResolver } from '../../resolvers/account-from-path/account-from-path.resolver';
import { MatPaginatorModule } from '@angular/material';
import { ResolutionSearchResultModule } from 'src/app/shared/components/resolution-search-result/resolution-search-result.module';
import { ResolutionsInitialQueryResolver } from '../resolutions/resolvers/resolutions-initial-query/resolutions-initial-query.resolver';
import { SearchLayoutModule } from 'src/app/shared/components/search-layout/search-layout.module';

import { DatePipe, TitleCasePipe } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [ CreateResolutionComponent ],
  imports: [
    AccountsRoutingModule,
    FeatherModule.pick({ ChevronDown, ChevronUp }),
    MatTabsModule,
    MatOptionModule,
    MatRadioModule,
    MatSelectModule,
    MatCheckboxModule,
    MatPaginatorModule,
    ResolutionSearchResultModule,
    SearchLayoutModule,
    SharedModule
  ],
  providers: [
    AccountFromPathResolver,
    FulfillmentPartnerBranchesResolver,
    LocalizationService,
    DatePipe,
    ResolutionsInitialQueryResolver,
    TitleCasePipe,
    {
      provide: 'Window',
      useValue: window
    },
    {
      provide: 'localizations',
      useValue: [
        { locale: 'en-US', currencyCode: 'USD', dateFormat: 'MM/dd/yyyy' },
        { locale: 'en-GB', currencyCode: 'GBP', dateFormat: 'dd/MM/yyyy' },
        { locale: 'en-CA', currencyCode: 'CAD', dateFormat: 'yyyy-MM-dd' }
      ]
    },
    {
      provide: 'defaultLocale',
      useValue: 'en-US'
    }
  ]
})
export class AccountsModule { }
