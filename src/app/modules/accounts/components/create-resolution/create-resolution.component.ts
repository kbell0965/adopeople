import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FulfillmentPartnerMemberViewModel } from 'src/app/models/fulfillment-partner-member-view-model';
import { AccountViewModel } from '../../../../models/account-view-model';
import {
  PartnerModels,
  PeopleServiceRequestRepository,
  PartnerResolutionRepository,
  PeopleModels,
  PeopleFulfillmentPartnerRepository,
  PeopleFulfillmentPartnerMembersRepository
} from '@pplsi-core/datalayer';
import { MembershipViewModel } from '../../../../models/membership-view-model';
import { forkJoin, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  templateUrl: './create-resolution.component.html',
  styleUrls: [ './create-resolution.component.scss' ]
})
export class CreateResolutionComponent implements OnInit, OnDestroy {
  private _destroyed = new Subject<void>();
  resolutionForm: FormGroup;
  account: AccountViewModel;
  fulfillmentPartners: PartnerModels.FulfillmentPartner[];
  fulfillmentPartnerBranches: PeopleModels.FulfillmentPartnerBranch[];
  targetMembers: FulfillmentPartnerMemberViewModel[];
  serviceRequests: PeopleModels.ServiceRequest[];
  conflictServiceRequests: PeopleModels.ServiceRequest[];
  showOtherContactMethodInput = false;
  readonly CONCERNS = [
    'Untimely Callback',
    'Disagree with Legal Advice/Opinion',
    'Has not received will, letter etc',
    'Attorney/Staff Attitude',
    'Retainer or Hourly Rate',
    'Referral Attorney',
    'Coverage Advised',
    'Disposition of Case',
    'Attorney did not appear',
    'Miscellaneous/Unknown',
    'Has not been refereed to an Attorney',
    'Improper PPL Procedure',
    'Improper use of Software',
    'Survey Scored Incorrectly',
    'No Callback from Attorney',
    'Unable to Speak with Attorney',
    'Insufficient Advice',
    'Wrong Area/Distance too far',
    'Attorney has Conflict',
    'Attorney does not handle the Area of Law',
    'Attorney would not take the Case',
    'Member requests a different Attorney',
    'Attorney would not provide Discount'
  ];

  constructor(private activatedRoute: ActivatedRoute,
              private location: Location,
              private formBuilder: FormBuilder,
              private peopleFulfillmentPartnerRepository: PeopleFulfillmentPartnerRepository,
              private peopleServiceRequestRepository: PeopleServiceRequestRepository,
              private partnerResolutionRepository: PartnerResolutionRepository,
              private router: Router,
              private peopleFulfillmentPartnerMembersRepository: PeopleFulfillmentPartnerMembersRepository) { }

  ngOnInit(): void {
    this.account = this.activatedRoute.snapshot.data['account'];
    this.fulfillmentPartnerBranches = this.activatedRoute.snapshot.data['fulfillmentPartnerBranches'];
    this.resolutionForm = this.formBuilder.group({
      membership: [ this.account.getActiveMemberships()[0], Validators.required ],
      concern: [ null, Validators.required ],
      resolutionType: [ 'provider', Validators.required ],
      source: [ null, Validators.required ],
      targetPartner: [ null ],
      targetBranch: [ null ],
      targetMember: [ null ],
      serviceRequest: [ null ],
      contactMethod: [ null, Validators.required ],
      contactMethodOther: [ null ],
      referral: [ null ],
      approved: [ null ],
      description: [ null, Validators.required ]
    });

    this.updateRequiredFields(this.resolutionType);
  }

  ngOnDestroy() {
    this._destroyed.next();
    this._destroyed.complete();
  }

  get resolutionType(): string {
    return this.resolutionForm.controls['resolutionType'].value;
  }

  get membership(): MembershipViewModel {
    return this.resolutionForm.controls['membership'].value;
  }

  get targetPartner(): PartnerModels.FulfillmentPartner {
    return this.resolutionForm.controls['targetPartner'].value;
  }

  get targetBranch(): PartnerModels.FulfillmentPartnerBranch {
    return this.resolutionForm.controls['targetBranch'].value;
  }

  get email(): string {
    return this.membership.user.email;
  }

  get contactMethods(): PartnerModels.ContactMethod[] {
    return this.membership.user.contact_methods;
  }

  updateRequiredFields(resolutionType): void {
    if (resolutionType === 'conflict') {
      this.resolutionForm.controls['targetPartner'].setValidators(null);
      this.resolutionForm.controls['targetBranch'].setValidators(null);
      this.resolutionForm.controls['referral'].setValidators(Validators.required);
    } else {
      this.resolutionForm.controls['targetPartner'].setValidators(Validators.required);
      this.resolutionForm.controls['targetBranch'].setValidators(Validators.required);
      this.resolutionForm.controls['referral'].setValidators(null);
    }

    this.resolutionForm.controls['targetPartner'].updateValueAndValidity();
    this.resolutionForm.controls['targetBranch'].updateValueAndValidity();
    this.resolutionForm.controls['referral'].updateValueAndValidity();
  }

  loadConflictServiceRequests(): void {
    this.peopleServiceRequestRepository
      .findAllByMembershipId(this.membership.id)
      .pipe(takeUntil(this._destroyed))
      .subscribe((serviceRequests: PeopleModels.ServiceRequest[]) => this.conflictServiceRequests = serviceRequests);
  }

  showOtherContactMethodField(event): void {
    this.showOtherContactMethodInput = event.value === 'other';
  }

  submit(): void {
    const body = {
      source: this.resolutionForm.controls['source'].value,
      status: 'unassigned',
      membership_id: this.membership.id,
      concern: this.resolutionForm.controls['concern'].value,
      resolution_type: this.resolutionType,
      approved_to_continue: this.resolutionForm.controls['approved'].value,
      description: this.resolutionForm.controls['description'].value
    };

    if (this.resolutionForm.controls['contactMethod'].value === 'other') {
      body['preferred_contact_method'] = this.resolutionForm.controls['contactMethodOther'].value;
    } else if (this.resolutionForm.controls['contactMethod'].value === 'email') {
      body[ 'preferred_contact_method' ] = this.email;
    } else {
      body['preferred_contact_method'] = this.resolutionForm.controls['contactMethod'].value.value;
    }

    if (this.resolutionType === 'conflict') {
      body['referral'] = this.resolutionForm.controls['referral'].value;

      if (this.resolutionForm.controls['serviceRequest'].value) {
        body['service_request_id'] = this.resolutionForm.controls['serviceRequest'].value.id;
      }
    } else {
      body['target_id'] = this.targetPartner.id;
      body['target_branch_id'] = this.targetBranch.id;

      if (this.resolutionForm.controls['serviceRequest'].value) {
        body['service_request_id'] = this.resolutionForm.controls['serviceRequest'].value.id;
      }

      if (this.resolutionType === 'provider') {
        if (this.resolutionForm.controls['targetMember'].value) {
          body['target_member_id'] = this.resolutionForm.controls['targetMember'].value.id;
        }
      } else {
        if (this.resolutionForm.controls['referral'].value) {
          body['referral'] = this.resolutionForm.controls['referral'].value;
        }
      }
    }

    this.partnerResolutionRepository.create(this.account.id, body)
      .pipe(takeUntil(this._destroyed))
      .subscribe((resolution: PartnerModels.Resolution) => this.router.navigate(['/resolutions', resolution.id]));
  }

  back(): void {
    this.location.back();
  }

  targetBranchChanged(): void {
    forkJoin([
      this.peopleServiceRequestRepository.findAllByMembershipId(this.membership.id,
        { fulfillment_partner_branch_id: this.targetBranch.id }),
      this.peopleFulfillmentPartnerRepository.findAll(this.targetBranch.id),
      this.peopleFulfillmentPartnerMembersRepository.findAllByFulfillmentPartnerBranch(this.targetBranch.id)
    ])
    .pipe(takeUntil(this._destroyed))
    .subscribe((results: [PeopleModels.ServiceRequest[], PartnerModels.FulfillmentPartner[], PeopleModels.FulfillmentPartnerMember[]]) => {
      this.serviceRequests = results[0];
      this.fulfillmentPartners = results[1];
      this.targetMembers = results[2].map((fulfillmentPartnerMember: PeopleModels.FulfillmentPartnerMember) => {
        return new FulfillmentPartnerMemberViewModel(fulfillmentPartnerMember);
      });
    });
  }
}
