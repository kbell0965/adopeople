import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, getTestBed, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule, Validators } from '@angular/forms';
import { MatButtonModule, MatCheckboxModule, MatInputModule, MatOptionModule, MatRadioModule, MatSelectModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ActivatedRoute, Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';
import { Location } from '@angular/common';
import { FulfillmentPartnerMemberViewModel } from 'src/app/models/fulfillment-partner-member-view-model';

import { AccountViewModel } from '../../../../models/account-view-model';
import { PartnerFactories, PeopleFactories } from '@pplsi-core/factories';
import { CreateResolutionComponent } from './create-resolution.component';
import {
  PartnerModels,
  PeopleServiceRequestRepository,
  PartnerResolutionRepository,
  PeopleModels,
  PeopleFulfillmentPartnerRepository,
  PeopleFulfillmentPartnerMembersRepository
} from '@pplsi-core/datalayer';

const fulfillmentPartners: PartnerModels.FulfillmentPartner[] = [PeopleFactories.FulfillmentPartnerFactory.build()].map(
  (fulfillmentPartner: object) => new PartnerModels.FulfillmentPartner(fulfillmentPartner)
);
const fulfillmentPartnerBranches: PeopleModels.FulfillmentPartnerBranch[] = [
  new PeopleModels.FulfillmentPartnerBranch(PeopleFactories.FulfillmentPartnerBranchFactory.build({ id: '1' }))
];

describe('CreateResolutionComponent.Unit', () => {
  let component: CreateResolutionComponent;
  let fixture: ComponentFixture<CreateResolutionComponent>;
  let router: Router;
  const accountFake: object = PartnerFactories.AccountFactory.build();
  const account: AccountViewModel = new AccountViewModel(accountFake);
  const activatedRouteStub = {
    snapshot: {
      params: { 'account_id': account['id'] },
      data: {
        account: account,
        fulfillmentPartners: fulfillmentPartners,
        fulfillmentPartnerBranches: fulfillmentPartnerBranches
      }
    },
    params: of({
      account_id: account['id']
    })
  };
  let peopleServiceRequestRepository: PeopleServiceRequestRepository;
  let peopleFulfillmentPartnerRepository: PeopleFulfillmentPartnerRepository;
  let partnerResolutionRepository: PartnerResolutionRepository;
  let peopleFulfillmentPartnerMembersRepository: PeopleFulfillmentPartnerMembersRepository;
  let injector: TestBed;
  const locationStub = {
    back() {
      return true;
    }
  };
  let location: Location;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateResolutionComponent ],
      imports: [
        RouterTestingModule,
        ReactiveFormsModule,
        HttpClientTestingModule,
        MatInputModule,
        MatButtonModule,
        MatRadioModule,
        MatSelectModule,
        MatOptionModule,
        MatCheckboxModule,
        BrowserAnimationsModule
      ],
      providers: [
        { provide: ActivatedRoute, useValue: activatedRouteStub },
        PeopleFulfillmentPartnerRepository,
        PeopleServiceRequestRepository,
        PartnerResolutionRepository,
        PeopleFulfillmentPartnerMembersRepository,
        { provide: Location, useValue: locationStub },
        {
          provide: Router,
          useValue: { navigate: jasmine.createSpy('navigate') }
        }
      ],
      schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateResolutionComponent);
    injector = getTestBed();
    peopleServiceRequestRepository = injector.get(PeopleServiceRequestRepository);
    peopleFulfillmentPartnerRepository = injector.get(PeopleFulfillmentPartnerRepository);
    partnerResolutionRepository = injector.get(PartnerResolutionRepository);
    peopleFulfillmentPartnerMembersRepository = injector.get(PeopleFulfillmentPartnerMembersRepository);
    location = injector.get(Location);
    router = injector.get(Router);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  describe('#ngOnInit', () => {
    it('sets the account', () => {
      expect(component.account).toEqual(account);
    });

    describe('resolutionForm', () => {
      beforeEach(() => {
        component.ngOnInit();
      });

      it('sets up the form', () => {
        expect(component.resolutionForm).toBeDefined();
      });

      it('creates a membership control', () => {
        expect(component.resolutionForm.controls['membership']).toBeDefined();
        expect(component.resolutionForm.controls['membership'].value).toEqual(account.getActiveMemberships()[0]);
        expect(component.resolutionForm.controls['membership'].validator).toEqual(Validators.required);
      });

      it('creates a concern control', () => {
        expect(component.resolutionForm.controls['concern']).toBeDefined();
        expect(component.resolutionForm.controls['concern'].value).toBeNull();
        expect(component.resolutionForm.controls['concern'].validator).toEqual(Validators.required);
      });

      it('creates a resolutionType control', () => {
        expect(component.resolutionForm.controls['resolutionType']).toBeDefined();
        expect(component.resolutionForm.controls['resolutionType'].value).toEqual('provider');
        expect(component.resolutionForm.controls['resolutionType'].validator).toEqual(Validators.required);
      });

      it('creates a source control', () => {
        expect(component.resolutionForm.controls['source']).toBeDefined();
        expect(component.resolutionForm.controls['source'].value).toBeNull();
        expect(component.resolutionForm.controls['source'].validator).toEqual(Validators.required);
      });

      it('creates a target partner control', () => {
        expect(component.resolutionForm.controls['targetPartner']).toBeDefined();
        expect(component.resolutionForm.controls['targetPartner'].value).toBeNull();
        expect(component.resolutionForm.controls['targetPartner'].validator).toEqual(Validators.required);
      });

      it('creates a targetBranch control', () => {
        expect(component.resolutionForm.controls['targetBranch']).toBeDefined();
        expect(component.resolutionForm.controls['targetBranch'].value).toBeNull();
        expect(component.resolutionForm.controls['targetBranch'].validator).toEqual(Validators.required);
      });

      it('creates a targetMember control', () => {
        expect(component.resolutionForm.controls['targetMember']).toBeDefined();
        expect(component.resolutionForm.controls['targetMember'].value).toBeNull();
        expect(component.resolutionForm.controls['targetMember'].validator).not.toEqual(Validators.required);
      });

      it('creates a serviceRequest control', () => {
        expect(component.resolutionForm.controls['serviceRequest']).toBeDefined();
        expect(component.resolutionForm.controls['serviceRequest'].value).toBeNull();
        expect(component.resolutionForm.controls['serviceRequest'].validator).not.toEqual(Validators.required);
      });

      it('creates a contactMethod control', () => {
        expect(component.resolutionForm.controls['contactMethod']).toBeDefined();
        expect(component.resolutionForm.controls['contactMethod'].value).toBeNull();
        expect(component.resolutionForm.controls['contactMethod'].validator).toEqual(Validators.required);
      });

      it('creates a contactMethodOther control', () => {
        expect(component.resolutionForm.controls['contactMethodOther']).toBeDefined();
        expect(component.resolutionForm.controls['contactMethodOther'].value).toBeNull();
        expect(component.resolutionForm.controls['contactMethodOther'].validator).not.toEqual(Validators.required);
      });

      it('creates a referral control', () => {
        expect(component.resolutionForm.controls['referral']).toBeDefined();
        expect(component.resolutionForm.controls['referral'].value).toBeNull();
        expect(component.resolutionForm.controls['referral'].validator).not.toEqual(Validators.required);
      });

      it('creates a approved control', () => {
        expect(component.resolutionForm.controls['approved']).toBeDefined();
        expect(component.resolutionForm.controls['approved'].value).toBeNull();
        expect(component.resolutionForm.controls['approved'].validator).not.toEqual(Validators.required);
      });

      it('creates a description control', () => {
        expect(component.resolutionForm.controls['description']).toBeDefined();
        expect(component.resolutionForm.controls['description'].value).toBeNull();
        expect(component.resolutionForm.controls['description'].validator).toEqual(Validators.required);
      });
    });

    it('calls updateRequiredFields', () => {
      spyOn(component, 'updateRequiredFields');

      component.ngOnInit();

      expect(component.updateRequiredFields).toHaveBeenCalledWith(component.resolutionType);
    });
  });

  describe('getters', () => {
    beforeEach(() => {
      component.ngOnInit();
    });

    describe('#resolutionType', () => {
      it('returns the control value', () => {
        component.resolutionForm.controls['resolutionType'].setValue('some value');

        expect(component.resolutionType).toEqual('some value');
      });
    });

    describe('#membership', () => {
      it('returns the control value', () => {
        component.resolutionForm.controls['membership'].setValue(account.memberships[0]);

        expect(component.membership).toEqual(account.memberships[0]);
      });
    });

    describe('#targetPartner', () => {
      it('returns the control value', () => {
        component.resolutionForm.controls['targetPartner'].setValue(new PartnerModels.FulfillmentPartner({}));

        expect(component.targetPartner).toEqual(new PartnerModels.FulfillmentPartner({}));
      });
    });

    describe('#targetBranch', () => {
      it('returns the control value', () => {
        component.resolutionForm.controls['targetBranch'].setValue(new PartnerModels.FulfillmentPartnerBranch({}));

        expect(component.targetBranch).toEqual(new PartnerModels.FulfillmentPartnerBranch({}));
      });
    });

    describe('#email', () => {
      it('returns the email on the membership', () => {
        expect(component.email).toEqual(account.memberships[0].user.email);
      });
    });

    describe('#contactMethods', () => {
      it('returns the contactMethods on the membership', () => {
        expect(component.contactMethods).toEqual(account.memberships[0].user.contact_methods);
      });
    });
  });

  describe('#updateRequiredFields', () => {
    beforeEach(() => {
      component.ngOnInit();
    });

    describe('conflict resolution type', () => {
      it('updates the required fields', () => {
        component.updateRequiredFields('conflict');

        expect(component.resolutionForm.controls['targetPartner'].validator).not.toEqual(Validators.required);
        expect(component.resolutionForm.controls['targetBranch'].validator).not.toEqual(Validators.required);
        expect(component.resolutionForm.controls['referral'].validator).toEqual(Validators.required);
      });
    });

    describe('any other resolution type', () => {
      it('updates the required fields', () => {
        component.updateRequiredFields('anything else');

        expect(component.resolutionForm.controls['targetPartner'].validator).toEqual(Validators.required);
        expect(component.resolutionForm.controls['targetBranch'].validator).toEqual(Validators.required);
        expect(component.resolutionForm.controls['referral'].validator).not.toEqual(Validators.required);
      });
    });
  });

  describe('#targetBranchChanged', () => {
    const serviceRequests = [new PeopleModels.ServiceRequest(PeopleFactories.ServiceRequestFactory.build())];
    const targetMember = new PeopleModels.FulfillmentPartnerMember(PeopleFactories.FulfillmentPartnerMemberFactory.build());

    beforeEach(() => {
      component.ngOnInit();
      component.resolutionForm.controls['targetBranch'].setValue(new PartnerModels.FulfillmentPartnerBranch({ id: 2 }));

      spyOn(peopleServiceRequestRepository, 'findAllByMembershipId').and.returnValue(of(serviceRequests));
      spyOn(peopleFulfillmentPartnerRepository, 'findAll').and.returnValue(of(fulfillmentPartners));
      spyOn(peopleFulfillmentPartnerMembersRepository, 'findAllByFulfillmentPartnerBranch').and.returnValue(of([targetMember]));

      component.targetBranchChanged();
    });

    it('calls the people service request repository', () => {
      expect(peopleServiceRequestRepository.findAllByMembershipId)
        .toHaveBeenCalledWith(component.membership.id, { fulfillment_partner_branch_id: component.targetBranch.id });
    });

    it('calls the fulfillment partner repository', () => {
      expect(peopleFulfillmentPartnerRepository.findAll).toHaveBeenCalledWith(component.targetBranch.id);
    });

    it('calls the people fulfillment partner member repository', () => {
      expect(peopleFulfillmentPartnerMembersRepository.findAllByFulfillmentPartnerBranch).toHaveBeenCalledWith(component.targetBranch.id);
    });

    it('sets the service requests on the component', () => {
      expect(component.serviceRequests).toEqual(serviceRequests);
    });

    it('sets the fulfillment partners on the component', () => {
      expect(component.fulfillmentPartners).toEqual(fulfillmentPartners);
    });

    it('sets the target members on the component', () => {
      expect(component.targetMembers).toEqual([new FulfillmentPartnerMemberViewModel(targetMember)]);
    });
  });

  describe('#loadConflictServiceRequests', () => {
    const serviceRequests = [new PeopleModels.ServiceRequest(PeopleFactories.ServiceRequestFactory.build())];

    beforeEach(() => {
      component.ngOnInit();

      spyOn(peopleServiceRequestRepository, 'findAllByMembershipId').and
        .returnValue(of(serviceRequests));
    });

    it('calls the people service request repository', () => {
      component.loadConflictServiceRequests();

      expect(peopleServiceRequestRepository.findAllByMembershipId).toHaveBeenCalledWith(component.membership.id);
    });

    it('sets the service requests on the component', () => {
      component.loadConflictServiceRequests();

      expect(component.conflictServiceRequests).toEqual(serviceRequests);
    });
  });

  describe('#showOtherContactMethodField', () => {
    it('returns false when other is not the value', () => {
      component.showOtherContactMethodField({ value: 'something else' });

      expect(component.showOtherContactMethodInput).toBe(false);
    });

    it('returns true when other is the value', () => {
      component.showOtherContactMethodField({ value: 'other' });

      expect(component.showOtherContactMethodInput).toBe(true);
    });
  });

  describe('#submit', () => {
    let body: object;
    let resolution: PartnerModels.Resolution;

    beforeEach(() => {
      component.ngOnInit();

      component.resolutionForm.controls['source'].setValue('some source');
      component.resolutionForm.controls['concern'].setValue('some concern');
      component.resolutionForm.controls['resolutionType'].setValue('conflict');
      component.resolutionForm.controls['approved'].setValue(true);
      component.resolutionForm.controls['description'].setValue('some description');

      body = {
        source: 'some source',
        status: 'unassigned',
        membership_id: component.membership.id,
        concern: 'some concern',
        resolution_type: 'conflict',
        approved_to_continue: true,
        description: 'some description'
      };

      resolution = new PartnerModels.Resolution(PartnerFactories.ResolutionFactory.build());

      spyOn(partnerResolutionRepository, 'create').and.returnValue(of(resolution));
    });

    describe('contact method not other', () => {
      it('creates a resolutions with the correct body', () => {
        const contactMethod = new PartnerModels.ContactMethod(PartnerFactories.ContactMethodFactory.build());

        component.resolutionForm.controls['contactMethod'].setValue(contactMethod);
        component.resolutionForm.controls['referral'].setValue('some referral');

        body['preferred_contact_method'] = contactMethod.value;
        body['referral'] = 'some referral';

        component.submit();

        expect(partnerResolutionRepository.create).toHaveBeenCalledWith(component.account.id, body);
        expect(router.navigate).toHaveBeenCalledWith(['/resolutions', resolution.id]);
      });
    });

    describe('contact method other', () => {
      it('creates a resolutions with the correct body', () => {
        component.resolutionForm.controls['contactMethod'].setValue('other');
        component.resolutionForm.controls['contactMethodOther'].setValue('some other contact method');
        component.resolutionForm.controls['referral'].setValue('some referral');

        body['preferred_contact_method'] = 'some other contact method';
        body['referral'] = 'some referral';

        component.submit();

        expect(partnerResolutionRepository.create).toHaveBeenCalledWith(component.account.id, body);
        expect(router.navigate).toHaveBeenCalledWith(['/resolutions', resolution.id]);
      });
    });

    describe('contact method email', () => {
      it('creates a resolutions with the correct body', () => {
        component.resolutionForm.controls['contactMethod'].setValue('email');
        component.resolutionForm.controls['referral'].setValue('some referral');

        body['preferred_contact_method'] = component.email;
        body['referral'] = 'some referral';

        component.submit();

        expect(partnerResolutionRepository.create).toHaveBeenCalledWith(component.account.id, body);
      });
    });

    describe('conflict resolution type', () => {
      describe('when a service request is not set', () => {
        it('creates a resolutions with the correct body', () => {
          const contactMethod = new PartnerModels.ContactMethod(PartnerFactories.ContactMethodFactory.build());

          component.resolutionForm.controls['contactMethod'].setValue(contactMethod);
          component.resolutionForm.controls['referral'].setValue('some referral');
          component.resolutionForm.controls['resolutionType'].setValue('conflict');

          body['preferred_contact_method'] = contactMethod.value;
          body['referral'] = 'some referral';

          component.submit();

          expect(partnerResolutionRepository.create).toHaveBeenCalledWith(component.account.id, body);
          expect(router.navigate).toHaveBeenCalledWith(['/resolutions', resolution.id]);
        });
      });

      describe('when a service request is set', () => {
        it('creates a resolutions with the correct body', () => {
          const contactMethod = new PartnerModels.ContactMethod(PartnerFactories.ContactMethodFactory.build());
          const serviceRequest = new PeopleModels.ServiceRequest(PeopleFactories.ServiceRequestFactory.build());

          component.resolutionForm.controls['contactMethod'].setValue(contactMethod);
          component.resolutionForm.controls['referral'].setValue('some referral');
          component.resolutionForm.controls['resolutionType'].setValue('conflict');
          component.resolutionForm.controls['serviceRequest'].setValue(serviceRequest);

          body['preferred_contact_method'] = contactMethod.value;
          body['referral'] = 'some referral';
          body['service_request_id'] = serviceRequest.id;

          component.submit();

          expect(partnerResolutionRepository.create).toHaveBeenCalledWith(component.account.id, body);
          expect(router.navigate).toHaveBeenCalledWith(['/resolutions', resolution.id]);
        });
      });
    });

    describe('any other resolution type', () => {
      describe('when a service request is not set', () => {
        it('creates a resolutions with the correct body', () => {
          const contactMethod = new PartnerModels.ContactMethod(PartnerFactories.ContactMethodFactory.build());
          const target = new PartnerModels.FulfillmentPartner(PartnerFactories.FulfillmentPartnerFactory.build());
          const targetBranch = new PartnerModels.FulfillmentPartnerBranch(PartnerFactories.FulfillmentPartnerBranchFactory.build());

          component.resolutionForm.controls['contactMethod'].setValue(contactMethod);
          component.resolutionForm.controls['resolutionType'].setValue('some other type');
          component.resolutionForm.controls['targetPartner'].setValue(target);
          component.resolutionForm.controls['targetBranch'].setValue(targetBranch);

          body['preferred_contact_method'] = contactMethod.value;
          body['target_id'] = target.id;
          body['target_branch_id'] = targetBranch.id;
          body['resolution_type'] = 'some other type';

          component.submit();

          expect(partnerResolutionRepository.create).toHaveBeenCalledWith(component.account.id, body);
          expect(router.navigate).toHaveBeenCalledWith(['/resolutions', resolution.id]);
        });
      });

      describe('when a service request is set', () => {
        it('creates a resolutions with the correct body', () => {
          const contactMethod = new PartnerModels.ContactMethod(PartnerFactories.ContactMethodFactory.build());
          const serviceRequest = new PeopleModels.ServiceRequest(PeopleFactories.ServiceRequestFactory.build());
          const target = new PartnerModels.FulfillmentPartner(PartnerFactories.FulfillmentPartnerFactory.build());
          const targetBranch = new PartnerModels.FulfillmentPartnerBranch(PartnerFactories.FulfillmentPartnerBranchFactory.build());

          component.resolutionForm.controls['contactMethod'].setValue(contactMethod);
          component.resolutionForm.controls['resolutionType'].setValue('some other type');
          component.resolutionForm.controls['targetPartner'].setValue(target);
          component.resolutionForm.controls['targetBranch'].setValue(targetBranch);
          component.resolutionForm.controls['serviceRequest'].setValue(serviceRequest);

          body['preferred_contact_method'] = contactMethod.value;
          body['target_id'] = target.id;
          body['target_branch_id'] = targetBranch.id;
          body['resolution_type'] = 'some other type';
          body['service_request_id'] = serviceRequest.id;

          component.submit();

          expect(partnerResolutionRepository.create).toHaveBeenCalledWith(component.account.id, body);
          expect(router.navigate).toHaveBeenCalledWith(['/resolutions', resolution.id]);
        });
      });

      describe('resolution type is provider', () => {
        describe('when a target member is set', () => {
          it('creates a resolutions with the correct body', () => {
            const contactMethod = new PartnerModels.ContactMethod(PartnerFactories.ContactMethodFactory.build());
            const target = new PartnerModels.FulfillmentPartner(PartnerFactories.FulfillmentPartnerFactory.build());
            const targetBranch = new PartnerModels.FulfillmentPartnerBranch(PartnerFactories.FulfillmentPartnerBranchFactory.build());
            const targetMember = new PartnerModels.FulfillmentPartnerMember(PartnerFactories.FulfillmentPartnerMemberFactory.build());

            component.resolutionForm.controls['contactMethod'].setValue(contactMethod);
            component.resolutionForm.controls['resolutionType'].setValue('provider');
            component.resolutionForm.controls['targetPartner'].setValue(target);
            component.resolutionForm.controls['targetBranch'].setValue(targetBranch);
            component.resolutionForm.controls['targetMember'].setValue(targetMember);

            body['preferred_contact_method'] = contactMethod.value;
            body['target_id'] = target.id;
            body['target_branch_id'] = targetBranch.id;
            body['target_member_id'] = targetMember.id;
            body['resolution_type'] = 'provider';

            component.submit();

            expect(partnerResolutionRepository.create).toHaveBeenCalledWith(component.account.id, body);
            expect(router.navigate).toHaveBeenCalledWith(['/resolutions', resolution.id]);
          });
        });

        describe('when a target member is not set', () => {
          it('creates a resolutions with the correct body', () => {
            const contactMethod = new PartnerModels.ContactMethod(PartnerFactories.ContactMethodFactory.build());
            const target = new PartnerModels.FulfillmentPartner(PartnerFactories.FulfillmentPartnerFactory.build());
            const targetBranch = new PartnerModels.FulfillmentPartnerBranch(PartnerFactories.FulfillmentPartnerBranchFactory.build());

            component.resolutionForm.controls['contactMethod'].setValue(contactMethod);
            component.resolutionForm.controls['resolutionType'].setValue('provider');
            component.resolutionForm.controls['targetPartner'].setValue(target);
            component.resolutionForm.controls['targetBranch'].setValue(targetBranch);

            body['preferred_contact_method'] = contactMethod.value;
            body['target_id'] = target.id;
            body['target_branch_id'] = targetBranch.id;
            body['resolution_type'] = 'provider';

            component.submit();

            expect(partnerResolutionRepository.create).toHaveBeenCalledWith(component.account.id, body);
            expect(router.navigate).toHaveBeenCalledWith(['/resolutions', resolution.id]);
          });
        });
      });

      describe('resolution type is not provider', () => {
        describe('when a referral is set', () => {
          it('creates a resolutions with the correct body', () => {
            const contactMethod = new PartnerModels.ContactMethod(PartnerFactories.ContactMethodFactory.build());
            const target = new PartnerModels.FulfillmentPartner(PartnerFactories.FulfillmentPartnerFactory.build());
            const targetBranch = new PartnerModels.FulfillmentPartnerBranch(PartnerFactories.FulfillmentPartnerBranchFactory.build());

            component.resolutionForm.controls['contactMethod'].setValue(contactMethod);
            component.resolutionForm.controls['resolutionType'].setValue('some other type');
            component.resolutionForm.controls['targetPartner'].setValue(target);
            component.resolutionForm.controls['targetBranch'].setValue(targetBranch);
            component.resolutionForm.controls['referral'].setValue('some referral');

            body['preferred_contact_method'] = contactMethod.value;
            body['target_id'] = target.id;
            body['target_branch_id'] = targetBranch.id;
            body['referral'] = 'some referral';
            body['resolution_type'] = 'some other type';

            component.submit();

            expect(partnerResolutionRepository.create).toHaveBeenCalledWith(component.account.id, body);
            expect(router.navigate).toHaveBeenCalledWith(['/resolutions', resolution.id]);
          });
        });

        describe('when a referral is not set', () => {
          it('creates a resolutions with the correct body', () => {
            const contactMethod = new PartnerModels.ContactMethod(PartnerFactories.ContactMethodFactory.build());
            const target = new PartnerModels.FulfillmentPartner(PartnerFactories.FulfillmentPartnerFactory.build());
            const targetBranch = new PartnerModels.FulfillmentPartnerBranch(PartnerFactories.FulfillmentPartnerBranchFactory.build());

            component.resolutionForm.controls['contactMethod'].setValue(contactMethod);
            component.resolutionForm.controls['resolutionType'].setValue('some other type');
            component.resolutionForm.controls['targetPartner'].setValue(target);
            component.resolutionForm.controls['targetBranch'].setValue(targetBranch);

            body['preferred_contact_method'] = contactMethod.value;
            body['target_id'] = target.id;
            body['target_branch_id'] = targetBranch.id;
            body['resolution_type'] = 'some other type';

            component.submit();

            expect(partnerResolutionRepository.create).toHaveBeenCalledWith(component.account.id, body);
            expect(router.navigate).toHaveBeenCalledWith(['/resolutions', resolution.id]);
          });
        });
      });
    });
  });

  describe('#back', () => {
    it('should call location back', () => {
      spyOn(location, 'back').and.callThrough();

      component.back();

      expect(location.back).toHaveBeenCalled();
    });
  });
});
