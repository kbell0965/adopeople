import { Location } from '@angular/common';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CUSTOM_ELEMENTS_SCHEMA, DebugElement } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule, MatCheckboxModule, MatInputModule, MatOptionModule, MatRadioModule, MatSelectModule } from '@angular/material';
import { By } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { AccountViewModel } from '../../../../models/account-view-model';
import { CreateResolutionComponent } from './create-resolution.component';
import {
  PartnerModels,
  PeopleServiceRequestRepository,
  PartnerResolutionRepository,
  PeopleFulfillmentPartnerRepository,
  PeopleFulfillmentPartnerMembersRepository
} from '@pplsi-core/datalayer';
import { PartnerFactories } from '@pplsi-core/factories';

describe('CreateResolutionComponent.Template', () => {
  let component: CreateResolutionComponent;
  let fixture: ComponentFixture<CreateResolutionComponent>;
  const accountFake: object = PartnerFactories.AccountFactory.build();
  const account: AccountViewModel = new AccountViewModel(accountFake);
  const fulfillmentPartnersFake: object[] = PartnerFactories.FulfillmentPartnerFactory.buildList(1);
  const fulfillmentPartners: PartnerModels.FulfillmentPartner[] = fulfillmentPartnersFake
    .map((fulfillmentPartnerFake: object) => new PartnerModels.FulfillmentPartner(fulfillmentPartnerFake));
  const activatedRouteStub = {
    snapshot: {
      params: { 'account_id': account['id'] },
      data: {
        account: account,
        fulfillmentPartners: fulfillmentPartners
      }
    }
  };
  const locationStub = {
    back() {
      return true;
    }
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateResolutionComponent ],
      imports: [
        RouterTestingModule,
        ReactiveFormsModule,
        HttpClientTestingModule,
        MatInputModule,
        MatButtonModule,
        MatRadioModule,
        MatSelectModule,
        MatOptionModule,
        MatCheckboxModule,
        BrowserAnimationsModule
      ],
      providers: [
        { provide: ActivatedRoute, useValue: activatedRouteStub },
        PeopleFulfillmentPartnerRepository,
        PeopleServiceRequestRepository,
        PartnerResolutionRepository,
        { provide: Location, useValue: locationStub },
        PeopleFulfillmentPartnerMembersRepository
      ],
      schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateResolutionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  describe('user input', () => {
    it('has an input for user selection', () => {
      component.ngOnInit();
      fixture.detectChanges();

      const debugElement: DebugElement = fixture.debugElement.query(By.css('.user-input span span'));

      expect(debugElement.nativeElement.innerHTML).toEqual(account.getActiveMemberships()[0].user.fullName());
    });
  });

  describe('resolution type input', () => {
    it('calls update required fields when changed', () => {
      fixture.detectChanges();

      spyOn(component, 'updateRequiredFields').and.callThrough();

      const debugElement: DebugElement = fixture.debugElement.query(By.css('.resolution-type-input'));
      debugElement.triggerEventHandler('change', { value: 'provider' });

      expect(component.updateRequiredFields).toHaveBeenCalledWith('provider');
    });

    it('calls load conflict service requests when clicked', () => {
      fixture.detectChanges();

      spyOn(component, 'loadConflictServiceRequests').and.callThrough();

      const debugElement: DebugElement = fixture.debugElement.query(By.css('.conflict-option'));
      debugElement.triggerEventHandler('click', {});

      expect(component.loadConflictServiceRequests).toHaveBeenCalled();
    });
  });

  describe('target section', () => {
    describe('conflict', () => {
      beforeEach(() => {
        component.resolutionForm.controls['resolutionType'].setValue('conflict');

        fixture.detectChanges();
      });

      it('has an input for service request selection', () => {
        const debugElement: DebugElement = fixture.debugElement.query(By.css('.service-request-input'));

        expect(debugElement.nativeElement).toBeTruthy();
      });

      it('has an input for referral selection', () => {
        const debugElement: DebugElement = fixture.debugElement.query(By.css('.referral-input'));

        expect(debugElement.nativeElement).toBeTruthy();
      });
    });

    describe('not a conflict', () => {
      it('does not have an input for target selection', () => {
        component.ngOnInit();
        component.resolutionForm.controls['resolutionType'].setValue('provider');
        fixture.detectChanges();
        const debugElement: DebugElement = fixture.debugElement.query(By.css('.target-input span span'));

        expect(debugElement).not.toBeTruthy();
      });

      describe('target branch input', () => {
        it('calls load target branch service requests when selection changed', () => {
          const targetBranch = new PartnerModels.FulfillmentPartnerBranch(PartnerFactories.FulfillmentPartnerBranchFactory.build());

          component.ngOnInit();
          component.resolutionForm.controls['resolutionType'].setValue('provider');
          component.resolutionForm.controls['targetBranch'].setValue(targetBranch);
          fixture.detectChanges();

          spyOn(component, 'targetBranchChanged').and.callThrough();

          const debugElement: DebugElement = fixture.debugElement.query(By.css('.target-branch-input'));
          debugElement.triggerEventHandler('selectionChange', {});

          expect(component.targetBranchChanged).toHaveBeenCalled();
        });

        it('has an input for target branch selection', () => {
          const debugElement: DebugElement = fixture.debugElement.query(By.css('.target-branch-input'));

          expect(debugElement.nativeElement).toBeTruthy();
        });
      });

      it('has an input for service request selection', () => {
        const debugElement: DebugElement = fixture.debugElement.query(By.css('.service-request-input'));

        expect(debugElement.nativeElement).toBeTruthy();
      });

      describe('provider resolution type', () => {
        it('has an input for target member selection', () => {
          const debugElement: DebugElement = fixture.debugElement.query(By.css('.target-member-input'));

          expect(debugElement.nativeElement).toBeTruthy();
        });

        it('does not have an input for referral selection', () => {
          const debugElement: DebugElement = fixture.debugElement.query(By.css('.referral-input'));

          expect(debugElement).toBeNull();
        });
      });

      describe('referral resolution type', () => {
        beforeEach(() => {
          component.resolutionForm.controls['resolutionType'].setValue('referral');

          fixture.detectChanges();
        });

        it('does not have an input for target member selection', () => {
          const debugElement: DebugElement = fixture.debugElement.query(By.css('.target-member-input'));

          expect(debugElement).toBeNull();
        });

        it('has an input for referral selection', () => {
          const debugElement: DebugElement = fixture.debugElement.query(By.css('.referral-input'));

          expect(debugElement.nativeElement).toBeTruthy();
        });
      });
    });
  });

  it('has an input for contact method selection', () => {
    const debugElement: DebugElement = fixture.debugElement.query(By.css('.contact-methods'));

    expect(debugElement.nativeElement).toBeTruthy();
  });

  describe('other contact method', () => {
    describe('contact method other selected', () => {
      it('has an input for contact method other selection', () => {
        component.showOtherContactMethodInput = true;
        fixture.detectChanges();

        const debugElement: DebugElement = fixture.debugElement.query(By.css('.contact-method-other-input'));

        expect(debugElement.nativeElement).toBeTruthy();
      });
    });

    describe('contact method other not selected', () => {
      it('does not have an input for contact method other selection', () => {
        component.showOtherContactMethodInput = false;
        fixture.detectChanges();

        const debugElement: DebugElement = fixture.debugElement.query(By.css('.contact-method-other-input'));

        expect(debugElement).toBeNull();
      });
    });
  });

  it('has a details input', () => {
    const debugElement: DebugElement = fixture.debugElement.query(By.css('.details'));

    expect(debugElement.nativeElement).toBeTruthy();
  });

  it('has an approved input', () => {
    const debugElement: DebugElement = fixture.debugElement.query(By.css('.approved-input'));

    expect(debugElement.nativeElement).toBeTruthy();
  });

  describe('cancel', () => {
    it('has a cancel button', () => {
      const debugElement: DebugElement = fixture.debugElement.query(By.css('.cancel-button'));

      expect(debugElement.nativeElement).toBeTruthy();
    });

    it('calls back when clicked', () => {
      spyOn(component, 'back').and.returnValue();

      const debugElement: DebugElement = fixture.debugElement.query(By.css('.cancel-button'));
      debugElement.triggerEventHandler('click', {});

      expect(component.back).toHaveBeenCalled();
    });
  });

  describe('source of concern input', () => {
    it('radio group exists', () => {
      expect(fixture.debugElement.query(By.css('mat-radio-group.source-input'))).not.toBeNull();
    });

    it('has an Email/Letter option', () => {
      expect(fixture.debugElement.query(By.css('.source-input mat-radio-button[value="letter"]'))).not.toBeNull();
    });

    it('has an Call/Chat option', () => {
      expect(fixture.debugElement.query(By.css('.source-input mat-radio-button[value="call"]'))).not.toBeNull();
    });

    it('has an Survey option', () => {
      expect(fixture.debugElement.query(By.css('.source-input mat-radio-button[value="survey"]'))).not.toBeNull();
    });

    it('has an Associate option', () => {
      expect(fixture.debugElement.query(By.css('.source-input mat-radio-button[value="associate"]'))).not.toBeNull();
    });

    it('has an Clarification option', () => {
      expect(fixture.debugElement.query(By.css('.source-input mat-radio-button[value="clarification"]'))).not.toBeNull();
    });
  });

  describe('submit', () => {
    it('has a submit button', () => {
      const debugElement: DebugElement = fixture.debugElement.query(By.css('.submit-button'));

      expect(debugElement.nativeElement).toBeTruthy();
    });

    it('is disabled until form is valid', () => {
      const debugElement: DebugElement = fixture.debugElement.query(By.css('.submit-button'));

      expect(debugElement.nativeElement.attributes['disabled']).toBeDefined();
    });

    it('is not disabled when form is valid', () => {
      spyOnProperty(component.resolutionForm, 'invalid', 'get').and.returnValue(false);

      fixture.detectChanges();

      const debugElement: DebugElement = fixture.debugElement.query(By.css('.submit-button'));

      expect(debugElement.nativeElement.attributes['disabled']).toBeUndefined();
    });

    it('calls submit when clicked', () => {
      spyOnProperty(component.resolutionForm, 'invalid', 'get').and.returnValue(false);
      spyOn(component, 'submit').and.returnValue();

      fixture.detectChanges();

      const debugElement: DebugElement = fixture.debugElement.query(By.css('.submit-button'));
      debugElement.triggerEventHandler('click', {});

      expect(component.submit).toHaveBeenCalled();
    });
  });
});
