import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AccountFromPathResolver } from '../../resolvers/account-from-path/account-from-path.resolver';
import { FulfillmentPartnerBranchesResolver } from '../../resolvers/fulfillment-partner-branches/fulfillment-partner-branches.resolver';
import { CreateResolutionComponent } from './components/create-resolution/create-resolution.component';

const routes: Routes = [
  {
    path: ':account_id/resolutions',
    component: CreateResolutionComponent,
    resolve: {
      account: AccountFromPathResolver,
      fulfillmentPartnerBranches: FulfillmentPartnerBranchesResolver
    }
  }
];

@NgModule({
  imports: [ RouterModule.forChild(routes) ],
  exports: [ RouterModule ]
})
export class AccountsRoutingModule { }
