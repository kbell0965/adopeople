import { NgModule } from '@angular/core';
import { UsersRoutingModule } from './users-routing.module';
import { AccountFromPathResolver } from '../../resolvers/account-from-path/account-from-path.resolver';
import { UserFromPathResolver } from 'src/app/resolvers/user-from-path/user-from-path.resolver';

@NgModule({
  imports: [UsersRoutingModule],
  providers: [AccountFromPathResolver, UserFromPathResolver]
})
export class UsersModule { }
