import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AccountFromPathResolver } from '../../resolvers/account-from-path/account-from-path.resolver';
import { UserFromPathResolver } from '../../resolvers/user-from-path/user-from-path.resolver';

const routes: Routes = [
  {
    path: ':account_id/user/:user_id',
    loadChildren: () => import('./modules/user-details/user-details.module').then(m => m.UserDetailsModule),
    resolve: {
      account: AccountFromPathResolver,
      user: UserFromPathResolver
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsersRoutingModule { }

