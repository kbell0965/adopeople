import { By } from '@angular/platform-browser';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountViewModel } from 'src/app/models/account-view-model';
import { PartnerFactories } from '@pplsi-core/factories';
import { UserActivePlansComponent } from './user-active-plans.component';
import { SubscriptionStatus } from '@pplsi-core/datalayer';

describe('UserActivePlansComponent.Template', () => {
  let component: UserActivePlansComponent;
  let fixture: ComponentFixture<UserActivePlansComponent>;
  const product: object = PartnerFactories.ProductFactory.build({ name: 'Product 1' });
  const sellable_products: object = [ product ];
  const subscriptions: object = [
    PartnerFactories.SubscriptionFactory.build({
      sellable_products: sellable_products,
      current_status: SubscriptionStatus.Active
    })
  ];
  const products: object = [ product ];
  const accountFake = PartnerFactories.AccountFactory.build({ subscriptions, products });
  const account: AccountViewModel = new AccountViewModel(accountFake);

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserActivePlansComponent ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserActivePlansComponent);
    component = fixture.componentInstance;
    component.account = account;
    fixture.detectChanges();
  });

  it('displays the active product', () => {
    const el = fixture.debugElement.query(By.css('.product'));

    expect(el.nativeElement.innerHTML).toEqual('Product 1');
  });

  it('displays the product image', () => {
    const el = fixture.debugElement.query(By.css('img'));

    expect(el.nativeElement.src).toContain(`assets/${ account.products[0].getProductType() }_plan_icon.svg`);
  });
});
