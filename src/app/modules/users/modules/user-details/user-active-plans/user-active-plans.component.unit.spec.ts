import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountViewModel } from 'src/app/models/account-view-model';
import { UserActivePlansComponent } from './user-active-plans.component';
import { PartnerFactories } from '@pplsi-core/factories';

describe('UserActivePlansComponent.Unit', () => {
  let component: UserActivePlansComponent;
  let fixture: ComponentFixture<UserActivePlansComponent>;
  const account: AccountViewModel = new AccountViewModel(PartnerFactories.AccountFactory.build());

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserActivePlansComponent ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserActivePlansComponent);
    component = fixture.componentInstance;
    component.account = account;
    fixture.detectChanges();
  });

  it('has an account input', () => {
    expect(component.account).toEqual(account);
  });

  describe('#ngOnInit', () => {
    it('sets the active products', () => {
      component.activeProducts = undefined;
      component.ngOnInit();
      expect(component.activeProducts).toEqual(account.serviceableProducts());
    });
  });

  describe('#ngOnChanges', () => {
    it('sets the active products', () => {
      component.activeProducts = undefined;
      component.ngOnChanges();
      expect(component.activeProducts).toEqual(account.serviceableProducts());
    });
  });
});
