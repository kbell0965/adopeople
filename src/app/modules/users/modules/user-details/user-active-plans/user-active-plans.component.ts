import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { AccountViewModel } from 'src/app/models/account-view-model';
import { ProductViewModel } from 'src/app/models/product-view-model';

@Component({
  selector: 'user-active-plans',
  templateUrl: './user-active-plans.component.html',
  styleUrls: [ './user-active-plans.component.scss' ]
})
export class UserActivePlansComponent implements OnInit, OnChanges {
  @Input() account: AccountViewModel;
  activeProducts: ProductViewModel[];

  ngOnInit(): void {
    this.activeProducts = this.account.serviceableProducts();
  }

  ngOnChanges(): void {
    this.activeProducts = this.account.serviceableProducts();
  }
}
