import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { AccountViewModel, Type } from '../../../../models/account-view-model';
import { PartnerUserViewModel } from '../../../../models/partner-user-view-model';
import { Subject } from 'rxjs';
import { MembershipViewModel } from '../../../../models/membership-view-model';
import { UserViewModel } from '../../models/user-view-model';

@Component({
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.scss']
})
export class UserDetailsComponent implements OnInit, OnDestroy {
  account: AccountViewModel;
  pathUserId: string;
  primaryMembership: MembershipViewModel;
  profileBaseUrl: string;
  spouseUser: PartnerUserViewModel;
  tabData: Array<{ label: string; link: string; }>;
  user: UserViewModel;

  private _destroyed = new Subject<void>();

  constructor(
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    this.setup();
  }

  setup(): void {
    const { data, params } = this.route.snapshot;
    this.user = data['user'];
    this.account = data['account'];
    this.pathUserId = params['user_id'];
    this.primaryMembership = this.getPrimaryMembership(this.account);
    this.spouseUser = this.account.getUserByType(Type.SPOUSE);
    this.profileBaseUrl = `/users/${this.primaryMembership.account_id}/user/${this.pathUserId}`;
    this.tabData = [
      { label: 'Profile', link: `${this.profileBaseUrl}/profile` },
      { label: 'Partner/Spouse', link: `${this.profileBaseUrl}/partner` },
      { label: 'Resolutions', link: `${this.profileBaseUrl}/resolutions` }
    ];
    if (!this.spouseUser) {
      this.tabData = this.tabData.filter(tab => tab.label !== 'Partner/Spouse');
    }
    if (this.spouseUser && (this.spouseUser.id === this.pathUserId)) {
      this.router.navigate([`${this.profileBaseUrl}/partner`]);
    }
  }

  ngOnDestroy() {
    this._destroyed.next();
    this._destroyed.complete();
  }

  getPrimaryMembership(account: AccountViewModel): MembershipViewModel {
    return account.memberships.filter((membership: MembershipViewModel) => membership.type === 'primary')[0];
  }
}
