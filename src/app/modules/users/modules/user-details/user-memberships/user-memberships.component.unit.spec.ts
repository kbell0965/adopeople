import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { UserMembershipsComponent } from './user-memberships.component';
import { PartnerFactories } from '@pplsi-core/factories';
import { RouterTestingModule } from '@angular/router/testing';
import { AccountViewModel } from 'src/app/models/account-view-model';
import { FeatherModule } from 'angular-feather';
import { ChevronDown, ChevronUp } from 'angular-feather/icons';
import { TitleCasePipe } from '@angular/common';

describe('UserMembershipsComponent.Unit', () => {
  let component: UserMembershipsComponent;
  let fixture: ComponentFixture<UserMembershipsComponent>;
  const inputMemberships = PartnerFactories.MembershipFactory.buildList(10);
  const products = [PartnerFactories.ProductFactory.build({ name: 'Privacy Defense' }),
  PartnerFactories.ProductFactory.build({ name: 'Legal Plan Family (1)' })];
  const activeProducts = [PartnerFactories.ProductFactory.build({ name: 'Associate Future' })];
  const account = PartnerFactories.AccountFactory.build({ products: products });
  const activeAccount = PartnerFactories.AccountFactory.build({ products: activeProducts });
  const memberships = [PartnerFactories.MembershipFactory.build({ account: account }),
  PartnerFactories.MembershipFactory.build({ account: activeAccount })];

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserMembershipsComponent ],
      imports: [
        FeatherModule.pick({ ChevronDown, ChevronUp }),
        RouterTestingModule
      ],
      providers: [TitleCasePipe]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserMembershipsComponent);
    component = fixture.componentInstance;
    inputMemberships.forEach(m => m.account = PartnerFactories.AccountFactory.build());
    component.memberships = inputMemberships;
    component.user = component.memberships[0].user;
    component.account = new AccountViewModel(PartnerFactories.AccountFactory.build({ products: activeProducts }));

    fixture.detectChanges();
  });

  describe('#sortMembershipsByActiveMembership', () => {
    describe('memberships contain an active plan', () => {
      beforeEach(() => {
        spyOn(component.account, 'serviceableProducts').and.returnValue(activeProducts);
        component.memberships = memberships;
        component.setActiveMembershipAsDefault();
      });

      it('sets active memberships', () => {
        const membership = component.memberships[0].account;
        expect(membership.products[0].id).toEqual(activeProducts[0].id);
      });
    });

    describe('memberships do not contain an active plan', () => {
      beforeEach(() => {
        spyOn(component.account, 'serviceableProducts').and.returnValue(null);
        component.memberships = memberships;
        component.setActiveMembershipAsDefault();
      });

      it('doest not set active memberships', () => {
        expect(component.memberships[0].id).toEqual(memberships[0].id);
      });
    });
  });

  describe('showMore', () => {
    beforeEach(() => {
      component.memberships = inputMemberships;
      fixture.detectChanges();
    });

    it('toggles the viewMore to false', () => {
      component.viewMore = true;
      component.showMore();

      expect(component.viewMore).toEqual(false);
    });

    it('toggles the viewMore to true', () => {
      component.viewMore = false;
      component.showMore();

      expect(component.viewMore).toEqual(true);
    });

  });
});
