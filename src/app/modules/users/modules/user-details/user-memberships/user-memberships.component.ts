import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { PartnerUserViewModel } from 'src/app/models/partner-user-view-model';
import { AccountViewModel } from 'src/app/models/account-view-model';
import { TitleCasePipe } from '@angular/common';

@Component({
  selector: 'ls-user-memberships',
  templateUrl: './user-memberships.component.html',
  styleUrls: ['./user-memberships.component.scss']
})
export class UserMembershipsComponent implements OnChanges, OnInit {
  @Input() account: AccountViewModel;
  @Input() memberships: any;
  @Input() pathUserId: string;
  @Input() user: PartnerUserViewModel;

  canNavigate: boolean;
  viewMore: boolean;
  userFirstName: string;

  constructor(private titleCasePipe: TitleCasePipe) { }

  ngOnInit() {
    const { first_name, id } = this.user;

    this.canNavigate = this.pathUserId === id;
    this.userFirstName = first_name ? this.titleCasePipe.transform(first_name) : '';
  }

  ngOnChanges() {
    this.setActiveMembershipAsDefault();
  }

  setActiveMembershipAsDefault() {
    const activePlans = this.account ? this.account.serviceableProducts() : null;

    if (this.memberships && activePlans) {
      this.memberships.sort(a => a.account.products
      && a.account.products.some(product => product.id.includes(activePlans[0].id)) ? -1 : 0);
    }
  }

  showMore(): void {
    this.viewMore = this.viewMore ? false : true;
  }

  showMoreIcon(): string {
    return this.viewMore ? 'chevron-down' : 'chevron-up';
  }
}
