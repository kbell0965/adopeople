import { async, ComponentFixture, TestBed, getTestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { UserMembershipsComponent } from './user-memberships.component';
import { PartnerUserViewModel } from 'src/app/models/partner-user-view-model';
import { PartnerFactories } from '@pplsi-core/factories';
import { RouterTestingModule } from '@angular/router/testing';
import { FeatherModule } from 'angular-feather';
import { ChevronDown, ChevronUp } from 'angular-feather/icons';
import { TitleCasePipe } from '@angular/common';

describe('UserMembershipsComponent.Template', () => {
  let component: UserMembershipsComponent;
  let fixture: ComponentFixture<UserMembershipsComponent>;
  const inputMemberships = PartnerFactories.MembershipFactory.buildList(10);
  let injector: TestBed;
  let titleCasePipe: TitleCasePipe;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserMembershipsComponent ],
      imports: [
        FeatherModule.pick({ ChevronDown, ChevronUp }),
        RouterTestingModule
      ],
      providers: [TitleCasePipe]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    injector = getTestBed();
    fixture = TestBed.createComponent(UserMembershipsComponent);
    component = fixture.componentInstance;
    inputMemberships.forEach(m => m.account = PartnerFactories.AccountFactory.build());
    component.memberships = inputMemberships;
    component.user = new PartnerUserViewModel(component.memberships[0].user);
    titleCasePipe = injector.get(TitleCasePipe);
    fixture.detectChanges();
  });

  describe('user memberships', () => {
    it('displays the member first name in the memberships header', () => {
      const el = fixture.debugElement.query(By.css('.header-content'));

      expect(el.nativeElement.textContent).toContain(`${titleCasePipe.transform(component.user.first_name)}'s Memberships`);
    });

    it('displays the list of memberships', () => {
      const elements = fixture.debugElement.queryAll(By.css('.member-membership'));
      const limit = !component.viewMore ? 1 : component.memberships.length;

      expect(elements.length).toBe(limit);
    });

    it('displays the product associated with the membership and its account on init', () => {
      const elements = fixture.debugElement.queryAll(By.css('.membership-data'));
      const limit = !component.viewMore ? 1 : component.memberships.length;
      const visible = component.memberships.filter((ms, i) => i < limit);

      expect(elements.map(e => e.nativeElement.innerHTML)).toEqual(visible.map(ms => ms.account.products[0].name));
    });

    it('displays the product associated with the membership and its account after expanding', () => {
      fixture.detectChanges();

      const elements = fixture.debugElement.queryAll(By.css('.membership-data'));
      const limit = !component.viewMore ? 1 : component.memberships.length;
      const visible = component.memberships.filter((ms, i) => i < limit);

      expect(elements.map(e => e.nativeElement.innerHTML)).toEqual(visible.map(ms => ms.account.products[0].name));
    });

    describe('Product links', () => {
      beforeEach(() => {
        component.canNavigate = true;
        fixture.detectChanges();
      });

      it('have the expected href URL', () => {
        const el = fixture.debugElement.queryAll(By.css('.member-membership a'))[0];
        const expected = `/users/${component.memberships[0].account_id}/user/${component.user.id}`;

        expect(el.nativeElement.getAttribute('href')).toContain(expected);
      });

      it('allows you to click on a product to change the url to that account if you are allowed to', () => {
        const el = fixture.debugElement.queryAll(By.css('.member-membership a'))[0];

        expect(el).not.toBe(undefined);
      });

      it('does not allow you to click on a product if you are not allowed to', () => {
        component.canNavigate = false;
        fixture.detectChanges();
        const el = fixture.debugElement.queryAll(By.css('.member-membership a'))[0];

        expect(el).toBe(undefined);
      });
    });
  });
});
