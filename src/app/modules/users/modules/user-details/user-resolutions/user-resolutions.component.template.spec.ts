import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { UserResolutionsComponent } from './user-resolutions.component';
import { PartnerFactories } from '@pplsi-core/factories';
import { ActivatedRoute } from '@angular/router';
import { PartnerResolutionRepository } from '@pplsi-core/datalayer';
import { AccountViewModel } from 'src/app/models/account-view-model';
import { SearchLayoutModule } from 'ng-components.adonis';
import { ResolutionSearchResultModule } from 'src/app/shared/components/resolution-search-result/resolution-search-result.module';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DatePipe } from '@angular/common';
import { RouterTestingModule } from '@angular/router/testing';
import { By } from '@angular/platform-browser';

const accountFake: object = PartnerFactories.AccountFactory.build();
const account: AccountViewModel = new AccountViewModel(accountFake);
const fakeResolutions = PartnerFactories.ResolutionFactory.buildList(10);
const initialQueryResult = { resolutions: fakeResolutions, total_length: 10 };
const activatedRouteStub = {
  parent: { snapshot: { data: { account } } },
  snapshot: { data: { initialQueryResult } }
};

describe('UserResolutionsComponent.Template', () => {
  let component: UserResolutionsComponent;
  let fixture: ComponentFixture<UserResolutionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [UserResolutionsComponent],
      imports: [
        BrowserAnimationsModule,
        HttpClientModule,
        ResolutionSearchResultModule,
        RouterTestingModule,
        SearchLayoutModule
      ],
      providers: [
        { provide: ActivatedRoute, useValue: activatedRouteStub },
        DatePipe,
        PartnerResolutionRepository
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserResolutionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('has search layout', () => {
    const searchLayout = fixture.debugElement.query(By.css('ls-search-layout'));

    expect(searchLayout).not.toBeNull();
  });

  it('displays all of the paged resolution results', () => {
    const resultEls = fixture.debugElement.queryAll(By.css('ls-resolution-search-result'));

    expect(resultEls.length).toBe(fakeResolutions.length);
  });
});
