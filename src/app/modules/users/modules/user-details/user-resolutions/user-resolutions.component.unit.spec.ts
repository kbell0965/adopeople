import { async, ComponentFixture, TestBed, getTestBed } from '@angular/core/testing';
import { UserResolutionsComponent } from './user-resolutions.component';
import { PartnerFactories } from '@pplsi-core/factories';
import { ActivatedRoute } from '@angular/router';
import { PartnerResolutionRepository, PartnerModels } from '@pplsi-core/datalayer';
import { of } from 'rxjs';
import { ResolutionViewModel } from 'src/app/modules/resolutions/models/resolution-view-model';
import { AccountViewModel } from 'src/app/models/account-view-model';
import { SearchLayoutModule } from 'ng-components.adonis';
import { ResolutionSearchResultModule } from 'src/app/shared/components/resolution-search-result/resolution-search-result.module';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DatePipe } from '@angular/common';
import { RouterTestingModule } from '@angular/router/testing';

const accountFake: object = PartnerFactories.AccountFactory.build();
const account: AccountViewModel = new AccountViewModel(accountFake);
const fakeResolutions = PartnerFactories.ResolutionFactory.buildList(10);
const initialQueryResult = { resolutions: fakeResolutions, total_length: 10 };
const activatedRouteStub = {
  parent: { snapshot: { data: { account } } },
  snapshot: { data: { initialQueryResult } }
};

describe('UserResolutionsComponent.Unit', () => {
  let component: UserResolutionsComponent;
  let fixture: ComponentFixture<UserResolutionsComponent>;
  let injector: TestBed;
  let resolutionRepositorySpy: jasmine.Spy;
  let resolutionRepository: PartnerResolutionRepository;
  let route: ActivatedRoute;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [UserResolutionsComponent],
      imports: [
        BrowserAnimationsModule,
        HttpClientModule,
        ResolutionSearchResultModule,
        RouterTestingModule,
        SearchLayoutModule
      ],
      providers: [
        { provide: ActivatedRoute, useValue: activatedRouteStub },
        DatePipe,
        PartnerResolutionRepository
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserResolutionsComponent);
    component = fixture.componentInstance;
    injector = getTestBed();
    resolutionRepository = injector.get(PartnerResolutionRepository);
    resolutionRepositorySpy = spyOn(resolutionRepository, 'findAll');
    resolutionRepositorySpy.and.returnValue(of(
      new PartnerModels.ResolutionsWithPagination({ total_length: 0, resolutions: [] })
    ));
    route = TestBed.get(ActivatedRoute);
    fixture.detectChanges();
  });

  it('has resolutions in the route params', () => {
    expect(route.snapshot.data.initialQueryResult.resolutions.length).toBe(10);
  });

  describe('#ngOnInit', () => {
    it('sets pager total length from activated route data', () => {
      component.ngOnInit();
      expect(component.submitResultsPage.length).toBe(10);
    });

    it('sets resolutions to the component', () => {
      component.resolutions$.subscribe(resolutions => {
        expect(resolutions).toEqual(fakeResolutions.map(r => new ResolutionViewModel(r)));
      });
      component.ngOnInit();
    });
  });

  describe('#pageResults', () => {
    it('calls resolutionRepository with the page index, size and account id', () => {
      resolutionRepositorySpy.calls.reset();
      component.pageResults({ pageIndex: 2, pageSize: 10, length: 10 });
      expect(resolutionRepositorySpy).toHaveBeenCalledTimes(1);
      expect(resolutionRepositorySpy).toHaveBeenCalledWith({page: 3, per_page: 10}, account.id);
    });

    it('updates resolutions with the new page data', () => {
      resolutionRepositorySpy.and.returnValue(initialQueryResult);
      component.resolutions$.subscribe((result: ResolutionViewModel[]) => {
        expect(result.length).toBe(10);
      });
    });
  });
});
