import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable, of } from 'rxjs';
import { ResolutionViewModel } from 'src/app/modules/resolutions/models/resolution-view-model';
import { map, pluck, tap } from 'rxjs/operators';
import { PartnerModels, PartnerResolutionRepository } from '@pplsi-core/datalayer';

@Component({
  selector: 'app-user-resolutions',
  templateUrl: './user-resolutions.component.html',
  styleUrls: ['./user-resolutions.component.scss']
})
export class UserResolutionsComponent implements OnInit {

  pageSizeOptions: number[] = [10, 25, 50];
  resolutions$: Observable<ResolutionViewModel[]>;
  submitResultsPage = { pageIndex: 0, pageSize: this.pageSizeOptions[0], length: 0 };

  constructor(
    private route: ActivatedRoute,
    private partnerResolutionRepository: PartnerResolutionRepository
  ) { }

  ngOnInit() {
    const { resolutions, total_length } = this.route.snapshot.data['initialQueryResult'];
    if (resolutions) {
      this.resolutions$ = of(resolutions).pipe(
        map((results: PartnerModels.Resolution[]) => this.createResolutionViewModels(results))
      );
    }
    this.submitResultsPage.length = total_length;
  }

  private createResolutionViewModels(resolutions: PartnerModels.Resolution[]) {
    return resolutions.map(resolution => new ResolutionViewModel(resolution));
  }

  pageResults($event) {
    const query = {
      page : $event.pageIndex + 1,
      per_page : $event.pageSize
    };
    this.resolutions$ = this.partnerResolutionRepository.findAll(query, this.route.parent.snapshot.data['account'].id).pipe(
      pluck('resolutions'),
      tap(() => { this.submitResultsPage = $event; }),
      map((results: PartnerModels.Resolution[]) => this.createResolutionViewModels(results))
    );
  }

}
