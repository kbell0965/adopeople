import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed, getTestBed } from '@angular/core/testing';
import { MatTabsModule, MatMenuModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ActivatedRoute, Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { PartnerModels, PplsiCoreDatalayerModule, PartnerUserRepository, PeopleModels } from '@pplsi-core/datalayer';
import { AccountViewModel } from '../../../../models/account-view-model';
import { PartnerFactories, PeopleFactories } from '@pplsi-core/factories';
import { UserDetailsComponent } from './user-details.component';
import { HttpClientModule } from '@angular/common/http';
import { of } from 'rxjs';
import { PageLayoutModule } from 'ng-components.adonis';
import { FeatherModule } from 'angular-feather';
import { Clock, MapPin, MoreVertical, Shield } from 'angular-feather/icons';
import { UserViewModel } from '../../models/user-view-model';
import { MembershipViewModel } from 'src/app/models/membership-view-model';
import { PartnerUserViewModel } from 'src/app/models/partner-user-view-model';

const testAddress = PartnerFactories.AddressFactory.build({ locality: 'Ada', administrative_area: 'OK' });
const testPartnerUser = PartnerFactories.UserFactory.build({ first_name: 'John', last_name: 'Doe', addresses: [testAddress] });
const testSpouseUser = PartnerFactories.UserFactory.build({ first_name: 'Jane', last_name: 'Doe' });
const testMembership = PartnerFactories.MembershipFactory.build({ user: testPartnerUser });
const testSpouseMembership = PartnerFactories.MembershipFactory.build({ user: testSpouseUser, type: 'spouse' });
const accountFake: PartnerModels.Account = PartnerFactories.AccountFactory.build({ memberships: [testMembership, testSpouseMembership] });
const account: AccountViewModel = new AccountViewModel(accountFake);

let addresses: PeopleModels.Address[];
let groups: PeopleModels.Group[];
let testUser: UserViewModel;

describe('UserDetailsComponent.Unit', () => {
  let component: UserDetailsComponent;
  let fixture: ComponentFixture<UserDetailsComponent>;
  let injector: TestBed;
  let partnerUserRepository: PartnerUserRepository;
  let partnerUserFindSpy: jasmine.Spy;
  let router: Router;

  addresses = [new PeopleModels.Address(PeopleFactories.AddressFactory.build({ locality: 'Ada', administrative_area: 'OK' }))];
  groups = PeopleFactories.GroupFactory.buildList(3).map((group: Object) => new PeopleModels.Group(group));
  testUser = new UserViewModel(PeopleFactories.UserFactory.build({
    id: testPartnerUser.id,
    first_name: testPartnerUser.first_name,
    last_name: testPartnerUser.last_name,
    addresses,
    groups,
    national_plan: true
  }));

  const getRouteStub = (user: PartnerModels.User): object => {
    const url = [`/users/`, `${account.id}`, `/user/`, `${user.id}`];
    return {
      snapshot: {
        params: { account_id: account.id, user_id: user.id },
        data: { account, user: testUser }
      },
      url: of(url)
    };
  };

  const setupTestBed = (user: PartnerModels.User) => {
    TestBed.configureTestingModule({
      declarations: [ UserDetailsComponent ],
      imports: [
        FeatherModule.pick({ Clock, MapPin, MoreVertical, Shield }),
        MatTabsModule,
        MatMenuModule,
        BrowserAnimationsModule,
        RouterTestingModule,
        HttpClientModule,
        PplsiCoreDatalayerModule.forRoot(),
        PageLayoutModule
      ],
      providers: [
        { provide: ActivatedRoute, useValue: getRouteStub(user) },
        PartnerUserRepository
      ],
      schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
    }).compileComponents();

    fixture = TestBed.createComponent(UserDetailsComponent);
    component = fixture.componentInstance;
    injector = getTestBed();
    router = TestBed.get(Router);
    partnerUserRepository = injector.get(PartnerUserRepository);
    partnerUserFindSpy = spyOn(partnerUserRepository, 'find');
    partnerUserFindSpy.and.returnValue(of(new PartnerModels.User(testPartnerUser)));
    spyOn(router, 'navigate').and.returnValue(Promise.resolve(true));
    fixture.detectChanges();
  };

  describe('With primary user', () => {
    beforeEach(() => setupTestBed(testPartnerUser));

    describe('#ngOnInit', () => {
      it('calls setup', () => {
        spyOn(component, 'setup');
        component.ngOnInit();

        expect(component.setup).toHaveBeenCalled();
      });
    });

    describe('#setup', () => {
      beforeEach(() => component.setup());

      it('sets the user', () => {
        expect(component.user).toEqual(testUser);
      });

      it('sets the account', () => {
        expect(component.account).toEqual(account);
      });

      it('sets the user id from path', () => {
        expect(component.pathUserId).toEqual(testPartnerUser.id);
      });

      it('sets the primary membership', () => {
        expect(component.primaryMembership).toEqual(new MembershipViewModel(testMembership));
      });

      it('sets the spouse user', () => {
        expect(component.spouseUser).toEqual(account.memberships[1].user);
      });

      it('routes to the partner tab', () => {
        expect(router.navigate).not.toHaveBeenCalled();
      });
    });

    describe('#ngOnDestroy', () => {
      beforeEach(() => {
        spyOn((component as any)._destroyed, 'next');
        spyOn((component as any)._destroyed, 'complete');
        component.ngOnDestroy();
      });

      it('calls #next on `_destroyed`', () => expect((component as any)._destroyed.next).toHaveBeenCalled());
      it('calls #complete on `_destroyed`', () => expect((component as any)._destroyed.complete).toHaveBeenCalled());
    });
  });

  describe('With spouse user', () => {
    beforeEach(() => setupTestBed(testSpouseUser));

    describe('#ngOnInit', () => {
      it('sets the spouse user', () => {
        expect(component.spouseUser).toEqual(new PartnerUserViewModel(testSpouseUser));
      });

      it('routes to the partner tab', () => {
        expect(router.navigate).toHaveBeenCalledWith([`${component.profileBaseUrl}/partner`]);
      });
    });
  });
});
