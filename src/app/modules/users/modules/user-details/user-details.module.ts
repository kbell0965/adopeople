import { NgModule } from '@angular/core';
import { CommonModule, DatePipe, TitleCasePipe } from '@angular/common';
import { MatPaginatorModule, MatTabsModule, MatMenuModule } from '@angular/material';
import { FeatherModule } from 'angular-feather';
import { ChevronDown, ChevronUp, Clock, MapPin, MoreVertical, Shield, Minus } from 'angular-feather/icons';
import { ResolutionSearchResultModule } from 'src/app/shared/components/resolution-search-result/resolution-search-result.module';
import { SearchLayoutModule } from 'src/app/shared/components/search-layout/search-layout.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { PageLayoutModule } from 'ng-components.adonis';
import { LocalizationService } from '@pplsi-core/shared-modules/dist/src/services/localization.service';

import { UserDetailsRoutingModule } from './user-details-routing.module';
import { UserDetailsComponent } from './user-details.component';
import { UserActivePlansComponent } from './user-active-plans/user-active-plans.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { UserMembershipsComponent } from './user-memberships/user-memberships.component';
import { UserResolutionsComponent } from './user-resolutions/user-resolutions.component';
import {
  ResolutionsInitialQueryResolver
} from 'src/app/modules/resolutions/resolvers/resolutions-initial-query/resolutions-initial-query.resolver';

@NgModule({
  declarations: [
    UserDetailsComponent,
    UserActivePlansComponent,
    UserProfileComponent,
    UserMembershipsComponent,
    UserResolutionsComponent
  ],
  imports: [
    CommonModule,
    FeatherModule.pick({ ChevronDown, ChevronUp, Clock, MapPin, MoreVertical, Shield, Minus }),
    MatMenuModule,
    MatTabsModule,
    MatPaginatorModule,
    PageLayoutModule,
    ResolutionSearchResultModule,
    SearchLayoutModule,
    SharedModule,
    UserDetailsRoutingModule
  ],
  providers: [
    LocalizationService,
    DatePipe,
    TitleCasePipe,
    {
      provide: 'Window',
      useValue: window
    },
    {
      provide: 'localizations',
      useValue: [
        { locale: 'en-US', currencyCode: 'USD', dateFormat: 'MM/dd/yyyy' },
        { locale: 'en-GB', currencyCode: 'GBP', dateFormat: 'dd/MM/yyyy' },
        { locale: 'en-CA', currencyCode: 'CAD', dateFormat: 'yyyy-MM-dd' }
      ]
    },
    {
      provide: 'defaultLocale',
      useValue: 'en-US'
    },
    ResolutionsInitialQueryResolver
  ]
})
export class UserDetailsModule { }
