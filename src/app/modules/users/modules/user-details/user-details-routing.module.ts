import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserDetailsComponent } from './user-details.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { UserResolutionsComponent } from './user-resolutions/user-resolutions.component';
import {
  ResolutionsInitialQueryResolver
} from 'src/app/modules/resolutions/resolvers/resolutions-initial-query/resolutions-initial-query.resolver';

const routes: Routes = [{
  path: '',
  component: UserDetailsComponent,
  children: [
    { path: 'profile', component: UserProfileComponent },
    { path: 'partner', component: UserProfileComponent },
    { path: 'resolutions', component: UserResolutionsComponent, resolve: { initialQueryResult: ResolutionsInitialQueryResolver } },
    { path: '**', pathMatch: 'full', redirectTo: 'profile' }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserDetailsRoutingModule { }
