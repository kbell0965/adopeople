import { By } from '@angular/platform-browser';
import { CUSTOM_ELEMENTS_SCHEMA, DebugElement } from '@angular/core';
import { async, ComponentFixture, TestBed, getTestBed } from '@angular/core/testing';
import { MatTabsModule, MatMenuModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { AccountViewModel } from '../../../../models/account-view-model';
import { PartnerFactories, PeopleFactories } from '@pplsi-core/factories';
import { UserDetailsComponent } from './user-details.component';
import { PartnerModels, PartnerUserRepository, PeopleModels } from '@pplsi-core/datalayer';
import { HttpClientModule } from '@angular/common/http';
import { of } from 'rxjs';
import { PageLayoutModule, AvatarModule } from 'ng-components.adonis';
import { FeatherModule } from 'angular-feather';
import { Clock, MapPin, MoreVertical, Shield } from 'angular-feather/icons';
import { UserViewModel } from '../../models/user-view-model';

const testAddress = PartnerFactories.AddressFactory.build({ locality: 'Ada', administrative_area: 'OK' });
const testUser = PartnerFactories.UserFactory.build({ first_name: 'John', last_name: 'Doe', addresses: [testAddress] });
const testSpouseUser = PartnerFactories.UserFactory.build({ first_name: 'Jane', last_name: 'Doe' });
const testMembership = PartnerFactories.MembershipFactory.build({ user: testUser });
const accountFake: object = PartnerFactories.AccountFactory.build({ memberships: [testMembership] });
const account: AccountViewModel = new AccountViewModel(accountFake);
const fakeResolutions = Array.from(Array(10), (x, i) => i + 1).map(i => PartnerFactories.ResolutionFactory.build({}));
const testSubmitResults = { resolutions: fakeResolutions, total_length: 10 };
let addresses: PeopleModels.Address[];
let groups: PeopleModels.Group[];
let user: UserViewModel;

describe('UserDetailsComponent.Template', () => {
  let component: UserDetailsComponent;
  let fixture: ComponentFixture<UserDetailsComponent>;
  let injector: TestBed;
  let partnerUserRepository: PartnerUserRepository;
  let partnerUserFindSpy: jasmine.Spy;
  let route: ActivatedRoute;

  addresses = [new PeopleModels.Address(PeopleFactories.AddressFactory.build({ locality: 'Ada', administrative_area: 'OK' }))];
  groups = PeopleFactories.GroupFactory.buildList(3).map((group: Object) => new PeopleModels.Group(group));
  user = new UserViewModel(PeopleFactories.UserFactory.build({
    first_name: 'John',
    last_name: 'Doe',
    addresses,
    groups,
    national_plan: true
  }));

  const activatedRouteStub = {
    snapshot: {
      params: { 'account_id': account.id },
      data: {
        account: account,
        initialQueryResult: testSubmitResults,
        user
      }
    }
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserDetailsComponent ],
      imports: [
        FeatherModule.pick({ Clock, MapPin, MoreVertical, Shield }),
        MatTabsModule,
        MatMenuModule,
        BrowserAnimationsModule,
        RouterTestingModule,
        HttpClientModule,
        PageLayoutModule,
        AvatarModule
      ],
      providers: [
        { provide: ActivatedRoute, useValue: activatedRouteStub },
        PartnerUserRepository
      ],
      schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserDetailsComponent);
    component = fixture.componentInstance;
    injector = getTestBed();
    route = TestBed.get(ActivatedRoute);
    partnerUserRepository = injector.get(PartnerUserRepository);
    partnerUserFindSpy = spyOn(partnerUserRepository, 'find');
    partnerUserFindSpy.and.returnValue(of(new PartnerModels.User(testUser)));
    fixture.detectChanges();
  });

  describe('Page Header', () => {
    it('displays the primary user name on the breadcrumbs', () => {
      const el = fixture.debugElement.query(By.css('.page-breadcrumb-row span span'));

      expect(el.nativeElement.innerHTML).toEqual(`${testUser.first_name} ${testUser.last_name}`);
    });

    describe('Page Title Row', () => {
      it('displays the primary user name in the primary user name section', () => {
        const debugElement: DebugElement = fixture.debugElement.query(By.css('h2.page-title-username'));

        expect(debugElement.nativeElement.innerHTML).toEqual('John Doe');
      });

      it('displays the primary user avatar', () => {
        const el: DebugElement = fixture.debugElement.query(By.css('.page-title ls-avatar span'));

        expect(el.nativeElement.innerHTML).toContain('JD');
      });

      describe('National Plan Tag', () => {
        it('displays if true', () => {
          expect(fixture.debugElement.query(By.css('.page-title .national-plan'))).toBeTruthy();
        });

        it('does not display if false', () => {
          component.user = new UserViewModel(PeopleFactories.UserFactory.build({ national_plan: false }));
          fixture.detectChanges();
          expect(fixture.debugElement.query(By.css('.page-title .national-plan'))).not.toBeTruthy();
        });
      });

      describe('Group Member Tag', () => {
        it('displays if truthy', () => {
          expect(fixture.debugElement.query(By.css('.page-title .group-member'))).toBeTruthy();
        });

        it('does not display if falsey', () => {
          component.user = new UserViewModel(PeopleFactories.UserFactory.build({ groups: [] }));
          fixture.detectChanges();

          expect(fixture.debugElement.query(By.css('.page-title .group-member'))).not.toBeTruthy();
        });
      });
    });

    describe('Page Actions Menu', () => {
      it('has a Create Resolution button', () => {
        const pageMenuEl: DebugElement = fixture.debugElement.query(By.css('.page-layout-tr-controls .page-menu'));

        pageMenuEl.nativeElement.click();
        fixture.detectChanges();

        const createResolutionLinkEl: DebugElement = fixture.debugElement.query(By.css('.create-resolution-link'));

        expect(createResolutionLinkEl.nativeElement.getAttribute('href')).toContain(`/accounts/${ account.id }/resolutions`);
      });
    });

    describe('User Locale Details', () => {
      it('displays the user location if available', () => {
        expect(fixture.debugElement.query(By.css('.user-location div')).nativeElement.innerHTML).toContain('Ada, OK');
      });

      it('does not display if no address available', () => {
        component.user = new UserViewModel(PeopleFactories.UserFactory.build({ addresses: [] }));
        fixture.detectChanges();

        expect(fixture.debugElement.query(By.css('.user-location div'))).not.toBeTruthy();
      });
    });

    describe('User Group Name', () => {
      it('displays the first group name if any groups are present', () => {
        const el = fixture.debugElement.query(By.css('.user-group div'));
        expect(el.nativeElement.innerHTML).toContain(groups[0].name);
      });

      it('does not display the user group container if no groups present', () => {
        component.user = new UserViewModel(PeopleFactories.UserFactory.build({ groups: [] }));
        fixture.detectChanges();
        expect(fixture.debugElement.query(By.css('.user-group div'))).toBeNull();
      });
    });
  });

  describe('Page Tabs', () => {
    it('displays the Profile tab', done => {
      partnerUserRepository.find(testUser.id).subscribe(() => {
        const tabEls: DebugElement[] = fixture.debugElement.queryAll(By.css('.mat-tab-link'));

        expect(tabEls[0].nativeElement.innerHTML).toContain('Profile');
        done();
      });
    });

    it('displays the Resolutions tab', done => {
      partnerUserRepository.find(testUser.id).subscribe(() => {
        const tabEls: DebugElement[] = fixture.debugElement.queryAll(By.css('.mat-tab-link'));

        expect(tabEls[1].nativeElement.innerHTML).toContain('Resolutions');
        done();
      });
    });

    it('does not display the Partner/Spouse tab when there are no spouse to display', done => {
      partnerUserRepository.find(testSpouseUser.id).subscribe(() => {
        const tabEls: DebugElement[] = fixture.debugElement.queryAll(By.css('.mat-tab-link'));

        expect(tabEls[0].nativeElement.innerHTML).toContain('Profile');
        expect(tabEls[1].nativeElement.innerHTML).toContain('Resolutions');
        done();
      });
    });
  });
});
