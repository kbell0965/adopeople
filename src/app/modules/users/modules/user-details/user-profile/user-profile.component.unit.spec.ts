import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, getTestBed } from '@angular/core/testing';
import { ActivatedRoute, Router } from '@angular/router';
import { LocalizationService } from '@pplsi-core/shared-modules/dist/src/services/localization.service';
import { AccountViewModel } from 'src/app/models/account-view-model';
import { UserProfileComponent } from './user-profile.component';
import { PartnerFactories } from '@pplsi-core/factories';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';
import { PartnerModels, PartnerUserRepository, MembershipStatus, ContactMethodType } from '@pplsi-core/datalayer';
import { PartnerUserViewModel } from 'src/app/models/partner-user-view-model';
import { HttpClientModule } from '@angular/common/http';

const fakeSpouse: PartnerModels.User = PartnerFactories.UserFactory.build();
const fakeSpouseMembership: PartnerModels.Membership = PartnerFactories.MembershipFactory.build({
  user: fakeSpouse,
  status: MembershipStatus.Active,
  type: 'spouse'
});
const fakeUserCellPhone: PartnerModels.ContactMethod = PartnerFactories.ContactMethodFactory.build({
  type: ContactMethodType.Phone,
  value: '1234567890',
  label: 'cell'
});
const fakeUserHomePhone: PartnerModels.ContactMethod = PartnerFactories.ContactMethodFactory.build({
  type: ContactMethodType.Phone,
  value: '0987654321',
  label: 'home'
});
const fakeUserWorkPhone: PartnerModels.ContactMethod = PartnerFactories.ContactMethodFactory.build({
  type: ContactMethodType.Phone,
  value: '9998881234',
  label: 'work'
});
const fakeUser: PartnerModels.User = PartnerFactories.UserFactory.build({
  memberships: [fakeSpouseMembership],
  contact_methods: [fakeUserCellPhone, fakeUserHomePhone, fakeUserWorkPhone]
});
const fakeUserMembership: PartnerModels.Membership = PartnerFactories.MembershipFactory.build({
  user: fakeUser,
  status: MembershipStatus.Active
});
const fakeAccount: PartnerModels.Account = PartnerFactories.AccountFactory.build({
  memberships: [fakeUserMembership, fakeSpouseMembership]
});
const account: AccountViewModel = new AccountViewModel(fakeAccount);
const getRouteStub = (user: PartnerModels.User): object => {
  const url = [`/users/`, `${account.id}`, `/user/`, `${user.id}`];
  if (user && user.id) {
    if (user.id === fakeSpouse.id) {
      url.push('/partner');
    }
  }
  return {
    parent: { snapshot: { data: { account }, params: { user_id: user.id } } },
    url: of(url)
  };
};

describe('UserProfileComponent.Unit', () => {
  let component: UserProfileComponent;
  let fixture: ComponentFixture<UserProfileComponent>;
  let injector: TestBed;
  let route: ActivatedRoute;
  let router: Router;
  let partnerUserRepository: PartnerUserRepository;
  let partnerUserFindSpy: jasmine.Spy;

  const setupTestBed = (user: PartnerModels.User) => {
    TestBed.configureTestingModule({
      declarations: [ UserProfileComponent ],
      imports: [HttpClientModule, RouterTestingModule],
      providers: [
        LocalizationService,
        { provide: 'Window', useValue: window },
        { provide: 'localizations', useValue: [{ locale: 'en-US', currencyCode: 'USD', dateFormat: 'MM/dd/yyyy' }] },
        { provide: 'defaultLocale', useValue: 'en-US' },
        { provide: ActivatedRoute, useValue: getRouteStub(user) },
        PartnerUserRepository
      ],
      schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
    }).compileComponents();

    fixture = TestBed.createComponent(UserProfileComponent);
    component = fixture.componentInstance;
    injector = getTestBed();
    route = TestBed.get(ActivatedRoute);
    router = TestBed.get(Router);
    partnerUserRepository = injector.get(PartnerUserRepository);
    partnerUserFindSpy = spyOn(partnerUserRepository, 'find');
    partnerUserFindSpy.and.returnValue(of(new PartnerModels.User(user)));
    fixture.detectChanges();
  };

  describe('when Primary User', () => {
    beforeEach(() => setupTestBed(fakeUser));

    describe('#ngOnInit', () => {
      it('sets the locale', () => {
        expect(component.locale).toEqual('en-US');
      });

      it('sets the date format', () => {
        expect(component.dateFormat).not.toBeNull();
      });

      it('sets the account to the component from activated route', () => {
        expect(component.account).toEqual(account);
      });

      it('sets the path user id to the component from activated route', () => {
        expect(component.pathUserId).toEqual(fakeUser.id);
      });

      it('sets isPartner based on the url to be falsy', done => {
        route.url.subscribe(() => {
          expect(component.isPartner).toBeFalsy();
          done();
        });
      });

      it('sets user based on the url to primary user', done => {
        route.url.subscribe(() => {
          expect(component.user).toEqual(new PartnerUserViewModel(fakeUser));
          done();
        });
      });

      it('sets the user primary phone', done => {
        route.url.subscribe(() => {
          expect(component.userPrimaryPhone)
            .toEqual(component.user.formattedPhoneNumber(component.locale.slice(-2), 'primary'));
          done();
        });
      });

      it('sets the user secondary phone', done => {
        route.url.subscribe(() => {
          expect(component.userSecondaryPhone)
            .toEqual(component.user.formattedPhoneNumber(component.locale.slice(-2), 'secondary'));
          done();
        });
      });

      it('sets the user cell phone', done => {
        route.url.subscribe(() => {
          expect(component.userCellPhone)
            .toEqual(component.user.formattedPhoneNumber(component.locale.slice(-2), 'cell'));
          done();
        });
      });

      it('sets the user home phone', done => {
        route.url.subscribe(() => {
          expect(component.userHomePhone)
            .toEqual(component.user.formattedPhoneNumber(component.locale.slice(-2), 'home'));
          done();
        });
      });

      it('sets the user work phone', done => {
        route.url.subscribe(() => {
          expect(component.userWorkPhone)
            .toEqual(component.user.formattedPhoneNumber(component.locale.slice(-2), 'work'));
          done();
        });
      });

      it('partner user repository is called with primary user id', () => {
        expect(partnerUserFindSpy).toHaveBeenCalledWith(fakeUser.id);
      });

      it('sets primaryMemberships', done => {
        partnerUserRepository.find(fakeUser.id).subscribe(() => {
          expect(component.primaryMemberships).toEqual((fakeUser as any).memberships);
          done();
        });
      });
    });

    describe('#ngOnDestroy', () => {
      beforeEach(() => {
        spyOn((component as any)._destroyed, 'next');
        spyOn((component as any)._destroyed, 'complete');
        component.ngOnDestroy();
      });

      it('calls #next on `_destroyed`', () => expect((component as any)._destroyed.next).toHaveBeenCalled());
      it('calls #complete on `_destroyed`', () => expect((component as any)._destroyed.complete).toHaveBeenCalled());
    });

    describe('#setPrimaryMemberships', () => {
      it('sets the primaryMemberships with provided memberships', () => {
        const user = new PartnerUserViewModel(fakeUser);
        component.setPrimaryMemberships(user.memberships);
        expect(component.primaryMemberships[0]).toEqual(user.memberships[0]);
      });
    });
  });

  describe('when Spouse/Partner User', () => {
    beforeEach(() => setupTestBed(fakeSpouse));

    describe('#ngOnInit', () => {
      it('sets the path user id to the component from activated route', () => {
        expect(component.pathUserId).toEqual(fakeSpouse.id);
      });

      it('sets isPartner based on the url to be truthy', done => {
        route.url.subscribe(() => {
          expect(component.isPartner).toBeTruthy();
          done();
        });
      });

      it('sets user based on the url to primary user', done => {
        route.url.subscribe(() => {
          expect(component.user).toEqual(new PartnerUserViewModel(fakeSpouse));
          done();
        });
      });
    });
  });
});
