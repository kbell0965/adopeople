import { CUSTOM_ELEMENTS_SCHEMA, DebugElement, Injector } from '@angular/core';
import { ComponentFixture, getTestBed, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { LocalizationService } from '@pplsi-core/shared-modules/dist/src/services/localization.service';
import { PartnerUserViewModel } from 'src/app/models/partner-user-view-model';
import { AccountViewModel } from 'src/app/models/account-view-model';
import { UserProfileComponent } from './user-profile.component';
import { PartnerFactories } from '@pplsi-core/factories';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http';
import { PartnerUserRepository, PartnerModels } from '@pplsi-core/datalayer';
import { of } from 'rxjs';
import { MembershipViewModel } from 'src/app/models/membership-view-model';

describe('UserProfileComponent.Template', () => {
  let component: UserProfileComponent;
  let fixture: ComponentFixture<UserProfileComponent>;
  let injector: Injector;
  let datePipe: DatePipe;
  let partnerUserRepository: PartnerUserRepository;
  let partnerUserFindSpy: jasmine.Spy;

  const getRouteStub = (user: PartnerModels.User, account: AccountViewModel): object => {
    const url = [`/users/`, `${account.id}`, `/user/`, `${user.id}`];
    return {
      parent: { snapshot: { data: { account }, params: { user_id: user.id } } },
      url: of(url)
    };
  };

  const setupTestBed = (user: PartnerModels.User, account: AccountViewModel) => {
    TestBed.configureTestingModule({
      declarations: [ UserProfileComponent ],
      imports: [
        HttpClientModule,
        RouterTestingModule
      ],
      providers: [
        LocalizationService,
        {
          provide: 'Window',
            useValue: window
        },
        {
          provide: 'localizations',
            useValue: [
          { locale: 'en-US', currencyCode: 'USD', dateFormat: 'MM/dd/yyyy' }
        ]
        },
        {
          provide: 'defaultLocale',
            useValue: 'en-US'
        },
        { provide: ActivatedRoute, useValue: getRouteStub(user, account) },
        DatePipe,
        PartnerUserRepository
      ],
      schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
    }).compileComponents();

    fixture = TestBed.createComponent(UserProfileComponent);
    injector = getTestBed();
    datePipe = injector.get(DatePipe);
    component = fixture.componentInstance;
    partnerUserRepository = injector.get(PartnerUserRepository);
    partnerUserFindSpy = spyOn(partnerUserRepository, 'find');
    partnerUserFindSpy.and.returnValue(of(new PartnerModels.User(user)));
    fixture.detectChanges();
  };

  describe('with all user data', () => {
    beforeEach(() => {
      const contact_methods: Array<object> = [
        PartnerFactories.ContactMethodFactory.build({ label: 'cell', value: '1234567890' }),
        PartnerFactories.ContactMethodFactory.build({ label: 'home', value: '1234567890' })
      ];
      const testUser: PartnerUserViewModel = new PartnerUserViewModel(PartnerFactories.UserFactory.build({ contact_methods }));
      const testAccount: AccountViewModel = new AccountViewModel(PartnerFactories.AccountFactory.build({ user: testUser }));
      setupTestBed(testUser, testAccount);
    });

    it('displays the date of birth', () => {
      const element: DebugElement = fixture.debugElement.query(By.css('.date-of-birth p'));

      expect(element.nativeElement.innerHTML).toEqual(datePipe.transform(component.user.date_of_birth, component.dateFormat));
    });

    it('displays the national id', () => {
      const element: DebugElement = fixture.debugElement.query(By.css('.national-id p'));

      expect(element.nativeElement.innerHTML).toEqual(component.user.nationalId(component.locale));
    });

    describe('displays address parts', () => {
      it('address 1', () => {
        const element: DebugElement = fixture.debugElement.query(By.css('.address'));

        expect(element.nativeElement.innerHTML).toContain(component.user.firstAddress().address_1);
      });

      it('displays address 2', () => {
        const element: DebugElement = fixture.debugElement.query(By.css('.address'));

        expect(element.nativeElement.innerHTML).toContain(component.user.firstAddress().address_2);
      });

      it('displays the city', () => {
        const element: DebugElement = fixture.debugElement.query(By.css('.address'));

        expect(element.nativeElement.innerHTML).toContain(component.user.firstAddress().locality);
      });

      it('displays the region', () => {
        const element: DebugElement = fixture.debugElement.query(By.css('.address'));

        expect(element.nativeElement.innerHTML).toContain(component.user.firstAddress().administrative_area);
      });

      it('displays the country', () => {
        const element: DebugElement = fixture.debugElement.query(By.css('.address'));

        expect(element.nativeElement.innerHTML).toContain(component.user.firstAddress().country);
      });

      it('displays the postal code', () => {
        const element: DebugElement = fixture.debugElement.query(By.css('.address'));

        expect(element.nativeElement.innerHTML).toContain(component.user.firstAddress().postal_code);
      });
    });

    it('displays the email', () => {
      const element: DebugElement = fixture.debugElement.query(By.css('.email p a'));

      expect(element.nativeElement.innerHTML).toEqual(component.user.email);
    });

    it('displays the primary phone number', () => {
      component.userPrimaryPhone = '(123) 456-7890';
      fixture.detectChanges();
      const element: DebugElement = fixture.debugElement.query(By.css('.primary-phone p'));

      expect(element.nativeElement.innerHTML).toEqual('(123) 456-7890');
    });

    it('displays the secondary phone number', () => {
      component.userSecondaryPhone = '(123) 456-7890';
      fixture.detectChanges();
      const element: DebugElement = fixture.debugElement.query(By.css('.secondary-phone p'));

      expect(element.nativeElement.innerHTML).toEqual('(123) 456-7890');
    });

    it('displays the cell phone number', () => {
      component.userCellPhone = '(123) 456-7890';
      fixture.detectChanges();
      const element: DebugElement = fixture.debugElement.query(By.css('.cell-phone p'));

      expect(element.nativeElement.innerHTML).toEqual('(123) 456-7890');
    });

    it('displays the home phone number', () => {
      component.userHomePhone = '(123) 456-7890';
      fixture.detectChanges();
      const element: DebugElement = fixture.debugElement.query(By.css('.home-phone p'));

      expect(element.nativeElement.innerHTML).toEqual('(123) 456-7890');
    });

    it('displays the work phone number', () => {
      component.userWorkPhone = '(123) 456-7890';
      fixture.detectChanges();
      const element: DebugElement = fixture.debugElement.query(By.css('.work-phone p'));

      expect(element.nativeElement.innerHTML).toEqual('(123) 456-7890');
    });

    describe('impersonation button', () => {
      it('has the correct href value', () => {
        const element: DebugElement = fixture.debugElement.query(By.css('.impersonate'));
        const expectation = `/impersonate?user_id=${component.user.id}&account_id=${component.account.id}`;

        expect(element.nativeElement.getAttribute('href')).toContain(expectation);
      });

      it('has the correct button label', () => {
        const element: DebugElement = fixture.debugElement.query(By.css('.impersonate button'));

        expect(element.nativeElement.innerHTML).toEqual('Accounts');
      });
    });
  });

  describe('with user missing data', () => {
    beforeEach(() => {
      const testUser: PartnerUserViewModel = new PartnerUserViewModel(PartnerFactories.UserFactory.build({
        addresses: [],
        contact_methods: [],
        date_of_birth: null,
        email: null,
        phone_number: null,
        truncated_national_id: null
      }));
      const testMembership: MembershipViewModel = new MembershipViewModel(PartnerFactories.MembershipFactory.build({
        user: testUser
      }));
      const testAccount: AccountViewModel = new AccountViewModel(PartnerFactories.AccountFactory.build({
        memberships: [testMembership]
      }));

      setupTestBed(testUser, testAccount);
    });

    it('displays two minus icons when no date of birth is available', () => {
      const els: DebugElement[] = fixture.debugElement.queryAll(By.css('.date-of-birth i-feather'));

      expect(els.length).toBe(2);
    });

    it('displays two minus icons when no national id is available', () => {
      const els: DebugElement[] = fixture.debugElement.queryAll(By.css('.national-id i-feather'));

      expect(els.length).toBe(2);
    });

    it('displays two minus icons when no address is available', () => {
      const els: DebugElement[] = fixture.debugElement.queryAll(By.css('.address i-feather'));

      expect(els.length).toBe(2);
    });

    it('displays two minus icons when no email is available', () => {
      const els: DebugElement[] = fixture.debugElement.queryAll(By.css('.email i-feather'));

      expect(els.length).toBe(2);
    });

    it('displays two minus icons when no primary phone is available', () => {
      const els: DebugElement[] = fixture.debugElement.queryAll(By.css('.cell-phone i-feather'));

      expect(els.length).toBe(2);
    });

    it('displays two minus icons when no secondary phone is available', () => {
      const els: DebugElement[] = fixture.debugElement.queryAll(By.css('.home-phone i-feather'));

      expect(els.length).toBe(2);
    });
  });
});
