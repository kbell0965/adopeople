import { ActivatedRoute, UrlSegment } from '@angular/router';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { PartnerUserViewModel } from 'src/app/models/partner-user-view-model';
import { AccountViewModel, Type } from 'src/app/models/account-view-model';
import { LocalizationService } from '@pplsi-core/shared-modules/dist/src/services/localization.service';
import { PartnerModels, PartnerUserRepository } from '@pplsi-core/datalayer';
import { takeUntil, map } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { MembershipViewModel } from 'src/app/models/membership-view-model';

@Component({
  selector: 'user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss']
})
export class UserProfileComponent implements OnInit, OnDestroy {
  private _destroyed = new Subject<void>();

  account: AccountViewModel;
  dateFormat: string;
  isPartner: boolean;
  locale: string;
  pathUserId: string;
  primaryMemberships: Array<PartnerModels.Membership>;
  user: PartnerUserViewModel;
  userPrimaryPhone: string;
  userSecondaryPhone: string;
  userCellPhone: string;
  userHomePhone: string;
  userWorkPhone: string;

  constructor(private route: ActivatedRoute,
    private localizationService: LocalizationService,
    private partnerUserRepository: PartnerUserRepository) { }

  ngOnInit() {
    const { data, params } = this.route.parent.snapshot;
    this.locale = this.localizationService.getLocale();
    this.dateFormat = this.localizationService.getDateFormat();
    this.account = data['account'];
    this.pathUserId = params['user_id'];
    this.route.url.pipe(takeUntil(this._destroyed)).subscribe((segments: UrlSegment[]) => {
      this.isPartner = (segments.join('').indexOf('partner')) > -1;
      this.user = this.account.getUserByType(!this.isPartner ? Type.PRIMARY : Type.SPOUSE);
      this.userPrimaryPhone = this.user.formattedPhoneNumber(this.locale.slice(-2), 'primary');
      this.userSecondaryPhone = this.user.formattedPhoneNumber(this.locale.slice(-2), 'secondary');
      this.userCellPhone = this.user.formattedPhoneNumber(this.locale.slice(-2), 'cell');
      this.userHomePhone = this.user.formattedPhoneNumber(this.locale.slice(-2), 'home');
      this.userWorkPhone = this.user.formattedPhoneNumber(this.locale.slice(-2), 'work');
      this.partnerUserRepository.find(this.user.id)
        .pipe(takeUntil(this._destroyed))
        .pipe(map(user => new PartnerUserViewModel(user)))
        .subscribe((user) => this.setPrimaryMemberships(user.memberships));
    });
  }

  ngOnDestroy() {
    this._destroyed.next();
    this._destroyed.complete();
  }

  setPrimaryMemberships(memberships: MembershipViewModel[]): void {
    this.primaryMemberships = memberships;
  }
}
