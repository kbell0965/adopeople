import { PeopleModels, ContactMethodType } from '@pplsi-core/datalayer/dist/@pplsi-core/datalayer';
import { PhonePipe } from '@pplsi-core/shared-modules';

export class UserViewModel extends PeopleModels.User {
  private phonePipe: PhonePipe = new PhonePipe();

  hasAddress: boolean;
  firstAddress: PeopleModels.Address;

  constructor(params: PeopleModels.User) {
    super(params);

    this.hasAddress = this.addresses && this.addresses.length > 0;
    this.firstAddress = this.hasAddress ? this.addresses[0] : null;
  }

  get hasGroup(): boolean {
    return this.groups && this.groups.length > 0;
  }

  get firstGroupName(): string | null {
    return (this.hasGroup && this.groups[0].name) ? this.groups[0].name : null;
  }

  get csvProductNames(): string | null {
    const products: string[] = [];

    this.accounts.forEach((account: PeopleModels.Account) => {
      products.push(account.products.map((product: PeopleModels.Product) => product.name).join(', '));
    });

    return products.length > 0 ? products.join(', ') : null;
  }

  fullName(): string | null {
    return this.first_name || this.last_name
      ? `${this.first_name ? this.first_name : ''}${this.first_name && this.last_name ? ' ' : ''}${this.last_name ? this.last_name : ''}`
      : null;
  }

  formattedLocation(): string | null {
    if (this.hasAddress) {
      if (!!this.firstAddress.locality && !!this.firstAddress.administrative_area) {
        return `${this.firstAddress.locality}, ${this.firstAddress.administrative_area}`;
      }
      return this.firstAddress.locality || this.firstAddress.administrative_area;
    }
    return null;
  }

  formattedPhoneNumber(region = 'GB', label: string): string | null {
    region = region.length === 5 ? region.substr(3) : region;
    const phone_number = this.phoneNumberWithLabel(label);
    return phone_number ? this.phonePipe.transform(phone_number, region) : null;
  }

  linkTitle(): string {
    return [this.fullName(), this.email, this.formattedLocation()]
      .map((part, i) => (((i > 0) && part) ? `\n` : '') + (part ? `${part}` : '')).join('');
  }

  nationalId(region: string = 'us'): string | null {
    const last4 = this.truncated_national_id ? (this.truncated_national_id.length > 4 ?
      this.truncated_national_id.substr(this.truncated_national_id.length - 4) :
      this.truncated_national_id) : null;

    switch (region) {
      case 'en-GB': {
        return last4 ? `•• •••${last4.substring(0, 3)} ${last4[3]}` : null;
      }
      case 'en-CA': {
        return last4 ? `••• ••${last4[0]} ${last4.substring(1)}` : null;
      }
      default: {
        return last4 ? `••• •• ${last4}` : null;
      }
    }
  }

  phoneNumberWithLabel(label: string): string | null {
    const matching_methods = this.contact_methods.filter(contact_method =>
      contact_method.type === ContactMethodType.Phone && contact_method.label === label);
    return matching_methods.length > 0 ? matching_methods[0].value : null;
  }
}
