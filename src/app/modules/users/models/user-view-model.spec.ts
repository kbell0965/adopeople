import { TestBed } from '@angular/core/testing';
import { PeopleModels } from '@pplsi-core/datalayer/dist/@pplsi-core/datalayer';
import { UserViewModel } from './user-view-model';
import { PeopleFactories } from '@pplsi-core/factories/dist/@pplsi-core/factories';
import { PhonePipe } from '@pplsi-core/shared-modules';

const fakeContactMethods: PeopleModels.ContactMethod[] = [
  PeopleFactories.ContactMethodFactory.build({ label: 'cell', value: '8004441111' }),
  PeopleFactories.ContactMethodFactory.build({ label: 'home', value: '8004441111' }),
  PeopleFactories.ContactMethodFactory.build({ label: 'work', value: '8004441111' })
];
const fakeProducts: PeopleModels.Product[] = [
  PeopleFactories.ProductFactory.build({ name: 'ProductA' }),
  PeopleFactories.ProductFactory.build({ name: 'ProductB' })
];
const fakeAccount: PeopleModels.Account = new PeopleModels.Account({ products: fakeProducts });

describe('UserViewModel', () => {
  let user: UserViewModel;
  let noDataUser: UserViewModel;
  let addresses: PeopleModels.Address[];
  let groups: PeopleModels.Group[];

  const getTitleParts = (m: UserViewModel): string[] => [m.fullName(), m.email, m.formattedLocation()];

  beforeEach(() => {
    addresses = PeopleFactories.AddressFactory.buildList(3).map((address: Object) => new PeopleModels.Address(address));
    groups = PeopleFactories.GroupFactory.buildList(3).map((group: Object) => new PeopleModels.Group(group));
    user = new UserViewModel(PeopleFactories.UserFactory.build({
      addresses, groups, contact_methods: fakeContactMethods, accounts: [fakeAccount]
    }));
    noDataUser = new UserViewModel(PeopleFactories.UserFactory.build({ accounts: [] }));

    TestBed.configureTestingModule({ providers: [PhonePipe] });
  });

  describe('#csvProductNames', () => {
    it('returns the expected product names as comma separated values', () => {
      expect(user.csvProductNames).toEqual('ProductA, ProductB');
    });

    it('returns null when there are no product names', () => {
      expect(noDataUser.csvProductNames).toEqual(null);
    });
  });

  describe('.hasAddress', () => {
    it('is true when an address is present', () => {
      expect(user.hasAddress).toBe(true);
    });

    it('is false when there are no addresses', () => {
      user = new UserViewModel(PeopleFactories.UserFactory.build({ addresses: [] }));
      expect(user.hasAddress).toBe(false);
    });
  });

  describe('.firstAddress', () => {
    it('returns the first address if addresses are available', () => {
      expect(user.firstAddress.id).toEqual(addresses[0].id);
    });

    it('returns null if no addresses are available', () => {
      user = new UserViewModel(PeopleFactories.UserFactory.build({ addresses: [] }));

      expect(user.firstAddress).toBeNull();
    });

    it('returns null when addresses is null', () => {
      user = new UserViewModel(PeopleFactories.UserFactory.build({ addresses: null }));

      expect(user.firstAddress).toBeNull();
    });
  });

  describe('#hasGroup', () => {
    it('is true when a group is present', () => {
      expect(user.hasGroup).toBe(true);
    });

    it('is false when there are no groups', () => {
      user = new UserViewModel(PeopleFactories.UserFactory.build({ groups: [] }));
      expect(user.hasGroup).toBe(false);
    });
  });

  describe('#firstGroupName', () => {
    it('returns the first group if groups are available', () => {
      expect(user.firstGroupName).toEqual(groups[0].name);
    });

    it('returns null if no groups are available', () => {
      user = new UserViewModel(PeopleFactories.UserFactory.build({ groups: [] }));

      expect(user.firstGroupName).toBeNull();
    });

    it('returns null when groups is null', () => {
      user = new UserViewModel(PeopleFactories.UserFactory.build({ groups: null }));

      expect(user.firstGroupName).toBeNull();
    });
  });

  describe('#fullName', () => {
    describe('when first name and last name exist', () => {
      it('combines first and last name into a combined name', () => {
        user.first_name = 'Foo';
        user.last_name = 'Bar';

        expect(user.fullName()).toEqual(`${user.first_name} ${user.last_name}`);
      });
    });

    describe('when first name exists but last name is null', () => {
      it('returns the first name only', () => {
        user.first_name = 'Foo';
        user.last_name = null;

        expect(user.fullName()).toEqual(`${user.first_name}`);
      });
    });

    describe('when first name is null but last name exists', () => {
      it('returns the last name only', () => {
        user.first_name = null;
        user.last_name = 'Bar';

        expect(user.fullName()).toEqual(`${user.last_name}`);
      });
    });

    describe('when both first and last name are null', () => {
      it('returns null', () => {
        user.first_name = null;
        user.last_name = null;

        expect(user.fullName()).toBeNull();
      });
    });
  });

  describe('#formattedLocation', () => {
    describe('when address exists with locality and administrative area', () => {
      it('should return ${locality}, ${administrative_area}', () => {
        const address = user.addresses[0];
        expect(user.formattedLocation())
          .toEqual(`${address.locality}, ${address.administrative_area}`);
      });
    });

    describe('when address exists with null locality', () => {
      beforeEach(() => {
        addresses = PeopleFactories.AddressFactory.buildList(1, { locality: null })
          .map((address) => new PeopleModels.Address(address));
        user = new UserViewModel(PeopleFactories.UserFactory.build({ addresses }));
      });

      it('should return only the administrative area', () => {
        expect(user.formattedLocation()).toEqual(user.addresses[0].administrative_area);
      });
    });

    describe('when address exists with null administrative_area', () => {
      beforeEach(() => {
        addresses = PeopleFactories.AddressFactory.buildList(1, { administrative_area: null })
          .map((address) => new PeopleModels.Address(address));
          user = new UserViewModel(PeopleFactories.UserFactory.build({ addresses }));
      });

      it('should return only the locality', () => {
        expect(user.formattedLocation()).toEqual(user.addresses[0].locality);
      });
    });

    describe('when address does not exist', () => {
      beforeEach(() => user = new UserViewModel(PeopleFactories.UserFactory.build({ addresses: null })));

      it('should return null', () => {
        user = new UserViewModel(PeopleFactories.UserFactory.build({ addresses: null }));
        expect(user.formattedLocation()).toBeNull();
      });
    });
  });

  describe('#formattedPhoneNumber', () => {
    beforeEach(() => {
      spyOn(PhonePipe.prototype, 'transform').and.returnValue('1234');
    });

    it('calls the PhonePipe transform method with GB by default', () => {
      user.formattedPhoneNumber('en-GB', 'cell');

      expect(PhonePipe.prototype.transform).toHaveBeenCalledWith(fakeContactMethods[0].value, 'GB');
    });

    it('calls the PhonePipe transform method with NA for region NA', () => {
      user.formattedPhoneNumber('NA', 'cell');

      expect(PhonePipe.prototype.transform).toHaveBeenCalledWith(fakeContactMethods[0].value, 'NA');
    });

    it('does not call the PhonePipe transform method when user phone number does not exist', () => {
      user.contact_methods = [];
      user.formattedPhoneNumber('US', 'cell');

      expect(PhonePipe.prototype.transform).not.toHaveBeenCalled();
    });

    it('returns the string returned by the phone pipe when the user has a phone number', () => {
      expect(user.formattedPhoneNumber('US', 'cell')).toEqual('1234');
    });

    it('returns null when the user does not have a phone number', () => {
      user.contact_methods = [];

      expect(user.formattedPhoneNumber('US', 'cell')).toBeNull();
    });
  });

  describe('#linkTitle', () => {
    it('returns a title that displays all good values present on a user search result', () => {
      getTitleParts(user).forEach((part) => {
        if (part && part !== '') {
          expect(user.linkTitle()).toContain(part);
        }
      });
    });

    it('returns a title that does not have a new line at the beginning', () => {
      const linkTitle = user.linkTitle();
      expect(linkTitle).toContain('\n');
      expect(linkTitle.slice(0, 1)).not.toContain('\n');
    });

    it('returns a title that has a new line only for every good part', () => {
      user.email = '';
      user.formattedLocation = () => null;
      expect(user.linkTitle().split('\n').length).toBe(getTitleParts(user).length - 2);
    });

    it('returns an empty string when none of the values are good', () => {
      user.fullName = () => null;
      user.email = '';
      user.formattedLocation = () => undefined;
      expect(user.linkTitle()).toBe('');
    });
  });

  describe('#nationalId', () => {
    beforeEach(() => {
      user.truncated_national_id = '1234';
    });

    it('formats the national id for UK', () => {
      expect(user.nationalId('en-GB')).toBe('•• •••123 4');
    });

    it('formats the national id for CA', () => {
      expect(user.nationalId('en-CA')).toBe('••• ••1 234');
    });

    it('uses the US format as a default if region is not defined', () => {
      expect(user.nationalId('foo')).toBe('••• •• 1234');
    });

    it('uses the US format as a default if region is not given', () => {
      expect(user.nationalId()).toBe('••• •• 1234');
    });

    it('returns null if national id is null', () => {
      user.truncated_national_id = null;

      expect(user.nationalId('en-GB')).toBeNull();
    });
  });

  describe('#phoneNumberWithLabel', () => {
    it('returns the expected phone number', () => {
      const expected: string = user.phoneNumberWithLabel('cell');
      expect(expected).toEqual(user.contact_methods[0].value);
    });

    it('returns null if no match found', () => {
      const expected: string = user.phoneNumberWithLabel('whatever');
      expect(expected).toEqual(null);
    });
  });
});
