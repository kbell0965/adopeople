import { PartnerModels } from '@pplsi-core/datalayer';
import { PhonePipe } from '@pplsi-core/shared-modules';
import { PartnerUserViewModel } from '../../../models/partner-user-view-model';

export class ResolutionViewModel extends PartnerModels.Resolution {
  constructor(params: Object) {
    super(params);

    if (!!params['user']) {
      this.user = new PartnerUserViewModel(params['user']);
    }
  }

  get displayStatus(): string {
    return ResolutionViewModel.STATUSES.get(this.status);
  }

  get displaySource(): string {
    return this.SOURCES.get(this.source);
  }

  static readonly STATUSES: Map<string, string> = new Map([
    [ 'unassigned', 'New' ],
    [ 'assigned', 'Open'],
    [ 'pending member', 'Pending Member' ],
    [ 'pending provider', 'Pending Provider' ],
    [ 'closed with merit', 'Closed with Merit' ],
    [ 'closed without merit', 'Closed without Merit' ]
  ]);
  private phonePipe: PhonePipe = new PhonePipe();

  readonly SOURCES: Map<string, string> = new Map([
    [ 'letter', 'Email/Letter' ],
    [ 'call', 'Call'],
    [ 'survey', 'Survey' ],
    [ 'associate', 'Associate' ],
    [ 'clarification', 'Clarification']
  ]);

  user: PartnerUserViewModel;

  fullUserName(first: string, last: string): string {
    if (first && last) { return `${first} ${last}`; }
    if (first) { return first; }
    if (last) { return last; }
    return '';
  }

  userCityState(city: string, state: string): string {
    if (city && state) { return `${city}, ${state}`; }
    if (city) { return city; }
    if (state) { return state; }
    return '';
  }

  formattedPreferredContactMethod(region = 'US'): string {
    let preferredContactMethod: string;

    try {
      preferredContactMethod = this.phonePipe.transform(this.preferred_contact_method.value, region);
    } catch (e) {
      preferredContactMethod = this.preferred_contact_method.value;
    }

    return preferredContactMethod;
  }
}
