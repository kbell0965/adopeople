import { PhonePipe } from '@pplsi-core/shared-modules';
import { ResolutionViewModel } from './resolution-view-model';
import { PartnerFactories } from '@pplsi-core/factories';

const getResolution = (cfg) => new ResolutionViewModel(PartnerFactories.ResolutionFactory.build(cfg));

describe('ResolutionViewModel', () => {
  let resolution: ResolutionViewModel;

  beforeEach(() => {
    resolution = getResolution({});
  });

  describe('#fullUserName', () => {
    it('returns first and last name separated by a space', () => {
      const expectedOutput = resolution.fullUserName(resolution.user.first_name, resolution.user.last_name);
      const expectedName = `${resolution.user.first_name} ${resolution.user.last_name}`;
      expect(expectedOutput).toBe(expectedName);
    });

    it('returns just the first name and no space if last name is undefined', () => {
      const { first_name } = resolution.user;
      expect(resolution.fullUserName(first_name, undefined)).toBe(`${first_name}`);
    });

    it('returns just the last name and no space if first name is undefined', () => {
      const { last_name } = resolution.user;
      expect(resolution.fullUserName(undefined, last_name)).toBe(`${last_name}`);
    });

    it('returns empty string if first name and last name are undefined', () => {
      expect(resolution.fullUserName(undefined, undefined)).toBe('');
    });
  });

  describe('#userCityState', () => {
    it('returns the city and the state separated by a comma', () => {
      const { locality, administrative_area } = resolution.user.addresses[0];
      expect(resolution.userCityState(locality, administrative_area))
        .toBe(`${locality}, ${administrative_area}`);
    });

    it('returns just the city and no comma if state is undefined', () => {
      const { locality } = resolution.user.addresses[0];
      expect(resolution.userCityState(locality, undefined)).toBe(`${locality}`);
    });

    it('returns just the state and no comma if city is undefined', () => {
      const { administrative_area } = resolution.user.addresses[0];
      expect(resolution.userCityState(undefined, administrative_area)).toBe(`${administrative_area}`);
    });

    it('returns empty string if city and state are undefined', () => {
      expect(resolution.userCityState(undefined, undefined)).toBe('');
    });
  });

  describe('#displayStatus', () => {
    it('returns the STATUSES map value using the key', () => {
      expect(getResolution({ status: 'unassigned' }).displayStatus).toBe('New');
      expect(getResolution({ status: 'assigned' }).displayStatus).toBe('Open');
      expect(getResolution({ status: 'pending member' }).displayStatus).toBe('Pending Member');
      expect(getResolution({ status: 'pending provider' }).displayStatus).toBe('Pending Provider');
      expect(getResolution({ status: 'closed with merit' }).displayStatus).toBe('Closed with Merit');
      expect(getResolution({ status: 'closed without merit' }).displayStatus).toBe('Closed without Merit');
    });
  });

  describe('#displaySource', () => {
    it('returns the SOURCES map value using the key', () => {
      expect(getResolution({ source: 'letter' }).displaySource).toBe('Email/Letter');
      expect(getResolution({ source: 'call' }).displaySource).toBe('Call');
      expect(getResolution({ source: 'survey' }).displaySource).toBe('Survey');
      expect(getResolution({ source: 'associate' }).displaySource).toBe('Associate');
      expect(getResolution({ source: 'clarification' }).displaySource).toBe('Clarification');
    });
  });

  describe('#formattedPreferredContactMethod', () => {
    describe('when preferred contact method is a phone number', () => {
      beforeEach(() => {
        resolution.preferred_contact_method.value = '1231231234';

        spyOn(PhonePipe.prototype, 'transform').and.returnValue('1234');
      });

      it('calls the PhonePipe transform method with US by default', () => {
        resolution.formattedPreferredContactMethod();

        expect(PhonePipe.prototype.transform).toHaveBeenCalledWith('1231231234', 'US');
      });

      it('calls the PhonePipe transform method with GB for region GB', () => {
        resolution.formattedPreferredContactMethod('GB');

        expect(PhonePipe.prototype.transform).toHaveBeenCalledWith('1231231234', 'GB');
      });

      it('returns the string returned by the phone pipe', () => {
        expect(resolution.formattedPreferredContactMethod()).toEqual('1234');
      });
    });

    describe('when preferred contact method is not a phone number', () => {
      it('returns the preferred contact method string', () => {
        resolution.preferred_contact_method.value = 'not a phone number';

        expect(resolution.formattedPreferredContactMethod('US')).toEqual('not a phone number');
      });
    });
  });
});
