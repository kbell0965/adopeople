import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ResolutionDetailsComponent } from './components/resolution-details/resolution-details.component';
import { ResolutionSearchComponent } from './components/resolution-search/resolution-search.component';
import { ResolutionFromPathResolver } from './resolvers/resolution-from-path/resolution-from-path.resolver';
import { ResolutionsInitialQueryResolver } from './resolvers/resolutions-initial-query/resolutions-initial-query.resolver';
import { FulfillmentPartnerMemberResolver } from './resolvers/fulfillment-partner-members/fulfillment-partner-members.resolver';
import { FulfillmentPartnerBranchesResolver } from './resolvers/fulfillment-partner-branches/fulfillment-partner-branches.resolver';

const routes: Routes = [
  {
    path: '',
    component: ResolutionSearchComponent,
    resolve: {
      initialQueryResult: ResolutionsInitialQueryResolver,
      membersResult: FulfillmentPartnerMemberResolver,
      branchesResult: FulfillmentPartnerBranchesResolver
    }
  },
  {
    path: ':resolution_id',
    component: ResolutionDetailsComponent,
    resolve: {
      resolution: ResolutionFromPathResolver
    }
  },
  { path: '**', redirectTo: '', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ResolutionsRoutingModule { }
