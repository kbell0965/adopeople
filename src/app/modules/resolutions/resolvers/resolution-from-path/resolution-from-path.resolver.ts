import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';

import { PartnerResolutionRepository, PartnerModels } from '@pplsi-core/datalayer';
import { DataService } from '../../../../services/data/data.service';
import { ResolutionViewModel } from '../../models/resolution-view-model';

@Injectable()
export class ResolutionFromPathResolver implements Resolve<ResolutionViewModel> {
  constructor(private partnerResolutionRepository: PartnerResolutionRepository, private dataService: DataService) {}

  resolve(activatedRouteSnapshot: ActivatedRouteSnapshot): Observable<ResolutionViewModel> {
    const resolutionId = activatedRouteSnapshot.params['resolution_id'];

    return this.partnerResolutionRepository.find(resolutionId as string).pipe(
      map((resolution: PartnerModels.Resolution) => new ResolutionViewModel(resolution)),
      tap((resolution: ResolutionViewModel) => this.dataService.setResolution(resolution))
    );
  }
}
