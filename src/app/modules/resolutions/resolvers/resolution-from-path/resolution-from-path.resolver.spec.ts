import { TestBed, getTestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpClientTestingModule } from '@angular/common/http/testing';

import { PartnerResolutionRepository, PartnerModels } from '@pplsi-core/datalayer';
import { PartnerFactories } from '@pplsi-core/factories';
import { DataService } from '../../../../services/data/data.service';
import { ResolutionViewModel } from '../../models/resolution-view-model';
import { ResolutionFromPathResolver } from './resolution-from-path.resolver';

describe('ResolutionFromPathResolver', () => {
  let injector: TestBed;
  let resolutionFromPathResolver: ResolutionFromPathResolver;
  let partnerResolutionRepository: PartnerResolutionRepository;
  let dataService: DataService;
  const fakeResolution: object = PartnerFactories.ResolutionFactory.build();
  const resolution: PartnerModels.Resolution = new PartnerModels.Resolution(fakeResolution);

  const activatedRouteSnapshotStub = {
    params: {
      resolution_id: fakeResolution['id']
    }
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        ResolutionFromPathResolver,
        PartnerResolutionRepository,
        DataService
      ],
      imports: [
        HttpClientTestingModule
      ]
    });

    injector = getTestBed();
    resolutionFromPathResolver = injector.get(ResolutionFromPathResolver);
    partnerResolutionRepository = injector.get(PartnerResolutionRepository);
    dataService = injector.get(DataService);

    spyOn(partnerResolutionRepository, 'find').and.returnValue(of(resolution));
  });

  describe('#resolve', () => {
    it('gets resolution from the PartnerResolutionRepository', (done) => {
      resolutionFromPathResolver.resolve(activatedRouteSnapshotStub as any).subscribe((returnedResolution: ResolutionViewModel) => {
        expect(returnedResolution).toEqual(new ResolutionViewModel(resolution));
        done();
      });
    });

    it('sets the resolution from the PartnerResolutionRepository on the data service', (done) => {
      spyOn(dataService, 'setResolution');

      resolutionFromPathResolver.resolve(activatedRouteSnapshotStub as any).subscribe((returnedResolution: ResolutionViewModel) => {
        expect(dataService.setResolution).toHaveBeenCalledWith(returnedResolution);
        done();
      });
    });

    it('gets resolution from the PartnerResolutionRepository with the resolution id from the path', (done) => {
      resolutionFromPathResolver.resolve(activatedRouteSnapshotStub as any).subscribe(() => {
        expect(partnerResolutionRepository.find).toHaveBeenCalledWith(resolution['id']);
        done();
      });
    });
  });
});
