import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { Observable } from 'rxjs';

import { PartnerFulfillmentPartnerMemberRepository, PartnerModels } from '@pplsi-core/datalayer';

@Injectable()
export class FulfillmentPartnerMemberResolver implements Resolve<PartnerModels.FulfillmentPartnerMember[]> {
  constructor(private fulfillmentPartnerMemberRepository: PartnerFulfillmentPartnerMemberRepository) { }

  resolve(): Observable<PartnerModels.FulfillmentPartnerMember[]> {
    return this.fulfillmentPartnerMemberRepository.findAll(1, 1000);
  }
}
