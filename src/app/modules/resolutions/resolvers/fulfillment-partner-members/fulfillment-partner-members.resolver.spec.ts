import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { PartnerFulfillmentPartnerMemberRepository, PartnerModels } from '@pplsi-core/datalayer';
import { PartnerFactories } from '@pplsi-core/factories';
import { DataService } from '../../../../services/data/data.service';
import { FulfillmentPartnerMemberResolver } from './fulfillment-partner-members.resolver';
import { of } from 'rxjs';

describe('FulfillmentPartnerMemberResolver', () => {
  let injector: TestBed;
  let dataService: DataService;
  let fulfillmentPartnerMemberResolver: FulfillmentPartnerMemberResolver;
  let partnerFulfillmentPartnerMemberRepository: PartnerFulfillmentPartnerMemberRepository;
  const fakeFulfillmentPartnerMember: object = PartnerFactories.FulfillmentPartnerMemberFactory.build();
  const fulfillmentPartnerMember: PartnerModels.FulfillmentPartnerMember =
    new PartnerModels.FulfillmentPartnerMember(fakeFulfillmentPartnerMember);

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        FulfillmentPartnerMemberResolver,
        PartnerFulfillmentPartnerMemberRepository,
        DataService
      ],
      imports: [
        HttpClientTestingModule
      ]
    });

    injector = getTestBed();
    fulfillmentPartnerMemberResolver = injector.get(FulfillmentPartnerMemberResolver);
    partnerFulfillmentPartnerMemberRepository = injector.get(PartnerFulfillmentPartnerMemberRepository);
    dataService = injector.get(DataService);

    spyOn(partnerFulfillmentPartnerMemberRepository, 'findAll').and.returnValue(of([fulfillmentPartnerMember]));
  });

  describe('#resolve', () => {
    it('gets all assignees from the PartnerFulfillmentPartnerMemberRepository', (done) => {
      fulfillmentPartnerMemberResolver.resolve().subscribe(() => {
        expect(partnerFulfillmentPartnerMemberRepository.findAll).toHaveBeenCalled();
        done();
      });
    });
  });
});
