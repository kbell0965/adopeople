import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { PeopleFulfillmentPartnerBranchRepository, PeopleModels } from '@pplsi-core/datalayer';
import { PeopleFactories } from '@pplsi-core/factories';
import { DataService } from '../../../../services/data/data.service';
import { FulfillmentPartnerBranchesResolver } from './fulfillment-partner-branches.resolver';
import { of } from 'rxjs';

describe('FulfillmentPartnerBranchesResolver', () => {
  let injector: TestBed;
  let dataService: DataService;
  let branchesResolver: FulfillmentPartnerBranchesResolver;
  let peopleFulfillmentPartnerBranchRepository: PeopleFulfillmentPartnerBranchRepository;
  const fakeFulfillmentPartnerBranch: object = PeopleFactories.FulfillmentPartnerBranchFactory.build();
  const fulfillmentPartnerBranch: PeopleModels.FulfillmentPartnerBranch =
    new PeopleModels.FulfillmentPartnerBranch(fakeFulfillmentPartnerBranch);

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        FulfillmentPartnerBranchesResolver,
        PeopleFulfillmentPartnerBranchRepository,
        DataService
      ],
      imports: [
        HttpClientTestingModule
      ]
    });

    injector = getTestBed();
    branchesResolver = injector.get(FulfillmentPartnerBranchesResolver);
    peopleFulfillmentPartnerBranchRepository = injector.get(PeopleFulfillmentPartnerBranchRepository);
    dataService = injector.get(DataService);

    spyOn(peopleFulfillmentPartnerBranchRepository, 'findAll').and.returnValue(of([fulfillmentPartnerBranch]));
  });

  describe('#resolve', () => {
    it('gets all branches from the PeopleFulfillmentPartnerBranchRepository', (done) => {
      branchesResolver.resolve().subscribe(() => {
        expect(peopleFulfillmentPartnerBranchRepository.findAll).toHaveBeenCalled();
        done();
      });
    });
  });
});
