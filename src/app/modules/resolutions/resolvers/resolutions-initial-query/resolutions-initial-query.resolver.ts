import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { PartnerResolutionRepository, PartnerModels } from '@pplsi-core/datalayer';

@Injectable()
export class ResolutionsInitialQueryResolver implements Resolve<PartnerModels.ResolutionsWithPagination> {
  constructor(private resolutionRepository: PartnerResolutionRepository) { }

  resolve(activatedRouteSnapshot: ActivatedRouteSnapshot): Observable<PartnerModels.ResolutionsWithPagination> {
    let accountId: string;

    if (activatedRouteSnapshot.params) {
      accountId = activatedRouteSnapshot.params['account_id'];
    }
    if (!accountId && activatedRouteSnapshot.parent && activatedRouteSnapshot.parent.params) {
      accountId = activatedRouteSnapshot.parent.params['account_id'];
    }

    if (accountId) {
      return this.resolutionRepository.findAll({
        page : '1',
        per_page : '10'
      }, accountId);
    }
    return this.resolutionRepository.findAll({
      page : '1',
      per_page : '10',
      'statuses[]' : [
        'assigned',
        'unassigned',
        'pending_member',
        'pending_provider'
      ]
    });
  }
}
