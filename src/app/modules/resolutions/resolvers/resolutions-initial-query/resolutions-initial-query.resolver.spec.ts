import { TestBed, getTestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { PartnerResolutionRepository, PartnerModels } from '@pplsi-core/datalayer';
import { PartnerFactories } from '@pplsi-core/factories';
import { ResolutionsInitialQueryResolver } from './resolutions-initial-query.resolver';
import { ActivatedRouteSnapshot } from '@angular/router';
import { AccountViewModel } from 'src/app/models/account-view-model';

describe('ResolutionsInitialQueryResolver', () => {
  let injector: TestBed;
  let resolutionsInitialQueryResolver: ResolutionsInitialQueryResolver;
  let partnerResolutionRepository: PartnerResolutionRepository;
  let route: ActivatedRouteSnapshot;
  const getResolution = (cfg) => PartnerFactories.ResolutionFactory.build(cfg);
  const getResolutions = () => Array.from(Array(10), (x, i) => i + 1).map(i => getResolution({}));
  const getResolutionsWithPagination = { resolutions: getResolutions(), total_length: 10 };
  const accountFake: object = PartnerFactories.AccountFactory.build();
  const account: AccountViewModel = new AccountViewModel(accountFake);

  const queryParamsWithAccountId: object = { page: '1', per_page: '10' };
  const queryParamsWithoutAccountId: object = {
    page: '1', per_page: '10', 'statuses[]': [ 'assigned', 'unassigned', 'pending_member', 'pending_provider' ]
  };

  const getQueryParams = (accountId?: string): object => {
    return accountId ? queryParamsWithAccountId : queryParamsWithoutAccountId;
  };

  const setupTestBed = (routeStub) => {
    let accountId: string;
    if (!accountId && routeStub.params) {
      accountId = routeStub.params['account_id'];
    }
    if (!accountId && routeStub.parent) {
      accountId = routeStub.parent.params['account_id'];
    }
    const queryParams: object = getQueryParams(accountId);

    TestBed.configureTestingModule({
      providers: [
        ResolutionsInitialQueryResolver,
        PartnerResolutionRepository,
        { provide: ActivatedRouteSnapshot, useValue: routeStub }
      ],
      imports: [HttpClientTestingModule]
    });

    route = TestBed.get(ActivatedRouteSnapshot);
    injector = getTestBed();
    resolutionsInitialQueryResolver = injector.get(ResolutionsInitialQueryResolver);
    partnerResolutionRepository = injector.get(PartnerResolutionRepository);

    if (accountId) {
      spyOn(partnerResolutionRepository, 'findAll').withArgs(queryParams, accountId)
        .and.returnValue(of(getResolutionsWithPagination as any));
    } else {
      spyOn(partnerResolutionRepository, 'findAll').withArgs(queryParams)
        .and.returnValue(of(getResolutionsWithPagination as any));
    }
  };

  describe('with account id in path', () => {
    const activatedRouteStub = { params: { 'account_id': account.id } };
    beforeEach(() => setupTestBed(activatedRouteStub));

    describe('#resolve', () => {
      it('gets resolutions with pagination from the resolutions initial query resolver', () => {
        resolutionsInitialQueryResolver.resolve(route)
          .subscribe((result: PartnerModels.ResolutionsWithPagination) => expect(result.resolutions.length).toBe(10));
      });

      it('calls resolution repository with the account id', () => {
        resolutionsInitialQueryResolver.resolve(route)
          .subscribe(() => expect(partnerResolutionRepository.findAll).toHaveBeenCalledWith(queryParamsWithAccountId, account.id));
      });
    });
  });

  describe('with account id in the parent path', () => {
    const activatedRouteStub = { parent: { params: { 'account_id': account.id }} };
    beforeEach(() => setupTestBed(activatedRouteStub));

    describe('#resolve', () => {
      it('gets resolutions with pagination from the resolutions initial query resolver', () => {
        resolutionsInitialQueryResolver.resolve(route)
          .subscribe((result: PartnerModels.ResolutionsWithPagination) => expect(result.resolutions.length).toBe(10));
      });

      it('calls resolution repository with the account id', () => {
        resolutionsInitialQueryResolver.resolve(route)
          .subscribe(() => expect(partnerResolutionRepository.findAll).toHaveBeenCalledWith(queryParamsWithAccountId, account.id));
      });
    });
  });

  describe('without account id in path', () => {
    const activatedRouteStub = { params: { } };
    beforeEach(() => setupTestBed(activatedRouteStub));

    describe('#resolve', () => {
      it('gets resolutions with pagination from the resolutions initial query resolver', () => {
        resolutionsInitialQueryResolver.resolve(route)
          .subscribe((result: PartnerModels.ResolutionsWithPagination) => expect(result.resolutions.length).toBe(10));
      });

      it('calls resolution repository without the account id', () => {
        resolutionsInitialQueryResolver.resolve(route)
          .subscribe(() => expect(partnerResolutionRepository.findAll).toHaveBeenCalledWith(queryParamsWithoutAccountId));
      });
    });
  });
});
