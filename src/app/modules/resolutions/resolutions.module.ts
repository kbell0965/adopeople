import { NgModule } from '@angular/core';
import { FeatherModule } from 'angular-feather';
import { DatePipe } from '@angular/common';
import { Activity, CheckCircle, Layers, UserPlus, Minus, Edit } from 'angular-feather/icons';
import { MatPaginatorModule, MatCheckboxModule } from '@angular/material';
import { ResolutionDetailsComponent } from './components/resolution-details/resolution-details.component';
import { ResolutionsRoutingModule } from './resolutions-routing.module';
import { ResolutionSearchComponent } from './components/resolution-search/resolution-search.component';
import { PageLayoutModule } from 'ng-components.adonis';
import { PplsiCoreDatalayerModule } from '@pplsi-core/datalayer';
import { ResolutionFromPathResolver } from './resolvers/resolution-from-path/resolution-from-path.resolver';
import { ResolutionsInitialQueryResolver } from './resolvers/resolutions-initial-query/resolutions-initial-query.resolver';
import { FulfillmentPartnerMemberResolver } from './resolvers/fulfillment-partner-members/fulfillment-partner-members.resolver';
import { FulfillmentPartnerBranchesResolver } from './resolvers/fulfillment-partner-branches/fulfillment-partner-branches.resolver';
import { LocalizationService } from '@pplsi-core/shared-modules/dist/src/services/localization.service';
import { ResolutionSearchResultModule } from 'src/app/shared/components/resolution-search-result/resolution-search-result.module';
import { SearchLayoutModule } from 'src/app/shared/components/search-layout/search-layout.module';
import { ActivityLogModule } from 'src/app/shared/components/activity-log/activity-log.module';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [
    ResolutionSearchComponent,
    ResolutionDetailsComponent
  ],
  imports: [
    ActivityLogModule,
    FeatherModule.pick({ Activity, CheckCircle, Layers, UserPlus, Minus, Edit }),
    MatPaginatorModule,
    MatCheckboxModule,
    PageLayoutModule,
    PplsiCoreDatalayerModule.forRoot(),
    ResolutionsRoutingModule,
    ResolutionSearchResultModule,
    SearchLayoutModule,
    SharedModule
  ],
  providers: [
    DatePipe,
    ResolutionsInitialQueryResolver,
    ResolutionFromPathResolver,
    FulfillmentPartnerMemberResolver,
    FulfillmentPartnerBranchesResolver,
    LocalizationService,
    {
      provide: 'Window',
      useValue: window
    },
    {
      provide: 'localizations',
      useValue: [
        { locale: 'en-US', currencyCode: 'USD', dateFormat: 'MM/dd/yyyy' },
        { locale: 'en-GB', currencyCode: 'GBP', dateFormat: 'dd/MM/yyyy' },
        { locale: 'en-CA', currencyCode: 'CAD', dateFormat: 'yyyy-MM-dd' }
      ]
    },
    {
      provide: 'defaultLocale',
      useValue: 'en-US'
    }
  ]
})
export class ResolutionsModule { }
