import { ActivatedRoute } from '@angular/router';
import { async, ComponentFixture, TestBed, getTestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { FormArray, FormControl, FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { ResolutionSearchComponent } from './resolution-search.component';
import { PageLayoutModule } from 'ng-components.adonis';
import { ResolutionSearchResultComponent } from '../../../../shared/components/resolution-search-result/resolution-search-result.component';
import { FeatherModule } from 'angular-feather';
import { Activity, CheckCircle, Layers, UserPlus } from 'angular-feather/icons';
import {
  PartnerResolutionRepository, PartnerModels, PeopleModels, PplsiCoreDatalayerModule, ResolutionStatus
} from '@pplsi-core/datalayer';
import { of } from 'rxjs';
import { PartnerFactories, PeopleFactories } from '@pplsi-core/factories';
import { DatePipe } from '@angular/common';
import { MatPaginatorModule, MatCheckboxModule } from '@angular/material';
import { RouterTestingModule } from '@angular/router/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ResolutionViewModel } from '../../models/resolution-view-model';
import { SearchLayoutModule } from 'src/app/shared/components/search-layout/search-layout.module';
import { SharedModule } from 'src/app/shared/shared.module';

const fakeResolutions = PartnerFactories.ResolutionFactory.buildList(10);
const fakeMembers = PartnerFactories.FulfillmentPartnerMemberFactory.buildList(2);
const fakeBranches = PartnerFactories.FulfillmentPartnerBranchFactory.buildList(2);

describe('ResolutionSearchComponent.Unit', () => {
  let component: ResolutionSearchComponent;
  let fixture: ComponentFixture<ResolutionSearchComponent>;
  let formBuilder: FormBuilder;
  let injector: TestBed;
  let resolutionRepository: PartnerResolutionRepository;
  let resolutionRepositorySpy: jasmine.Spy;

  const testSubmitResults = { resolutions: fakeResolutions, total_length: 10 };
  const activatedRouteStub = {
    snapshot: {
      data: {
        initialQueryResult: testSubmitResults,
        membersResult: fakeMembers,
        branchesResult: fakeBranches
      }
    }
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ResolutionSearchComponent, ResolutionSearchResultComponent],
      imports: [
        BrowserAnimationsModule,
        FeatherModule.pick({ Activity, CheckCircle, Layers, UserPlus }),
        MatPaginatorModule,
        MatCheckboxModule,
        PageLayoutModule,
        PplsiCoreDatalayerModule.forRoot(),
        RouterTestingModule,
        SearchLayoutModule,
        SharedModule,
        ReactiveFormsModule
      ],
      providers: [
        { provide: ActivatedRoute, useValue: activatedRouteStub },
        DatePipe
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResolutionSearchComponent);
    component = fixture.componentInstance;
    injector = getTestBed();
    formBuilder = injector.get(FormBuilder);
    resolutionRepository = injector.get(PartnerResolutionRepository);
    resolutionRepositorySpy = spyOn(resolutionRepository, 'findAll');
    resolutionRepositorySpy.and.returnValue(of(
      <PartnerModels.ResolutionsWithPagination> {}
    ));
    fixture.detectChanges();
  });

  describe('#ngOnInit', () => {
    beforeEach(() => {
      resolutionRepositorySpy.calls.reset();
      resolutionRepositorySpy.and.returnValue(testSubmitResults);
      component.ngOnInit();
    });

    it('sets the submitResults from activated route data', () => {
      expect(component.submitResults).toEqual(fakeResolutions.map((resolution: object) => new ResolutionViewModel(resolution)));
    });

    it('sets the membersResult from activated route data', () => {
      expect(component.members).toEqual(fakeMembers.map(assignee => new PartnerModels.FulfillmentPartnerMember(assignee)));
    });

    it('sets the branches from activated route data', () => {
      expect(component.branches).toEqual(fakeBranches.map(branch => new PeopleModels.FulfillmentPartnerBranch(branch)));
    });

    it('sets the submitResultsPage event length from activated route data', () => {
      expect(component.submitResultsPage.length).toBe(10);
    });
  });

  describe('#ngOnDestroy', () => {
    beforeEach(() => {
      spyOn((component as any)._destroyed, 'next');
      spyOn((component as any)._destroyed, 'complete');
      component.ngOnDestroy();
    });

    it('calls #next on `_destroyed`', () => expect((component as any)._destroyed.next).toHaveBeenCalled());
    it('calls #complete on `_destroyed`', () => expect((component as any)._destroyed.complete).toHaveBeenCalled());
  });

  describe('#filterBranches', () => {
    it('returns the filterForm branch controls', () => {
      component.filterForm.controls['branches'] = component.buildFilterControls(component.branches);
      expect(component.filterBranches).toEqual(component.filterForm.get('branches'));
    });
  });

  describe('when checkbox is interacted with', () => {
    const expectedFilterQuery = {
      'statuses[]' : [
        ResolutionStatus.Assigned,
        ResolutionStatus.Unassigned,
        ResolutionStatus.PendingMember,
        ResolutionStatus.PendingProvider
      ],
      'target_branch_id[]' : [fakeBranches[0].id],
      'assignee_id[]' : []
    };
    const secondExpectedFilterQuery = {
      'statuses[]' : [
        ResolutionStatus.Assigned,
        ResolutionStatus.Unassigned,
        ResolutionStatus.PendingMember,
        ResolutionStatus.PendingProvider
      ],
      'target_branch_id[]' : [],
      'assignee_id[]' : [fakeMembers[0].id]
    };

    it('changes the branches filter query correctly', () => {
      const elToggle = fixture.debugElement.queryAll(By.css('.sidebar-expansion-panel .toggle'));
      elToggle[0].nativeElement.click();
      fixture.detectChanges();
      const el = fixture.debugElement.queryAll(By.css('.sidebar-expansion-content .mat-checkbox-input'))[0].nativeElement;
      el.click();
      fixture.detectChanges();
      expect(component.filterQuery).toEqual(expectedFilterQuery);
    });

    it('changes the members filter query correctly', () => {
      const elToggle = fixture.debugElement.queryAll(By.css('.sidebar-expansion-panel .toggle'));
      elToggle[1].nativeElement.click();
      fixture.detectChanges();
      const el = fixture.debugElement.queryAll(By.css('.sidebar-expansion-content .mat-checkbox-input'))[0].nativeElement;
      el.click();
      fixture.detectChanges();
      expect(component.filterQuery).toEqual(secondExpectedFilterQuery);
    });
  });

  describe('#filterMembers', () => {
    it('returns the filterForm assignee controls', () => {
      component.filterForm.controls['members'] = component.buildFilterControls(component.members);
      expect(component.filterMembers).toEqual(component.filterForm.get('members'));
    });
  });

  describe('#buildFilterControls', () => {
    it('builds the filterForm controls from options that are passed', () => {
      const expected = formBuilder.array(fakeMembers.map(() => formBuilder.control(false)));

      expect(component.buildFilterControls(fakeMembers)[0]).toEqual(expected[0]);
    });
  });

  describe('#handleFormData', () => {
    const formData = { 'branches': [true, false], 'members': [false, true] };
    const expectedFilterQuery = {
      'statuses[]' : [
        ResolutionStatus.Assigned,
        ResolutionStatus.Unassigned,
        ResolutionStatus.PendingMember,
        ResolutionStatus.PendingProvider
       ],
      'target_branch_id[]' : [fakeBranches[0].id],
      'assignee_id[]' : [fakeMembers[1].id]
    };

    it('sets filter query to an updated value', () => {
      component.handleFormData(formData);
      expect(component.filterQuery).toEqual(expectedFilterQuery);
    });
  });

  describe('#getFilterIds', () => {
    const formData = {
      branches: [ true, false ],
      members: [ false, false ]
    };

    it('returns the correct ids of the selected form checkboxes', () => {
      expect(component.getFilterIds(formData.branches, fakeBranches)).toEqual([fakeBranches[0].id]);
      expect(component.getFilterIds(formData.members, fakeMembers)).toEqual([]);
    });
  });

  describe('#updateSubmitResults', () => {
    const testResult = {
      resolutions: fakeResolutions,
      total_length: fakeResolutions.length
    };
    const expectedResolutions = fakeResolutions.map((resolution: object) => new ResolutionViewModel(resolution));

    it('sets submitResults to updated values', () => {
      component.updateSubmitResults(testResult);
      expect(component.submitResults).toEqual(expectedResolutions);
      expect(component.submitResultsPage.length).toEqual(fakeResolutions.length);
    });
  });

  describe('#pageResults', () => {
    it('calls the resolutionRepository with the page index and size', () => {
      resolutionRepositorySpy.calls.reset();
      component.pageResults({ pageIndex: 2, pageSize: 10, length: 10 });
      expect(resolutionRepositorySpy).toHaveBeenCalledTimes(1);
      expect(resolutionRepositorySpy).toHaveBeenCalledWith({
        page: 3, per_page: 10, 'statuses[]' : [
          ResolutionStatus.Assigned,
          ResolutionStatus.Unassigned,
          ResolutionStatus.PendingMember,
          ResolutionStatus.PendingProvider
        ]
      });
    });

    it('updates submitResults with the new page data', () => {
      resolutionRepositorySpy.calls.reset();
      resolutionRepositorySpy.and.returnValue(testSubmitResults);
      expect(component.submitResults).toEqual(fakeResolutions.map((resolution: object) => new ResolutionViewModel(resolution)));
    });
  });

  describe('#updateResults', () => {
    it('is fired when the search layout component submit button is clicked', () => {
      spyOn(component, 'updateResults');
      const el = fixture.debugElement.query(By.css('.search-button'));
      el.nativeElement.click();
      expect(component.updateResults).toHaveBeenCalled();
    });
  });
});
