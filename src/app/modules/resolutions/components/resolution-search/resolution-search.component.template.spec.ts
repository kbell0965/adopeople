import { ActivatedRoute } from '@angular/router';
import { async, ComponentFixture, TestBed, getTestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { RouterTestingModule } from '@angular/router/testing';
import { ResolutionSearchComponent } from './resolution-search.component';
import { PageLayoutModule } from 'ng-components.adonis';
import { FeatherModule } from 'angular-feather';
import { Activity, CheckCircle, Layers, UserPlus } from 'angular-feather/icons';
import { PartnerResolutionRepository, PartnerModels, PplsiCoreDatalayerModule } from '@pplsi-core/datalayer';
import { of } from 'rxjs';
import { MatPaginatorModule, MatCheckboxModule } from '@angular/material';
import { DatePipe } from '@angular/common';
import { PartnerFactories } from '@pplsi-core/factories';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ResolutionSearchResultComponent } from '../../../../shared/components/resolution-search-result/resolution-search-result.component';
import { SearchLayoutModule } from 'src/app/shared/components/search-layout/search-layout.module';
import { SharedModule } from 'src/app/shared/shared.module';

const fakeResolutions = PartnerFactories.ResolutionFactory.buildList(10);
const fakeMembers = PartnerFactories.FulfillmentPartnerMemberFactory.buildList(2);
const fakeBranches = PartnerFactories.FulfillmentPartnerBranchFactory.buildList(2);

describe('ResolutionSearchComponent.Template', () => {
  let component: ResolutionSearchComponent;
  let fixture: ComponentFixture<ResolutionSearchComponent>;
  let injector: TestBed;
  let resolutionRepository: PartnerResolutionRepository;
  const testSubmitResults = { resolutions: fakeResolutions, total_length: 10 };
  const activatedRouteStub = {
    snapshot: {
      data: {
        initialQueryResult: testSubmitResults,
        membersResult: fakeMembers,
        branchesResult: fakeBranches
      }
    }
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ResolutionSearchComponent, ResolutionSearchResultComponent],
      imports: [
        RouterTestingModule,
        BrowserAnimationsModule,
        FeatherModule.pick({ Activity, CheckCircle, Layers, UserPlus }),
        MatPaginatorModule,
        MatCheckboxModule,
        PageLayoutModule,
        PplsiCoreDatalayerModule.forRoot(),
        SearchLayoutModule,
        SharedModule
      ],
      providers: [
        { provide: ActivatedRoute, useValue: activatedRouteStub },
        DatePipe
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResolutionSearchComponent);
    component = fixture.componentInstance;
    injector = getTestBed();
    resolutionRepository = injector.get(PartnerResolutionRepository);
    spyOn(resolutionRepository, 'findAll').and.returnValue(of(
      <PartnerModels.ResolutionsWithPagination> {}));
    fixture.detectChanges();
  });

  it('has a page title of Resolutions', () => {
    const el = fixture.debugElement.query(By.css('h2'));
    expect(el.nativeElement.innerHTML).toBe('Resolutions');
  });

  describe('page layout', () => {
    it('has a page sidebar', () => {
      const el = fixture.debugElement.query(By.css('.page-layout-sidebar'));
      expect(el).toBeTruthy();
    });

    it('has a page body', () => {
      const el = fixture.debugElement.query(By.css('.page-layout-body'));
      expect(el).toBeTruthy();
    });
  });

  describe('filter options', () => {
    it('adds the expected branches filter checkbox options', () => {
      const elToggle = fixture.debugElement.queryAll(By.css('.sidebar-expansion-panel .toggle'));
      elToggle[0].nativeElement.click();
      fixture.detectChanges();
      const el = fixture.debugElement.queryAll(By.css('.sidebar-expansion-content .mat-checkbox-input'));
      expect(el.length).toEqual(fakeBranches.length);
    });

    it('adds the expected member filter checkbox options', () => {
      const elToggle = fixture.debugElement.queryAll(By.css('.sidebar-expansion-panel .toggle'));
      elToggle[1].nativeElement.click();
      fixture.detectChanges();
      const el = fixture.debugElement.queryAll(By.css('.sidebar-expansion-content .mat-checkbox-input'));
      expect(el.length).toEqual(fakeMembers.length);
    });
  });
});
