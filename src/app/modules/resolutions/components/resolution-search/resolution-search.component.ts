import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { Subject } from 'rxjs';
import { ResolutionViewModel } from '../../models/resolution-view-model';
import { FulfillmentPartnerMemberViewModel } from '../../../../models/fulfillment-partner-member-view-model';
import { PartnerModels, PeopleModels, PartnerResolutionRepository, ResolutionStatus } from '@pplsi-core/datalayer';
import { PageEvent } from '@angular/material';
import { ActivatedRoute } from '@angular/router';
import { pluck, map, tap, takeUntil } from 'rxjs/operators';

@Component({
  selector: 'resolution-search',
  styleUrls: ['./resolution-search.component.scss'],
  templateUrl: './resolution-search.component.html'
})
export class ResolutionSearchComponent implements OnInit, OnDestroy {
  pageSizeOptions: Array<number>;
  submitResults: ResolutionViewModel[];
  members: PartnerModels.FulfillmentPartnerMember[];
  branches: PeopleModels.FulfillmentPartnerBranch[];
  submitResultsPage: PageEvent;
  filtersAffectedByView: string[];
  private _destroyed = new Subject<void>();
  filterForm: FormGroup;
  filterQuery: object;

  constructor(
    private activatedRoute: ActivatedRoute,
    private resolutionRepository: PartnerResolutionRepository,
    private formBuilder: FormBuilder
  ) {
    this.pageSizeOptions = [10, 25, 50];
    this.submitResultsPage = { pageIndex: 0, pageSize: this.pageSizeOptions[0], length: 0 };
  }

  ngOnInit() {
    const members = this.activatedRoute.snapshot.data['membersResult'];
    const branches = this.activatedRoute.snapshot.data['branchesResult'];
    this.updateSubmitResults(this.activatedRoute.snapshot.data['initialQueryResult']);
    this.members = members.map(assignee => new PartnerModels.FulfillmentPartnerMember(assignee));
    this.branches = branches.map(branch => new PeopleModels.FulfillmentPartnerBranch(branch));

    this.filterForm = this.formBuilder.group({
      branches: this.buildFilterControls(this.branches),
      members: this.buildFilterControls(this.members)
    });

    this.filterForm.valueChanges.subscribe(formValue => {
      this.handleFormData(formValue);
    });
  }

  ngOnDestroy() {
    this._destroyed.next();
    this._destroyed.complete();
  }

  get filterBranches() {
    return this.filterForm.get('branches');
  }

  get filterMembers() {
    return this.filterForm.get('members');
  }

  buildFilterControls(values) {
    const arr = values.map(value => {
      return this.formBuilder.control(false);
    });
    return this.formBuilder.array(arr);
  }

  handleFormData(formValue) {
    const params = { 'statuses[]' : [
      ResolutionStatus.Assigned,
      ResolutionStatus.Unassigned,
      ResolutionStatus.PendingMember,
      ResolutionStatus.PendingProvider
    ] };
    params['target_branch_id[]'] = this.getFilterIds(formValue.branches, this.branches);
    params['assignee_id[]'] = this.getFilterIds(formValue.members, this.members);
    this.filterQuery = params;
    this.updateResults();
  }

  getFilterIds(formValues: boolean[], options: any[]): string[] {
    return formValues.map((selected, i) => {
      return { selected, id: options[i].id };
    }).filter(option => option.selected).map(option => {
      return option.id;
    });
  }

  updateSubmitResults(result: PartnerModels.ResolutionsWithPagination) {
    const { resolutions, total_length } = result;
    if (resolutions) {
      this.submitResults = resolutions.map(resolution => new ResolutionViewModel(resolution));
    }
    this.submitResultsPage.length = total_length;
  }

  pageResults($event) {
    const query_params = {
      page : $event.pageIndex + 1,
      per_page : $event.pageSize,
      'statuses[]' : [
        ResolutionStatus.Assigned,
        ResolutionStatus.Unassigned,
        ResolutionStatus.PendingMember,
        ResolutionStatus.PendingProvider
      ],
      ...this.filterQuery
    };

    this.resolutionRepository.findAll(query_params).pipe(
      takeUntil(this._destroyed),
      tap(() => { this.submitResultsPage = $event; })
    ).subscribe((result: PartnerModels.ResolutionsWithPagination) => this.updateSubmitResults(result));
  }

  updateResults() {
    const query_params = {
      page : '1',
      per_page : `${this.submitResultsPage.pageSize}`,
      ...this.filterQuery
    };

    this.resolutionRepository.findAll(query_params).pipe(
      takeUntil(this._destroyed)
    ).subscribe((result: PartnerModels.ResolutionsWithPagination) => this.updateSubmitResults(result));
  }
}
