import { async, ComponentFixture, TestBed, getTestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { FeatherModule } from 'angular-feather';
import { Activity, CheckCircle, Layers, UserPlus, Minus, Edit } from 'angular-feather/icons';
import { of } from 'rxjs';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { LocalizationService } from '@pplsi-core/shared-modules/dist/src/services/localization.service';
import { FulfillmentPartnerMemberViewModel } from 'src/app/models/fulfillment-partner-member-view-model';
import { AccountViewModel } from '../../../../models/account-view-model';
import { PartnerFulfillmentPartnerMemberViewModel } from '../../../../models/partner-fulfillment-partner-member-view-model';
import { DataService } from '../../../../services/data/data.service';
import { DrawerService } from '../../../../services/drawer/drawer.service';
import { ResolutionEditComponent } from '../../../../shared/components/resolution-edit/resolution-edit.component';
import { ResolutionViewModel } from '../../models/resolution-view-model';
import { ResolutionDetailsComponent } from './resolution-details.component';
import { PartnerFactories, PeopleFactories } from '@pplsi-core/factories';
import { ActivityLogModule } from 'src/app/shared/components/activity-log/activity-log.module';
import {
  PartnerActivityRepository,
  PartnerModels,
  PartnerAccountRepository,
  PartnerFulfillmentPartnerMemberRepository,
  PartnerFulfillmentPartnerRepository,
  PplsiCoreDatalayerModule,
  PeopleModels,
  PeopleFulfillmentPartnerBranchRepository,
  PeopleFulfillmentPartnerRepository,
  PeopleFulfillmentPartnerMembersRepository
} from '@pplsi-core/datalayer';
import { SharedModule } from 'src/app/shared/shared.module';

const account: PartnerModels.Account = new PartnerModels.Account(PartnerFactories.AccountFactory.build());
const fulfillmentPartnerBranches: PeopleModels.FulfillmentPartnerBranch[] = [
  new PeopleModels.FulfillmentPartnerBranch(PeopleFactories.FulfillmentPartnerBranchFactory.build())
];
const fulfillmentPartners: PartnerModels.FulfillmentPartner[] = [
  new PartnerModels.FulfillmentPartner(PeopleFactories.FulfillmentPartnerFactory.build())
];
const fulfillmentPartnerMembers: PartnerModels.FulfillmentPartnerMember[] = [
  new PartnerModels.FulfillmentPartnerMember(PartnerFactories.FulfillmentPartnerMemberFactory.build())
];

describe('ResolutionDetailsComponent.Unit', () => {
  let component: ResolutionDetailsComponent;
  let fixture: ComponentFixture<ResolutionDetailsComponent>;
  let drawerService: DrawerService;
  let dataService: DataService;
  let injector: TestBed;
  let activityRepository: PartnerActivityRepository;
  let partnerAccountRepository: PartnerAccountRepository;
  let partnerFulfillmentPartnerMemberRepository: PartnerFulfillmentPartnerMemberRepository;
  let partnerFulfillmentPartnerRepository: PartnerFulfillmentPartnerRepository;
  let peopleFulfillmentPartnerRepository: PeopleFulfillmentPartnerRepository;
  let peopleFulfillmentPartnerBranchRepository: PeopleFulfillmentPartnerBranchRepository;
  let peopleFulfillmentPartnerMembersRepository: PeopleFulfillmentPartnerMembersRepository;
  let activityFindAllSpy: jasmine.Spy;
  let activityCreateSpy: jasmine.Spy;
  const resolution: ResolutionViewModel = new ResolutionViewModel(PartnerFactories.ResolutionFactory.build());
  const activatedRouteStub = {
    snapshot: {
      data: {
        resolution: resolution
      }
    }
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResolutionDetailsComponent ],
      imports: [
        ActivityLogModule,
        FeatherModule.pick({ Activity, CheckCircle, Layers, UserPlus, Minus, Edit }),
        PplsiCoreDatalayerModule.forRoot(),
        ReactiveFormsModule,
        SharedModule,
        RouterTestingModule
      ],
      providers: [
        { provide: ActivatedRoute, useValue: activatedRouteStub },
        LocalizationService,
        {
          provide: 'Window',
          useValue: window
        },
        {
          provide: 'localizations',
          useValue: [
            { locale: 'en-US', currencyCode: 'USD', dateFormat: 'MM/dd/yyyy' }
          ]
        },
        {
          provide: 'defaultLocale',
          useValue: 'en-US'
        },
        DrawerService,
        DataService
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    injector = getTestBed();
    fixture = TestBed.createComponent(ResolutionDetailsComponent);
    component = fixture.componentInstance;
    drawerService = injector.get(DrawerService);
    activityRepository = injector.get(PartnerActivityRepository);
    partnerAccountRepository = injector.get(PartnerAccountRepository);
    dataService = injector.get(DataService);
    partnerFulfillmentPartnerMemberRepository = injector.get(PartnerFulfillmentPartnerMemberRepository);
    partnerFulfillmentPartnerRepository = injector.get(PartnerFulfillmentPartnerRepository);
    peopleFulfillmentPartnerRepository = injector.get(PeopleFulfillmentPartnerRepository);
    peopleFulfillmentPartnerBranchRepository = injector.get(PeopleFulfillmentPartnerBranchRepository);
    peopleFulfillmentPartnerMembersRepository = injector.get(PeopleFulfillmentPartnerMembersRepository);
    activityFindAllSpy = spyOn(activityRepository, 'findAllByResolutionId').and.returnValue(of(
      <PartnerModels.Activity[]> [{
        comment: 'test',
        activity_type: 'comment',
        created_at: new Date(),
        fulfillment_partner_member: 'john doe',
        id: '12345',
        updated_at: new Date()
      }]
    ));
    activityCreateSpy = spyOn(activityRepository, 'create').and.returnValue(of(
      <PartnerModels.Activity> {
        comment: 'test',
        activity_type: 'comment',
        created_at: new Date(),
        fulfillment_partner_member: 'john doe',
        id: '12345',
        updated_at: new Date()
      }
    ));
    spyOnProperty(dataService, 'resolution', 'get').and.returnValue(of(resolution));
    fixture.detectChanges();
  });

  afterAll(() => {
    TestBed.resetTestingModule();
  });

  describe('#ngOnInit', () => {
    beforeEach(() => component.ngOnInit());

    it('sets the resolution property', () => expect(component.resolution).toBeDefined());

    it('sets the locale property', () => expect(component.resolution).toBeDefined());

    it('calls the activity repository for activity log notes', () => {
      activityFindAllSpy.calls.reset();
      component.ngOnInit();
      expect(activityRepository.findAllByResolutionId).toHaveBeenCalledTimes(1);
    });
  });

  describe('#ngOnDestroy', () => {
    beforeEach(() => {
      spyOn((component as any)._destroyed, 'next');
      spyOn((component as any)._destroyed, 'complete');
      component.ngOnDestroy();
    });

    it('calls #next on `_destroyed`', () => expect((component as any)._destroyed.next).toHaveBeenCalled());
    it('calls #complete on `_destroyed`', () => expect((component as any)._destroyed.complete).toHaveBeenCalled());
  });

  describe('#actionTakenClass', () => {
    it('returns ok when resolution type equals provider action taken', () => {
      component.resolution = new ResolutionViewModel(
        PartnerFactories.ResolutionFactory.build({ action_taken: 'provider action taken' })
      );

      expect(component.actionTakenClass).toBe('ok');
    });

    it('returns warning when resolution type is not equal to provider action taken', () => {
      component.resolution = new ResolutionViewModel(
        PartnerFactories.ResolutionFactory.build({ action_taken: 'anything else' })
      );

      expect(component.actionTakenClass).toBe('warning');
    });
  });

  describe('#loadEdit', () => {
    const targetMembers: PeopleModels.FulfillmentPartnerMember[] = PeopleFactories.FulfillmentPartnerMemberFactory.buildList();

    beforeEach(() => {
      spyOn(partnerAccountRepository, 'find').and.returnValue(of(account));
      spyOn(partnerFulfillmentPartnerMemberRepository, 'findAll').and.returnValue(of(fulfillmentPartnerMembers));
      spyOn(peopleFulfillmentPartnerBranchRepository, 'findAll').and.returnValue(of(fulfillmentPartnerBranches));
      spyOn(peopleFulfillmentPartnerRepository, 'findAll').and.returnValue(of(fulfillmentPartners));
      spyOn(peopleFulfillmentPartnerMembersRepository, 'findAllByFulfillmentPartnerBranch').and.returnValue(of(targetMembers));
      spyOn(component, 'openDrawer');
    });

    describe('if resolution has a target branch', () => {
      beforeEach(() => {
        component.resolution.target_branch.id = 'some id';
        component.loadEdit();
      });

      it('calls the account repository', () => {
        expect(partnerAccountRepository.find).toHaveBeenCalledWith(component.resolution.account_id);
      });

      it('calls the fulfillment partner member repository', () => {
        expect(partnerFulfillmentPartnerMemberRepository.findAll).toHaveBeenCalledWith(1, 1000);
      });

      it('calls the fulfillment partner branch repository', () => {
        expect(peopleFulfillmentPartnerBranchRepository.findAll).toHaveBeenCalledTimes(1);
      });

      it('calls the fulfillment partner member repository', () => {
        expect(peopleFulfillmentPartnerMembersRepository.findAllByFulfillmentPartnerBranch).toHaveBeenCalledTimes(1);
      });

      it('caches the account on the component', () => {
        expect(component.account).toEqual(new AccountViewModel(account));
      });

      it('caches the fulfillment partner members on the component', () => {
        expect(component.fulfillmentPartnerMembers).toEqual(
          fulfillmentPartnerMembers.map(
            (fulfillmentPartnerMember: PartnerModels.FulfillmentPartnerMember) =>
              new PartnerFulfillmentPartnerMemberViewModel(fulfillmentPartnerMember)
          )
        );
      });

      it('caches the fulfillment partners on the component', () => {
        expect(component.fulfillmentPartners).toEqual(fulfillmentPartners);
      });

      it('caches the target members on the component', () => {
        expect(component.targetMembers).toEqual(
          targetMembers.map(
            (fulfillmentPartnerMember: PeopleModels.FulfillmentPartnerMember) =>
              new FulfillmentPartnerMemberViewModel(fulfillmentPartnerMember)
          )
        );
      });

      it('calls the openDrawer on the component', () => {
        expect(component.openDrawer).toHaveBeenCalled();
      });
    });

    describe('if resolution does not have target branch', () => {
      beforeEach(() => {
        component.resolution.target_branch.id = null;
        component.loadEdit();
      });

      it('does not call findAll fulfillment partners by target branch', () => {
        expect(peopleFulfillmentPartnerRepository.findAll).toHaveBeenCalledTimes(0);
      });

      it('does not call the people fulfillment partner member repository by target branch', () => {
        expect(peopleFulfillmentPartnerMembersRepository.findAllByFulfillmentPartnerBranch).toHaveBeenCalledTimes(0);
      });
    });

    describe('if account, fulfillment partner members and fulfillment partners are cached on the component', () => {
      it('calls openDrawer', () => {
        component.account = new AccountViewModel({});
        component.fulfillmentPartnerMembers = [new PartnerFulfillmentPartnerMemberViewModel(
          new PartnerModels.FulfillmentPartnerMember({})
        )];
        component.fulfillmentPartners = [new PartnerModels.FulfillmentPartner({})];
        component.loadEdit();

        expect(component.openDrawer).toHaveBeenCalled();
      });
    });
  });

  describe('#openDrawer', () => {
    const targetMembers: FulfillmentPartnerMemberViewModel[] = [new FulfillmentPartnerMemberViewModel(
      new PeopleModels.FulfillmentPartnerMember(PeopleFactories.FulfillmentPartnerMemberFactory.build())
    )];

    it('calls open on the drawerService', () => {
      spyOn(drawerService, 'open');
      component.account = new AccountViewModel(account);
      component.fulfillmentPartnerMembers = fulfillmentPartnerMembers.map(
        (fulfillmentPartnerMember: PartnerModels.FulfillmentPartnerMember) =>
          new PartnerFulfillmentPartnerMemberViewModel(fulfillmentPartnerMember)
      );
      component.fulfillmentPartners = fulfillmentPartners;
      component.fulfillmentPartnerBranches = fulfillmentPartnerBranches;
      component.targetMembers = targetMembers;
      component.openDrawer();

      expect(drawerService.open).toHaveBeenCalledWith(ResolutionEditComponent, {
        resolution: component.resolution,
        account: component.account,
        fulfillmentPartnerMembers: component.fulfillmentPartnerMembers,
        fulfillmentPartners: component.fulfillmentPartners,
        fulfillmentPartnerBranches: component.fulfillmentPartnerBranches,
        targetMembers: component.targetMembers
      });
    });
  });

  describe('#updateActivities', () => {
    it('calls the activity repository for new log notes', () => {
      activityFindAllSpy.calls.reset();
      component.updateActivities();
      expect(activityRepository.findAllByResolutionId).toHaveBeenCalledTimes(1);
      expect(activityRepository.findAllByResolutionId).toHaveBeenCalledWith(component.resolution.id, 1, 1000);
    });
  });

  describe('#createActivityLogItem', () => {
    it('calls the activity repository create method with the current resolution id and provided note', () => {
      const activity = PartnerFactories.ActivityFactory.build({ comment: 'test', alert: false });
      component.createActivityLogItem(activity);
      expect(activityRepository.create).toHaveBeenCalledWith(component.resolution.id, activity);
    });

    it('does not call the activity log create service if comment is empty', () => {
      component.createActivityLogItem(PartnerFactories.ActivityFactory.build({ comment: '', alert: false }));
      expect(activityRepository.create).not.toHaveBeenCalled();
    });

    it('calls a function to update the activities', () => {
      spyOn(component, 'updateActivities');
      component.createActivityLogItem(PartnerFactories.ActivityFactory.build({ comment: 'test', alert: false }));
      expect(component.updateActivities).toHaveBeenCalledTimes(1);
    });

    it('calls the currentNote observable next with empty string (to reset note field)', () => {
      spyOn(component.currentNote$, 'next');
      component.createActivityLogItem(PartnerFactories.ActivityFactory.build({ comment: 'test', alert: false }));
      expect(component.currentNote$.next).toHaveBeenCalledWith('');
    });
  });
});
