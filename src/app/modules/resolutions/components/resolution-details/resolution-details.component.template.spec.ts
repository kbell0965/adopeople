import { DatePipe } from '@angular/common';
import { DebugElement } from '@angular/core';
import { async, ComponentFixture, TestBed, getTestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { FeatherModule } from 'angular-feather';
import { Activity, CheckCircle, Layers, UserPlus, Minus, Edit } from 'angular-feather/icons';

import { LocalizationService } from '@pplsi-core/shared-modules/dist/src/services/localization.service';
import { DataService } from '../../../../services/data/data.service';
import { DrawerService } from '../../../../services/drawer/drawer.service';
import { ResolutionViewModel } from '../../models/resolution-view-model';
import { ResolutionDetailsComponent } from './resolution-details.component';
import { PartnerFactories } from '@pplsi-core/factories';
import { ActivityLogModule } from 'src/app/shared/components/activity-log/activity-log.module';
import { PplsiCoreDatalayerModule, PartnerActivityRepository, PartnerModels } from '@pplsi-core/datalayer';
import { ReactiveFormsModule } from '@angular/forms';
import { of } from 'rxjs';
import { SharedModule } from 'src/app/shared/shared.module';

describe('ResolutionDetailsComponent.Template', () => {
  let component: ResolutionDetailsComponent;
  let fixture: ComponentFixture<ResolutionDetailsComponent>;
  const datePipe: DatePipe = new DatePipe('en_us');
  let activityRepository: PartnerActivityRepository;
  let dataService: DataService;
  const resolution: ResolutionViewModel = new ResolutionViewModel(PartnerFactories.ResolutionFactory.build({ opened_at: new Date()}));
  const activatedRouteStub = {
    snapshot: {
      data: {
        resolution: resolution
      }
    }
  };
  let injector: TestBed;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResolutionDetailsComponent ],
      imports: [
        ActivityLogModule,
        FeatherModule.pick({ Activity, CheckCircle, Layers, UserPlus, Minus, Edit }),
        PplsiCoreDatalayerModule.forRoot(),
        ReactiveFormsModule,
        SharedModule,
        RouterTestingModule
      ],
      providers: [
        { provide: ActivatedRoute, useValue: activatedRouteStub },
        LocalizationService,
        {
          provide: 'Window',
          useValue: window
        },
        {
          provide: 'localizations',
          useValue: [
            { locale: 'en-US', currencyCode: 'USD', dateFormat: 'MM/dd/yyyy' }
          ]
        },
        {
          provide: 'defaultLocale',
          useValue: 'en-US'
        },
        DrawerService,
        DataService
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResolutionDetailsComponent);
    component = fixture.componentInstance;
    injector = getTestBed();
    activityRepository = injector.get(PartnerActivityRepository);
    dataService = injector.get(DataService);
    spyOn(activityRepository, 'findAllByResolutionId').and.returnValue(of(<PartnerModels.Activity[]> []));
    spyOnProperty(dataService, 'resolution', 'get').and.returnValue(of(resolution));
    fixture.detectChanges();
  });

  it('displays the resolution display id in the breadcrumbs', () => {
    const debugElement: DebugElement = fixture.debugElement.query(By.css('.breadcrumbs'));

    expect(debugElement.nativeElement.innerHTML).toContain(resolution.display_id);
  });

  it('displays the resolution user name in the header', () => {
    const debugElement: DebugElement = fixture.debugElement.query(By.css('h2'));

    expect(debugElement.nativeElement.innerHTML).toContain(resolution.user.fullName());
  });

  describe('resolution details', () => {
    it('displays the resolution display id in the header', () => {
      const debugElement: DebugElement = fixture.debugElement.query(By.css('.card h3'));

      expect(debugElement.nativeElement.innerHTML).toContain(resolution.display_id);
    });

    it('displays the resolution created date', () => {
      const debugElement: DebugElement = fixture.debugElement.query(By.css('.created'));

      expect(debugElement.nativeElement.innerHTML).toContain(
        `${datePipe.transform(resolution.opened_at, 'longDate')} ${datePipe.transform(resolution.opened_at, 'shortTime')}`
      );
    });

    describe('edit button', () => {
      it('displays the resolution edit button', () => {
        const debugElement: DebugElement = fixture.debugElement.query(By.css('.edit-container i-feather'));

        expect(debugElement.nativeElement.innerHTML).toBeDefined();
      });

      it('calls loadEdit on the component when clicked', () => {
        const debugElement: DebugElement = fixture.debugElement.query(By.css('.edit-container i-feather'));
        spyOn(component, 'loadEdit');
        debugElement.nativeElement.click();

        expect(component.loadEdit).toHaveBeenCalled();
      });
    });

    describe('resolution assignee', () => {
      describe('when the assignee is present', () => {
        it('displays the resolution assignee', () => {
          component.resolution.assignee.full_name = 'John Doe';

          fixture.detectChanges();

          const debugElement: DebugElement = fixture.debugElement.query(By.css('.assignee'));

          expect(debugElement.nativeElement.innerHTML).toContain(resolution.assignee.full_name);
        });
      });

      describe('when the assignee is not present', () => {
        it('displays the feather icon', () => {
          component.resolution.assignee.full_name = null;

          fixture.detectChanges();

          const debugElement: DebugElement = fixture.debugElement.query(By.css('.assignee i-feather'));

          expect(debugElement).toBeTruthy();
        });
      });
    });

    it('displays the resolution status', () => {
      component.resolution.status = 'unassigned';

      fixture.detectChanges();

      const debugElement: DebugElement = fixture.debugElement.query(By.css('.status'));

      expect(debugElement.nativeElement.innerHTML).toContain(resolution.displayStatus);
    });

    it('displays the resolution type', () => {
      component.resolution.resolution_type = 'provider';

      fixture.detectChanges();

      const debugElement: DebugElement = fixture.debugElement.query(By.css('.type'));

      expect(debugElement.nativeElement.innerHTML).toContain(resolution.resolution_type);
    });

    it('displays the resolution concern', () => {
      component.resolution.concern = 'Some Concern';

      fixture.detectChanges();

      const debugElement: DebugElement = fixture.debugElement.query(By.css('.concern'));

      expect(debugElement.nativeElement.innerHTML).toContain(resolution.concern);
    });

    it('displays the resolution source', () => {
      component.resolution.source = 'call';

      fixture.detectChanges();

      const debugElement: DebugElement = fixture.debugElement.query(By.css('.source'));

      expect(debugElement.nativeElement.innerHTML).toContain(resolution.displaySource);
    });

    describe('targets', () => {
      describe('conflict', () => {
        beforeEach(() => {
          component.resolution.resolution_type = 'conflict';
        });

        it('displays the resolution referral', () => {
          component.resolution.referral = 'some referral';

          fixture.detectChanges();

          const debugElement: DebugElement = fixture.debugElement.query(By.css('.referral'));

          expect(debugElement.nativeElement.innerHTML).toContain(resolution.referral);
        });

        describe('resolution action taken', () => {
          describe('when the action taken is present', () => {
            it('displays the resolution action taken', () => {
              component.resolution.action_taken = 'provider action taken';

              fixture.detectChanges();

              const debugElement: DebugElement = fixture.debugElement.query(By.css('.action-taken'));

              expect(debugElement.nativeElement.innerHTML).toContain(resolution.action_taken);
            });
          });

          describe('when the action taken is not present', () => {
            it('displays the feather icon', () => {
              component.resolution.action_taken = null;

              fixture.detectChanges();

              const debugElement: DebugElement = fixture.debugElement.query(By.css('.action-taken i-feather'));

              expect(debugElement).toBeTruthy();
            });
          });
        });
      });

      describe('not a conflict', () => {
        beforeEach(() => {
          component.resolution.resolution_type = 'provider';
        });

        it('displays the resolution target', () => {
          component.resolution.target.name = 'some target';

          fixture.detectChanges();

          const debugElement: DebugElement = fixture.debugElement.query(By.css('.target'));

          expect(debugElement.nativeElement.innerHTML).toContain(resolution.target.name);
        });

        it('displays the resolution target branch', () => {
          component.resolution.target_branch.name = 'some target branch';

          fixture.detectChanges();

          const debugElement: DebugElement = fixture.debugElement.query(By.css('.target-branch'));

          expect(debugElement.nativeElement.innerHTML).toContain(resolution.target_branch.name);
        });

        describe('referral', () => {
          beforeEach(() => {
            component.resolution.resolution_type = 'referral';
          });

          describe('resolution referral', () => {
            describe('when the referral is present', () => {
              it('displays the resolution referral', () => {
                component.resolution.referral = 'some referral';

                fixture.detectChanges();

                const debugElement: DebugElement = fixture.debugElement.query(By.css('.referral'));

                expect(debugElement.nativeElement.innerHTML).toContain(resolution.referral);
              });
            });

            describe('when the referral is not present', () => {
              it('displays the feather icon', () => {
                component.resolution.referral = null;

                fixture.detectChanges();

                const debugElement: DebugElement = fixture.debugElement.query(By.css('.referral i-feather'));

                expect(debugElement).toBeTruthy();
              });
            });
          });

          describe('resolution action taken', () => {
            describe('when the action taken is present', () => {
              it('displays the resolution action taken', () => {
                component.resolution.action_taken = 'provider action taken';

                fixture.detectChanges();

                const debugElement: DebugElement = fixture.debugElement.query(By.css('.action-taken'));

                expect(debugElement.nativeElement.innerHTML).toContain(resolution.action_taken);
              });
            });

            describe('when the action taken is not present', () => {
              it('displays the feather icon', () => {
                component.resolution.action_taken = null;

                fixture.detectChanges();

                const debugElement: DebugElement = fixture.debugElement.query(By.css('.action-taken i-feather'));

                expect(debugElement).toBeTruthy();
              });
            });
          });
        });

        describe('provider', () => {
          describe('resolution target member', () => {
            describe('when the target member is present', () => {
              it('displays the resolution target member ', () => {
                component.resolution.target_member.full_name = 'some target member';

                fixture.detectChanges();

                const debugElement: DebugElement = fixture.debugElement.query(By.css('.target-member'));

                expect(debugElement.nativeElement.innerHTML).toContain(resolution.target_member.full_name);
              });
            });

            describe('when the target member is not present', () => {
              it('displays the feather icon', () => {
                component.resolution.target_member.full_name = null;

                fixture.detectChanges();

                const debugElement: DebugElement = fixture.debugElement.query(By.css('.target-member i-feather'));

                expect(debugElement).toBeTruthy();
              });
            });
          });

          describe('resolution action taken', () => {
            describe('when the action taken is present', () => {
              it('displays the resolution action taken', () => {
                component.resolution.action_taken = 'provider action taken';

                fixture.detectChanges();

                const debugElement: DebugElement = fixture.debugElement.query(By.css('.action-taken'));

                expect(debugElement.nativeElement.innerHTML).toContain(resolution.action_taken);
              });
            });

            describe('when the action taken is not present', () => {
              it('displays the feather icon', () => {
                component.resolution.action_taken = null;

                fixture.detectChanges();

                const debugElement: DebugElement = fixture.debugElement.query(By.css('.action-taken i-feather'));

                expect(debugElement).toBeTruthy();
              });
            });
          });
        });
      });
    });

    describe('resolution intake', () => {
      describe('when the intake is present', () => {
        it('displays the resolution intake', () => {
          component.resolution.service_request.case_id = 'some intake';

          fixture.detectChanges();

          const debugElement: DebugElement = fixture.debugElement.query(By.css('.intake'));

          expect(debugElement.nativeElement.innerHTML).toContain(resolution.service_request.case_id);
        });
      });

      describe('when the intake is not present', () => {
        it('displays the feather icon', () => {
          component.resolution.service_request.case_id = null;

          fixture.detectChanges();

          const debugElement: DebugElement = fixture.debugElement.query(By.css('.intake i-feather'));

          expect(debugElement).toBeTruthy();
        });
      });
    });
  });

  describe('user details', () => {
    it('displays the user avatar', () => {
      const debugElement: DebugElement = fixture.debugElement.query(By.css('ls-avatar'));

      expect(debugElement).toBeTruthy();
    });

    it('displays the resolution user in the header', () => {
      const debugElement: DebugElement = fixture.debugElement.query(By.css('.user-name-container .user-name'));

      expect(debugElement.nativeElement.innerHTML).toContain(resolution.user.fullName());
    });

    it('links the resolution user in the header to their details page', () => {
      const debugElement: DebugElement = fixture.debugElement.query(By.css('.user-name-container .user-name'));
      const expectation = `/users/${component.resolution.account_id}/user/${component.resolution.user.id}`;

      expect(debugElement.nativeElement.getAttribute('href')).toContain(expectation);
    });

    describe('national plan', () => {
      it('displays the national plan badge when it is a national plan resolution', () => {
        component.resolution.national_plan = true;

        fixture.detectChanges();

        const debugElement: DebugElement = fixture.debugElement.query(By.css('.result-national-plan'));

        expect(debugElement).toBeTruthy();
      });

      it('does not display when national plan is null or undefined', () => {
        component.resolution.national_plan = null;

        fixture.detectChanges();

        expect(fixture.debugElement.query(By.css('.result-national-plan'))).toBeNull();

        component.resolution.national_plan = undefined;

        fixture.detectChanges();

        expect(fixture.debugElement.query(By.css('.result-national-plan'))).toBeNull();
      });

      it('does not display the national plan badge when it is not a national plan resolution', () => {
        component.resolution.national_plan = false;

        fixture.detectChanges();

        const debugElement: DebugElement = fixture.debugElement.query(By.css('.result-national-plan'));

        expect(debugElement).toBeNull();
      });
    });

    describe('group icon', () => {
      it('should not display if resolution group is true and national plan is true', () => {
        component.resolution.group = true;
        component.resolution.national_plan = true;
        component.ngOnInit();
        fixture.detectChanges();

        expect(fixture.debugElement.query(By.css('.resolution-is-member'))).toBeNull();
        expect(fixture.debugElement.query(By.css('.result-national-plan'))).toBeTruthy();
      });

      it('does not display when group is null or undefined', () => {
        component.resolution.group = null;

        fixture.detectChanges();

        expect(fixture.debugElement.query(By.css('.resolution-is-member'))).toBeNull();

        component.resolution.group = undefined;

        fixture.detectChanges();

        expect(fixture.debugElement.query(By.css('.resolution-is-member'))).toBeNull();
      });

      it('does not display when resolution group is false', () => {
        component.resolution.group = false;
        component.ngOnInit();
        fixture.detectChanges();

        expect(fixture.debugElement.query(By.css('.resolution-is-member'))).toBeNull();
      });

      it('displays when resolution group is truthy', () => {
        component.resolution.national_plan = false;
        component.resolution.group = true;
        component.ngOnInit();
        fixture.detectChanges();

        expect(fixture.debugElement.query(By.css('.resolution-is-member'))).toBeTruthy();
      });
    });

    it('displays the resolution user location in the header', () => {
      const debugElement: DebugElement = fixture.debugElement.query(By.css('.user-location'));

      expect(debugElement.nativeElement.innerHTML).toContain(resolution.user.formattedLocalityAdministrativeArea());
    });

    it('displays the resolution user location in the header', () => {
      const debugElement: DebugElement = fixture.debugElement.query(By.css('.user-location'));

      expect(debugElement.nativeElement.innerHTML).toContain(resolution.user.formattedLocalityAdministrativeArea());
    });

    it('displays the resolution preferred contact method', () => {
      const debugElement: DebugElement = fixture.debugElement.query(By.css('.preferred-contact-method'));

      expect(debugElement.nativeElement.innerHTML).toContain(resolution.formattedPreferredContactMethod('US'));
    });

    describe('user cell phone', () => {
      const contact_method = PartnerFactories.ContactMethodFactory.build({
        value: '1231231234', label: 'cell'
      });

      describe('when the user cell phone is present', () => {
        it('displays the user cell phone', () => {
          component.resolution.user.contact_methods = [ contact_method ];

          fixture.detectChanges();

          const debugElement: DebugElement = fixture.debugElement.query(By.css('.cell-phone'));

          expect(debugElement.nativeElement.innerHTML).toContain(component.resolution.user.formattedCellPhoneNumber('US'));
        });
      });

      describe('when the user cell phone is not present', () => {
        it('displays the feather icon', () => {
          component.resolution.user.contact_methods = [];

          fixture.detectChanges();

          const debugElement: DebugElement = fixture.debugElement.query(By.css('.cell-phone i-feather'));

          expect(debugElement).toBeTruthy();
        });
      });
    });

    describe('user home phone', () => {
      const contact_method = PartnerFactories.ContactMethodFactory.build({
        value: '1231231234', label: 'home'
      });

      describe('when the user home phone is present', () => {
        it('displays the user home phone', () => {
          component.resolution.user.contact_methods = [ contact_method ];

          fixture.detectChanges();

          const debugElement: DebugElement = fixture.debugElement.query(By.css('.home-phone'));

          expect(debugElement.nativeElement.innerHTML).toContain(component.resolution.user.formattedHomePhoneNumber('US'));
        });
      });

      describe('when the user home phone is not present', () => {
        it('displays the feather icon', () => {
          component.resolution.user.contact_methods = [];

          fixture.detectChanges();

          const debugElement: DebugElement = fixture.debugElement.query(By.css('.home-phone i-feather'));

          expect(debugElement).toBeTruthy();
        });
      });
    });

    describe('user email', () => {
      describe('when the user email is present', () => {
        it('displays the user email', () => {
          component.resolution.user.email = 'johndoe@example.com';

          fixture.detectChanges();

          const debugElement: DebugElement = fixture.debugElement.query(By.css('.email'));

          expect(debugElement.nativeElement.innerHTML).toContain(component.resolution.user.email);
        });
      });

      describe('when the user email is not present', () => {
        it('displays the feather icon', () => {
          component.resolution.user.email = null;

          fixture.detectChanges();

          const debugElement: DebugElement = fixture.debugElement.query(By.css('.email i-feather'));

          expect(debugElement).toBeTruthy();
        });
      });
    });
  });

  describe('resolution activity log', () => {
    it('exists', () => {
      expect(fixture.debugElement.query(By.css('ls-activity-log'))).toBeTruthy();
    });

    it('has expected placeholder text', () => {
      const el = fixture.debugElement.query(By.css('.log-input textarea'));
      expect(el.nativeElement.getAttribute('placeholder')).toContain('Add note...');
    });
  });
});
