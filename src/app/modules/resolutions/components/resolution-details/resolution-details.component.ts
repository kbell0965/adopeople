import { Component, OnInit, OnDestroy } from '@angular/core';
import { takeUntil } from 'rxjs/operators';
import { Observable, BehaviorSubject, Subscription, forkJoin, Subject } from 'rxjs';
import { LocalizationService } from '@pplsi-core/shared-modules/dist/src/services/localization.service';
import { AccountViewModel } from '../../../../models/account-view-model';
import { FulfillmentPartnerMemberViewModel } from '../../../../models/fulfillment-partner-member-view-model';
import { PartnerFulfillmentPartnerMemberViewModel } from '../../../../models/partner-fulfillment-partner-member-view-model';
import { DrawerService } from '../../../../services/drawer/drawer.service';
import { ResolutionEditComponent } from '../../../../shared/components/resolution-edit/resolution-edit.component';
import { DataService } from '../../../../services/data/data.service';
import { ResolutionViewModel } from '../../models/resolution-view-model';
import {
  PartnerActivityRepository,
  PartnerModels,
  PartnerAccountRepository,
  PartnerFulfillmentPartnerMemberRepository,
  PeopleFulfillmentPartnerRepository,
  PeopleFulfillmentPartnerBranchRepository,
  PeopleModels,
  PeopleFulfillmentPartnerMembersRepository
} from '@pplsi-core/datalayer';

@Component({
  templateUrl: './resolution-details.component.html',
  styleUrls: ['./resolution-details.component.scss']
})
export class ResolutionDetailsComponent implements OnInit, OnDestroy {
  readonly PROVIDER_ACTION_TAKEN = 'provider action taken';
  resolution: ResolutionViewModel;
  locale: string;
  activity$: Observable<PartnerModels.Activity[]>;
  createSubscription: Subscription;
  currentNote$ = new BehaviorSubject('');
  account: AccountViewModel;
  fulfillmentPartnerMembers: PartnerModels.FulfillmentPartnerMember[];
  fulfillmentPartners: PartnerModels.FulfillmentPartner[];
  fulfillmentPartnerBranches: PeopleModels.FulfillmentPartnerBranch[];
  targetMembers: FulfillmentPartnerMemberViewModel[];

  private _destroyed = new Subject<void>();

  constructor(
    private activityRepository: PartnerActivityRepository,
    private dataService: DataService,
    private drawerService: DrawerService,
    private localizationService: LocalizationService,
    private partnerAccountRepository: PartnerAccountRepository,
    private partnerFulfillmentPartnerMemberRepository: PartnerFulfillmentPartnerMemberRepository,
    private peopleFulfillmentPartnerRepository: PeopleFulfillmentPartnerRepository,
    private peopleFulfillmentPartnerBranchRepository: PeopleFulfillmentPartnerBranchRepository,
    private peopleFulfillmentPartnerMembersRepository: PeopleFulfillmentPartnerMembersRepository) { }

  ngOnInit() {
    this.dataService.resolution.subscribe((resolution: ResolutionViewModel) => {
      this.resolution = resolution;
    });
    this.locale = this.localizationService.getLocale();
    this.updateActivities();
  }

  ngOnDestroy() {
    this._destroyed.next();
    this._destroyed.complete();
  }

  get actionTakenClass(): string {
    return this.resolution.action_taken === this.PROVIDER_ACTION_TAKEN ? 'ok' : 'warning';
  }

  createActivityLogItem(activity: PartnerModels.Activity): void {
    if (activity.comment) {
      this.createSubscription = this.activityRepository.create(this.resolution.id, activity)
        .pipe(takeUntil(this._destroyed))
        .subscribe(() => {
          this.updateActivities();
          this.currentNote$.next('');
        });
    }
  }

  loadEdit(): void {
    if (this.resolution.target_branch.id) {
      forkJoin([
        this.partnerAccountRepository.find(this.resolution.account_id),
        this.partnerFulfillmentPartnerMemberRepository.findAll(1, 1000),
        this.peopleFulfillmentPartnerRepository.findAll(this.resolution.target_branch.id),
        this.peopleFulfillmentPartnerBranchRepository.findAll(),
        this.peopleFulfillmentPartnerMembersRepository.findAllByFulfillmentPartnerBranch(this.resolution.target_branch.id)
      ]).subscribe((results: [
        PartnerModels.Account,
        PartnerModels.FulfillmentPartnerMember[],
        PartnerModels.FulfillmentPartner[],
        PeopleModels.FulfillmentPartnerBranch[],
        PeopleModels.FulfillmentPartnerMember[]
      ]) => {
        this.account = new AccountViewModel(results[0]);
        this.fulfillmentPartnerMembers = results[1]
          .map((result: PartnerModels.FulfillmentPartnerMember) => new PartnerFulfillmentPartnerMemberViewModel(result));
        this.fulfillmentPartners = results[2];
        this.fulfillmentPartnerBranches = results[3];
        this.targetMembers = results[4].map((fulfillmentPartnerMember: PeopleModels.FulfillmentPartnerMember) => {
          return new FulfillmentPartnerMemberViewModel(fulfillmentPartnerMember);
        });
        this.openDrawer();
      });
    } else {
      forkJoin([
        this.partnerAccountRepository.find(this.resolution.account_id),
        this.partnerFulfillmentPartnerMemberRepository.findAll(1, 1000),
        this.peopleFulfillmentPartnerBranchRepository.findAll()
      ]).subscribe((results: [
        PartnerModels.Account,
        PartnerModels.FulfillmentPartnerMember[],
        PeopleModels.FulfillmentPartnerBranch[]
      ]) => {
        this.account = new AccountViewModel(results[0]);
        this.fulfillmentPartnerMembers = results[1]
          .map((result: PartnerModels.FulfillmentPartnerMember) => new PartnerFulfillmentPartnerMemberViewModel(result));
        this.fulfillmentPartnerBranches = results[2];
        this.openDrawer();
      });
    }
  }

  openDrawer(): void {
    this.drawerService.open(ResolutionEditComponent, {
      resolution: this.resolution,
      account: this.account,
      fulfillmentPartnerMembers: this.fulfillmentPartnerMembers,
      fulfillmentPartners: this.fulfillmentPartners,
      fulfillmentPartnerBranches: this.fulfillmentPartnerBranches,
      targetMembers: this.targetMembers
    });
  }

  updateActivities(): void {
    this.activity$ = this.activityRepository.findAllByResolutionId(this.resolution.id, 1, 1000);
  }

  toggleIsAlert(activity: PartnerModels.Activity) {
    this.activityRepository.update(this.resolution.id, activity.id, { alert: !activity.alert })
      .pipe(takeUntil(this._destroyed))
      .subscribe(() => {
        this.updateActivities();
      }, () => {});
  }
}
