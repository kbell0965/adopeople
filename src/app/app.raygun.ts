import * as rg4js from 'raygun4js';
import { ErrorHandler } from '@angular/core';

import { environment } from '../environments/environment';

rg4js('apiKey', environment.raygunApiKey);
rg4js('setVersion', '1.0.0.0');
rg4js('enableCrashReporting', true);
rg4js('enablePulse', environment.raygunPulse);

export class RaygunErrorHandler implements ErrorHandler {
  handleError(e: any) {
    if (environment.raygunOffline) {
      console.error('Error that would be reported to Raygun: ' + e);
    } else {
      rg4js('send', {
        error: e
      });
    }
  }
}
