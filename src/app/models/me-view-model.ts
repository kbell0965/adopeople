import { Me } from '@pplsi-core/datalayer';

export class MeViewModel extends Me {
  constructor(params: Me) {
    super(params);
  }

  get fullName(): string {
    return this.full_name;
  }

  get firstName(): string {
    return this.fullName.split(' ')[0];
  }

  get lastName(): string {
    const parts = this.fullName.split(' ');
    return parts[parts.length - 1];
  }

  hasPermission(name: string, namespace: string): boolean {
    const perm = this.permissions.find((p) => {
      return p.name === name && p.namespace === namespace;
    });

    return !!perm;
  }

  areEqual(me: Me | MeViewModel) {
    return me ? this.id === me.id : false;
  }
}

