export enum Role {
  Csr = 'csr'
}

export class Me {
  id: string;
  name: string;
  roles: Role[];

  constructor(object: object) {
    this.id = object['id'];
    this.name = object['name'];
    this.roles = object['roles'];
  }

  get firstName(): string {
    return this.name.split(' ')[0];
  }

  get lastName(): string {
    const parts = this.name.split(' ');
    return parts[parts.length - 1];
  }

  hasRole(role: Role): boolean {
    return this.roles.includes(role);
  }
}
