import { ContactMethodType, PartnerModels } from '@pplsi-core/datalayer/dist/@pplsi-core/datalayer';
import { ConjunctionPipe, PhonePipe } from '@pplsi-core/shared-modules';
import { MembershipViewModel } from './membership-view-model';

export class PartnerUserViewModel extends PartnerModels.User {
  private conjunctionPipe: ConjunctionPipe = new ConjunctionPipe();
  private phonePipe: PhonePipe = new PhonePipe();
  memberships: MembershipViewModel[];

  constructor(params: PartnerModels.User) {
    super(params);
  }

  fullName(): string | null {
    return this.first_name || this.last_name
      ? `${this.first_name ? this.first_name : ''}${this.first_name && this.last_name ? ' ' : ''}${this.last_name ? this.last_name : ''}`
      : null;
  }

  formattedCellPhoneNumber(region = 'GB'): string | null {
    const phone_number = this.phoneNumberWithLabel('cell');
    return phone_number ? this.phonePipe.transform(phone_number, region) : null;
  }

  formattedHomePhoneNumber(region = 'GB'): string | null {
    const phone_number = this.phoneNumberWithLabel('home');
    return phone_number ? this.phonePipe.transform(phone_number, region) : null;
  }

  formattedPhoneNumber(region = 'GB', label: string): string | null {
    const phone_number = this.phoneNumberWithLabel(label);
    return phone_number ? this.phonePipe.transform(phone_number, region) : null;
  }

  formattedLocation(): string | null {
    const hasAddress = this.addresses && this.addresses.length > 0;
    const address = hasAddress ? this.addresses[0] : new PartnerModels.Address({});

    if (!!address.locality && !!address.administrative_area) {
      return `${address.locality}, ${address.administrative_area.toUpperCase()}`;
    }

    return address.locality || address.administrative_area || null;
  }

  formattedLocalityAdministrativeArea(): string | null {
    const hasAddress = this.addresses && this.addresses.length > 0;

    return hasAddress ? `${this.firstAddress().locality}, ${this.firstAddress().administrative_area}` : null;
  }

  linkTitle(locale: string): string {
    return [
      this.fullName(),
      this.email,
      this.formattedLocation(),
      this.formattedCellPhoneNumber(locale),
      this.formattedHomePhoneNumber(locale),
      this.serviceablePlanNames()
    ]
      .map((part, i) => {
        return (((i > 0) && part) ? `\n` : '') + (part ? `${part}` : '');
      })
      .join('');
  }

  nationalId(region: string = 'us'): string | null {
    const last4 = this.truncated_national_id ? (this.truncated_national_id.length > 4 ?
      this.truncated_national_id.substr(this.truncated_national_id.length - 4) :
      this.truncated_national_id) : null;

    switch (region) {
      case 'en-GB': {
        return last4 ? `•• •••${last4.substring(0, 3)} ${last4[3]}` : null;
      }
      case 'en-CA': {
        return last4 ? `••• ••${last4[0]} ${last4.substring(1)}` : null;
      }
      default: {
        return last4 ? `••• •• ${last4}` : null;
      }
    }
  }

  phoneNumberWithLabel(label: string): string | null {
    const matching_methods = this.contact_methods.filter(contact_method =>
      contact_method.type === ContactMethodType.Phone && contact_method.label === label);
    return matching_methods.length > 0 ? matching_methods[0].value : null;
  }

  firstAddress(): PartnerModels.Address {
    const hasAddress = this.addresses && this.addresses.length > 0;

    return hasAddress ? this.addresses[0] : null;
  }

  serviceablePlanNames(): string {
    return this.conjunctionPipe.transform(Array.from(this.unique_product_names));
  }
}
