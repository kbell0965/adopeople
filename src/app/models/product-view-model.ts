import { PartnerModels } from '@pplsi-core/datalayer';

export class ProductViewModel extends PartnerModels.Product {
  constructor (product: object) {
    super(product);
  }

  getProductType(): string {
    try {
      return this.covered_entities[0].entity_type;
    } catch {
      return null;
    }
  }
}
