import { TestBed } from '@angular/core/testing';
import { PartnerModels } from '@pplsi-core/datalayer/dist/@pplsi-core/datalayer';
import { PartnerUserViewModel } from './partner-user-view-model';
import { PartnerFactories } from '@pplsi-core/factories/dist/@pplsi-core/factories';
import { PhonePipe } from '@pplsi-core/shared-modules';

describe('PartnerUserViewModel', () => {
  let member: PartnerUserViewModel;
  let addresses: PartnerModels.Address[];
  let contact_methods: PartnerModels.ContactMethod[];

  const getTitleParts = (m: PartnerUserViewModel): string[] => {
    return [
      m.fullName(),
      m.email,
      m.formattedLocation(),
      m.formattedCellPhoneNumber('NA'),
      m.formattedHomePhoneNumber('NA'),
      m.serviceablePlanNames()
    ];
  };

  beforeEach(() => {
    addresses = PartnerFactories.AddressFactory.buildList(3).map((address: Object) => new PartnerModels.Address(address));
    contact_methods = PartnerFactories.ContactMethodFactory.buildList(2);
    contact_methods[0].value = '1231231234';
    contact_methods[0].label = 'cell';
    contact_methods[1].value = '3213214321';
    contact_methods[1].label = 'home';
    member = new PartnerUserViewModel(PartnerFactories.UserFactory.build({ addresses, contact_methods }));

    TestBed.configureTestingModule({
      providers: [
        PhonePipe
      ]
    });
  });

  describe('#fullName', () => {
    describe('when first name and last name exist', () => {
      it('combines first and last name into a combined name', () => {
        member.first_name = 'Foo';
        member.last_name = 'Bar';

        expect(member.fullName()).toEqual(`${member.first_name} ${member.last_name}`);
      });
    });

    describe('when first name exists but last name is null', () => {
      it('returns the first name only', () => {
        member.first_name = 'Foo';
        member.last_name = null;

        expect(member.fullName()).toEqual(`${member.first_name}`);
      });
    });

    describe('when first name is null but last name exists', () => {
      it('returns the last name only', () => {
        member.first_name = null;
        member.last_name = 'Bar';

        expect(member.fullName()).toEqual(`${member.last_name}`);
      });
    });

    describe('when both first and last name are null', () => {
      it('returns null', () => {
        member.first_name = null;
        member.last_name = null;

        expect(member.fullName()).toBeNull();
      });
    });
  });

  describe('#formatedPhoneNumber', () => {
    beforeEach(() => {
      spyOn(PhonePipe.prototype, 'transform').and.returnValue('1234');
    });

    it('calls the PhonePipe transform method with GB by default', () => {
      member.formattedCellPhoneNumber();

      expect(PhonePipe.prototype.transform).toHaveBeenCalledWith(contact_methods[0].value, 'GB');
    });

    it('calls the PhonePipe transform method with NA for region NA', () => {
      member.formattedCellPhoneNumber('NA');

      expect(PhonePipe.prototype.transform).toHaveBeenCalledWith(contact_methods[0].value, 'NA');
    });

    it('does not call the PhonePipe transform method when member phone number does not exist', () => {
      member.contact_methods = [];
      member.formattedCellPhoneNumber();

      expect(PhonePipe.prototype.transform).not.toHaveBeenCalled();
    });

    it('returns the string returned by the phone pipe when the member has a phone number', () => {
      expect(member.formattedCellPhoneNumber()).toEqual('1234');
    });

    it('returns null when the member does not have a phone number', () => {
      member.contact_methods = [];

      expect(member.formattedCellPhoneNumber()).toBeNull();
    });
  });

  describe('#formatedSecondPhoneNumber', () => {
    beforeEach(() => {
      spyOn(PhonePipe.prototype, 'transform').and.returnValue('1234');
    });

    it('calls the PhonePipe transform method with GB by default', () => {
      member.formattedHomePhoneNumber();

      expect(PhonePipe.prototype.transform).toHaveBeenCalledWith(contact_methods[1].value, 'GB');
    });

    it('calls the PhonePipe transform method with NA for region NA', () => {
      member.formattedHomePhoneNumber('NA');

      expect(PhonePipe.prototype.transform).toHaveBeenCalledWith(contact_methods[1].value, 'NA');
    });

    it('does not call the PhonePipe transform method when member phone number does not exist', () => {
      member.contact_methods = [];
      member.formattedHomePhoneNumber();

      expect(PhonePipe.prototype.transform).not.toHaveBeenCalled();
    });

    it('returns the string returned by the phone pipe when the member has a phone number', () => {
      expect(member.formattedHomePhoneNumber()).toEqual('1234');
    });

    it('returns null when the member does not have a phone number', () => {
      member.contact_methods = [];

      expect(member.formattedHomePhoneNumber()).toBeNull();
    });
  });

  describe('#formattedLocalityAdministrativeArea', () => {
    it('returns null when addresses do not exist', () => {
      member.addresses = [];

      expect(member.formattedLocalityAdministrativeArea()).toBeNull();
    });

    it('returns a formatted string with locality and administrative area when address exists', () => {
      member.addresses = PartnerFactories.AddressFactory.buildList(1);

      expect(member.formattedLocalityAdministrativeArea()).toBe(
        `${member.firstAddress().locality}, ${member.firstAddress().administrative_area}`
      );
    });
  });

  describe('#formattedLocation', () => {
    describe('when address exists with locality and administrative area', () => {
      it('should return ${locality}, ${administrative_area.toUpperCase()}', () => {
        const address = member.addresses[0];
        expect(member.formattedLocation())
          .toEqual(`${address.locality}, ${address.administrative_area.toUpperCase()}`);
      });
    });

    describe('when address exists with null locality', () => {
      beforeEach(() => {
        addresses = PartnerFactories.AddressFactory.buildList(1, { locality: null })
          .map((address) => new PartnerModels.Address(address));
        member.addresses = addresses;
      });

      it('should return only the adminsitrative area', () => {
        expect(member.formattedLocation()).toEqual(member.addresses[0].administrative_area);
      });
    });

    describe('when address exists with null administrative_area', () => {
      beforeEach(() => {
        addresses = PartnerFactories.AddressFactory.buildList(1, { administrative_area: null })
          .map((address) => new PartnerModels.Address(address));
        member.addresses = addresses;
      });

      it('should return only the adminsitrative area', () => {
        expect(member.formattedLocation()).toEqual(member.addresses[0].locality);
      });
    });

    describe('when address does not exists', () => {
      beforeEach(() => member.addresses = null);

      it('should return null', () => {
        expect(member.formattedLocation()).toBeNull();
      });
    });
  });

  describe('#linkTitle', () => {
    it('returns a title that displays all good values present on a member search result', () => {
      getTitleParts(member).forEach((part) => {
        if (part && part !== '') {
          expect(member.linkTitle('NA')).toContain(part);
        }
      });
    });

    it('returns a title that does not have a new line at the beginning', () => {
      const linkTitle = member.linkTitle('NA');
      expect(linkTitle).toContain('\n');
      expect(linkTitle.slice(0, 1)).not.toContain('\n');
    });

    it('returns a title that has a new line only for every good part', () => {
      member.email = '';
      member.formattedLocation = () => null;
      expect(member.linkTitle('NA').split('\n').length).toBe(getTitleParts(member).length - 2);
    });

    it('returns an empty string when none of the values are good', () => {
      member.fullName = () => null;
      member.email = '';
      member.formattedLocation = () => undefined;
      member.formattedCellPhoneNumber = () => null;
      member.formattedHomePhoneNumber = () => '';
      member.serviceablePlanNames = () => undefined;
      expect(member.linkTitle('NA')).toBe('');
    });
  });

  describe('#serviceablePlanNames', () => {
    it('should return a serviceable plan name', () => {
      member.unique_product_names = ['Marketzing'];
      expect(member.serviceablePlanNames()).toBe('Marketzing');
    });
  });

  describe('#nationalId', () => {
    describe('with full id', () => {
      beforeEach(() => {
        member.truncated_national_id = '1234567890';
      });

      it('formats the national id for UK', () => expect(member.nationalId('en-GB')).toBe('•• •••789 0'));

      it('formats the national id for CA', () => expect(member.nationalId('en-CA')).toBe('••• ••7 890'));

      it('uses the US format as a default if region is not defined', () => expect(member.nationalId('foo')).toBe('••• •• 7890'));

      it('uses the US format as a default if region is not given', () => expect(member.nationalId()).toBe('••• •• 7890'));

      it('returns null if national id is null', () => {
        member.truncated_national_id = null;

        expect(member.nationalId('en-GB')).toBeNull();
      });
    });

    describe('with short or partial id', () => {
      beforeEach(() => {
        member.truncated_national_id = '7890';
      });

      it('formats the national id for UK', () => expect(member.nationalId('en-GB')).toBe('•• •••789 0'));

      it('formats the national id for CA', () => expect(member.nationalId('en-CA')).toBe('••• ••7 890'));

      it('uses the US format as a default if region is not defined', () => expect(member.nationalId('foo')).toBe('••• •• 7890'));

      it('uses the US format as a default if region is not given', () => expect(member.nationalId()).toBe('••• •• 7890'));

      it('returns null if national id is null', () => {
        member.truncated_national_id = null;

        expect(member.nationalId('en-GB')).toBeNull();
      });
    });
  });

  describe('#firstAddress', () => {
    it('returns the first address when an address exists', () => {
      expect(member.firstAddress()).toEqual(member.addresses[0]);
    });

    it('returns null when there are no addresses', () => {
      member.addresses = [];

      expect(member.firstAddress()).toEqual(null);
    });

    it('returns null when addresses is null', () => {
      member.addresses = null;

      expect(member.firstAddress()).toEqual(null);
    });
  });
});
