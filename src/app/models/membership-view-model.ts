import { PartnerModels } from '@pplsi-core/datalayer';
import { PartnerUserViewModel } from './partner-user-view-model';

export class MembershipViewModel extends PartnerModels.Membership {
  user: PartnerUserViewModel;

  constructor(membership: object) {
    super(membership);

    if (!!membership['user']) {
      this.user = new PartnerUserViewModel(membership['user']);
    }
  }
}
