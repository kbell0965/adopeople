import { Me, Role } from './me';

const testMe = new Me({ id: 1, name: 'Albus Percival Wulfric Brian Dumbledore', roles: [] });

describe('Me', () => {
  describe('#firstName', () => {
    it('returns only the first name in a set of names or words separated by spaces', () => {
      expect(testMe.firstName).toBe('Albus');
    });
  });

  describe('#lastName', () => {
    it('returns only the last name in a set of names or words separated by spaces', () => {
      expect(testMe.lastName).toBe('Dumbledore');
    });
  });

  describe('#hasRole', () => {
    it('returns true if the roles array includes the role', () => {
      testMe.roles = [ Role.Csr ];

      expect(testMe.hasRole(Role.Csr)).toBeTruthy();
    });

    it('returns false if the roles array includes the role', () => {
      testMe.roles = [];

      expect(testMe.hasRole(Role.Csr)).toBeFalsy();
    });
  });
});
