import { SubscriptionStatus, MembershipStatus } from '@pplsi-core/datalayer';
import { PartnerFactories } from '@pplsi-core/factories';
import { AccountViewModel, Type } from './account-view-model';
import { MembershipViewModel } from './membership-view-model';
import { ProductViewModel } from './product-view-model';
import { PartnerUserViewModel } from './partner-user-view-model';

describe('AccountViewModel', () => {
  let account: AccountViewModel;
  let primaryMembership: MembershipViewModel;
  let dependentMembership: MembershipViewModel;
  const product1: object = PartnerFactories.ProductFactory.build({ name: 'Product 1' });
  const product2: object = PartnerFactories.ProductFactory.build({ name: 'Product 2' });
  const product3: object = PartnerFactories.ProductFactory.build({ name: 'Product 3' });
  const product4: object = PartnerFactories.ProductFactory.build({ name: 'Product 4' });
  const sellable_products_active: object = [
    product1,
    product2,
    product2
  ];
  const sellable_products_cancelled: object = [ product3 ];
  const sellable_products_expiring: object = [ product4 ];
  const subscriptions: object = [
    PartnerFactories.SubscriptionFactory.build({
      sellable_products: sellable_products_active,
      current_status: SubscriptionStatus.Active
    }),
    PartnerFactories.SubscriptionFactory.build({
      sellable_products: sellable_products_cancelled,
      current_status: SubscriptionStatus.Cancelled
    }),
    PartnerFactories.SubscriptionFactory.build({
      sellable_products: sellable_products_expiring,
      current_status: SubscriptionStatus.Expiring
    })
  ];
  const products: object = [
    product1,
    product2,
    product3,
    product4
  ];

  beforeEach(() => {
    const primaryMembershipObject = PartnerFactories.MembershipFactory.build({ status: MembershipStatus.Active, type: 'primary'});
    const dependentMembershipObject = PartnerFactories.MembershipFactory.build({ status: MembershipStatus.Archived, type: 'dependent'});
    const accountFake = PartnerFactories.AccountFactory.build({
      subscriptions: subscriptions,
      products: products,
      memberships: [primaryMembershipObject, dependentMembershipObject]
    });

    account = new AccountViewModel(accountFake);
    primaryMembership = new MembershipViewModel(primaryMembershipObject);
    dependentMembership = new MembershipViewModel(dependentMembershipObject);
  });

  describe('#getUserByType', () => {
    it('returns the user on the account by type', () => {
      const user: PartnerUserViewModel = account.getUserByType(Type.PRIMARY);

      expect(user).toEqual(primaryMembership.user);
    });

    it('returns undefined when type not found', () => {
      const user: PartnerUserViewModel = account.getUserByType(Type.SPOUSE);

      expect(user).toEqual(undefined);
    });
  });

  describe('#getMembershipByType', () => {
    it('returns the membership on the account by type', () => {
      expect(account.getMembershipByType(Type.PRIMARY)).toEqual(primaryMembership);
    });

    it('returns undefined when type not found', () => {
      expect(account.getMembershipByType(Type.SPOUSE)).toEqual(undefined);
    });

    it('returns undefined when type is found but status is archived', () => {
      const user: PartnerUserViewModel = account.getUserByType(Type.DEPENDENT);

      expect(user).toEqual(undefined);
    });
  });

  describe('#getActiveMemberships', () => {
    it('returns the active memberships on the account', () => {
      expect(account.getActiveMemberships()).toEqual([primaryMembership]);
    });
  });

  describe('#serviceableProducts', () => {
    describe('when subscriptions exists', () => {
      it('should return a list of serviceable plans', () => {
        expect(account.serviceableProducts()).toEqual([
          new ProductViewModel(product1),
          new ProductViewModel(product2),
          new ProductViewModel(product4)
        ]);
      });
    });

    describe('when subscriptions do not exist', () => {
      it('should return empty list', () => {
        account.subscriptions = [];
        expect(account.serviceableProducts()).toEqual([]);
      });
    });
  });
});
