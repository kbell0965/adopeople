import { PeopleFactories } from '@pplsi-core/factories/dist/@pplsi-core/factories';
import { FulfillmentPartnerMemberViewModel } from './fulfillment-partner-member-view-model';

describe('FulfillmentPartnerMemberViewModel', () => {
  const fulfillmentPartnerMemberViewModel: FulfillmentPartnerMemberViewModel = new FulfillmentPartnerMemberViewModel(
    PeopleFactories.FulfillmentPartnerMemberFactory.build()
  );

  describe('#fullName', () => {
    describe('when first name and last name exist', () => {
      it('combines first and last name into a combined name', () => {
        fulfillmentPartnerMemberViewModel.first_name = 'Foo';
        fulfillmentPartnerMemberViewModel.last_name = 'Bar';

        expect(fulfillmentPartnerMemberViewModel.fullName())
          .toEqual(`${fulfillmentPartnerMemberViewModel.first_name} ${fulfillmentPartnerMemberViewModel.last_name}`);
      });
    });

    describe('when first name exists but last name is null', () => {
      it('returns the first name only', () => {
        fulfillmentPartnerMemberViewModel.first_name = 'Foo';
        fulfillmentPartnerMemberViewModel.last_name = null;

        expect(fulfillmentPartnerMemberViewModel.fullName()).toEqual(`${fulfillmentPartnerMemberViewModel.first_name}`);
      });
    });

    describe('when first name is null but last name exists', () => {
      it('returns the last name only', () => {
        fulfillmentPartnerMemberViewModel.first_name = null;
        fulfillmentPartnerMemberViewModel.last_name = 'Bar';

        expect(fulfillmentPartnerMemberViewModel.fullName()).toEqual(`${fulfillmentPartnerMemberViewModel.last_name}`);
      });
    });

    describe('when both first and last name are null', () => {
      it('returns null', () => {
        fulfillmentPartnerMemberViewModel.first_name = null;
        fulfillmentPartnerMemberViewModel.last_name = null;

        expect(fulfillmentPartnerMemberViewModel.fullName()).toBeNull();
      });
    });
  });
});
