import { MembershipStatus, PartnerModels, SubscriptionStatus } from '@pplsi-core/datalayer';
import { MembershipViewModel } from './membership-view-model';
import { ProductViewModel } from './product-view-model';
import { PartnerUserViewModel } from './partner-user-view-model';

export enum Type {
  PRIMARY = 'primary',
  SPOUSE = 'spouse',
  DEPENDENT = 'dependent'
}

export class AccountViewModel extends PartnerModels.Account {
  memberships: MembershipViewModel[];
  products: ProductViewModel[];

  constructor(account: object) {
    super(account);

    if (!!account['products']) {
      this.products = account['products'].map(product => new ProductViewModel(product));
    }

    if (!!account['memberships']) {
      this.memberships = account['memberships'].map(membership => new MembershipViewModel(membership));
    }
  }

  getUserByType(type: Type): PartnerUserViewModel | undefined {
    const membership: MembershipViewModel = this.getMembershipByType(type);

    return !!membership ? membership.user : undefined;
  }

  getMembershipByType(type: Type): MembershipViewModel | undefined {
    return this.memberships
      .filter((membership: MembershipViewModel) => membership.type === type && membership.status === MembershipStatus.Active)[0];
  }

  getActiveMemberships(): MembershipViewModel[] {
    return this.memberships.filter((membership: MembershipViewModel) => membership.status === MembershipStatus.Active);
  }

  serviceableProducts(): ProductViewModel[] {
    const serviceableSubscriptions = this.subscriptions.filter((subscription: PartnerModels.Subscription) =>
      subscription.current_status === SubscriptionStatus.Active || subscription.current_status === SubscriptionStatus.Expiring
    );
    const serviceableProducts = serviceableSubscriptions.reduce((accum: any[], subscription: PartnerModels.Subscription) => [
      ...accum, ...subscription.sellable_products
    ], []);
    const serviceableProductIds = serviceableProducts.map((sellable_product: object) => sellable_product['id']);

    return this.products
      .filter(p => serviceableProductIds.indexOf(p.id) > -1);
  }
}
