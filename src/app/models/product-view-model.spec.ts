import { PartnerFactories } from '@pplsi-core/factories';
import { ProductViewModel } from './product-view-model';

describe('ProductViewModel', () => {
 describe('#getProductType', () => {
    it('returns the entity type', () => {
      const product = new ProductViewModel(PartnerFactories.ProductFactory.build());
      product.covered_entities[0].entity_type = 'family';

      expect(product.getProductType()).toBe('family');
    });

    it('returns null if no covered_entity', () => {
      const product = new ProductViewModel(PartnerFactories.ProductFactory.build());
      product.covered_entities[0] = undefined;

      expect(product.getProductType()).toBe(null);
    });
  });
});
