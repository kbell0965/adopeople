
import { Me, PermissionNamespaceType } from '@pplsi-core/datalayer';
import { MeViewModel } from './me-view-model';
import { MeFactory } from '@pplsi-core/factories';

describe('UserViewModel', () => {
  let meView: MeViewModel;
  const me = MeFactory.build({
    full_name: 'John Doe',
    permissions: [{ name: 'view:some_application', namespace: PermissionNamespaceType.Admin }]
  });

  beforeEach(() => {
    meView = new MeViewModel(me);
  });

  describe('#fullName', () => {
    it('it returns the member full name', () => {
      expect(meView.fullName).toEqual('John Doe');
    });

    it('it returns the member first name', () => {
      expect(meView.firstName).toEqual('John');
    });

    it('it returns the member last name', () => {
      expect(meView.lastName).toEqual('Doe');
    });
  });

  describe('#hasPermission', () => {
    it('returns true when the permission matches the name and namespace', () => {
      expect(meView.hasPermission('view:some_application', PermissionNamespaceType.Admin)).toEqual(true);
    });

    it('returns false when the name does not match', () => {
      expect(meView.hasPermission('view:other_thing', PermissionNamespaceType.Admin)).toEqual(false);
    });

    it('returns false when the namespace does not match', () => {
      expect(meView.hasPermission('view:some_application', 'groups')).toEqual(false);
    });
  });

  describe('#areEqual', () => {
    it('returns false for a null me', () => expect(meView.areEqual(null)).toBe(false));

    it('returns true for other models with the same id', () => {
      const otherMeView = new MeViewModel(me);

      expect(meView.areEqual(me)).toBe(true);
      expect(meView.areEqual(otherMeView)).toBe(true);
    });

    it('returns false for other models with different id', () => {
      const otherMeView = new MeViewModel({ ...me, id: 'different-id-guid-12345' });

      expect(otherMeView.areEqual(me)).toBe(false);
      expect(meView.areEqual(otherMeView)).toBe(false);
    });
  });
});
