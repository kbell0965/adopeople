import { PartnerModels } from '@pplsi-core/datalayer/dist/@pplsi-core/datalayer';

export class PartnerFulfillmentPartnerMemberViewModel extends PartnerModels.FulfillmentPartnerMember {
  constructor(params: PartnerModels.FulfillmentPartnerMember) {
    super(params);
  }

  fullName(): string | null {
    return this.first_name || this.last_name
      ? `${this.first_name ? this.first_name : ''}${this.first_name && this.last_name ? ' ' : ''}${this.last_name ? this.last_name : ''}`
      : null;
  }
}
