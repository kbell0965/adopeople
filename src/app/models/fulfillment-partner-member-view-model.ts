import { PeopleModels } from '@pplsi-core/datalayer/dist/@pplsi-core/datalayer';

export class FulfillmentPartnerMemberViewModel extends PeopleModels.FulfillmentPartnerMember {
  constructor(params: PeopleModels.FulfillmentPartnerMember) {
    super(params);
  }

  fullName(): string | null {
    return this.first_name || this.last_name
      ? `${this.first_name ? this.first_name : ''}${this.first_name && this.last_name ? ' ' : ''}${this.last_name ? this.last_name : ''}`
      : null;
  }
}
