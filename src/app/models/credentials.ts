export class Credentials {
  username: string;
  password: string;

  constructor(object: object) {
    this.username = object['username'];
    this.password = object['password'];
  }
}
