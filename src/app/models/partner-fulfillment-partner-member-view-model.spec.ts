import { PartnerFactories } from '@pplsi-core/factories/dist/@pplsi-core/factories';
import { PartnerFulfillmentPartnerMemberViewModel } from './partner-fulfillment-partner-member-view-model';

describe('PartnerFulfillmentPartnerMemberViewModel', () => {
  const partnerFulfillmentPartnerMemberViewModel: PartnerFulfillmentPartnerMemberViewModel = new PartnerFulfillmentPartnerMemberViewModel(
    PartnerFactories.FulfillmentPartnerMemberFactory.build()
  );

  describe('#fullName', () => {
    describe('when first name and last name exist', () => {
      it('combines first and last name into a combined name', () => {
        partnerFulfillmentPartnerMemberViewModel.first_name = 'Foo';
        partnerFulfillmentPartnerMemberViewModel.last_name = 'Bar';

        expect(partnerFulfillmentPartnerMemberViewModel.fullName())
          .toEqual(`${partnerFulfillmentPartnerMemberViewModel.first_name} ${partnerFulfillmentPartnerMemberViewModel.last_name}`);
      });
    });

    describe('when first name exists but last name is null', () => {
      it('returns the first name only', () => {
        partnerFulfillmentPartnerMemberViewModel.first_name = 'Foo';
        partnerFulfillmentPartnerMemberViewModel.last_name = null;

        expect(partnerFulfillmentPartnerMemberViewModel.fullName())
          .toEqual(`${partnerFulfillmentPartnerMemberViewModel.first_name}`);
      });
    });

    describe('when first name is null but last name exists', () => {
      it('returns the last name only', () => {
        partnerFulfillmentPartnerMemberViewModel.first_name = null;
        partnerFulfillmentPartnerMemberViewModel.last_name = 'Bar';

        expect(partnerFulfillmentPartnerMemberViewModel.fullName())
          .toEqual(`${partnerFulfillmentPartnerMemberViewModel.last_name}`);
      });
    });

    describe('when both first and last name are null', () => {
      it('returns null', () => {
        partnerFulfillmentPartnerMemberViewModel.first_name = null;
        partnerFulfillmentPartnerMemberViewModel.last_name = null;

        expect(partnerFulfillmentPartnerMemberViewModel.fullName()).toBeNull();
      });
    });
  });
});
