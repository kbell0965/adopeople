const express = require('express');
const webAdonis = require('pplsi.web');
const bodyParser = require('body-parser');
const config = require('./config');
const request = require('request-promise');
const session = require('express-session');
const raygun = require('raygun');

const dirname = __dirname;

let raygunClient = new raygun.Client().init(config.raygun.options);
if (config.raygun.offline) { raygunClient.offline(); }

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(raygunClient.expressHandler);

webAdonis.run(express, app, config, request, dirname);
