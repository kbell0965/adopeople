const URL = require('url');

function clone(o) { return JSON.parse(JSON.stringify(o)); }

let oneDayInSeconds  = (24 * 60 * 60);
let redisSessionConfiguration = URL.parse(process.env.REDIS_URL || 'redis://127.0.0.1:6379/');

if (redisSessionConfiguration.auth) {
  redisSessionConfiguration.auth = redisSessionConfiguration.auth.split(':')[1];
}

const defaultEnvironment = {
  environment: process.env.NODE_ENV || 'development',
  app: {
    domain: process.env.APP_DOMAIN || 'http://localhost:9990',
    port: process.env.PORT || 9990,
    https: process.env.HTTPS || false
  },
  logger: 'dev',
  session: {
    name: process.env.SESSION_NAME || 'people.adonis_connect.sid',
    secret: process.env.SESSION_SECRET || 'super-secret',
    resave: false,
    saveUninitialized: false
  },
  redis: {
    port: redisSessionConfiguration.port,
    host: redisSessionConfiguration.hostname,
    auth_pass: redisSessionConfiguration.auth,
    ttl: oneDayInSeconds
  },
  id: {
    domain: process.env.API_DOMAIN || 'http://localhost:3000'
  },
  api: {
    domain: process.env.API_DOMAIN || 'http://localhost:3000'
  },
  accounts: {
    domain: process.env.ACCOUNTS_DOMAIN || 'http://localhost:9989'
  },
  auth: {
    strategy: '@pplsi/passport-pplsi',
    issuer: {
      token_endpoint: process.env.API_DOMAIN + '/auth/o_auth2/v1/token'
    },
    authorization_code_name: 'pplsi-oauth2-authorization-code',
    password_name: 'pplsi-oauth2-password',
    base_url: `${process.env.API_DOMAIN || 'http://localhost:3000'}/`,
    redirect_base_url: `${process.env.REDIRECT_AUTH_BASE || process.env.API_DOMAIN || 'http://localhost:3000'}/`,
    base_protocol: '',
    client_id: process.env.AUTH_CLIENT_ID || 'de131930-e6de-11e9-a359-2a2ae2dbcce4',
    client_secret: process.env.AUTH_CLIENT_SECRET || 'e41e1e06-e6de-11e9-81b4-2a2ae2dbcce4',
    redirect_uri: `${process.env.APP_DOMAIN || 'http://localhost:9990'}/auth/legalshield/callback`,
    scope: 'openid roles name'
  },
  raygun: {
    options: {
      apiKey: process.env.RAYGUN_API_KEY || 'super-secret',
      filters: [ 'password' ],
      offlineStorageOptions: {
        cachePath: 'raygun-cache/'
      }
    },
    offline: true
  }
};

let environments = {
  test: clone(defaultEnvironment),
  development: clone(defaultEnvironment),
  sandbox: clone(defaultEnvironment),
  uat: clone(defaultEnvironment),
  production: clone(defaultEnvironment)
};

environments.test.auth.strategy = 'passport-mocked';
environments.test.auth.authorization_code_name = 'mocked';
environments.test.api.domain = 'http://adonis:3000';

environments.sandbox.raygun.offline = false;

environments.uat.raygun.offline = false;

environments.production.raygun.offline = false;

module.exports = environments[defaultEnvironment.environment];
